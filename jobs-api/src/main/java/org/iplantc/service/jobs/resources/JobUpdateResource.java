/**
 * 
 */
package org.iplantc.service.jobs.resources;

import java.util.Date;

import org.apache.log4j.Logger;
import org.iplantc.service.apps.util.ServiceUtils;
import org.iplantc.service.jobs.dao.JobDao;
import org.iplantc.service.jobs.model.Job;
import org.iplantc.service.jobs.model.enumerations.JobStatusType;
import org.iplantc.service.common.representation.IplantErrorRepresentation;
import org.iplantc.service.common.representation.IplantSuccessRepresentation;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.Representation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.Variant;

/**
 * Class to handle get and post requests for jobs
 * 
 * @author dooley
 * 
 */
public class JobUpdateResource extends AbstractJobResource 
{
	private static final Logger	log	= Logger.getLogger(JobUpdateResource.class);

	private String				sJobId;
	private String				token;
	private String				newStatus;
	private String				localId;

	/**
	 * @param context
	 * @param request
	 * @param response
	 */
	public JobUpdateResource(Context context, Request request, Response response)
	{
		super(context, request, response);

		this.sJobId = (String) request.getAttributes().get("jobid");
		this.token = (String) request.getAttributes().get("token");
		this.newStatus = (String) request.getAttributes().get("status");
		this.localId = (String) request.getAttributes().get("localId");

		getVariants().add(new Variant(MediaType.APPLICATION_JSON));
	}

	/**
	 * This method represents the HTTP GET action. It provides a simple,
	 * convenient way for jobs to update their own status information while
	 * running. If the job id, token, or status included in the URL are invalid
	 * or the form data is invalid, a HTTP
	 * {@link org.restlet.data.Status#CLIENT_ERROR_BAD_REQUEST 400} code is
	 * sent. If a problem occurs looking up the job a HTTP
	 * {@link org.restlet.data.Status#SERVER_ERROR_INTERNAL 500} code is sent.
	 */
	@Override
	public Representation represent(Variant variant) throws ResourceException
	{

		//Long jobId = null;
		if (!ServiceUtils.isValid(sJobId))
		{
			getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			return new IplantErrorRepresentation("Invalid job id.", prettyPrint);
		} else {
//			try
//			{
//				jobId = Long.valueOf(sJobId);
//			} catch(Exception e)
//			{
//				getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
//				return new IplantErrorRepresentation("Invalid job id.", prettyPrint);
//			}
		}

		if (!ServiceUtils.isValid(token))
		{
			getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			return new IplantErrorRepresentation("Invalid token.", prettyPrint);
		}
		JobStatusType status;
		try
		{
			status = JobStatusType.valueOf(newStatus.toUpperCase());
			if (status == null)
			{
				getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return new IplantErrorRepresentation("Invalid status type.", prettyPrint);
			}
		}
		catch (Exception e)
		{
			getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			return new IplantErrorRepresentation("Invalid status type.", prettyPrint);
		}

		// update the job status if the tokens match
		try
		{
			Job job = JobDao.getByUuid(sJobId);
			
			if (job == null) {
				throw new ResourceException(Status.CLIENT_ERROR_NOT_FOUND, 
						"No job found with job id " + sJobId);
			}
			else if (job.getUpdateToken().equals(token))
			{
				if (!job.isRunning() && JobStatusType.isRunning(status))
				{
					// can't set a stopped job back to running. Bad request
					throw new ResourceException(
							Status.CLIENT_ERROR_BAD_REQUEST, "Job " + sJobId
									+ " is not running.");
				}

				Date date = new Date();
				String message = null;
				
				if (job.getLocalJobId() == null)
				{
					job.setLocalJobId(localId);
				}
				else if (ServiceUtils.isValid(localId))
				{	
					throw new ResourceException(
							Status.CLIENT_ERROR_BAD_REQUEST, "Job " + sJobId
							+ " has already been assigned a local id by the scheduler.");
				}
				
				// the HEARTBEAT status is used to update the job timestamp and is used
				// by app developers just to keep aware of a job being alive.
				if (job.isRunning() && status.equals(JobStatusType.HEARTBEAT)) 
				{
					status = job.getStatus();
					message = "Job heartbeat received";
				}
				else if (!job.isArchiveOutput() && (status.equals(JobStatusType.ARCHIVING) || 
						status.equals(JobStatusType.ARCHIVING_FAILED) || 
						status.equals(JobStatusType.ARCHIVING_FINISHED))) 
				{
					// can't update an archive status when the job is not set to archive
					throw new ResourceException(
							Status.CLIENT_ERROR_BAD_REQUEST, "Job " + sJobId
							+ " is not configured for archive.");
				} 
				else if (job.isArchived()) 
				{
					// can't update an archive status when a job is already archived
					throw new ResourceException(
							Status.CLIENT_ERROR_BAD_REQUEST, "Job " + sJobId
							+ " has already been archived.");
					
				}
				else if (job.getStatus().equals(JobStatusType.ARCHIVING) && 
						(status.equals(JobStatusType.ARCHIVING_FAILED) || 
						status.equals(JobStatusType.ARCHIVING_FINISHED))) 
				{
					if (status.equals(JobStatusType.ARCHIVING_FAILED))
					{ 
						message = "Job archiving failed"; 	
					}
					else if (status.equals(JobStatusType.ARCHIVING_FINISHED)) 
					{
						message = "Job archiving complete";
					}
				}
				else if (job.getStatus().equals(JobStatusType.ARCHIVING) && 
						!(status.equals(JobStatusType.ARCHIVING_FAILED) || 
								status.equals(JobStatusType.ARCHIVING_FINISHED))) 
				{
					// can't downgrade a status
					throw new ResourceException(
							Status.CLIENT_ERROR_BAD_REQUEST, "Job " + sJobId
							+ " is archiving.");
				}
				else if (!job.isFinished() && (status.equals(JobStatusType.ARCHIVING) || 
						status.equals(JobStatusType.ARCHIVING_FAILED) || 
						status.equals(JobStatusType.ARCHIVING_FINISHED))) 
				{
					// can't update an archive status when a job is not completed
					throw new ResourceException(
							Status.CLIENT_ERROR_BAD_REQUEST, "Job " + sJobId
							+ " has not yet finished.");
					
				}
				else if (job.isFinished())
				{
					if (!status.equals(JobStatusType.ARCHIVING)) 
					{	throw new ResourceException(
							Status.CLIENT_ERROR_BAD_REQUEST, "Job " + sJobId
							+ " has already finished.");
					}
				}
				else if (status.equals(JobStatusType.RUNNING))
				{
					job.setStartTime(date);
					message = "Job started running";
				}
				else if (!JobStatusType.isRunning(status))
				{
					job.setEndTime(date);
					message = "Job completed execution";
				}

				job.setLastUpdated(date);
				job.setStatus(status, message);
				JobDao.persist(job);
				
//				if (ServiceUtils.isValid(job.getCallbackUrl()))
//				{
//					if ( job.isArchiveOutput() ) 
//					{
//						// if they specified archive, only update when done
//						if (job.isArchived() ) 
//						{ 
//							if (job.isFailed()) {
//								Postback.submitError(job, CallbackMacroResolver.resolve(job, job.getCallbackUrl()));
//							} else {
//								Postback.submitSuccess(job, CallbackMacroResolver.resolve(job, job.getCallbackUrl()));
//							}
//						} 
//						else if (!ServiceUtils.isEmailAddress(job.getCallbackUrl()))
//						{
//							if (job.isFailed()) {
//								Postback.submitError(job, CallbackMacroResolver.resolve(job, job.getCallbackUrl()));
//							} else {
//								Postback.submitSuccess(job, CallbackMacroResolver.resolve(job, job.getCallbackUrl()));
//							}
//						}
//					} 
//					else if (job.isFinished()) 
//					{
//						// invoke the callback endpoint if present
//						if (job.isFailed()) {
//							Postback.submitError(job, CallbackMacroResolver.resolve(job, job.getCallbackUrl()));
//						} else {
//							Postback.submitSuccess(job, CallbackMacroResolver.resolve(job, job.getCallbackUrl()));
//						}
//					} 
//					else if (ServiceUtils.isValid(job.getCallbackUrl()) && 
//							!ServiceUtils.isEmailAddress(job.getCallbackUrl())) 
//					{ 
//						// for all non-email clients, send a postback on all status events
//						Postback.post(job, CallbackMacroResolver.resolve(job, job.getCallbackUrl()));
//					}
//				}
				
				return new IplantSuccessRepresentation(prettyPrint);

			}
			else
			{
				// can't set a stopped job back to running. Bad request
				throw new ResourceException(Status.CLIENT_ERROR_UNAUTHORIZED,
						"Invalid job id or key");
			}
		}
		catch (ResourceException e)
		{
			log.debug("Callback failed: " + e.getMessage());
			getResponse().setStatus(e.getStatus());
			return new IplantErrorRepresentation(e.getMessage(), prettyPrint);
		}
		catch (Exception e)
		{
			log.debug("Callback failed: " + e.getMessage());
			// can't set a stopped job back to running. Bad request
			getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
			return new IplantErrorRepresentation("Failed to update job", prettyPrint);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.restlet.resource.Resource#allowDelete()
	 */
	@Override
	public boolean allowDelete()
	{
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.restlet.resource.Resource#allowGet()
	 */
	@Override
	public boolean allowGet()
	{
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.restlet.resource.Resource#allowPost()
	 */
	@Override
	public boolean allowPost()
	{
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.restlet.resource.Resource#allowPut()
	 */
	@Override
	public boolean allowPut()
	{
		return false;
	}

}
