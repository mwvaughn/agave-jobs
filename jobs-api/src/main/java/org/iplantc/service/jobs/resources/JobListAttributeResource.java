/**
 * 
 */
package org.iplantc.service.jobs.resources;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.iplantc.service.apps.util.ServiceUtils;
import org.iplantc.service.common.clients.IPlantLogServiceClient;
import org.iplantc.service.jobs.Settings;
import org.iplantc.service.jobs.dao.JobDao;
import org.iplantc.service.jobs.model.Job;
import org.iplantc.service.common.persistence.TenancyHelper;
import org.iplantc.service.common.representation.IplantErrorRepresentation;
import org.iplantc.service.common.representation.IplantSuccessRepresentation;
import org.json.JSONArray;
import org.json.JSONObject;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.Representation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.Variant;

/**
 * Class to handle job listings for the authenticated user.
 * 
 * @author dooley
 * 
 */
public class JobListAttributeResource extends AbstractJobResource 
{
	private static final Logger	log	= Logger.getLogger(JobListAttributeResource.class);
	
	public static List<String> jobAttributes = new ArrayList<String>();
	
	static {
		for(Field field : Job.class.getFields()) {
			jobAttributes.add(field.getName());
		}
	}
	
	private String	attribute = null;

	/**
	 * @param context
	 * @param request
	 * @param response
	 */
	public JobListAttributeResource(Context context, Request request,
			Response response)
	{
		super(context, request, response);

		this.username = getAuthenticatedUsername();
		
		String userAttr = (String) request.getAttributes().get("attribute");
		if (ServiceUtils.isValid(userAttr)) {
			// only add valid attributes, preserve their case for the sql query
			for(String attr: jobAttributes) {
				if (attr.toLowerCase().equals(userAttr.toLowerCase())) {
					attribute = attr;
					break;
				}
			}
		}
		
		getVariants().add(new Variant(MediaType.APPLICATION_JSON));
		
		IPlantLogServiceClient.log(IPlantLogServiceClient.ServiceKeys.JOBS02.name(), 
				IPlantLogServiceClient.ActivityKeys.JobAttributeList.name(), 
				username, "", request.getClientInfo().getAddress());
	}

	/**
	 * Returns a json array of jobs matching the key value pairs.
	 */
	@Override
	public Representation represent(Variant variant) throws ResourceException
	{
		if (attribute == null) {
			getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			return new IplantErrorRepresentation("Unknown job attribute", prettyPrint);
		}
		
		try
		{
			//String json = "";
			
			JSONArray json = new JSONArray();
			List<Job> jobs = JobDao.getByUsername(username);
			for (int i=offset; i< Math.min((limit+offset), jobs.size()); i++)
			{
				Job job = jobs.get(i);
				JSONObject jsonJob = new JSONObject();
				jsonJob.put("id", job.getUuid());
				jsonJob.put(attribute, job.getValueForAttributeName(attribute));
				jsonJob.put("_links",
						new JSONObject().put("self", 
								new JSONObject().put("href",
										TenancyHelper.resolveURLToCurrentTenant(Settings.IPLANT_JOB_SERVICE) + job.getUuid())));
				json.put(jsonJob);
			}
			return new IplantSuccessRepresentation(json.toString(), prettyPrint);
		}
		catch (Exception e)
		{
			log.error("Failed to search jobs by attribute " + attribute + " for user " + username, e);
			getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
			return new IplantErrorRepresentation(e.getMessage(), prettyPrint);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.restlet.resource.Resource#allowDelete()
	 */
	@Override
	public boolean allowDelete()
	{
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.restlet.resource.Resource#allowGet()
	 */
	@Override
	public boolean allowGet()
	{
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.restlet.resource.Resource#allowPost()
	 */
	@Override
	public boolean allowPost()
	{
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.restlet.resource.Resource#allowPut()
	 */
	@Override
	public boolean allowPut()
	{
		return false;
	}

}
