/**
 * 
 */
package org.iplantc.service.jobs.resources;

import java.util.List;

import org.iplantc.service.apps.util.ServiceUtils;
import org.iplantc.service.common.clients.IPlantLogServiceClient;
import org.iplantc.service.common.representation.IplantErrorRepresentation;
import org.iplantc.service.common.representation.IplantSuccessRepresentation;
import org.iplantc.service.jobs.dao.JobDao;
import org.iplantc.service.jobs.exceptions.JobException;
import org.iplantc.service.jobs.managers.JobPermissionManager;
import org.iplantc.service.jobs.model.Job;
import org.iplantc.service.jobs.model.JobEvent;
import org.iplantc.service.transfer.dao.TransferTaskDao;
import org.iplantc.service.transfer.exceptions.TransferException;
import org.iplantc.service.transfer.model.TransferSummary;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONObject;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.Representation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.Variant;

/**
 * The JobManageResource is the job management interface for users. Through the
 * actions bound to this class, users can obtain individual job
 * description(GET) and kill jobs (DELETE).
 * 
 * @author dooley
 * 
 */
public class JobHistoryResource extends AbstractJobResource 
{
	private String				sJobId;
	private Job					job;

	/**
	 * @param context
	 * @param request
	 * @param response
	 */
	public JobHistoryResource(Context context, Request request, Response response)
	{
		super(context, request, response);

		this.username = getAuthenticatedUsername();
		
		this.sJobId = (String) request.getAttributes().get("jobid");
		
		getVariants().add(new Variant(MediaType.APPLICATION_JSON));
	}

	/**
	 * Returns a collection of JobEvent objects representing the history
	 * of events for this job.
	 */
	@Override
	public Representation represent(Variant variant) throws ResourceException
	{

		IPlantLogServiceClient.log(IPlantLogServiceClient.ServiceKeys.JOBS02.name(), 
				IPlantLogServiceClient.ActivityKeys.JobsGetHistory.name(), 
				username, "", getRequest().getClientInfo().getAddress());
		
		if (!ServiceUtils.isValid(sJobId))
		{
			getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			return new IplantErrorRepresentation("Job id cannot be empty", prettyPrint);
		}

		try
		{
			job = JobDao.getByUuid(sJobId);
			if (job == null) {
				throw new ResourceException(Status.CLIENT_ERROR_NOT_FOUND, 
						"No job found with job id " + sJobId);
			}
			else if (new JobPermissionManager(job, username).canRead(username))
			{
				JSONArray history = new JSONArray();
				List<JobEvent> events = job.getEvents();
            	for (int i=offset; i< Math.min((limit+offset), events.size()); i++)
				{
            		JSONObject jsonEvent = new JSONObject();
            		JobEvent event = events.get(i);
    				if (event.getTransferTask() != null) 
					{
						JSONObject jsonTransferTask = new JSONObject();
						
						try {
							TransferSummary summary = TransferTaskDao.getTransferSummary(event.getTransferTask());
							
							jsonTransferTask
								.put("source", event.getTransferTask().getSource())
								.put("totalActiveTransfers",  summary.getTotalActiveTransfers())
								.put("totalFiles", summary.getTotalTransfers())
								.put("totalBytesTransferred", summary.getTotalTransferredBytes())
								.put("totalBytes", summary.getTotalBytes())
								.put("averageRate", summary.getAverageTransferRate());
							
							jsonEvent.put("progress", jsonTransferTask);
						} catch (TransferException e) {
							jsonEvent.put("progress", (String) null);
						}
					}
					jsonEvent
						.put("status", event.getStatus())
						.put("created", new DateTime(event.getCreated()).toString())
						//.put("ipAddress", event.getIpAddress())
						.put("description", event.getDescription());
					
					history.put(jsonEvent);
				}
				return new IplantSuccessRepresentation(history.toString(), prettyPrint);
			}
			else
			{
				getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN);
				return new IplantErrorRepresentation(
						"User does not have permission to view this job history", prettyPrint);
			}
		}
		catch (ResourceException e) {
			throw e;
		}
		catch (JobException e)
		{
			getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			return new IplantErrorRepresentation("Invalid job id "
					+ e.getMessage(), prettyPrint);
		}
		catch (Exception e)
		{
			// can't set a stopped job back to running. Bad request
			getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
			return new IplantErrorRepresentation(e.getMessage(), prettyPrint);
		}

	}

	@Override public boolean allowDelete() { return false; }
	@Override public boolean allowGet() { return true; }
	@Override public boolean allowPut() { return false; }
	@Override public boolean allowPost() { return false; }
}
