/**
 * 
 */
package org.iplantc.service.jobs.resources;

import java.util.List;

import org.apache.log4j.Logger;
import org.iplantc.service.common.clients.IPlantLogServiceClient;
import org.iplantc.service.common.persistence.TenancyHelper;
import org.iplantc.service.common.representation.IplantErrorRepresentation;
import org.iplantc.service.common.representation.IplantSuccessRepresentation;
import org.iplantc.service.jobs.Settings;
import org.iplantc.service.jobs.dao.JobDao;
import org.iplantc.service.jobs.exceptions.JobProcessingException;
import org.iplantc.service.jobs.managers.JobManager;
import org.iplantc.service.jobs.model.Job;
import org.joda.time.DateTime;
import org.json.JSONStringer;
import org.json.JSONWriter;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.Representation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.Variant;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * The JobResource object enables HTTP GET and POST actions on contrast jobs.
 * This resource is primarily used for submitting jobs and viewing a sample HTML
 * job submission form.
 * 
 * @author dooley
 * 
 */
public class JobsResource extends AbstractJobResource {
	private static final Logger	log	= Logger.getLogger(JobsResource.class);

	private String internalUsername;
	
	/**
	 * @param context
	 * @param request
	 * @param response
	 */
	public JobsResource(Context context, Request request, Response response)
	{
		super(context, request, response);

		this.username = getAuthenticatedUsername();
		
		internalUsername = (String) context.getAttributes().get("internalUsername");
		
		getVariants().add(new Variant(MediaType.TEXT_HTML));
	}
	
	/**
	 * This method represents the HTTP GET action. A list of jobs is retrieved
	 * from the service database and serialized to a {@link org.json.JSONArray
	 * JSONArray} of {@link org.json.JSONObject JSONObject}. On error, a HTTP
	 * {@link org.restlet.data.Status#SERVER_ERROR_INTERNAL 500} code is sent.
	 */
	@Override
	public Representation represent(Variant variant) throws ResourceException
	{
		IPlantLogServiceClient.log(IPlantLogServiceClient.ServiceKeys.JOBS02.name(), 
				IPlantLogServiceClient.ActivityKeys.JobsList.name(), 
				username, "", getRequest().getClientInfo().getAddress());
		
		try
		{
			JSONWriter writer = new JSONStringer();
			writer.array();
			List<Job> jobs = JobDao.getByUsername(username);
			for (int i=offset; i< Math.min((limit+offset), jobs.size()); i++)
			{
				Job job = jobs.get(i);
				writer.object()
					.key("id").value(job.getUuid())
					.key("name").value(job.getName())
					.key("owner").value(job.getOwner())
					.key("executionSystem").value(job.getSystem())
					.key("appId").value(job.getSoftwareName())
					.key("status").value(job.getStatus())
					.key("startTime").value(job.getStartTime() == null ? null : new DateTime(job.getStartTime()).toString())
					.key("endTime").value(job.getEndTime() == null ? null : new DateTime(job.getEndTime()).toString())
					.key("_links").object()
			        	.key("self").object()
			        		.key("href").value(TenancyHelper.resolveURLToCurrentTenant(Settings.IPLANT_JOB_SERVICE) + job.getUuid())
				        .endObject()
			       .endObject()
		        .endObject();
			}
			
			writer.endArray();
			
			return new IplantSuccessRepresentation(writer.toString(), prettyPrint);
		}
		catch (Exception e)
		{
			getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
			return new IplantErrorRepresentation(e.getMessage(), prettyPrint);
		}
	}

	/**
	 * This method represents the HTTP POST action. Posting a job submission
	 * form to this service will submit a contrast job on behalf of the
	 * authenticated user. While this method does not return a value internally,
	 * a {@link org.json.JSONObject JSONObject} representation of the
	 * successfully submitted job is written to the output stream. If the job
	 * fails due to an internal error, a
	 * {@link org.restlet.data.Status#SERVER_ERROR_INTERNAL 500} HTTP code will
	 * be sent. If the job fails due to a user error, a link
	 * {@link org.restlet.data.Status#CLIENT_ERROR_BAD_REQUEST 400} HTTP code
	 * will be sent.
	 */
	@Override
	public void acceptRepresentation(Representation entity)
	{
		IPlantLogServiceClient.log(IPlantLogServiceClient.ServiceKeys.JOBS02.name(), 
				IPlantLogServiceClient.ActivityKeys.JobsSubmit.name(), 
				username, "", getRequest().getClientInfo().getAddress());
		
		try 
		{
			JsonNode json = super.getPostedEntityAsObjectNode(true);
			Job job = JobManager.processJob(json, username, internalUsername);
			
			getResponse().setStatus(Status.SUCCESS_CREATED);
			getResponse().setEntity(new IplantSuccessRepresentation(job.toJSON(), prettyPrint));
		}
		catch (JobProcessingException e) {
			getResponse().setEntity(
					new IplantErrorRepresentation(e.getMessage(), prettyPrint));
			getResponse().setStatus(Status.valueOf(e.getStatus()));
		}
    	catch (ResourceException e) 
		{
			getResponse().setEntity(
					new IplantErrorRepresentation(e.getMessage(), prettyPrint));
			getResponse().setStatus(e.getStatus());
			log.error("Job submission failed for user " + username, e);
		}
		catch (Exception e) {
			getResponse().setEntity(
					new IplantErrorRepresentation("Failed to submit job: " + e.getMessage(), prettyPrint));
			getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
			log.error("Job submission failed for user " + username, e);
		}
	}
	
	@Override
	public boolean allowDelete()
	{
		return false;
	}

	@Override
	public boolean allowGet()
	{
		return true;
	}

	@Override
	public boolean allowPost()
	{
		return true;
	}

	@Override
	public boolean allowPut()
	{
		return false;
	}
}
