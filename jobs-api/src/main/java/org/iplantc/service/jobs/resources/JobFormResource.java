/**
 * 
 */
package org.iplantc.service.jobs.resources;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.WordUtils;
import org.iplantc.service.apps.dao.SoftwareDao;
import org.iplantc.service.apps.exceptions.SoftwareException;
import org.iplantc.service.apps.managers.ApplicationManager;
import org.iplantc.service.apps.model.Software;
import org.iplantc.service.apps.model.SoftwareInput;
import org.iplantc.service.apps.model.SoftwareParameter;
import org.iplantc.service.apps.model.enumerations.ExecutionType;
import org.iplantc.service.apps.model.enumerations.ParallelismType;
import org.iplantc.service.apps.model.enumerations.SoftwareParameterType;
import org.iplantc.service.apps.util.ServiceUtils;
import org.iplantc.service.common.clients.IPlantLogServiceClient;
import org.iplantc.service.jobs.Settings;
import org.iplantc.service.common.representation.IplantErrorRepresentation;
import org.iplantc.service.common.representation.IplantSuccessRepresentation;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONStringer;
import org.json.JSONWriter;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.Representation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.Variant;

/**
 * The JobResource object enables HTTP GET and POST actions on contrast jobs.
 * This resource is primarily used for submitting jobs and viewing a sample HTML
 * job submission form.
 * 
 * @author dooley
 * 
 */
public class JobFormResource extends AbstractJobResource 
{
	private String softwareName; // unique name of the app

	/**
	 * @param context
	 * @param request
	 * @param response
	 */
	public JobFormResource(Context context, Request request, Response response)
	{
		super(context, request, response);

		this.username = getAuthenticatedUsername();
				
		softwareName = (String) request.getAttributes().get("name");

		getVariants().add(new Variant(MediaType.APPLICATION_JSON));
		
		IPlantLogServiceClient.log(IPlantLogServiceClient.ServiceKeys.JOBS02.name(), 
				IPlantLogServiceClient.ActivityKeys.JobSubmissionForm.name(), 
				username, "", request.getClientInfo().getAddress());

	}

	/**
	 * This method represents the HTTP GET action. Without specifying a job
	 * handle, there is no job information to retrieve, so we bind this action
	 * to simply serving a static HTML form for job submission.
	 */
	@Override
	public Representation represent(Variant variant) throws ResourceException
	{

		if (!ServiceUtils.isValid(softwareName))
		{
			getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			return new IplantErrorRepresentation("Software name cannot be empty", prettyPrint);
		} 
		else if (!softwareName.contains("-") || softwareName.endsWith("-")) 
		{
			getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			return new IplantErrorRepresentation("Invalid software name. " +
							"Please specify an application using its unique id. " +
							"The unique id is defined by the application name " +
							"and version separated by a hyphen. eg. example-1.0", prettyPrint);
		}
		
		Software software = SoftwareDao.getSoftwareByUniqueName(softwareName.trim());
		try {
			if (!ApplicationManager.isInvokableByUser(software, username)) {
				throw new SoftwareException("User does not have permission to access this application");
			}
		} catch (SoftwareException e) {
			getResponse().setStatus(Status.CLIENT_ERROR_UNAUTHORIZED);
			return new IplantErrorRepresentation(e.getMessage(), prettyPrint);
		}
		
		try 
		{
			String submitForm = "<form name=\"" + software.getUniqueName() + "\" " +
								"method=\"POST\" " +
								"action=\"" + Settings.IPLANT_JOB_SERVICE + "\"" +
								"class=\"job_submission_form\">";
			
			submitForm += "<table align=\"center\" id=\"contactArea\">\n";
			submitForm += "<tr><td style=\"text-align:center;\" colspan=\"2\">Base Parameters</td></tr>\n";
			submitForm += "<tr><td>Job Name:</td><td><input type=\"text\" name=\"jobName\" value=\"\"></td></tr>\n";
			submitForm += "<tr><td>Software Name:</td><td><input type=\"text\" name=\"softwareName\" value=\"" + software.getUniqueName() + "\"></td></tr>\n";
			if (software.getParallelism().equals(ParallelismType.PARALLEL))
			{
				submitForm += "<tr><td>Processor Count:</td><td><input type=\"text\" name=\"processorCount\" value=\"\"></td></tr>\n";
			}
			if (software.getExecutionType().equals(ExecutionType.HPC))
			{
				submitForm += "<tr><td>Requested Time:</td><td><input type=\"text\" name=\"requestedTime\" value=\"01:00:00\" title=\"Enter in hh:mm:ss format\"></td></tr>\n";
			}
			submitForm += "<tr><td>Max Memory:</td><td><input type=\"text\" name=\"maxMemory\" value=\"\"></td></tr>\n";
			submitForm += "<tr><td>Callback Url:</td><td><input type=\"text\" name=\"callbackUrl\" value=\"\"></td></tr>\n";
			submitForm += "<tr><td>Archive:</td><td><input type=\"checkbox\" value=\"1\" name=\"archive\" checked></td></tr>\n";
			submitForm += "<tr><td>Archive Path:</td><td><input type=\"input\" name=\"archivePath\" value=\"\"></td></tr>\n";
			
			submitForm += "<tr><td style=\"text-align:center;\" colspan=\"2\">Input Files</td></tr>";
			for (SoftwareInput input : software.getInputs())
			{
				if (!input.isVisible()) continue;
				submitForm += "<tr><td>"
						+ WordUtils.capitalizeFully(input.getKey())
						+ ": </td><td><input type=\"text\" name=\"" + input.getKey()
						+ "\" value=\"" + (input.getDefaultValue() == null? "" : input.getDefaultValue()) + "\" title=\"" + input.getDescription() + "\"></td></tr>\n";
			}
	
			submitForm += "<tr><td style=\"text-align:center;\" colspan=\"2\">Input Parameters</td></tr>\n";
			for (SoftwareParameter param : software.getParameters())
			{
				if (!param.isVisible()) continue;
				if (param.getType().equals(SoftwareParameterType.enumeration))
				{
					// get the defaults so we can mark them as selected
					List<String> defaults = new ArrayList<String>();
					if (ServiceUtils.isValid((String)param.getDefaultValue()))
					{
						JSONArray enums = new JSONArray((String)param.getDefaultValue());
						for (int i=0;i<enums.length();i++)
						{
							defaults.add((String)enums.getString(i));
						}
					}
					
					// select box
					submitForm += "<tr><td>"
						+ WordUtils.capitalizeFully(param.getLabel())
						+ ": </td><td><select name=\"" + param.getKey() + "\" " 
						+ (defaults.size() > 1 ? "multiple=\"multiple\"" : "") + ">";
					
					JSONArray enums = new JSONArray(param.getValidator());
					for (int i=0;i<enums.length();i++)
					{
						JSONObject json = enums.getJSONObject(i);
						String key = (String)json.keys().next();
						submitForm += "<option value=\"" + key + "\"" + 
							(defaults.contains(json.get(key).toString()) ? "selected" : "" ) + 
							">" + json.get(key) + "</option>";
					}
					submitForm += "</select></td></tr>\n";
					
				} 
				else if (param.getType().equals(SoftwareParameterType.bool))
				{
					submitForm += "<tr><td>"
						+ WordUtils.capitalizeFully(param.getLabel())
						+ ": </td><td><select name=\"" + param.getKey() + "\">\n" 
						+ "<option value=\"1\""+ (param.getDefaultValue() != null && Boolean.getBoolean(param.getDefaultValue().toString()) ? " selected=\"selected\"" : "") + ">True</option>\n"
						+ "<option value=\"0\""+ (param.getDefaultValue() == null || !Boolean.getBoolean(param.getDefaultValue().toString()) ? " selected=\"selected\"" : "") + ">False</option>\n"
						+ "</select></td></tr>\n";
				}
				else
				{
					submitForm += "<tr><td>"
						+ WordUtils.capitalizeFully(param.getLabel())
						+ ": </td><td><input type=\"text\" name=\"" + param.getKey()
						+ "\" value=\"" + (param.getDefaultValue() == null ? "" : param.getDefaultValue()) + "\" title=\"" + param.getDescription() + "\"></td></tr>\n";
				}
			}
			submitForm += "<tr><td style=\"text-align:center;\" colspan=\"2\"><input type=\"submit\" name=\"submit\" value=\"Submit\"/></td></tr>\n";
			submitForm += "</table></form>";
			JSONWriter writer = new JSONStringer();
			writer.object().key("submitForm").value(submitForm).endObject();
			return new IplantSuccessRepresentation(writer.toString(), prettyPrint);
		}
		catch (Exception e)
		{
			getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
			return new IplantErrorRepresentation(
					"There was an error generating the job submission form for " + software.getUniqueName(), prettyPrint);
		}
		
//		return new IplantSuccessRepresentation(ServiceUtils.getContents(
//				new File(submissionTemplatePath)).replaceAll("JOB_INPUT_FILES",
//				inputFileHTML).replaceAll("BASE_URL", baseUrl).replaceAll(
//				"SOFTWARE_NAME", WordUtils.capitalizeFully(sw.getName())),
//				MediaType.TEXT_HTML);
	}

}
