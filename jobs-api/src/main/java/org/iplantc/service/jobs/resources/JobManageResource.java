/**
 * 
 */
package org.iplantc.service.jobs.resources;

import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.iplantc.service.apps.dao.SoftwareDao;
import org.iplantc.service.apps.managers.ApplicationManager;
import org.iplantc.service.apps.util.ServiceUtils;
import org.iplantc.service.common.clients.IPlantLogServiceClient;
import org.iplantc.service.common.representation.IplantErrorRepresentation;
import org.iplantc.service.common.representation.IplantSuccessRepresentation;
import org.iplantc.service.jobs.dao.JobDao;
import org.iplantc.service.jobs.exceptions.JobException;
import org.iplantc.service.jobs.managers.JobManager;
import org.iplantc.service.jobs.managers.JobPermissionManager;
import org.iplantc.service.jobs.model.Job;
import org.iplantc.service.systems.dao.SystemDao;
import org.iplantc.service.systems.model.RemoteSystem;
import org.iplantc.service.systems.model.enumerations.RemoteSystemType;
import org.restlet.Context;
import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.data.Parameter;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.Representation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.Variant;

/**
 * The JobManageResource is the job management interface for users. Through the
 * actions bound to this class, users can obtain individual job
 * description(GET) and kill jobs (DELETE).
 * 
 * @author dooley
 * 
 */
public class JobManageResource extends AbstractJobResource {
	private static final Logger	log	= Logger.getLogger(JobManageResource.class);

	private String				sJobId;
	private String				internalUsername;
	private Job					job;

	/**
	 * @param context
	 * @param request
	 * @param response
	 */
	public JobManageResource(Context context, Request request, Response response)
	{
		super(context, request, response);

		this.username = getAuthenticatedUsername();
		
		this.sJobId = (String) request.getAttributes().get("jobid");
		
		this.internalUsername = (String) context.getAttributes().get("internalUsername");
		
		getVariants().add(new Variant(MediaType.APPLICATION_JSON));
	}
	
	

	/* (non-Javadoc)
	 * @see org.restlet.resource.Resource#acceptRepresentation(org.restlet.resource.Representation)
	 */
	@Override
	public void acceptRepresentation(Representation entity)
			throws ResourceException
	{

		if (!ServiceUtils.isValid(sJobId))
		{
			getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			getResponse().setEntity(new IplantErrorRepresentation("Job id cannot be empty", prettyPrint));
		}
		

		try
		{
			job = JobDao.getByUuid(sJobId);
			if (job == null) {
				getResponse().setStatus(Status.CLIENT_ERROR_NOT_FOUND);
				getResponse().setEntity(new IplantErrorRepresentation(
						"No job found with job id " + sJobId, prettyPrint));
			}
			
			
			if (new JobPermissionManager(job, username).canWrite(username))
			{
				Map<String,String> pTable = super.getPostedEntityAsMap();
				
				if (!pTable.containsKey("action")) {
					throw new JobException("No action specified");
				}
				
//				if (pTable.get("action").equalsIgnoreCase("archive"))
//				{
//					if (!job.isFinished()) {
//						throw new JobException("Job has not reached a finished state. " +
//								"Please wait until the job stops to archive its output");
//					}
//					
//					String itemsToArchive = null;
//					if (!pTable.containsKey("itemsToArchive")) {
//						throw new JobException("itemsToArchive not specified");
//					} else if (!ServiceUtils.isValid(pTable.get("itemsToArchive"))) {
//						throw new JobException("itemsToArchive cannot be empty");
//					}
//					
//					String path = null;
//					if (!pTable.containsKey("path")) {
//						throw new JobException("path not specified");
//					} else if (!ServiceUtils.isValid(pTable.get("path"))) {
//						throw new JobException("path cannot be empty");
//					}
//					
//				}
//				else if (pTable.get("action").equalsIgnoreCase("resubmit"))
				if (pTable.get("action").equalsIgnoreCase("resubmit"))
				{
					IPlantLogServiceClient.log(IPlantLogServiceClient.ServiceKeys.JOBS02.name(), 
							IPlantLogServiceClient.ActivityKeys.JobsResubmit.name(), 
							username, "", getRequest().getClientInfo().getAddress());
					
					try
					{
						RemoteSystem executionSystem = new SystemDao().findUserSystemBySystemId(
								username, job.getSystem(), RemoteSystemType.EXECUTION);
						
						if (executionSystem == null || !executionSystem.getUserRole(username).canUse()) 
						{
							getResponse().setEntity(new IplantErrorRepresentation(
									"User does not have the necessary role to run " +
									"applications on \"" + job.getSystem()  + "\"", prettyPrint));
							getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN);
							return;
						}
						
						if (!ApplicationManager.isInvokableByUser(SoftwareDao.get(job.getSoftwareName()), username))
						{
							getResponse().setEntity(new IplantErrorRepresentation(
									"Permission denied. You do not have permission to access this application", prettyPrint));
							getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN);
							return;
						}
						
						// create a job object
						Job jobToResubmit = job.copy();
						jobToResubmit.setInternalUsername(internalUsername);
						
						// persisting the job makes it available to the job queue
						// for submission
						JobDao.persist(jobToResubmit);

						if (jobToResubmit.getId() >= 0)
						{
							getResponse().setEntity(
									new IplantSuccessRepresentation(jobToResubmit.toJSON(), prettyPrint));
							getResponse().setStatus(Status.SUCCESS_ACCEPTED);
						}
						else
						{
							getResponse().setEntity(new IplantErrorRepresentation(
									"Failed ot resubmit job request to the queue.", prettyPrint));
							getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
							return;
						}
					}
					catch (Exception e)
					{
						getResponse().setEntity(
								new IplantErrorRepresentation(e.getMessage(), prettyPrint));
						getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
						log.error("Job resubmission failed for user " + username, e);
						return;
					}
				}
				else if (pTable.get("action").equalsIgnoreCase("stop"))
				{
					IPlantLogServiceClient.log(IPlantLogServiceClient.ServiceKeys.JOBS02.name(), 
							IPlantLogServiceClient.ActivityKeys.JobsKill.name(), 
							username, "", getRequest().getClientInfo().getAddress());
					
					try
					{
						JobManager.kill(job);
							
						getResponse().setEntity(new IplantSuccessRepresentation(prettyPrint));
						getResponse().setStatus(Status.SUCCESS_OK);
					}
					catch (JobException e)
					{
						getResponse().setEntity(
								new IplantErrorRepresentation("Failed to kill remote job. " + e.getMessage(), prettyPrint));
						getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST);

					}
					catch (Exception e)
					{
						getResponse().setEntity(
								new IplantErrorRepresentation("Job deletion failed", prettyPrint));
						getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
						log.error("Job deletion failed for user " + username, e);
					}
				}
				else
				{
					throw new JobException("Invalid action " + pTable.get("action"));
				}
			}
			else
			{
				getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN);
				getResponse().setEntity(new IplantErrorRepresentation(
						"User does not have permission to view this job", prettyPrint));
				return;
			}
		}
		catch (ResourceException e)
		{
			getResponse().setStatus(e.getStatus());
			getResponse().setEntity(new IplantErrorRepresentation(
					e.getMessage(), prettyPrint));
			return;
		}
		catch (JobException e)
		{
			getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			getResponse().setEntity(new IplantErrorRepresentation(e.getMessage(), prettyPrint));
		}
		catch (Exception e)
		{
			// can't set a stopped job back to running. Bad request
			getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
			getResponse().setEntity(new IplantErrorRepresentation(e.getMessage(), prettyPrint));
		}
	}



	/**
	 * This method represents the HTTP GET action. Using the job id from the
	 * URL, the job information is retrieved from the databse and sent to the
	 * user as a {@link org.json.JSONObject JSONObject}. If the job id is
	 * invalid for any reason, a HTTP
	 * {@link org.restlet.data.Status#CLIENT_ERROR_BAD_REQUEST 400} code is
	 * sent. If an internal error occurs due to connectivity issues, etc, a
	 * {@link org.restlet.data.Status#SERVER_ERROR_INTERNAL 500} code is sent.
	 */
	@Override
	public Representation represent(Variant variant) throws ResourceException
	{

		IPlantLogServiceClient.log(IPlantLogServiceClient.ServiceKeys.JOBS02.name(), 
				IPlantLogServiceClient.ActivityKeys.JobsGetByID.name(), 
				username, "", getRequest().getClientInfo().getAddress());
		
		if (!ServiceUtils.isValid(sJobId))
		{
			getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			return new IplantErrorRepresentation("Job id cannot be empty", prettyPrint);
		}
//		else
//		{
//			try
//			{
//				jobId = Long.valueOf(sJobId);
//			}
//			catch (Exception e)
//			{
//				getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
//				return new IplantErrorRepresentation("Invalid job id", prettyPrint);
//			}
//		}

		try
		{
			job = JobDao.getByUuid(sJobId);
			if (job == null) {
				getResponse().setStatus(Status.CLIENT_ERROR_NOT_FOUND);
				return new IplantErrorRepresentation("No job found with job id " + sJobId, prettyPrint);
			}
			else if (new JobPermissionManager(job, username).canRead(username))
			{
				return new IplantSuccessRepresentation(job.toJSON(), prettyPrint);
			}
			else
			{
				getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN);
				return new IplantErrorRepresentation(
						"User does not have permission to view this job", prettyPrint);
			}
		}
		catch (JobException e)
		{
			getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			return new IplantErrorRepresentation("Invalid job id "
					+ e.getMessage(), prettyPrint);
		}
		catch (Exception e)
		{
			// can't set a stopped job back to running. Bad request
			getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
			return new IplantErrorRepresentation(e.getMessage(), prettyPrint);
		}

	}

	/**
	 * This method represents the HTTP DELETE action. Using this method, the
	 * user can effectively kill a running job. If the job id included in the
	 * URL is invalid, a HTTP
	 * {@link org.restlet.data.Status#CLIENT_ERROR_BAD_REQUEST 400} code is
	 * sent. If a problem occurs killing the job a HTTP
	 * {@link org.restlet.data.Status#SERVER_ERROR_INTERNAL 500} code is sent.
	 */
	@Override
	public void removeRepresentations()
	{
		IPlantLogServiceClient.log(IPlantLogServiceClient.ServiceKeys.JOBS02.name(), 
				IPlantLogServiceClient.ActivityKeys.JobsDelete.name(), 
				username, "", getRequest().getClientInfo().getAddress());
		
		if (!ServiceUtils.isValid(sJobId))
		{
			getResponse().setEntity(
					new IplantErrorRepresentation("Job id cannot be empty", prettyPrint));
			getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			return;

		}
		
		try
		{
			job = JobDao.getByUuid(sJobId);
			if (job == null) {
				getResponse().setStatus(Status.CLIENT_ERROR_NOT_FOUND);
				getResponse().setEntity(new IplantErrorRepresentation(
						"No job found with job id " + sJobId, prettyPrint));
			}
			else if (new JobPermissionManager(job, username).canWrite(username))
			{
				JobManager.hide(job.getId());
				
				getResponse().setEntity(new IplantSuccessRepresentation(prettyPrint));
				getResponse().setStatus(Status.SUCCESS_OK);
			}
			else
			{
				getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN);
				getResponse().setEntity(new IplantErrorRepresentation(
						"User does not have permission to view this job", prettyPrint));
			}
		}
		catch (JobException e)
		{
			getResponse().setEntity(
					new IplantErrorRepresentation("Failed to kill remote job. " + e.getMessage(), prettyPrint));
			getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST);

		}
		catch (Exception e)
		{
			getResponse().setEntity(
					new IplantErrorRepresentation("Job deletion failed", prettyPrint));
			getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
			log.error("Job deletion failed for user " + username, e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.restlet.resource.Resource#allowDelete()
	 */
	@Override
	public boolean allowDelete()
	{
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.restlet.resource.Resource#allowGet()
	 */
	@Override
	public boolean allowGet()
	{
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.restlet.resource.Resource#allowPost()
	 */
	@Override
	public boolean allowPost()
	{
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.restlet.resource.Resource#allowPut()
	 */
	@Override
	public boolean allowPut()
	{
		return false;
	}

	@SuppressWarnings("unused")
	private Hashtable<String, String> parseForm(Form form)
	{
		Hashtable<String, String> table = new Hashtable<String, String>();

		for (Parameter p : form)
		{
			// boolean foundKey = false;
			String key = "";
			String[] lines = p.getValue().split("\\n");
			for (String line : lines)
			{
				if (line.indexOf(",") == 0)
				{
					line = line.substring(2);
				}
				line = line.replaceAll("\\r", "");
				if (line.startsWith("--") || line.equals(""))
				{
					continue;
				}
				else
				{
					if (line.startsWith("\""))
					{
						key = line.replaceAll("\"", "");
					}
					else if (line.startsWith("Content-Disposition"))
					{
						key = line.substring(line.indexOf("=") + 1);
						key = key.replaceAll("\"", "");
						// foundKey = true;
					}
					else
					{
						table.put(key, line);
					}
				}
			}
		}
		return table;
	}

}
