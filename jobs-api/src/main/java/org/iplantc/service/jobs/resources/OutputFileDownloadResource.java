/**
 * 
 */
package org.iplantc.service.jobs.resources;


import javax.activation.MimetypesFileTypeMap;

import org.apache.commons.io.FilenameUtils;
import org.iplantc.service.apps.dao.SoftwareDao;
import org.iplantc.service.apps.model.Software;
import org.iplantc.service.apps.model.enumerations.ExecutionType;
import org.iplantc.service.apps.util.ServiceUtils;
import org.iplantc.service.common.clients.IPlantLogServiceClient;
import org.iplantc.service.common.representation.IplantErrorRepresentation;
import org.iplantc.service.common.representation.RemoteDataWriterRepresentation;
import org.iplantc.service.jobs.dao.JobDao;
import org.iplantc.service.jobs.exceptions.JobException;
import org.iplantc.service.jobs.managers.JobPermissionManager;
import org.iplantc.service.jobs.model.Job;
import org.iplantc.service.jobs.model.enumerations.JobStatusType;
import org.iplantc.service.jobs.util.DataLocator;
import org.iplantc.service.systems.exceptions.SystemException;
import org.iplantc.service.systems.model.RemoteSystem;
import org.iplantc.service.transfer.RemoteDataClient;
import org.iplantc.service.transfer.exceptions.RemoteDataException;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.Representation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.Variant;


/**
 * Class to handle get and post requests for jobs
 * 
 * @author dooley
 * 
 */
public class OutputFileDownloadResource extends AbstractJobResource 
{
	private String				path;
	private String				sJobId;

	/**
	 * @param context
	 * @param request
	 * @param response
	 */
	public OutputFileDownloadResource(Context context, Request request,
			Response response)
	{
		super(context, request, response);

		this.username = getAuthenticatedUsername();
		
		sJobId = (String) request.getAttributes().get("jobid");
		
		path = getFilePathFromURL();
		
		getVariants().add(new Variant(MediaType.APPLICATION_JSON));
		
		IPlantLogServiceClient.log(IPlantLogServiceClient.ServiceKeys.JOBS02.name(), 
				IPlantLogServiceClient.ActivityKeys.JobsGetOutput.name(), 
				username, "", getRequest().getClientInfo().getAddress());
		
	}

	/**
	 * This method represents the HTTP GET action. Using the job id and output
	 * file name from the URL, the file streamed from the file system to the
	 * user. If the job id is invalid for any reason, a HTTP
	 * {@link org.restlet.data.Status#CLIENT_ERROR_NOT_FOUND 404} code is sent.
	 * If the file is not present, a
	 * {@link org.restlet.data.Status#SERVER_ERROR_INTERNAL 500} is sent.
	 */
	@Override
	public Representation represent(Variant variant) throws ResourceException
	{

		if (!ServiceUtils.isValid(path))
		{
			getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			return new IplantErrorRepresentation("Invalid file name", prettyPrint);
		}

		try
		{

			Job job = JobDao.getByUuid(sJobId);

			if (job == null) { throw new ResourceException(
					Status.CLIENT_ERROR_BAD_REQUEST, "Invalid job id"); }

			JobPermissionManager pemManager = new JobPermissionManager(job, username);
			if (!pemManager.canRead(username)) { throw new ResourceException(
					Status.CLIENT_ERROR_UNAUTHORIZED,
					"User does not have permission to view this resource."); }

			String remotePath = null;
			
			DataLocator dataLocator = new DataLocator(job);
			
			RemoteSystem jobDataSystem = dataLocator.findOutputSystemForJobData();
			RemoteDataClient remoteClient = jobDataSystem.getRemoteDataClient(job.getInternalUsername());
			remoteClient.authenticate();
			
			if (job.isFailed() || job.isRunning() || !job.isArchiveOutput() || 
					job.getStatus().equals(JobStatusType.STOPPED))
			{
				Software software = SoftwareDao.getSoftwareByUniqueName(job.getSoftwareName());
				
				if (software == null) { throw new JobException(
						"Failed to find the software record for this job"); }
				
				if (software.getExecutionType().equals(ExecutionType.HPC))
				{ // job is an hpc job, use gridftp
	
					RemoteDataClient client = null;
					RemoteSystem system = null;
					try
					{
						system = software.getExecutionSystem();
						
						client = system.getRemoteDataClient(job.getInternalUsername());
						
						if (!ServiceUtils.isValid(job.getWorkPath()))
						{
							throw new RemoteDataException("No work directory specified for this job. Usually this occurs when a job failed during submission.");
						}
						else 
						{
							client.authenticate();
							
							if (client.doesExist(job.getWorkPath() + path))
							{
								remotePath = job.getWorkPath() + path;
							} else {
								throw new RemoteDataException(
									"Unable to locate job data. Work folder no longer exists.");
							}
						}
					}
					catch (RemoteDataException e)
					{
						throw e;
					}
					catch (Exception e)
					{
						throw new RemoteDataException(
								"Failed to list output folder " + job.getWorkPath() + path
										+ " for job " + job.getUuid(), e);
					}
				}
				else if (software.getExecutionType().equals(
						ExecutionType.ATMOSPHERE))
				{ // job is an atmo job
					throw new RemoteDataException(
							"Data cannot be retrieved from Atmosphere systems.");
				}
				else if (software.getExecutionType().equals(ExecutionType.CONDOR))
				{
					if (job.isFinished() && !job.isArchiveOutput()) {
						throw new ResourceException(Status.CLIENT_ERROR_NOT_FOUND,
							"Job was not archived and data cannot be retrieved from condor systems.");
					} else {
						throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST,
							"Data cannot be retrieved from condor systems.");
					}
				}
				else
				{
					throw new RemoteDataException("Unknown execution system type.");
				}
			}
			else if (job.isArchiveOutput())
			{
				remotePath = job.getArchivePath() + path;
			}
			
			String mimetype = new MimetypesFileTypeMap().getContentType(FilenameUtils.getName(remotePath));
			
			if (jobDataSystem != null && jobDataSystem.isAvailable()) {
				return new RemoteDataWriterRepresentation(
						remoteClient, null, remotePath, new MediaType(mimetype));
			} else {
				throw new SystemException("The submission system for this job is no longer available");
			}
		}
		catch (ResourceException e)
		{
			getResponse().setStatus(e.getStatus());
			return new IplantErrorRepresentation(e.getMessage(), prettyPrint);
		}
		catch (Exception e)
		{
			getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
			return new IplantErrorRepresentation(e.getMessage(), prettyPrint);
		}
	}
	
	private String getFilePathFromURL()
	{
		String path = getRequest().getResourceRef().getPath(true);
		
		path = path.substring(path.indexOf("outputs/media") + "outputs/media".length());
		
		path = "/" + path;
		
		path = path.replaceAll("\\.\\.", "").replaceAll("//", "/").replaceAll("~","");
		
		return path;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.restlet.resource.Resource#allowDelete()
	 */
	@Override
	public boolean allowDelete()
	{
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.restlet.resource.Resource#allowGet()
	 */
	@Override
	public boolean allowGet()
	{
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.restlet.resource.Resource#allowPost()
	 */
	@Override
	public boolean allowPost()
	{
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.restlet.resource.Resource#allowPut()
	 */
	@Override
	public boolean allowPut()
	{
		return false;
	}

}