package org.iplantc.service.jobs.managers.launchers;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.iplantc.service.apps.dao.SoftwareDao;
import org.iplantc.service.apps.exceptions.SoftwareException;
import org.iplantc.service.apps.model.Software;
import org.iplantc.service.apps.util.ServiceUtils;
import org.iplantc.service.apps.util.ZipUtil;
import org.iplantc.service.common.dao.TenantDao;
import org.iplantc.service.common.model.Tenant;
import org.iplantc.service.common.persistence.TenancyHelper;
import org.iplantc.service.jobs.Settings;
import org.iplantc.service.jobs.exceptions.JobException;
import org.iplantc.service.jobs.exceptions.QuotaViolationException;
import org.iplantc.service.jobs.exceptions.SchedulerException;
import org.iplantc.service.jobs.exceptions.SystemUnavailableException;
import org.iplantc.service.jobs.model.Job;
import org.iplantc.service.jobs.model.enumerations.JobMacroType;
import org.iplantc.service.jobs.model.enumerations.JobStatusType;
import org.iplantc.service.jobs.util.EmailMessage;
import org.iplantc.service.jobs.util.Slug;
import org.iplantc.service.remote.RemoteSubmissionClient;
import org.iplantc.service.systems.dao.SystemDao;
import org.iplantc.service.systems.model.ExecutionSystem;
import org.iplantc.service.transfer.RemoteDataClient;
import org.iplantc.service.transfer.RemoteDataClientFactory;
import org.iplantc.service.transfer.util.MD5Checksum;
import org.joda.time.DateTime;

/**
 * Interface to define how to launch applications on various resources
 * 
 * @author dooley
 * 
 */
public abstract class AbstractJobLauncher 
{	
	private static final Logger log = Logger.getLogger(AbstractJobLauncher.class);
	public static final String ARCHIVE_FILENAME = ".agave.archive";
	
	protected File 						tempAppDir = null;
	protected String 						step;
    protected Job						job;
    protected Software 					software;
	protected ExecutionSystem 			executionSystem;
	protected RemoteDataClient	 		remoteExecutionDataClient;
	protected RemoteDataClient	 		remoteSoftwareDataClient;
	protected RemoteSubmissionClient 	submissionClient = null;
	
	public AbstractJobLauncher(Job job) {
		this.job = job;
        this.software = SoftwareDao.getSoftwareByUniqueName(job.getSoftwareName());
        this.executionSystem = (ExecutionSystem) new SystemDao().findBySystemId(job.getSystem());
	}
	
	public abstract void launch() throws JobException, SchedulerException, IOException, QuotaViolationException, SystemUnavailableException;
	
	protected String resolveMacros(String appTemplate) {
		
		// Replace all the iplant template tags
		String iplantStatusCallback = "curl -k \"" + TenancyHelper.resolveURLToCurrentTenant(Settings.IPLANT_JOB_SERVICE)
			+ "trigger/job/" + job.getUuid() + "/token/"
			+ job.getUpdateToken() + "/status/";
		
		String iplantMacroCallback = "curl -k \"" + TenancyHelper.resolveURLToCurrentTenant(Settings.IPLANT_JOB_SERVICE)
				+ "trigger/job/" + job.getUuid() + "/token/"
				+ job.getUpdateToken() + "/macro/";
		
		String iplantRunningCallback = iplantStatusCallback + JobStatusType.RUNNING + "\"";
		String iplantSuccessCallback = iplantStatusCallback + JobStatusType.FINISHED + "\"";
		String iplantFailureCallback = iplantStatusCallback + JobStatusType.FAILED + "\"";
		String iplantArchivingStartCallback = iplantStatusCallback + JobStatusType.ARCHIVING + "\"";
		String iplantArchivingSuccessCallback = iplantStatusCallback + JobStatusType.ARCHIVING_FINISHED + "\"";
		String iplantArchivingFailureCallback = iplantStatusCallback + JobStatusType.ARCHIVING_FAILED + "\"";
		String iplantCleaningUpCallback = iplantStatusCallback + JobStatusType.CLEANING_UP + "\"";
		String iplantHeartbeatCallback = iplantMacroCallback + JobMacroType.HEARTBEAT + "\"";
		String iplantNotificationCallback = iplantMacroCallback + JobMacroType.NOTIFICATION + "\"";
		
		appTemplate = appTemplate.replaceAll("\\$\\{IPLANT_JOB_NAME\\}", Slug.toSlug(job.getName()));
		appTemplate = appTemplate.replaceAll("\\$\\{AGAVE_JOB_NAME\\}", Slug.toSlug(job.getName()));
		
		appTemplate = appTemplate.replaceAll("\\$\\{AGAVE_JOB_ID\\}", 
				(ServiceUtils.isValid(job.getUuid()) ? job.getUuid().toString() : ""));
		
		appTemplate = appTemplate.replaceAll("\\$\\{AGAVE_JOB_APP_ID\\}", job.getSoftwareName());
				
		appTemplate = appTemplate.replaceAll("\\$\\{AGAVE_JOB_EXECUTION_SYSTEM\\}", 
				(ServiceUtils.isValid(job.getSystem()) ? job.getSystem() : ""));
		
		appTemplate = appTemplate.replaceAll("\\$\\{AGAVE_JOB_BATCH_QUEUE\\}", job.getBatchQueue());
		
		appTemplate = appTemplate.replaceAll("\\$\\{AGAVE_JOB_SUBMIT_TIME\\}", new DateTime(job.getSubmitTime()).toString());
		
		appTemplate = appTemplate.replaceAll("\\$\\{AGAVE_JOB_ARCHIVE_SYSTEM\\}", 
				(job.getArchiveSystem() == null ? "" : job.getArchiveSystem().getSystemId()));
		
		appTemplate = appTemplate.replaceAll("\\$\\{AGAVE_JOB_ARCHIVE_PATH\\}", 
				(ServiceUtils.isValid(job.getArchivePath()) ? job.getArchivePath() : ""));
		
		appTemplate = appTemplate.replaceAll("\\$\\{AGAVE_JOB_NODE_COUNT\\}", String.valueOf(job.getNodeCount()));
		
		appTemplate = appTemplate.replaceAll("\\$\\{IPLANT_CORES_REQUESTED\\}", String.valueOf(job.getProcessorsPerNode() * job.getNodeCount()));
		appTemplate = appTemplate.replaceAll("\\$\\{AGAVE_JOB_PROCESSORS_PER_NODE\\}", String.valueOf(job.getProcessorsPerNode()));
		
		appTemplate = appTemplate.replaceAll("\\$\\{AGAVE_JOB_MEMORY_PER_NODE\\}", String.valueOf(job.getMemoryPerNode()));
		
//		appTemplate = appTemplate.replaceAll("\\$\\{IPLANT_HOME\\}", 
//				job.getArchiveSystem().getStorageConfig().getHomeDir() + job.getOwner());
//		
		appTemplate = appTemplate.replaceAll("\\$\\{AGAVE_JOB_ARCHIVE_URL\\}", 
				TenancyHelper.resolveURLToCurrentTenant(Settings.IPLANT_IO_SERVICE) + "media/system/" + job.getArchiveSystem().getSystemId() + "/" + job.getArchivePath());
		
		appTemplate = appTemplate.replaceAll("\\$\\{IPLANT_RUNNING\\}", iplantRunningCallback);
		appTemplate = appTemplate.replaceAll("\\$\\{AGAVE_JOB_CALLBACK_RUNNING\\}", iplantRunningCallback);
		
		appTemplate = appTemplate.replaceAll("\\$\\{IPLANT_SUCCESS\\}", iplantSuccessCallback);
		appTemplate = appTemplate.replaceAll("\\$\\{AGAVE_JOB_CALLBACK_SUCCESS\\}", iplantSuccessCallback);
		
		appTemplate = appTemplate.replaceAll("\\$\\{IPLANT_CLEANING_UP\\}", iplantCleaningUpCallback);
		appTemplate = appTemplate.replaceAll("\\$\\{AGAVE_JOB_CALLBACK_CLEANING_UP\\}", iplantCleaningUpCallback);
		
		appTemplate = appTemplate.replaceAll("\\$\\{IPLANT_FAILURE\\}", iplantFailureCallback);
		appTemplate = appTemplate.replaceAll("\\$\\{AGAVE_JOB_CALLBACK_FAILURE\\}", iplantFailureCallback);
		
		appTemplate = appTemplate.replaceAll("\\$\\{IPLANT_ARCHIVING_START\\}", iplantArchivingStartCallback);
		appTemplate = appTemplate.replaceAll("\\$\\{AGAVE_JOB_CALLBACK_ARCHIVING_START\\}", iplantArchivingStartCallback);
		
		appTemplate = appTemplate.replaceAll("\\$\\{IPLANT_ARCHIVING_SUCCESS\\}", iplantArchivingSuccessCallback);
		appTemplate = appTemplate.replaceAll("\\$\\{AGAVE_JOB_CALLBACK_ARCHIVING_SUCCESS\\}", iplantArchivingSuccessCallback);
		
		appTemplate = appTemplate.replaceAll("\\$\\{IPLANT_ARCHIVING_FAILURE\\}", iplantArchivingFailureCallback);
		appTemplate = appTemplate.replaceAll("\\$\\{AGAVE_JOB_CALLBACK_ARCHIVING_FAILURE\\}", iplantArchivingFailureCallback);
		
		appTemplate = appTemplate.replaceAll("\\$\\{AGAVE_JOB_CALLBACK_ALIVE\\}", iplantHeartbeatCallback);
		appTemplate = appTemplate.replaceAll("\\$\\{AGAVE_JOB_CALLBACK_NOTIFICATION\\}", iplantNotificationCallback);
		
		return appTemplate;
	}
	
	protected String getInputFileName(String apiUsername, String internalUsername, String inputValue) 
	throws Exception {
		try
		{
			if (StringUtils.isEmpty(inputValue)) {
				return "";
			} else {
				URI uri = new URI(inputValue);
				RemoteDataClientFactory factory = new RemoteDataClientFactory();
				RemoteDataClient rdc = factory.getInstance(apiUsername, internalUsername, uri);
				return rdc.resolvePath(uri.getPath());
			}
		}
		catch (URISyntaxException e)
		{
			return FilenameUtils.getName(inputValue);
		}
		
	}
	
	/**
     * sets up the application dir for job launch
     * @throws JobException 
     */
    protected void createTempAppDir() throws JobException {
        step = "Creating temporary application directory for job";
        log.debug(step);
        
        // # local temp directory on server for staging execution folders.
        // this should have been created by the staging task, but in case
        // there were no inputs, we need to create it ourself.
        if (StringUtils.isEmpty(job.getWorkPath())) {
        	String remoteWorkPath = null;
        	
			if (!StringUtils.isEmpty(software.getExecutionSystem().getScratchDir())) {
				remoteWorkPath = software.getExecutionSystem().getScratchDir();
			} else if (!StringUtils.isEmpty(software.getExecutionSystem().getWorkDir())) {
				remoteWorkPath = software.getExecutionSystem().getWorkDir();
			}
			
			if (!StringUtils.isEmpty(remoteWorkPath)) {
				if (!remoteWorkPath.endsWith("/")) remoteWorkPath += "/";
			} else {
				remoteWorkPath = "";
			}
			
			remoteWorkPath += job.getOwner() +
					"/job-" + job.getUuid() + "-" + Slug.toSlug(job.getName());
			
//			if (software.isPubliclyAvailable()) {
//				remoteWorkPath +=  FilenameUtils.getBaseName(software.getDeploymentPath());
//			} else {
//				remoteWorkPath +=  FilenameUtils.getName(software.getDeploymentPath());
//			}
			
			job.setWorkPath(remoteWorkPath);
        }
        
        tempAppDir = new File(FileUtils.getTempDirectory(), FilenameUtils.getName(job.getWorkPath()));
        
        if (tempAppDir.exists()) {
        	FileUtils.deleteQuietly(tempAppDir);
        }
        
        tempAppDir.mkdirs();
    }
    
    /**
     * copies the deployment path in irods to the local tempAppDir in preparation of condor_submit
     */
    protected void copySoftwareToTempAppDir() throws JobException 
    {
        step = "Copy application package from software storage system " +
        		"to temp application directory";
        log.debug(step);
        
        try {
        	if (software.getStorageSystem() == null) {
        		throw new JobException("Storage system is no longer available");
        	} else {
        		remoteSoftwareDataClient = software.getStorageSystem().getRemoteDataClient();
	        	if (remoteSoftwareDataClient != null) {
	        		remoteSoftwareDataClient.authenticate();
	        		// what we really want is to just copy contents into tempAppDir, so we work around default behavior
	        		// first copy the remote data here
	        		remoteSoftwareDataClient.get(software.getDeploymentPath(), tempAppDir.getAbsolutePath());
	        		// now copy the contents of the deployment folder to the parent dir, which is tempAppDir
	        		File copiedDeploymentFolder = new File(tempAppDir, FilenameUtils.getName(software.getDeploymentPath()));
	        		if (!copiedDeploymentFolder.getAbsoluteFile().equals(tempAppDir) && copiedDeploymentFolder.exists() && copiedDeploymentFolder.isDirectory()) {
	        			FileUtils.copyDirectory(copiedDeploymentFolder, tempAppDir, null, true);
		        		// now delete the redundant deployment folder
		        		FileUtils.deleteQuietly(copiedDeploymentFolder);
	        		}
	        		
	        		if (software.isPubliclyAvailable()) {
	    				// validate the checksum to make sure the app itself hasn't  changed
	    				File zippedFile = new File(tempAppDir, FilenameUtils.getName(software.getDeploymentPath()));
	    				String checksum = MD5Checksum.getMD5Checksum(zippedFile.getAbsolutePath());
	    				if (checksum.equals(software.getChecksum())) 
	    				{
	    					ZipUtil.unzip(zippedFile, tempAppDir);
	    					if (tempAppDir.list().length > 1) {
	    						zippedFile.delete();
	    					} else {
	    						throw new SoftwareException("Failed to unpack the application bundle.");
	    					}
	    				} else {
	    					software.setAvailable(false);
	    					SoftwareDao.persist(software);
	    					Tenant tenant = new TenantDao().findByTenantId(TenancyHelper.getCurrentTenantId());
	    					EmailMessage.send(tenant.getContactName(), 
	    							tenant.getContactEmail(), 
	    							"Public app " + software.getUniqueName() + " has been corrupted.", 
	    							"While submitting a job, the Job Service noticed that the checksum " +
	    							"of the public app " + software.getUniqueName() + " had changed. This " +
	    							"will impact provenance and could impact experiment reproducability. " +
	    							"Please restore the application zip bundle from archive and re-enable " + 
	    							"the application via the admin console.\n\n" +
	    							"Name: " + software.getUniqueName() + "\n" + 
	    							"User: " + job.getOwner() + "\n" +
	    							"Job: " + job.getUuid() + "\n" +
	    							"Time: " + job.getCreated().toString() + "\n\n");
	    					throw new SoftwareException("Public application bundle has changed. " +
	    							"This application will be disabled. Please contact an admin for " +
	    							"further information.");
	    				}
	    				
	    				File standardLocation = new File(tempAppDir, new File(software.getDeploymentPath()).getName());
	    				if (standardLocation.exists()) {
	    					tempAppDir = standardLocation.getAbsoluteFile();
	    				} else {
	    					standardLocation = new File(tempAppDir, software.getExecutablePath());
	    					
	    					if (!standardLocation.exists()) {
	    						// need to go searching for the path. no idea how this could happen
	    						boolean foundDeploymentPath = false;
	    						for (File child: tempAppDir.listFiles()) 
	    						{
	    							if (child.isDirectory()) {
	    								standardLocation = new File(child, software.getExecutablePath());
	    								if (standardLocation.exists()) {
	    									File copyDir = new File(tempAppDir.getAbsolutePath()+".copy");
	    									FileUtils.moveDirectory(child, copyDir);
	    									FileUtils.deleteDirectory(tempAppDir);
	    									copyDir.renameTo(tempAppDir);
	    									//tempAppDir = child;
	    									foundDeploymentPath = true;
	    									break;
	    								}
	    							}
	    						}
	    						
	    						if (!foundDeploymentPath) {
	    							log.error("Unable to find app path for public app " + software.getUniqueName());
	    							throw new SoftwareException("Unable to find the deployment path for the public app " + software.getUniqueName());
	    						}
	    					}
	    				}
	    			}
	        	} else {
	        		throw new JobException("Unable to obtain a remote data client for " +
	        				"the storage system.");
	        	}
        	}
        } catch (JobException e) {
        	throw e;
        } catch (Exception e) {
            throw new JobException("remote data connection to " + software.getExecutionSystem().getSystemId() + " threw exception and stopped job execution", e);
        } finally {
        	 try { remoteSoftwareDataClient.disconnect(); } catch (Exception e) {}
        }
    }

    protected abstract void processApplicationTemplate() throws JobException;
    
    protected void stageSofwareApplication() throws JobException 
    {	
		try {
			log.debug("Staging app dependencies for job " + job.getUuid());
			remoteExecutionDataClient = new SystemDao().findBySystemId(job.getSystem()).getRemoteDataClient(job.getInternalUsername());
			remoteExecutionDataClient.authenticate();
			remoteExecutionDataClient.mkdirs(job.getWorkPath());
			remoteExecutionDataClient.put(tempAppDir.getAbsolutePath(), new File(job.getWorkPath()).getParent());
		} 
		catch (Exception e) {

			throw new JobException("Failed to stage application dependencies to execution system", e);
		} finally {
			try { remoteExecutionDataClient.disconnect();} catch (Exception e) {} 
		}
    }

    /**
     *  This method creates an archive log file
     * @param logFileName
     * @return
     * @throws JobException
     */
    protected File createArchiveLog(String logFileName) throws JobException {
        FileWriter logWriter = null;
        try 
        {
            File archiveLog = new File(tempAppDir, logFileName);
            archiveLog.createNewFile();
            logWriter = new FileWriter(archiveLog);

            printListing(tempAppDir, tempAppDir, logWriter);
            
            /* Uncomment to disable input file archiving with job outputs */
//            // add the inputs with their resolved filenames
//            Map<String, String> inputMap;
//			try
//			{
//				inputMap = job.getInputsAsMap();
//				for (String key : inputMap.keySet())
//				{
//					URI uri;
//					try {
//						uri = new URI(inputMap.get(key));
//						logWriter.write(FilenameUtils.getName(uri.getPath()) + "\n");
//					}
//					catch (URISyntaxException e) {}	
//				}
//			}
//			catch (JSONException e1) {}
//			
//			// add hidden (static) inputs
//			for (SoftwareInput swInput : software.getInputs())
//			{
//				if (!swInput.isVisible())
//				{
//					URI uri;
//					try {
//						uri = new URI(swInput.getDefaultValue());
//						logWriter.write(FilenameUtils.getName(uri.getPath()) + "\n");
//					}
//					catch (URISyntaxException e) {}
//					
//				}
//			}
            
            return archiveLog;
        } 
        catch (IOException e) 
        {
            step = "Creating an archive log file";
            log.debug(step);
            throw new JobException("Failed to create archive file for job " + job.getUuid(), e);
        }
		finally {
            try { logWriter.close(); } catch (Exception e) {}
        }
    }
    
    protected void printListing(File file, File baseFolder, FileWriter writer) throws IOException
    {
        if (file.isFile())
        {
        	String relativeFilePath = StringUtils.removeStart(file.getPath(), baseFolder.getPath());
        	if (relativeFilePath.startsWith("/"))
        		relativeFilePath = relativeFilePath.substring(1);
            writer.append(relativeFilePath + "\n");
        }
        else
        {
            for(File child: file.listFiles())
            {
            	String relativeChildPath = StringUtils.removeStart(child.getPath(), baseFolder.getPath());
            	if (relativeChildPath.startsWith("/"))
            		relativeChildPath = relativeChildPath.substring(1);
                writer.append(relativeChildPath + "\n");

                if (child.isDirectory())
                {
                    printListing(child, baseFolder, writer);
                }
            }
        }
    }
    
    protected abstract String submitJobToQueue() throws JobException;
}
