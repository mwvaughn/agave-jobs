/**
 * 
 */
package org.iplantc.service.jobs.managers.monitors;

import org.iplantc.service.jobs.exceptions.RemoteJobMonitoringException;
import org.iplantc.service.jobs.model.Job;

/**
 * Abstract class to structure individual job monitoring.
 * 
 * @author dooley
 *
 */
public abstract class AbstractJobMonitor implements JobMonitor {

	protected Job job;
	
	public AbstractJobMonitor(Job job) {
		this.job = job;
	}
	
	/* (non-Javadoc)
	 * @see org.iplantc.service.jobs.managers.monitors.JobMonitor#getStatus()
	 */
	@Override
	public abstract void updateStatus() throws RemoteJobMonitoringException;
}
