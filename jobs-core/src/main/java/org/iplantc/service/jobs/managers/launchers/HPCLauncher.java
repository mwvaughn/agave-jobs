/**
 * 
 */
package org.iplantc.service.jobs.managers.launchers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.iplantc.service.apps.exceptions.SoftwareException;
import org.iplantc.service.apps.model.SoftwareInput;
import org.iplantc.service.apps.model.SoftwareParameter;
import org.iplantc.service.apps.util.ServiceUtils;
import org.iplantc.service.common.persistence.HibernateUtil;
import org.iplantc.service.jobs.Settings;
import org.iplantc.service.jobs.dao.JobDao;
import org.iplantc.service.jobs.exceptions.JobException;
import org.iplantc.service.jobs.exceptions.QuotaViolationException;
import org.iplantc.service.jobs.exceptions.SchedulerException;
import org.iplantc.service.jobs.exceptions.SystemUnavailableException;
import org.iplantc.service.jobs.managers.JobManager;
import org.iplantc.service.jobs.managers.parsers.RemoteJobIdParser;
import org.iplantc.service.jobs.managers.parsers.RemoteJobIdParserFactory;
import org.iplantc.service.jobs.model.Job;
import org.iplantc.service.jobs.model.enumerations.JobStatusType;
import org.iplantc.service.jobs.model.scripts.CommandStripper;
import org.iplantc.service.jobs.model.scripts.SubmitScript;
import org.iplantc.service.jobs.model.scripts.SubmitScriptFactory;
import org.iplantc.service.systems.model.enumerations.SystemStatusType;

/**
 * @author dooley
 * 
 */
public class HPCLauncher extends AbstractJobLauncher 
{
	private final Logger log = Logger.getLogger(HPCLauncher.class);

	protected String batchScriptName = null;
    
	/**
	 * Creates an instance of a JobLauncher capable of submitting jobs to batch
	 * queuing systems on HPC resources.
	 */
	public HPCLauncher(Job job)
	{
		super(job);
	}

	/*
	 * Put the job in the batch scheduling queue
	 */
	@Override
	public void launch() throws JobException, SchedulerException, IOException, QuotaViolationException, SystemUnavailableException
	{
		File tempAppDir = null;
		try
		{
			if (software == null || !software.isAvailable()) {
				throw new SoftwareException("Application is not longer available for execution");
			}
			
			// if the system is down, return it to the queue to wait for the system
			// to come back up.
			if (executionSystem == null || !executionSystem.getStatus().equals(SystemStatusType.UP)) 
			{
				throw new SystemUnavailableException();
			} 
			
			// sets up the application directory to execute this job launch; see comments in method
            createTempAppDir();

            // copy our application package from the software.deploymentPath to our tempAppDir
            copySoftwareToTempAppDir();
            
            // prepend the application template with call back to let the Job service know the job has started
            // parse the application template and replace tokens with the inputs, parameters and outputs.
            processApplicationTemplate();
			
            // create the shadow file containing the exclusion files for archiving
            createArchiveLog(ARCHIVE_FILENAME);
			
            // move the local temp directory to the remote system
            stageSofwareApplication();
			
            String remoteJobId = submitJobToQueue();
            
            job.setSubmitTime(new Date()); // Date job submitted to queue
            job.setLastUpdated(new Date());  // Date job started by queue
            job.setLocalJobId(remoteJobId);
            job.setStatus(JobStatusType.QUEUED, "HPC job successfully placed into queue");
            JobDao.persist(job);
		}
		catch (Exception e)
		{
			log.error(e);
			throw new JobException(e);
		}
		finally
		{
			try { HibernateUtil.closeSession(); } catch (Exception e) {}
			try { FileUtils.deleteDirectory(tempAppDir); } catch (Exception e) {}
			try { remoteSoftwareDataClient.disconnect(); } catch (Exception e) {}
			try { remoteExecutionDataClient.disconnect(); } catch (Exception e) {}
			try { submissionClient.close(); } catch (Exception e) {}
		}
	}
	
	@Override
    protected void processApplicationTemplate() throws JobException 
    {
		step = "Process the application template ";
		 
        log.debug(step);
        FileWriter batchWriter = null;
        String appTemplate = null;
        try 
		{
			// create the submit script in the temp folder
			batchScriptName = job.getName().replaceAll(" ", "_") + ".ipcexe";
			batchWriter = new FileWriter(tempAppDir + File.separator
					+ batchScriptName);
			
			SubmitScript script = SubmitScriptFactory.getScript(job);
	
			// write the script header
			batchWriter.write(script.getScriptText());
	
			// write the callback to trigger status update at start
			batchWriter.write("\ncurl -k \"" + Settings.IPLANT_JOB_SERVICE
					+ "trigger/job/" + job.getUuid() + "/token/"
					+ job.getUpdateToken() + "/status/" + JobStatusType.RUNNING
					+ "\"\n");
			//batchWriter.write("\n\ncd " + job.getWorkPath());
			batchWriter.write("\n\n# Environmental settings for "
					+ job.getSoftwareName() + ":\n\n");
	
			
			// add modules. The irods module is required for all HPC systems
			// we add it last so it can't be purged by app module commands
			if (ServiceUtils.isValid(software.getModules()))
			{
				for (String module : software.getModules().split(","))
				{
					batchWriter.write("module " + module + "\n");
				}
			}
			
			// add in any custom environment variables that need to be set
            if (!StringUtils.isEmpty(software.getExecutionSystem().getEnvironment())) {
            	batchWriter.write(software.getExecutionSystem().getEnvironment() + "\n");
        	}
            
            //batchWriter.write("module load irods\n"); // already loaded by default
			
			// read in the template file
			File appTemplateFile = new File(tempAppDir
					+ File.separator + software.getExecutablePath());
			
			if (!appTemplateFile.exists()) {
				throw new FileNotFoundException("Unable to locate wrapper script for \"" + 
						software.getUniqueName() + "\" at " + 
						appTemplateFile.getAbsolutePath());
			}
			
			appTemplate = FileUtils.readFileToString(appTemplateFile);
			
			// replace the parameters with their passed in values
			Map<String, Object> parameterMap = job.getParametersAsMap();
			for (String key : parameterMap.keySet())
			{
				SoftwareParameter param = software.getParameter(key);
				if (param.isShowArgument()) {
					appTemplate = appTemplate.replaceAll("(?i)\\$\\{" + key + "\\}", 
							param.getArgument() + parameterMap.get(key).toString());
				} else {
					appTemplate = appTemplate.replaceAll("(?i)\\$\\{" + key + "\\}", 
							parameterMap.get(key).toString());
				}
			}
			
			// add hidden (static) parameters
			for (SoftwareParameter param : software.getParameters())
			{
				if (!param.isVisible())
				{
					if (param.isShowArgument()) {
						appTemplate = appTemplate.replaceAll("(?i)\\$\\{" + param.getKey() + "\\}", 
								param.getArgument() + param.getDefaultValue().toString());
					} else {
						appTemplate = appTemplate.replaceAll("(?i)\\$\\{" + param.getKey() + "\\}", 
								param.getDefaultValue().toString());
					}
				}
				else if (!parameterMap.containsKey(param.getKey())) {
					appTemplate = appTemplate.replaceAll("(?i)\\$\\{" + param.getKey() + "\\}", "");
				}
			}
			
			// replace the executable script file references with the file names
			// by this time, the inputs should all be staged into the work folder
			// so we can simply reference them by name
			Map<String, String> inputMap = job.getInputsAsMap();
			for (String key : inputMap.keySet())
			{
				URI uri = new URI(inputMap.get(key));
				SoftwareInput swInput = software.getInput(key);
				if (swInput.isShowArgument()) {
					appTemplate = appTemplate.replaceAll("(?i)\\$\\{" + key + "\\}", 
							swInput.getArgument() + FilenameUtils.getName(uri.getPath()));
				} else {
					appTemplate = appTemplate.replaceAll("(?i)\\$\\{" + key + "\\}", 
							FilenameUtils.getName(uri.getPath()));
				}		
			}
			
			// add hidden (static) inputs
			for (SoftwareInput swInput : software.getInputs())
			{
				if (!swInput.isVisible())
				{
					URI uri = new URI(swInput.getDefaultValue());
					if (swInput.isShowArgument()) {
						appTemplate = appTemplate.replaceAll("(?i)\\$\\{" + swInput.getKey() + "\\}", 
								swInput.getArgument() + FilenameUtils.getName(uri.getPath()));
					} else {
						appTemplate = appTemplate.replaceAll("(?i)\\$\\{" + swInput.getKey() + "\\}", 
								FilenameUtils.getName(uri.getPath()));
					}
				}
				else if (!inputMap.containsKey(swInput.getKey())) 
				{
					appTemplate = appTemplate.replaceAll("(?i)\\$\\{" + swInput.getKey() + "\\}", "");
				}
			}
			
			// strip out all references to banned commands such as icommands, etc
			if (executionSystem.isPubliclyAvailable()) {
				appTemplate = CommandStripper.strip(appTemplate); 
			}
			
			// add the success statement after the template by default. The user can add failure catches
			// in their scripts that will trump a later success status.
			appTemplate = appTemplate + "\n\n${IPLANT_CLEANING_UP}\n";
			
			// Replace all the iplant template tags
			appTemplate = resolveMacros(appTemplate);
			
			batchWriter.write(appTemplate);
		} 
        catch (IOException e) {
            e.printStackTrace();
            throw new JobException("FileUtil operation failed");
        } catch (JobException e) {
            throw new JobException("Json failure from job inputs or parameters", e);
        }
		catch (URISyntaxException e)
		{
			e.printStackTrace();
            throw new JobException("Failed to parse input URI");
		} finally {
            try {
                batchWriter.close();
            } catch (IOException e) {
                log.debug("failed to close batchWriter on Exception");
            }
        }
    }
	
	@Override
	protected String submitJobToQueue() throws JobException
	{
		String submissionResponse = null;
		try
		{
			submissionClient = executionSystem.getRemoteSubmissionClient(job.getInternalUsername());
			
			String cdCommand = "source ~/.bashrc; cd " + remoteExecutionDataClient.resolvePath(job.getWorkPath());
			
			String chmodCommand = "chmod +x " + batchScriptName;
			
			String submitCommand = executionSystem.getScheduler().getBatchSubmitCommand() + " "
					+ batchScriptName;
			
			submissionResponse = submissionClient.runCommand(
					cdCommand + "; " + chmodCommand + "; " + submitCommand);
					
			if (!ServiceUtils.isValid(submissionResponse.trim())) 
			{
				// retry once just in case it was a flickr
				submissionResponse = submissionClient.runCommand(
						cdCommand + "; " + chmodCommand + "; " + submitCommand);
				
				if (!ServiceUtils.isValid(submissionResponse.trim())) 
					throw new JobException("Failed to submit job. " + submissionResponse);
			}
			
			RemoteJobIdParser jobIdParser = 
					new RemoteJobIdParserFactory().getInstance(executionSystem.getScheduler());
			
			return jobIdParser.getJobId(submissionResponse);
		}
		catch (JobException e) {
			throw e;
		}
		catch (Exception e)
		{
			throw new JobException("Failed to submit job to batch queue", e);
		}
		finally
		{
			try { submissionClient.close(); } catch (Exception e){}
		}
	}
	
	public static void main(String[] args) 
	{
		try
		{
			//Job job = JobDao.getNextQucoeuedJob(JobStatusType.STAGED);
			Job job = JobDao.getById(6537);
			job.setStatus(JobStatusType.PENDING, "Job accepted and queued for submission.");
//			HPCLauncher launcher = new HPCLauncher(job);
//			launcher.launch();
			JobManager.submit(job);
			
//			String value = "120459.tg-login1.blacklight.psc.teragrid.org";
//			String pattern = "^[0-9]+(\\.[_A-Za-z0-9-\\.]+)*$";
//			System.out.println("Value " + value + (Pattern.matches(pattern, value)? " matches" : " does not match"));
			//File appDir = createArchiveLog(new File("/tmp/scratch/iplant/vaughn-99-1310759903000/ranger"));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}