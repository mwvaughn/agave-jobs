package org.iplantc.service.jobs.managers.parsers;

import org.apache.commons.lang3.StringUtils;
import org.iplantc.service.jobs.exceptions.RemoteJobIDParsingException;

public class ForkJobIdParser implements RemoteJobIdParser {

	@Override
	public String getJobId(String output) throws RemoteJobIDParsingException
	{
		String[] lines = StringUtils.split(output, "\n");
		for(String line: lines) {
			if (StringUtils.startsWith(line, "[")) continue;
			
			if (StringUtils.isNumeric(line.trim())) {
				return line.trim();
			} else {
				throw new RemoteJobIDParsingException("Failed to parse job id from output: " + output); 
			}
		}
		
		throw new RemoteJobIDParsingException("No response from server upon job launch");
	}

}
