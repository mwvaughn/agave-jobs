package org.iplantc.service.jobs.model.enumerations;

public enum JobStatusType
{
	PENDING("Job accepted and queued for submission."), 
	STAGING_INPUTS("Transferring job input data to execution system"), 
	CLEANING_UP("Job completed execution"), 
	ARCHIVING("Transferring job output to archive system"), 
	STAGING_JOB("Job inputs staged to execution system"), 
	FINISHED("Job complete"), 
	KILLED("Job execution killed at user request"), 
	FAILED("Job failed"), 
	STOPPED("Job execution intentionally stopped"), 
	RUNNING("Job started running"), 
	PAUSED("Job execution paused by user"), 
	QUEUED("Job successfully placed into queue"), 
	SUBMITTING("Preparing job for execution and staging binaries to execution system"), 
	STAGED("Job inputs staged to execution system"), 
	PROCESSING_INPUTS("Identifying input files for staging"), 
	ARCHIVING_FINISHED("Job archiving complete"), 
	ARCHIVING_FAILED("Job archiving failed"),
	HEARTBEAT("Job heartbeat received");
	
	private final String description;
	
	JobStatusType(String description) {
		this.description = description;
	}
	
	public String getDescription() {
		return description;
	}
	
	public static boolean isRunning(JobStatusType status)
	{

		return ( status.equals(PENDING) || status.equals(STAGING_INPUTS)
				|| status.equals(STAGING_JOB) || status.equals(RUNNING)
				|| status.equals(PAUSED) || status.equals(QUEUED)
				|| status.equals(SUBMITTING)
				|| status.equals(PROCESSING_INPUTS) || status.equals(STAGED) 
				|| status.equals(CLEANING_UP) );
	}

	public static boolean isSubmitting(JobStatusType status)
	{
		return ( status.equals(STAGING_JOB) || 
				status.equals(SUBMITTING) || 
				status.equals(STAGED) );
	}
	
	public static boolean hasQueued(JobStatusType status)
	{
		return !( status.equals(PENDING) 
				|| status.equals(STAGING_INPUTS)
				|| status.equals(STAGING_JOB)  
				|| status.equals(SUBMITTING)  
				|| status.equals(STAGED) 
				|| status.equals(STAGING_JOB));
	}

	public static boolean isFinished(JobStatusType status)
	{
		return ( status.equals(FINISHED) || status.equals(KILLED)
				|| status.equals(FAILED) || status.equals(STOPPED));
	}
	
	public static boolean isArchived(JobStatusType status) 
	{
		 return (status.equals(ARCHIVING_FAILED) 
				 || status.equals(ARCHIVING_FINISHED));
	}
		
	public static boolean isFailed(JobStatusType status) 
	{
		 return (status.equals(ARCHIVING_FAILED) 
				|| status.equals(FAILED)
		 		|| status.equals(KILLED));
	}

	public static JobStatusType[] getActiveStatuses()
	{
		return new JobStatusType[]{ CLEANING_UP, ARCHIVING, RUNNING, PAUSED, QUEUED };
	}
	
	public static String getActiveStatusValues()
	{
		return String.format("'%s','%s','%s','%s','%s'",
				CLEANING_UP.name(), ARCHIVING.name(), RUNNING.name(), PAUSED.name(), QUEUED.name());
	}
	
	@Override
	public String toString() {
		return name();
	}
	

}