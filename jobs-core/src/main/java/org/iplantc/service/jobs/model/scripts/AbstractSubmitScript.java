package org.iplantc.service.jobs.model.scripts;

import org.iplantc.service.apps.dao.SoftwareDao;
import org.iplantc.service.apps.model.enumerations.ParallelismType;
import org.iplantc.service.jobs.model.Job;
import org.iplantc.service.jobs.util.Slug;
import org.iplantc.service.systems.dao.SystemDao;
import org.iplantc.service.systems.model.BatchQueue;
import org.iplantc.service.systems.model.ExecutionSystem;

public abstract class AbstractSubmitScript implements SubmitScript {

	protected String			name;
	protected boolean			inCurrentWorkingDirectory	= true;
	protected boolean			verbose						= true;
	protected String			standardOutputFile;
	protected String			standardErrorFile;
	protected String			time						= "01:00:00";
	protected ParallelismType	parallelismType				= ParallelismType.SERIAL;
	protected BatchQueue		queue;
	protected long				nodes						= 1;
	protected long				processors					= 1;
	protected double			memoryPerNode				= 1.0;
	protected String			batchInstructions;
	protected ExecutionSystem	system;
	protected Job				job;

	public AbstractSubmitScript(Job job)
	{
		this.job = job;
		setName(job.getName());
		this.standardOutputFile = name + "-" + job.getUuid() + ".out";
		this.standardErrorFile = name + "-" + job.getUuid() + ".err";
		
		this.nodes = job.getNodeCount();
		this.memoryPerNode = job.getMemoryPerNode();
		this.processors = job.getProcessorsPerNode();
		this.time = job.getMaxRunTime();
		setParallelismType(SoftwareDao.getSoftwareByUniqueName(
				job.getSoftwareName()).getParallelism());
		this.system = (ExecutionSystem)new SystemDao().findBySystemId(job.getSystem());
		this.queue = system.getQueue(job.getBatchQueue());
	}
	
	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name)
	{
		name = Slug.toSlug(name);
		
		if (Character.isDigit(name.charAt(0))) {
			name = "ipc-" + name;
		}
		
		this.name = name;
	}
	
	static void main (String[] args) {
		
	}

	/**
	 * @return the inCurrentWorkingDirectory
	 */
	public boolean isInCurrentWorkingDirectory()
	{
		return inCurrentWorkingDirectory;
	}

	/**
	 * @param inCurrentWorkingDirectory
	 *            the inCurrentWorkingDirectory to set
	 */
	public void setInCurrentWorkingDirectory(boolean inCurrentWorkingDirectory)
	{
		this.inCurrentWorkingDirectory = inCurrentWorkingDirectory;
	}

	/**
	 * @return the verbose
	 */
	public boolean isVerbose()
	{
		return verbose;
	}

	/**
	 * @param verbose
	 *            the verbose to set
	 */
	public void setVerbose(boolean verbose)
	{
		this.verbose = verbose;
	}

	/**
	 * @return the standardOutputFile
	 */
	public String getStandardOutputFile()
	{
		return standardOutputFile;
	}

	/**
	 * @param standardOutputFile
	 *            the standardOutputFile to set
	 */
	public void setStandardOutputFile(String standardOutputFile)
	{
		this.standardOutputFile = standardOutputFile;
	}

	/**
	 * @return the standardErrorFile
	 */
	public String getStandardErrorFile()
	{
		return standardErrorFile;
	}

	/**
	 * @param standardErrorFile
	 *            the standardErrorFile to set
	 */
	public void setStandardErrorFile(String standardErrorFile)
	{
		this.standardErrorFile = standardErrorFile;
	}

	/**
	 * @return the time
	 */
	public String getTime()
	{
		return time;
	}

	/**
	 * @param time
	 *            the time to set
	 */
	public void setTime(String time)
	{
		this.time = time;
	}

	/**
	 * @return the parallel
	 */
	public ParallelismType getParallelismType()
	{
		return parallelismType;
	}

	/**
	 * @param parallel
	 *            the parallel to set
	 */
	public void setParallelismType(ParallelismType parallelismType)
	{
		this.parallelismType = parallelismType;
		if (parallelismType.equals(ParallelismType.SERIAL))
		{
			this.nodes = 1;
		}
	}

	/**
	 * @return the processors
	 */
	public long getProcessors()
	{
		return processors;
	}

	/**
	 * @param processors
	 *            the processors to set
	 */
	public void setProcessors(long processors)
	{
		this.processors = processors;
	}

	/**
	 * @param batchInstructions
	 *            the batchInstructions to set
	 */
	public void setBatchInstructions(String batchInstructions)
	{
		this.batchInstructions = batchInstructions;
	}

	/**
	 * @return the batchInstructions
	 */
	public String getBatchInstructions()
	{
		return batchInstructions;
	}

}