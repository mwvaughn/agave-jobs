package org.iplantc.service.jobs.managers.launchers;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.iplantc.service.apps.Settings;
import org.iplantc.service.apps.exceptions.SoftwareException;
import org.iplantc.service.apps.model.SoftwareInput;
import org.iplantc.service.apps.model.SoftwareParameter;
import org.iplantc.service.common.persistence.HibernateUtil;
import org.iplantc.service.jobs.dao.JobDao;
import org.iplantc.service.jobs.exceptions.JobException;
import org.iplantc.service.jobs.exceptions.SystemUnavailableException;
import org.iplantc.service.jobs.managers.parsers.CondorJobIdParser;
import org.iplantc.service.jobs.model.Job;
import org.iplantc.service.jobs.model.enumerations.JobStatusType;
import org.iplantc.service.jobs.model.scripts.CommandStripper;
import org.iplantc.service.jobs.model.scripts.CondorSubmitScript;
import org.iplantc.service.remote.RemoteSubmissionClient;
import org.iplantc.service.remote.local.CmdLineProcessHandler;
import org.iplantc.service.remote.local.CmdLineProcessOutput;
import org.iplantc.service.systems.dao.SystemDao;
import org.iplantc.service.systems.model.ExecutionSystem;
import org.iplantc.service.systems.model.enumerations.SystemStatusType;
import org.json.JSONException;

/**
 * Created with IntelliJ IDEA.
 * User: steve
 * Date: 11/4/12
 * Time: 7:35 PM
 * To change this template use File | Settings | File Templates.
 */
@SuppressWarnings("unused")
public class CondorLauncher  extends AbstractJobLauncher {
    
	private static final Logger log = Logger.getLogger(CondorLauncher.class);
    
    private CondorSubmitScript submitFileObject;
    private List<Map<String, String>> finalArgs = new ArrayList<Map<String, String>>();

    private File condorSubmitFile;
    private String timeMark;
    private String tag;
    
	private boolean jobFailed = false;

    /**
     * Creates an instance of a JobLauncher capable of submitting jobs to batch
     * queuing systems on Condor resources.
     */
    public CondorLauncher(Job job) {
        super(job);
        this.submitFileObject = new CondorSubmitScript(job);
    }

    /**
     * @throws JSONException
     */
    private void getFinalArguments() throws JobException {
        step = "Getting final arguments from Job submission";
        log.debug(step);
        // software has inputs and job has inputs?? how do we mash these together?
        Set<SoftwareInput> definedInputs = software.getInputs();
        
        Map<String, String> userInputs = job.getInputsAsMap();
        Map<String, String> inputFiles = new HashMap<String, String>();

        // put software definition inputs in inputFiles
        // TODO: shouldn't we check for hidden values here? seems like there could
        // be duplicates
        Iterator<SoftwareInput> defined = definedInputs.iterator();
        while (defined.hasNext()) {
            SoftwareInput input = defined.next();
            inputFiles.put(input.getKey(), input.getDefaultValue());
        }

        // there are inputs specified by user in the job submission that can overwrite values found in the software
        // definition. For input files it may replace the name of the file(s) to be used.
        for (String key : userInputs.keySet()) 
        {
            String remoteFile = userInputs.get(key);
            if (!StringUtils.isEmpty(remoteFile)) 
            {
            	try {
            		URI uri = new URI(remoteFile);
            		String remoteFileName = FilenameUtils.getName(uri.getPath());
                	if (!StringUtils.isEmpty(remoteFileName))
                		inputFiles.put(key, remoteFileName);
            	}
            	catch (URISyntaxException e)
        		{
        			throw new JobException("Invalid input specified. Failed to obtain " +
        					"filename for " + remoteFile);
        		}
            }
        }
    }

    /**
     * This method returns a command string built from final args
     *
     * @return String for the command line
     */
    private String getCommandString() {
        StringBuilder cliArgs = new StringBuilder();
        for (Map<String, String> input : finalArgs) {
            Iterator<Map.Entry<String, String>> entries = input.entrySet().iterator();
            while (entries.hasNext()) {
                Map.Entry<String, String> thisEntry = entries.next();
                String key = thisEntry.getKey();
                String value = thisEntry.getValue();
                cliArgs.append(" " + key + " " + value + " ");
            }
        }
        return cliArgs.toString();
    }

    /**
     * This method gets the current working directory
     *
     * @return String of directory path
     */
    private String getWorkDirectory() {
        String wd = "";
        try {
            wd = new File("test.txt").getCanonicalPath().replace("/test.txt", "");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return wd;
    }

    /**
     * makes and sets new time marker for creation of job run directories
     */
    private void setTimeMarker() {
        Date date = job.getCreated();
        Long time = date.getTime();
        timeMark = time.toString();
        tag = job.getName() + "-" + timeMark;
    }

    /**
     * generates a condor submit file for execution
     *
     * @param time string
     * @return a string representing the condor_submit file name
     */
    private String createSubmitFileName(String time) {
        return "condorSubmit-" + job.getName() + "-" + time;
    }

    

//    /**
//     * This method will read in all the inputs from the job request and stage them to the
//     * local directory. If the input is a path, then the default storage system will be 
//     * used. If the input is an URI, then the appropriate client is instantiated using the 
//     * parameters in the URI. We need to find a better way to accept inputs so the user
//     * can leverage their registered systems which may have more complicated auth mechanisms.
//     * We could use the host of the URI as the systemId, but there could be legitimate 
//     * namespace conflicts if there was a server with the same hostname as the system id.
//     * 
//     * @param software
//     * @throws JobException
//     * @deprecated data should already be staged there by the StagingWatch quartz job by the time this runs
//     */
//    private void copyInputDataToTempAppDir(Software software) throws JobException {
//        step = "Staging job input files to local job execution directory";
//        log.debug(step);
//        // this assumes that the input file(s) path is relative to the home location in irods
//        RemoteDataClientFactory factory = new RemoteDataClientFactory();
//        RemoteDataClient remoteDataClient = null;
//        try 
//        {
//	        if (!StringUtils.isEmpty(job.getInputs()))
//	        {
//	        	JSONObject jsonInputs = new JSONObject(job.getInputs());
//		        for( SoftwareInput input: software.getInputs())
//		        {
//		        	// we can probably optimize this or break it into a generic data staging task
//		        	// ...or a separate queue that the TransferService can handle. Let's talk...
//		        	// after the demo.
//		        	if (jsonInputs.has(input.getKey()) && !jsonInputs.isNull(input.getKey()))
//		        	{
//                        String jobInput = jsonInputs.getString(input.getKey());
//                        URI jobInputUri = new URI(jobInput);
//                        remoteDataClient = factory.getInstance(job.getOwner(), job.getInternalUsername(), jobInputUri);
//                        remoteDataClient.authenticate();
//                        remoteDataClient.get(jobInputUri.getPath(), tempAppDirPath);
//                        remoteDataClient.disconnect();
//                    } 
//		        	else if(input.isRequired()) 
//                    {
//                        throw new JobException("Input : "+input.getKey()+" is required.");
//                    } 
//                    else if (!input.isVisible()){
//                        // use the default value
//                        URI jobInputUri = new URI(input.getDefaultValue());
//                        remoteDataClient = factory.getInstance(job.getOwner(), job.getInternalUsername(), jobInputUri);
//                        remoteDataClient.authenticate();
//                        remoteDataClient.get(jobInputUri.getPath(), tempAppDirPath);
//                        remoteDataClient.disconnect();
//                    }
//		        }
//	        }
//        } catch(JobException e){
//            throw e;
//        } catch (Exception e) {
//        	throw new JobException("Failed to stage in job input data.", e);
//        } finally {
//           try { remoteDataClient.disconnect(); } catch (Exception e) {}
//        }
//    }

    /**
     * Creates the condor submit file to be handed to condor_submit
     *
     * @param timeMark current time
     */
    private void createCondorSubmitFile(String timeMark) {
        step = "Creating the Condor submit file for job";
        log.debug(step);

        // todo need to add classAd info to submit file
        submitFileObject = new CondorSubmitScript(job);
//        submitFileObject.setExecutable();
        // all the arguments to the executable have been set in the application wrapper and transfer will simply
        // call the application wrapper after the transfer package has been extracted.
        // submitFileObject.setArguments(this.getCommandString());
        /*  using defaults is fine
        submitFileObject.setError("err."+"condor." + tag );
        submitFileObject.setOutput("out."+"condor." + tag);
        submitFileObject.setLog("log."+"condor." + tag);
        */

        // this should be sufficient information in order to create a Condor submission script
        String condorSubmitFileContents = submitFileObject.getScriptText();
        String submitFileName = "condorSubmit";   //createSubmitFileName(timeMark);
        condorSubmitFile = new File(tempAppDir, submitFileName);

        try {
            FileUtils.writeStringToFile(condorSubmitFile, condorSubmitFileContents);
            
        } catch (IOException e) {
            log.error("Failed to create condor submit file.", e);
        }
    }

    /**
     * Method to help with multiple calls to outside processes for a variety of tasks
     *
     * @param command String of command line parameters
     * @param cmdName Name of the command being executed
     * @return an Object that includes int exit code, String out and String err from the execution
     * @throws JobException if the exitCode is not equal to 0
     */
    private CmdLineProcessOutput executeLocalCommand(String command, String cmdName) throws JobException {
        CmdLineProcessHandler cmdLineProcessHandler = new CmdLineProcessHandler();
        int exitCode = cmdLineProcessHandler.executeCommand(command);

        if (exitCode != 0) {
            throw new JobException("Job exited with error code " + exitCode + " please check your arguments and try again.");
        }
        return cmdLineProcessHandler.getProcessOutput();
    }

//    /**
//     * Method to help with multiple Condor calls to outside processes for a variety of tasks
//     *
//     * @param command String of command line parameters
//     * @param cmdName Name of the command being executed
//     * @return an Object that includes int exit code, String out and String err from the execution
//     * @throws JobException if the exitCode is not equal to 0
//     */
//    private CmdLineProcessOutput executeCondorCommand(String command, String cmdName) throws JobException {
//        step = "Calling condor_submit with condorSubmit file";
//        log.debug(step);
//        CondorProcessHandler condorProcessHandler = new CondorProcessHandler();
//        int exitCode = condorProcessHandler.executeCondorCommand(command);
//
//        if (exitCode != 0) {
//            throw new JobException("Job exited with error code " + exitCode + " please check your arguments and try again.");
//        }
//        return condorProcessHandler.getProcessOutput();
//    }

    /**
     * Takes a String representing the directory path and extracts the last directory name
     *
     * @param path String directory path
     * @return String of last directory name in path
     * @throws Exception if path is null or empty
     */
    private String getDirectoryName(String path) throws Exception {
        String name = null;

        // is it null or empty throw Exception
        if (path == null || path.isEmpty()) {
            throw new Exception("path can't be null or empty");
        }
        path = path.trim();
        String separator = File.separator;

        StringTokenizer st = new StringTokenizer(path, separator);
        while (st.hasMoreElements()) {
            name = (String) st.nextElement();
        }
        return name;
    }

    /**
	 * This method will write the generic transfer_wrapper.sh file that condor_submit
	 * uses as it's executable. The file wraps the defined executable for the job along
	 * with the data all tar'd and zipped to transfer to OSG
	 */
	private void createTransferWrapper() throws JobException {
	    step = "Creating transfer wrapper for Condor submission";
	    log.debug(step);
	    StringBuilder transferScript = new StringBuilder();
	    FileWriter transferWriter = null;
	    String executablePath;
	
	    try {
	        transferWriter = new FileWriter(tempAppDir + File.separator + "transfer_wrapper.sh");
	
	        transferScript.append("#!/bin/bash\n\n");
	        transferScript.append("tar xzvf transfer.tar.gz\n");
	        transferScript.append("# we supply the executable and path from the software definition\n");
	        executablePath = software.getExecutablePath();
	        if (executablePath.startsWith("/")) {
	        	executablePath = executablePath.substring(1);
	        }
	        //transferScript.append("chmod u+x " + executablePath + " \n");
	        transferScript.append("./" + executablePath + "  # this in turn wraps the final executable along with inputs, parameters and output.\n");
	
	        // write the transfer_wrapper.sh file
	        transferWriter.write(transferScript.toString());
	        transferWriter.flush();
	    } catch (IOException e) {
	        e.printStackTrace();
	        throw new JobException("Failure to write transfer script for Condor submission to " + tempAppDir);
	    } finally {
	        try {
	            transferWriter.close();
	        } catch (IOException e) {
	            log.info("failed to close transferWriter outputStream");
	            e.printStackTrace();
	        }
	    }
	
	    // need to make sure that transfer_wrapper.sh is executable
	    String chmodCmdString = new String("cd " + tempAppDir + "; " + "chmod +x transfer_wrapper.sh; chmod +x " + executablePath);
	    executeLocalCommand(chmodCmdString, "chmod +x transfer_wrapper.sh; chmod +x " + executablePath);
	}

	/**
     * This method will compare the archived log of the job at submission to files added after job run
     * and copy the delta to irods archive location
     */
    private void compareArchiveLogToOutput() {
        /*
            appTemplate = appTemplate + "\n\n${IPLANT_ARCHIVING_START}\n";
            appTemplate = appTemplate + "\nimkdir -p " + Settings.IRODS_STAGING_DIRECTORY + job.getArchivePath() +
                    "\nfor i in `find . -maxdepth 1`; do \n" +
                    "    exists=`grep -x \"$i\" " + localArchive.getName() + "` \n" +
                    "    if [ ! -n \"$exists\" ]; \n" +
                    "    then \n" +
                    "        iput -v -f -r $i " + Settings.IRODS_STAGING_DIRECTORY + job.getArchivePath() + " \n" +
                    "    fi\n" +
                    "done\n";
            appTemplate = appTemplate + "\n\n${IPLANT_ARCHIVING_SUCCESS}\n";
        */
    }

    @Override
    protected void processApplicationTemplate() throws JobException 
    {
        step = "Process the application template ";
        log.debug(step);

        // need tempAppDir + software.getExecutablePath()
        // read in the template file
        // create the submit script in the temp folder
        File appTemplateFile = new File(tempAppDir + File.separator + software.getExecutablePath());
        // replace the executable script file references with the file names
        Map<String, String> inputMap = null;
        String appTemplate = null;
        StringBuilder batchScript;
        FileWriter batchWriter = null;

        // throw exception if file not found
        try {
            if (!appTemplateFile.exists()) {
                throw new JobException("Unable to locate wrapper script for \"" +
                        software.getUniqueName() + "\" at " +
                        software.getDeploymentPath() + "/" + software.getExecutablePath());
            }
            appTemplate = FileUtils.readFileToString(appTemplateFile);

            batchScript = new StringBuilder();
            String callbackStart = "\ncurl -k \"" + Settings.IPLANT_JOB_SERVICE
                    + "trigger/job/" + job.getUuid() + "/token/"
                    + job.getUpdateToken() + "/status/" + JobStatusType.RUNNING
                    + "\"\n";
            
            batchScript.append(callbackStart);

            // add in any custom environment variables that need to be set
            if (!StringUtils.isEmpty(software.getExecutionSystem().getEnvironment())) {
            	batchScript.append(software.getExecutionSystem().getEnvironment());
        	}
            
            // replace the parameters with their passed in values
            Map<String, Object> parameterMap = null;
            parameterMap = job.getParametersAsMap();
            for (String key : parameterMap.keySet()) 
            {
            	SoftwareParameter param = software.getParameter(key);
				if (param.isShowArgument()) {
					appTemplate = appTemplate.replaceAll("(?i)\\$\\{" + key + "\\}", 
							param.getArgument() + parameterMap.get(key).toString());
				} else {
					appTemplate = appTemplate.replaceAll("(?i)\\$\\{" + key + "\\}", 
							parameterMap.get(key).toString());
				}
            }

            // add hidden (static) parameters
            for (SoftwareParameter param : software.getParameters()) 
            {
                if (!param.isVisible()) 
                {
                	if (param.isShowArgument()) {
						appTemplate = appTemplate.replaceAll("(?i)\\$\\{" + param.getKey() + "\\}", 
								param.getArgument() + param.getDefaultValue().toString());
					} else {
						appTemplate = appTemplate.replaceAll("(?i)\\$\\{" + param.getKey() + "\\}", 
								param.getDefaultValue().toString());
					}
                } 
                else if (!parameterMap.containsKey(param.getKey()))
                {
					appTemplate = appTemplate.replaceAll("(?i)\\$\\{" + param.getKey() + "\\}", "");
				}
            }
            
            inputMap = job.getInputsAsMap();
            for (String key : inputMap.keySet()) 
            {
            	// inputs have been staged already, so resolve to their file names
            	URI uri = new URI(inputMap.get(key));
				SoftwareInput swInput = software.getInput(key);
				if (swInput.isShowArgument()) {
					appTemplate = appTemplate.replaceAll("(?i)\\$\\{" + key + "\\}", 
							swInput.getArgument() + FilenameUtils.getName(uri.getPath()));
				} else {
					appTemplate = appTemplate.replaceAll("(?i)\\$\\{" + key + "\\}", 
							FilenameUtils.getName(uri.getPath()));
				}
            }

            // add hidden (static) inputs
            for (SoftwareInput swInput : software.getInputs()) 
            {
                if (!swInput.isVisible()) 
                {
                	// inputs have been staged already, so resolve to their file names
                	URI uri = new URI(swInput.getDefaultValue());
                	if (swInput.isShowArgument()) {
						appTemplate = appTemplate.replaceAll("(?i)\\$\\{" + swInput.getKey() + "\\}", 
								swInput.getArgument() + FilenameUtils.getName(uri.getPath()));
					} else {
						appTemplate = appTemplate.replaceAll("(?i)\\$\\{" + swInput.getKey() + "\\}", 
								FilenameUtils.getName(uri.getPath()));
					}
                }
                else if (!inputMap.containsKey(swInput.getKey())) 
				{
					appTemplate = appTemplate.replaceAll("(?i)\\$\\{" + swInput.getKey() + "\\}", "");
				}
            }
            
            // strip out all references to icommands and it irods shadow files
            if (executionSystem.isPubliclyAvailable()) {
				appTemplate = CommandStripper.strip(appTemplate);
            }
            
            // Replace all the iplant template tags
            appTemplate = resolveMacros(appTemplate);

            // append the template with
            batchScript.append(appTemplate);
            batchWriter = new FileWriter(appTemplateFile);
            // write new contents to appTemplate for execution
            if (batchWriter != null) {
                batchWriter.write(batchScript.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new JobException("FileUtil operation failed");
        } catch (JobException e) {
            throw new JobException("Json failure from job inputs or parameters", e);
        }
		catch (URISyntaxException e)
		{
			e.printStackTrace();
            throw new JobException("Failed to parse input URI");
		} finally {
            try {
                batchWriter.close();
            } catch (IOException e) {
                log.debug("failed to close batchWriter on Exception");
            }
        }

    }
    
    private void createRemoteTransferPackage() throws Exception {
        step = "Creating the transfer package ";
        log.debug(step);
        
        String createTransferPackageCmd = new String("cd " + job.getWorkPath() + "; "
                + "tar czvf ./transfer.tar.gz .");
        ExecutionSystem system = (ExecutionSystem) new SystemDao().findBySystemId(job.getSystem());
        
    	RemoteSubmissionClient remoteSubmissionClient = system.getRemoteSubmissionClient(job.getInternalUsername());
    	String response = remoteSubmissionClient.runCommand(createTransferPackageCmd);
    	if (response.contains("Cannot")) {
    		throw new JobException("Failed to create transfer package for condor submission. \n" + response);
    	}
    	remoteSubmissionClient.close();

    }

    /**
     * Update the Job object status in the database
     *
     * @param status
     */
    private void updateJobStatus(JobStatusType status, String description) throws JobException {
        step = "Updating job status in data store";
        log.debug(step);
        Date date = new Date();
        job.setLastUpdated(date);
        job.setStatus(status, description);

        JobDao.persist(job);
    }

//    /**
//     * Is Condor ready to take job submissions?
//     */
//    private boolean isCondorReady() throws JobException {
//        // todo make this tacc condor server specific see if condor_q is up
//        step = "Call to condor_q to see if Condor is active";
//        log.debug(step);
//        int exitCode = executeCommand("condor_q", "condor_q").getExitCode();
//        boolean isCondorUp = exitCode == 0 ? true : false;
//
//        if (!isCondorUp) {
//            // if the system is down, return it to the queue to wait for the system
//            // to come back up by setting job status to "Staged"
//            updateJobStatus(JobStatusType.STAGED, "Condor is not currently available. Returning job to queue.");
//        }
//        return isCondorUp;
//    }

    /**
     * We got here because some step in the arduous process of staging and submitting a
     * Condor job failed.
     *
     * @return boolean false to be used to exit launch
     */
    private boolean registerJobFailure(String message) {
        step = "Register Job failure";
        log.debug(step);
        try {
            updateJobStatus(JobStatusType.FAILED, message);
        } catch (JobException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Currently for the OSG condor submit host, IRods files pushed to the execution system lose their executable setting so they
     * need to be reset.
     *
     * @throws JobException
     */
    private void addExecutionPermissionsToWrapper(){
        step = "Changing execute permissions on transfer_wrapper and executable ";
        log.debug(step);

        String changePermissionsCmd = new String("cd " + job.getWorkPath() + "; "
                + "chmod +x *.sh");
        ExecutionSystem system = (ExecutionSystem) new SystemDao().findBySystemId(job.getSystem());

        RemoteSubmissionClient remoteSubmissionClient = null;
        try {
            remoteSubmissionClient = system.getRemoteSubmissionClient(job.getInternalUsername());
            String response = remoteSubmissionClient.runCommand(changePermissionsCmd);
            if (response.contains("Cannot")) {
                throw new JobException("Failed to create transfer package for condor submission. \n" + response);
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }finally{
            remoteSubmissionClient.close();

        }
    }

    @Override
    public void launch() throws JobException 
    {
        String condorSubmitCmdString = null;
        try 
        {
        	if (software == null || !software.isAvailable()) {
				throw new SoftwareException("Application is not longer available for execution");
			}
        	
        	// if the system is down, return it to the queue to wait for the system
			// to come back up.
			if (executionSystem == null || !executionSystem.getStatus().equals(SystemStatusType.UP)) 
			{
				throw new SystemUnavailableException();
			} 
        				
            // this is used to set the tempAppDir identifier so that we can come back
            // later to find the directory when the job is done (Failed for cleanup or Successful for archiving)
            setTimeMarker();

            //  get software executable and the final argument list for the software to be run from the job and software
            //  combination for the construction of the condor submit file.
            getFinalArguments(); // sets this.finalArgs to pass to executable

            // sets up the application directory to execute this job launch; see comments in method
            createTempAppDir();

            // copy our application package from the software.deploymentPath to our tempAppDir
            copySoftwareToTempAppDir();

            // create the generic transfer wrapper that condor_submit will use to un-tar the execution
            // package after the files have been transferred to OSG
            createTransferWrapper();

            // prepend the application template with call back to let the Job service know the job has started
            // parse the application template and replace tokens with the inputs, parameters and outputs.
            processApplicationTemplate();

            // create the shadow file containing the exclusion files for archiving
            createArchiveLog(ARCHIVE_FILENAME);
			
            // create the Condor submit file should include the path to executable, the transfer.tar.gz and input(s)
            // default classAds for Linux
            createCondorSubmitFile(timeMark);

            // move the local temp directory to the remote system
            stageSofwareApplication();

            // change permissions to executable for wrapper.sh and transfer_wrapper.sh
            addExecutionPermissionsToWrapper();
            
            // tar up the entire executable with input data on the remote system
            createRemoteTransferPackage();
            
            // run condor_submit
            String condorJobId = submitJobToQueue();
            
            job.setSubmitTime(new Date()); // Date job submitted to Condor
            job.setLastUpdated(new Date());  // Date job started by Condor
            job.setLocalJobId(condorJobId);
            job.setStatus(JobStatusType.QUEUED, "Condor job successfully placed into queue");
            JobDao.persist(job);
            
            // job updated everything successful so far it's now in Condor cluster hands.
            // should have a job status marked running in Condor submitTime, startTime and localJobId should be changed
        } catch (JobException e) {
        	jobFailed = true;
        	log.error(step);
        	updateJobStatus(JobStatusType.FAILED, e.getMessage());
        	throw e;
        } catch (Exception e) {
            jobFailed = true;
            log.error(step);
            updateJobStatus(JobStatusType.FAILED, e.getMessage());
            throw new JobException("Failed to invoke app: \"" + software.getUniqueName() + "\n\"   with command:  " + condorSubmitCmdString + "\n" + e.getMessage(), e);
        } finally {
        	FileUtils.deleteQuietly(tempAppDir);
        	
//            if(jobFailed){
//                updateJobStatus(JobStatusType.FAILED);
//                // cleanup(); // do we need to cleanup and under what conditions
//            }
        }
    }

    @Override
    protected String submitJobToQueue() throws JobException
    {
    	try {
	    	// construct the command line for call to shell process
	        //String[] condorSubmitCmdString = new String[]{"/bin/bash", "-cl", "cd " + tempAppDir.getAbsolutePath() + "; condor_submit " + condorSubmitFile.getName()};
    		String condorSubmitCmdString = "cd " + remoteExecutionDataClient.resolvePath(job.getWorkPath()) + "; " +
    				"condor_submit " + condorSubmitFile.getName();
	        log.debug("Submitting condor submit file to condor_submit: \"" + software.getName() + "\" with command:\n " + condorSubmitCmdString);
	        
	        ExecutionSystem system = (ExecutionSystem) new SystemDao().findBySystemId(job.getSystem());
	        submissionClient = system.getRemoteSubmissionClient(job.getInternalUsername());
	    	
	        String response = submissionClient.runCommand(condorSubmitCmdString);
	    	
	        CondorJobIdParser jobIdParser = new CondorJobIdParser();
	        
	        return jobIdParser.getJobId(response);
    	} 
    	catch (JobException e) {
    		throw e;
    	} 
    	catch (Exception e) {
    		throw new JobException("Failed to submit job to condor queue", e);
    	}
    	finally {
    		submissionClient.close();
    	}
    }
    /**
     * Cleanup all the loose ends if the job failed
     */
    private void cleanup() {
        try {
            HibernateUtil.closeSession();
        } catch (Exception e) {}
        try {
            FileUtils.deleteDirectory(tempAppDir);
        } catch (Exception e) {}
        try {
            remoteExecutionDataClient.disconnect();
        } catch (Exception e) {}
    }

    public static void main(String[] args) throws IOException 
    {
        CondorLauncher launcher = null;

        try {
            // load the service configuration settings and start the queues

            Job job = JobDao.getById(9);
            // job.setStatus(JobStatusType.PENDING);
            // job.setCreated(new Date());            // this should give us a new directory for storing output
            launcher = new CondorLauncher(job);
            //JobManager.submit(job);
            launcher.launch();
        } 
        catch (Exception e) {
            e.printStackTrace();
        }
    }

}



