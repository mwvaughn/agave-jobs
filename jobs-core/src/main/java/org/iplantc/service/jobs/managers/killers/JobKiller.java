package org.iplantc.service.jobs.managers.killers;

import java.io.IOException;

import org.iplantc.service.jobs.exceptions.JobException;

public interface JobKiller {

	public void attack() throws JobException, IOException;

}
