package org.iplantc.service.jobs.queue;

import java.net.MalformedURLException;
import java.net.URI;
import java.util.Date;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.StaleObjectStateException;
import org.iplantc.service.apps.dao.SoftwareDao;
import org.iplantc.service.apps.model.Software;
import org.iplantc.service.apps.util.ServiceUtils;
import org.iplantc.service.common.exceptions.PermissionException;
import org.iplantc.service.common.persistence.TenancyHelper;
import org.iplantc.service.io.dao.LogicalFileDao;
import org.iplantc.service.io.model.LogicalFile;
import org.iplantc.service.io.permissions.PermissionManager;
import org.iplantc.service.io.util.ApiUriUtil;
import org.iplantc.service.jobs.Settings;
import org.iplantc.service.jobs.dao.JobDao;
import org.iplantc.service.jobs.exceptions.JobException;
import org.iplantc.service.jobs.exceptions.QuotaViolationException;
import org.iplantc.service.jobs.exceptions.SystemUnavailableException;
import org.iplantc.service.jobs.managers.JobManager;
import org.iplantc.service.jobs.managers.JobQuotaCheck;
import org.iplantc.service.jobs.model.Job;
import org.iplantc.service.jobs.model.JobEvent;
import org.iplantc.service.jobs.model.enumerations.JobStatusType;
import org.iplantc.service.jobs.util.Slug;
import org.iplantc.service.systems.dao.SystemDao;
import org.iplantc.service.systems.model.ExecutionSystem;
import org.iplantc.service.systems.model.RemoteSystem;
import org.iplantc.service.systems.model.enumerations.StorageProtocolType;
import org.iplantc.service.systems.model.enumerations.SystemStatusType;
import org.iplantc.service.transfer.RemoteDataClient;
import org.iplantc.service.transfer.RemoteDataClientFactory;
import org.iplantc.service.transfer.URLCopy;
import org.iplantc.service.transfer.exceptions.RemoteDataException;
import org.iplantc.service.transfer.model.TransferTask;
import org.json.JSONException;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * Class to pull a job from the db queue and attempt to stage in any input
 * files specified as urls using the iPlant IO service.
 * 
 * @author dooley
 * 
 */
@DisallowConcurrentExecution
public class StagingWatch implements org.quartz.Job 
{
	private static final Logger	log	= Logger.getLogger(StagingWatch.class);
//	private static final Counter activeTasks = CommonMetrics.addCounter(StagingWatch.class, "active");
	
	public StagingWatch(){}

	public void execute(JobExecutionContext context)
			throws JobExecutionException
	{
//		activeTasks.inc();
		
		Job job = null;
		try
		{
			// pull the oldest job with JobStatusType.PENDING from the db
			job = JobDao.getNextQueuedJob(JobStatusType.PENDING);
            
			if (job != null) 
            {
				// this is a new thread and thus has no tenant info loaded. we set it up
				// here so things like app and system lookups will stay local to the 
				// tenant
				TenancyHelper.setCurrentTenantId(job.getTenantId());
				TenancyHelper.setCurrentEndUser(job.getOwner());
				
				Software software = SoftwareDao.getSoftwareByUniqueName(job.getSoftwareName());
                
            	// if the execution system for this job has a local storage config,
            	// all other transfer workers will pass on it.
                if (!StringUtils.equals(Settings.LOCAL_SYSTEM_ID, job.getSystem()) &&
                		software.getExecutionSystem().getStorageConfig().getProtocol().equals(StorageProtocolType.LOCAL)) 
                {
                    return;
                }
                else
                {
            		if (job != null)
					{
            			int attempts = 0;
            			boolean staged = false;
            			Map<String, String> jobInputMap = JobManager.getJobInputMap(job);
            			
            			if (jobInputMap.isEmpty()) 
        				{
        					job.setStatus(JobStatusType.STAGED, "No inputs for the given job. Skipping staging");
        					JobDao.persist(job);
        				} 
        				else 
        				{
	            			// attempt to stage the job several times
	            			while (!staged && attempts <= Settings.MAX_SUBMISSION_RETRIES)
	            			{
	            				attempts++;
	            				
	            				job.setRetries(attempts-1);
	            				
	            				log.debug("Attempt " + attempts + " to stage job " + job.getUuid() + " inputs");
	            				
            					// mark the job as submitting so no other process claims it
								job.setStatus(JobStatusType.PROCESSING_INPUTS, "Attempt " + attempts + 
										" to stage job inputs");
								JobDao.persist(job);
							
								try 
								{
									stageJobData(job, jobInputMap);
									
									job.setRetries(0);
									JobDao.persist(job);
									staged = true;
								}
								catch (QuotaViolationException e) 
								{
									try
									{
										JobManager.updateStatus(job, JobStatusType.PENDING, 
											e.getMessage() + 
											" This job inputs will be staged when one or more current jobs complete. " + 
											"For more information on job quotas, please consult https://agaveapi.co/tutorials.");
										log.error("Job[" + job.getUuid() + "] - " + job.getErrorMessage());
										break;
									}
									catch (Exception e1)
									{
										log.error("Failed to update job " + job.getUuid()
												+ " status to failed");
										break;
									}	
								}
								catch (SystemUnavailableException e) {
									try
									{
										JobManager.updateStatus(job, JobStatusType.PENDING, 
											"The target execution system for this application, \"" + job.getSystem() + ",\" is currently unavailable. " +
												"Job inputs will be staged once the system becomes available. ");
										log.error("Job[" + job.getUuid() + "] - " + job.getErrorMessage());
										break;
									}
									catch (Exception e1)
									{
										log.error("Failed to update job " + job.getUuid()
												+ " status to failed");
										break;
									}	
								}
								catch (Exception e) {
									if (attempts > Settings.MAX_SUBMISSION_RETRIES ) {
										log.error("Failed to stage job " + job.getUuid() + " inputs on attempt " + attempts, e);
										throw e;
									} else {
										JobManager.updateStatus(job, JobStatusType.PENDING, "Attempt " 
												+ attempts + " failed to stage job inputs.\\n" + e.getMessage());
									}
								}
            				}
        				
            			}
					}
                }
            }
		}
		catch (StaleObjectStateException e) {
			log.debug("Just avoided a job input staging race condition from worker " 
					+ context.getTrigger().getDescription());
		}
		catch (Exception e)
		{
			if (job == null)
			{
				log.error("Failed to retrieve job information from db", e);
			}
			else
			{
				log.error("Failed to stage input data for job " + job.getUuid(), e);

				try
				{
					JobManager.updateStatus(job, JobStatusType.FAILED,
							"Failed to stage input data for job " + job.getUuid() + 
							" after " + job.getRetries() + " attempts.\\n"
							+ e.getMessage());
				}
				catch (Exception e1)
				{
					log.error("Failed to update job " + job.getUuid()
							+ " status to failed");
					try {
						job.setErrorMessage(
								"Failed to stage input data for job " + job.getUuid()
								+ " inputs after " + job.getRetries() 
								+ " attempts.\\n" + e.getMessage());
					}
					catch (JobException e2) {}
				}
				
//				if (ServiceUtils.isValid(job.getCallbackUrl()))
//				{
//					Postback.submitError(job, CallbackMacroResolver.resolve(job, job.getCallbackUrl()));
//				}

			}
		}
		finally {
//			activeTasks.dec();
		}
	}
		
	public void stageJobData(Job job, Map<String, String> inputs) throws JobException, JSONException, RemoteDataException, SystemUnavailableException, QuotaViolationException
	{
		ExecutionSystem system = (ExecutionSystem) new SystemDao().findBySystemId(job.getSystem());
		
		if (system == null || !system.isAvailable() || !system.getStatus().equals(SystemStatusType.UP))
		{
			throw new SystemUnavailableException("System " + system.getName() + " is not available for staging.");
		}
		
		// verify the user is within quota to run the job before staging the data.
		JobQuotaCheck quotaValidator = new JobQuotaCheck(job);
		quotaValidator.check();
		
		
		log.debug("Beginning staging inputs for job " + job.getUuid() + " " + job.getName());
		// we need a way to parallelize this task. Ideally we'd just throw each input
		// file to the staging queue and 
		
		JobManager.updateStatus(job, JobStatusType.PROCESSING_INPUTS);
		
		for (String key : inputs.keySet())
		{
			String inputValue = inputs.get(key);
			String resolvedInputs = "";
			String[] explodedInputs = inputValue.split(";");
			URI inputUri = null;
			RemoteSystem remoteStorageSystem = null;
			RemoteDataClient remoteStorageDataClient = null;
			ExecutionSystem remoteExecutionSystem = null;
			RemoteDataClient remoteExecutionDataClient = null;
			try
			{
				for (String singleInput: explodedInputs) 
				{	
					inputUri = new URI(singleInput);
					String remotePath = null;
					if (ApiUriUtil.isInternalURI(inputUri))
					{
						remoteStorageSystem = ApiUriUtil.getRemoteSystem(job.getOwner(), inputUri);
						
						if (remoteStorageSystem == null) {
							throw new JobException("No storage system found for user matching \"" + singleInput  + "\"");
						} else {
							remoteStorageDataClient = remoteStorageSystem.getRemoteDataClient(job.getInternalUsername());
							remoteStorageDataClient.authenticate();
						}
						
						remotePath = ApiUriUtil.getPath(inputUri);
						
						if (remoteStorageDataClient.doesExist(remotePath))
						{
							LogicalFile logicalFile = LogicalFileDao.findByUserSystemPath(
									remoteStorageSystem, job.getOwner(), remotePath);
			                
			                PermissionManager pm = new PermissionManager(
			                		remoteStorageSystem, remoteStorageDataClient, logicalFile, job.getOwner());
			                
							if (!pm.canRead(remotePath))
							{
								throw new PermissionException("User does not have permission to access " + 
										singleInput);
							}
						}
						else
						{
							throw new JobException("Could not find specified input " + singleInput);
						}
					}
					else
					{
						remoteStorageDataClient = new RemoteDataClientFactory().getInstance(
								job.getOwner(), job.getInternalUsername(), inputUri);
						remoteStorageDataClient.authenticate();
						
						remotePath = inputUri.getPath();
					}
					
					// copy to remote execution work directory
					remoteExecutionSystem = (ExecutionSystem)new SystemDao().findBySystemId(job.getSystem());
					remoteExecutionDataClient = remoteExecutionSystem.getRemoteDataClient(job.getInternalUsername());
					remoteExecutionDataClient.authenticate();
					
					Software software = SoftwareDao.getSoftwareByUniqueName(job.getSoftwareName());
					
					String remoteWorkPath = null;
		        	
					if (!StringUtils.isEmpty(software.getExecutionSystem().getScratchDir())) {
						remoteWorkPath = software.getExecutionSystem().getScratchDir();
					} else if (!StringUtils.isEmpty(software.getExecutionSystem().getWorkDir())) {
						remoteWorkPath = software.getExecutionSystem().getWorkDir();
					}
					
					if (!StringUtils.isEmpty(remoteWorkPath)) {
						if (!remoteWorkPath.endsWith("/")) remoteWorkPath += "/";
					} else {
						remoteWorkPath = "";
					}
					
					remoteWorkPath += job.getOwner() +
							"/job-" + job.getUuid() + "-" + Slug.toSlug(job.getName());
					
					if (!remoteExecutionDataClient.doesExist(remoteWorkPath)) {
						remoteExecutionDataClient.mkdirs(remoteWorkPath);
					}
					
					job.setWorkPath(remoteWorkPath);
					job.setStatus(JobStatusType.STAGING_INPUTS, "Staging " + singleInput + " to execution system");
					TransferTask rootTask = new TransferTask(
							singleInput, 
							"agave://" + job.getSystem() + "/" + remoteWorkPath, 
							job.getOwner(), 
							null, 
							null);
					job.addEvent(new JobEvent(
							JobStatusType.STAGING_INPUTS, 
							"Copy in progress", 
							rootTask, 
							job.getOwner()));
					job.setLastUpdated(new Date());
					JobDao.persist(job);
					
					URLCopy urlCopy = new URLCopy(remoteStorageDataClient, remotePath, remoteExecutionDataClient, remoteWorkPath);
					// will close connections on its own
					urlCopy.copy(remotePath, remoteWorkPath, rootTask);
					
					resolvedInputs += "," + singleInput;
				}
				
				if (ServiceUtils.isValid(resolvedInputs)) 
					inputs.put(key, resolvedInputs.substring(1));
				else 
					inputs.put(key, inputValue);
			}
			catch (MalformedURLException e) 
			{
				log.error("Invalid input given for job(" + job.getUuid() + ") "
						+ job.getName() + " " + inputValue + 
						" is not a valid url or path.", e);

				JobManager.updateStatus(job, JobStatusType.FAILED,
						"Invalid input given for job(" + job.getUuid() + ") "
						+ job.getName() + " " + inputValue + 
						" is not a valid url or path.");
				
//				if (ServiceUtils.isValid(job.getCallbackUrl()))
//				{
//					Postback.submitError(job, CallbackMacroResolver.resolve(job, job.getCallbackUrl()));
//				}
				
				break;
			}
//			catch (RemoteDataException e) {
////				log.error("Failed to stage file " + inputValue
////						+ " for job " + job.getUuid(), e);
////				JobManager.updateStatus(job, JobStatusType.FAILED, e.getMessage());
//				throw e;
//			}
			catch (Exception e)
			{
//				log.error("Failed to stage file " + inputValue
//						+ " for job " + job.getUuid(), e);
//
//				JobManager.updateStatus(job, JobStatusType.FAILED,
//						"Failed to stage file " + inputValue + " "
//						+ e.getMessage());
//				
//				if (ServiceUtils.isValid(job.getCallbackUrl()))
//				{
//					Postback.submitError(job, CallbackMacroResolver.resolve(job, job.getCallbackUrl()));
//				}
				throw new RemoteDataException("Failed to stage file " + inputValue + "\n" + e.getMessage(), e);

//				break;
			}
			finally
			{
				try { remoteExecutionDataClient.disconnect(); } catch (Exception e) {}
				try { remoteStorageDataClient.disconnect(); } catch(Exception e) {};
			}
		}
		
		// status should have bene updated in job object if anything was
		// staged
		if (job.getStatus().equals(JobStatusType.STAGING_INPUTS))
		{
			// if no files need to be staged, throw it in queue
			JobManager.updateStatus(job, JobStatusType.STAGED);
			log.debug("Completed staging inputs for job " + job.getUuid() + " " + job.getName());
		}
	}
}