package org.iplantc.service.jobs.managers.killers;

import java.io.IOException;

import org.iplantc.service.jobs.exceptions.JobException;
import org.iplantc.service.jobs.model.Job;
import org.iplantc.service.remote.RemoteSubmissionClient;
import org.iplantc.service.systems.dao.SystemDao;
import org.iplantc.service.systems.model.ExecutionSystem;
import org.iplantc.service.systems.model.enumerations.SystemStatusType;

public class HPCKiller implements JobKiller {

	private Job	job;

	public HPCKiller(Job job)
	{
		this.job = job;
	}

	@Override
	public void attack() throws JobException, IOException
	{

		RemoteSubmissionClient remoteSubmissionClient = null;
		
		try
		{
			// if the system is down throw an exception to let the user know
			SystemDao systemDao = new SystemDao();
			ExecutionSystem system = (ExecutionSystem)systemDao.findBySystemId(job.getSystem());
			
			if (system == null || !system.isAvailable() || !system.getStatus().equals(SystemStatusType.UP))
			{
				throw new JobException("System " + job.getSystem() + " is not available."); 
			}
			
			remoteSubmissionClient = system.getRemoteSubmissionClient(job.getInternalUsername());
			
			String result = remoteSubmissionClient.runCommand(system.getScheduler().getBatchKillCommand() + " " + job.getLocalJobId());
			if (!result.contains("does not exist") && !result.contains("has deleted job")) 
			{ 
				throw new JobException(
					"Failed to delete remote job " + job.getLocalJobId() + " on " + job.getSystem()); 
			}

		}
		catch (IOException e) {
			throw e;
		}
		catch (JobException e) {
			throw e;
		}
		catch (Exception e)
		{
			throw new JobException("Failed to kill job " + job.getUuid(), e);
		}
	}

}
