package org.iplantc.service.jobs.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.Locale;
import java.util.regex.Pattern;

public class Slug {
	private static final Pattern NONLATIN = Pattern.compile("[^\\w-]");
	private static final Pattern WHITESPACE = Pattern.compile("[\\s]");

	public static String toSlug(String input)
	{
		String nowhitespace = WHITESPACE.matcher(input).replaceAll("-");
		String normalized = Normalizer.normalize(nowhitespace, Form.NFD);
		String slug = NONLATIN.matcher(normalized).replaceAll("");
		return slug.toLowerCase(Locale.ENGLISH);
	}
	
	public static void main(String args[]) {
		
		String input = "";
		
		while ((input = Slug.getCLI()) != null)
			System.out.println(Slug.toSlug(input));
		
	}

	public static String getCLI() {
		//  open up standard input
	    System.out.println("Enter input: ");  
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		String text = null;
  
		try {
			text = br.readLine();
		} catch (IOException ioe) {
			System.out.println("IO error trying to read your input");
			System.exit(1);
		}
		return text;
	}
}
