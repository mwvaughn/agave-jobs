package org.iplantc.service.jobs.queue;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.StaleObjectStateException;
import org.iplantc.service.apps.dao.SoftwareDao;
import org.iplantc.service.apps.model.Software;
import org.iplantc.service.common.persistence.TenancyHelper;
import org.iplantc.service.jobs.Settings;
import org.iplantc.service.jobs.dao.JobDao;
import org.iplantc.service.jobs.managers.JobManager;
import org.iplantc.service.jobs.model.Job;
import org.iplantc.service.jobs.model.enumerations.JobStatusType;
import org.iplantc.service.systems.model.enumerations.StorageProtocolType;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * Class to watch job status on remote systems. This can also run locally as in the case
 * of condor jobs. Probably want to batch jobs on a particular system together for
 * efficiency sake.
 * 
 * @author dooley
 * 
 */
@DisallowConcurrentExecution
public class JobStatusWatch implements org.quartz.Job 
{
	private static final Logger	log	= Logger.getLogger(JobStatusWatch.class);
	
	public JobStatusWatch() {}

	public void execute(JobExecutionContext context)
			throws JobExecutionException
	{		
		// pull the oldest job with JobStatusType.CLEANING_UP from the db
		Job job = null;
		try
		{	
			job = JobDao.getNextRunningJob();
			
			if (job != null) 
            {
				// this is a new thread and thus has no tenant info loaded. we set it up
				// here so things like app and system lookups will stay local to the 
				// tenant
				TenancyHelper.setCurrentTenantId(job.getTenantId());
				TenancyHelper.setCurrentEndUser(job.getOwner());
				
				if (job.getStatus().equals(JobStatusType.ARCHIVING)) 
				{
					job.setLastUpdated(new Date());
					JobDao.persist(job);
					return;
				} 
				else
				{
					Software software = SoftwareDao.getSoftwareByUniqueName(job.getSoftwareName());
	                
	            	// if the execution system for this job has a local storage config,
	            	// all other transfer workers will pass on it.
	                if (!StringUtils.equals(Settings.LOCAL_SYSTEM_ID, job.getSystem()) &&
	                		software.getExecutionSystem().getStorageConfig().getProtocol().equals(StorageProtocolType.LOCAL)) 
	                {
	                    return;
	                } 
	                else 
	                {
						JobManager.checkStatus(job);
	                }
				}
			} 
		}
		catch (StaleObjectStateException e) {
			log.debug("Just avoided a job archive race condition from worker " 
					+ context.getTrigger().getDescription());
		}
		catch (HibernateException e) {
			log.error("Failed to retrieve job information from db", e);
		}
		catch (Exception e)
		{
			if (job == null) {
				log.error("Failed to check status of job", e);
			} 
			else 
			{
				String message = "Failed to check status of job " + job.getUuid() + " " + e.getMessage();
				log.error(message, e);
					
				try
				{
					JobManager.updateStatus(job, job.getStatus(), message);
				}
				catch (Exception e1) {}
			}
		}
	}
}