/**
 * 
 */
package org.iplantc.service.jobs.model.scripts;

import org.iplantc.service.jobs.model.Job;

/**
 * @author dooley
 * 
 */
public class ForkSubmitScript extends AbstractSubmitScript {

	/**
	 * @param job
	 */
	public ForkSubmitScript(Job job)
	{
		super(job);
	}

	/**
	 * Returns an empty string.
	 */
	@Override
	public String getScriptText()
	{
		return "#!/bin/bash\n\n";
	}

}
