/**
 * 
 */
package org.iplantc.service.jobs.managers;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.ConnectException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;
import org.iplantc.service.apps.dao.SoftwareDao;
import org.iplantc.service.apps.exceptions.SoftwareException;
import org.iplantc.service.apps.managers.ApplicationManager;
import org.iplantc.service.apps.model.Software;
import org.iplantc.service.apps.model.SoftwareInput;
import org.iplantc.service.apps.model.SoftwareParameter;
import org.iplantc.service.apps.model.enumerations.SoftwareParameterType;
import org.iplantc.service.apps.util.ServiceUtils;
import org.iplantc.service.io.dao.LogicalFileDao;
import org.iplantc.service.io.model.LogicalFile;
import org.iplantc.service.io.permissions.PermissionManager;
import org.iplantc.service.jobs.Settings;
import org.iplantc.service.jobs.dao.JobDao;
import org.iplantc.service.jobs.exceptions.JobException;
import org.iplantc.service.jobs.exceptions.JobProcessingException;
import org.iplantc.service.jobs.exceptions.QuotaViolationException;
import org.iplantc.service.jobs.exceptions.RemoteJobMonitoringException;
import org.iplantc.service.jobs.exceptions.SchedulerException;
import org.iplantc.service.jobs.exceptions.SystemUnavailableException;
import org.iplantc.service.jobs.managers.killers.JobKiller;
import org.iplantc.service.jobs.managers.killers.JobKillerFactory;
import org.iplantc.service.jobs.managers.launchers.AbstractJobLauncher;
import org.iplantc.service.jobs.managers.launchers.JobLauncherFactory;
import org.iplantc.service.jobs.managers.monitors.JobMonitor;
import org.iplantc.service.jobs.managers.monitors.JobMonitorFactory;
import org.iplantc.service.jobs.model.Job;
import org.iplantc.service.jobs.model.JobEvent;
import org.iplantc.service.jobs.model.enumerations.JobMacroType;
import org.iplantc.service.jobs.model.enumerations.JobStatusType;
import org.iplantc.service.notification.exceptions.NotificationException;
import org.iplantc.service.notification.model.Notification;
import org.iplantc.service.remote.exceptions.RemoteExecutionException;
import org.iplantc.service.systems.dao.SystemDao;
import org.iplantc.service.systems.manager.SystemManager;
import org.iplantc.service.systems.model.AuthConfig;
import org.iplantc.service.systems.model.BatchQueue;
import org.iplantc.service.systems.model.ExecutionSystem;
import org.iplantc.service.systems.model.RemoteSystem;
import org.iplantc.service.systems.model.enumerations.RemoteSystemType;
import org.iplantc.service.systems.model.enumerations.SystemStatusType;
import org.iplantc.service.transfer.RemoteDataClient;
import org.iplantc.service.transfer.RemoteDataClientFactory;
import org.iplantc.service.transfer.RemoteFileInfo;
import org.iplantc.service.transfer.URLCopy;
import org.iplantc.service.transfer.dao.TransferTaskDao;
import org.iplantc.service.transfer.exceptions.RemoteDataException;
import org.iplantc.service.transfer.model.TransferTask;
import org.restlet.data.Form;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.stevesoft.pat.Regex;

/**
 * @author dooley
 * 
 */
/**
 * @author dooley
 *
 */
public class JobManager {
	private static final Logger	log	= Logger.getLogger(JobManager.class);

	/**
	 * Take the job description and submit the job for the authenticated user.
	 * 
	 * @param job
	 *            job description
	 * @return id of the submitted job
	 * @throws Exception
	 */
	public static void submit(Job job) throws Exception
	{
		boolean submitted = false;
		
		int attempts = job.getRetries();
		
		while (!submitted && attempts <= Settings.MAX_SUBMISSION_RETRIES)
		{
			job.setRetries(attempts);
			
			attempts++;
			
			log.debug("Attempt " + attempts + " to submit job " + job.getUuid());
			
			try 
			{
				AbstractJobLauncher launcher = JobLauncherFactory.getInstance(job);
				
				ExecutionSystem system = (ExecutionSystem) new SystemDao().findBySystemId(job.getSystem());
				
				if (system == null || !system.isAvailable() || !system.getStatus().equals(SystemStatusType.UP))
				{
					throw new SystemUnavailableException("System " + system.getName() + " is not available.");
				}
				else {
					
					JobQuotaCheck quotaValidator = new JobQuotaCheck(job);
					
					quotaValidator.check();
				
					launcher.launch();
					submitted = true;
					
					log.info("Successfully submitted job " + job.getUuid() + " to " + job.getSystem());
				}
			}
			catch (ConnectException e) {
				JobManager.updateStatus(job, JobStatusType.STAGED, 
						e.getMessage() + 
						" The service was unable to connect to the target execution system " +
						"for this application, \"" + job.getSystem() + ".\" This job will " +
						"remain in queue until the system becomes available. ");
					log.error("Job[" + job.getUuid() + "] - " + job.getErrorMessage());
					break;
			}
			catch (QuotaViolationException e) {
				JobManager.updateStatus(job, JobStatusType.STAGED, 
					e.getMessage() + 
					" This job will remain in queue until one or more current jobs complete. " + 
					"For more information on job quotas, please consult https://foundation.iplantcollaborative.org/docs.");
				log.error("Job[" + job.getUuid() + "] - " + job.getErrorMessage());
				break;
			}
			catch (SystemUnavailableException e) {
				JobManager.updateStatus(job, JobStatusType.STAGED, 
					"The target execution system for this application, \"" + job.getSystem() + ",\" is currently unavailable. " +
						"This job will remain in queue until the system becomes available. ");
				log.error("Job[" + job.getUuid() + "] - " + job.getErrorMessage());
				break;
			}
			catch (SchedulerException e) {
				JobManager.updateStatus(job, JobStatusType.STAGED, 
					"Failed to submit job " + job.getUuid() + 
					" on attempt " + attempts + 
					" due to scheduler malfunction. This attempt will not count against the retry count.");
				log.error("Job[" + job.getUuid() + "] - " + job.getErrorMessage());
				break;
			} 
			catch (Exception e) {
				if (attempts > Settings.MAX_SUBMISSION_RETRIES ) {
					log.error("Failed to submit job " + job.getUuid() + " on attempt " + attempts, e);
					throw e;
//				} else {
//					job.setRetries(job.getRetries() + 1);
//					JobDao.persist(job);
				}
			}
		}
	}

	public static void kill(Job job) throws Exception
	{
		if (!JobStatusType.hasQueued(job.getStatus())) 
		{
			// if it's not in queue, just update the status
			job.setStatus(JobStatusType.STOPPED, "Job cancelled by user.");
			JobDao.persist(job);
			return;
		}
		else if (!job.isRunning())
		{
			// nothing to be done for jobs that are not running
			return;
		}
		else
		{
			JobKiller killer = null;
			int retries = 0;
			while (retries < Settings.MAX_SUBMISSION_RETRIES) 
			{ 
				try {
					killer = JobKillerFactory.getInstance(job);
					killer.attack();
					break;
				} catch (IOException e) {
					log.error("Failed to stop job " + job.getUuid() + " on attempt " + retries);
					retries++;
				} catch (JobException e) {
					log.debug("Failed to stop job " + job.getUuid() + " on attempt " + retries + 
							". Aborting further kill attempts. " + e.getMessage());
					break;
				}
			}
			
			job.setStatus(JobStatusType.STOPPED,  JobStatusType.STOPPED.getDescription());
			job.setLastUpdated(new Date());
			job.setEndTime(job.getLastUpdated());
			JobDao.persist(job);
			
//			if (!StringUtils.isEmpty(job.getCallbackUrl())) 
//			{ 
//				Postback.submitSuccess(job, CallbackMacroResolver.resolve(job, job.getCallbackUrl()));
//			}
		}
	}
	
	public static void hide(long jobId) throws Exception
	{
		Job job = JobDao.getById(jobId);

		if (!job.isRunning())
		{
			job.setVisible(Boolean.FALSE);
		}
		else
		{
			JobKiller killer = null;
			try {
				killer = JobKillerFactory.getInstance(job);
				killer.attack();
			} catch (JobException e) {
				log.debug("Failed to stop job " + job.getUuid() + " at user's request. This may be caused by a ghost process.", e);
			}
			job.setVisible(Boolean.FALSE);
			job.setStatus(JobStatusType.STOPPED, "Job deleted by user.");
			job.setLastUpdated(new Date());
			job.setEndTime(job.getLastUpdated());
			
//			if (!StringUtils.isEmpty(job.getCallbackUrl())) 
//			{ 
//				Postback.submitSuccess(job, CallbackMacroResolver.resolve(job, job.getCallbackUrl()));
//			}
		}
		
		JobDao.persist(job);
	}
	
	public static void main(String[] args) {
		try
		{
			JobManager.hide(98);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
	}

	/**
	 * Updates the status of a job, updates the timestamps as appropriate
	 * based on the status, and writes a new JobEvent to the job's history.
	 * 
	 * @param job
	 * @param status
	 * @throws JobException
	 */
	public static void updateStatus(Job job, JobStatusType status)
			throws JobException
	{
		updateStatus(job, status, status.getDescription());
	}

	/**
	 * Updates the status of a job, its timestamps, and writes a new 
	 * JobEvent to the job's history with the given status and message.
	 * 
	 * @param job
	 * @param status
	 * @param errorMessage
	 * @throws JobException
	 */
	public static void updateStatus(Job job, JobStatusType status,
			String errorMessage) throws JobException
	{
		JobDao.refresh(job);
		job.setStatus(status, errorMessage);

		Date date = new Date();
		job.setLastUpdated(date);
		if (status.equals(JobStatusType.SUBMITTING))
		{
			job.setSubmitTime(date);
		}
		else if (status.equals(JobStatusType.RUNNING))
		{
			job.setStartTime(date);
		}
		else if (status.equals(JobStatusType.FINISHED)
				|| status.equals(JobStatusType.KILLED)
				|| status.equals(JobStatusType.STOPPED)
				|| status.equals(JobStatusType.FAILED))
		{
			job.setEndTime(date);
		}
		else if (status.equals(JobStatusType.STAGED)) {
			//
		}
		
		JobDao.persist(job);
	}
	
//	/**
//	 * Adds a status change event to a job's history record. This 
//	 * is called whenever JobManager.updateStatus() is called and 
//	 * should be called whenever a status change explicitly occurs.
//	 * 
//	 * @param job
//	 * @param status
//	 * @param message
//	 * @throws JobException
//	 */
//	public static void addJobEvent(Job job, JobStatusType status,
//			String message) throws JobException
//	{
//		JobEvent jobEvent = new JobEvent(job.getUuid(), status, message);
//		JobEventDao.persist(jobEvent);
//	}

//	/**
//	 * The jobs service submits jobs with inputs relative to the user's iplant
//	 * home folder. As such, if the user specifies a url as an input, we use the
//	 * IO service to copy that file into a job-specific sub folder in their
//	 * iplant home directory.
//	 * 
//	 * @deprecated
//	 * @param job
//	 * @param inputUrl
//	 * @return
//	 */
//	public static boolean stageFile(Job job, String inputUrl)
//	{
//		Map<String, String> map = new HashMap<String, String>();
//		map.put("url", inputUrl);
//		map.put("callbackUrl", Settings.IPLANT_JOB_SERVICE
//				+ "trigger/input/" + job.getUuid() + "/token/"
//				+ job.getUpdateToken());
//
//		String ioUrl = "https://foundation.iplantcollaborative.org/iplant-io/io/"
//				+ job.getOwner()
//				+ "/jobs/"
//				+ job.getName()
//				+ "-"
//				+ job.getCreated().getTime();
//
//		// String credential = TokenEncoder.encode(job.getOwner() + "|" +
//		// System.currentTimeMillis());
//		// HttpResponse response = Postback.post(ioUrl, map, job.getOwner(),
//		// credential);
//		HttpResponse response = Postback.post(ioUrl, map,
//				Settings.COMMUNITY_USERNAME, Settings.COMMUNITY_PASSWORD);
//		HttpEntity entity = response.getEntity();
//		String sResponse;
//		try
//		{
//			sResponse = EntityUtils.toString(entity);
//			JSONObject json = new JSONObject(sResponse);
//			return ( json.has("status") && json.getString("status").equals(
//					"success") );
//		}
//		catch (Exception e)
//		{
//			log.error("Failed to submit stage request to " + ioUrl, e);
//		}
//
//		return false;
//	}

	public static void archive(Job job) throws Exception
	{
		RemoteDataClient archiveDataClient = null;
		RemoteDataClient executionDataClient = null;
		
		// we should be able to archive from anywhere. Given that we can stage in condor 
		// job data from remote systems, we should be able to stage it out as well. At 
		// this point we are guaranteed that the worker running this bit of code has
		// access to the job output folder. The RemoteDataClient abstraction will handle
		// the rest.
		File archiveFile = null;
		try 
		{
			executionDataClient = new SystemDao()
								.findBySystemId(job.getSystem())
								.getRemoteDataClient(job.getInternalUsername());
			
			executionDataClient.authenticate();
			
			// copy remote archive file to temp space
			String remoteArchiveFile = job.getWorkPath() + File.separator + ".agave.archive";
			
			String localArchiveFile = FileUtils.getTempDirectoryPath() + File.separator +
					"job-" + job.getUuid() + "-" + System.currentTimeMillis();
			
			// pull remote .archive file and parse it for a list of paths relative
			// to the job.workDir to exclude from archiving. Generally this will be
			// the application binaries, but the app itself may have added or removed
			// things from this file, so we need to process it anyway.
			List<String> jobFileList = new ArrayList<String>();
			if (executionDataClient.doesExist(remoteArchiveFile)) 
			{
				executionDataClient.get(remoteArchiveFile, localArchiveFile);
				
				// read it in to find the original job files
				archiveFile = new File(localArchiveFile);
				if (archiveFile.exists()) {
					if (archiveFile.isFile()) {
						jobFileList.addAll(FileUtils.readLines(archiveFile));
					} else {
						archiveFile = new File(localArchiveFile, ".agave.archive");
						if (archiveFile.exists() && archiveFile.isFile()) {
							jobFileList.addAll(FileUtils.readLines(archiveFile));
						}
					}
				}
			} 
			
			// read in remote job work directory listing
			List<RemoteFileInfo> outputFiles = executionDataClient.ls(job.getWorkPath());
			
			archiveDataClient = job.getArchiveSystem().getRemoteDataClient(job.getInternalUsername());
			archiveDataClient.authenticate();
			
			if (!archiveDataClient.doesExist(job.getArchivePath())) {
				archiveDataClient.mkdirs(job.getArchivePath());
				
			}
			
			// iterate over the work folder and archive everything that wasn't
			// listed in the archive file. We use URL copy here to abstract the 
			// third party transfer we would like to do. If possible, URLCopy will
			// do a 3rd party transfer. When not possible, such as when we're going
			// cross-protocol, it will proxy the transfer.
			TransferTask rootTask = new TransferTask(
					"agave://" + job.getSystem() + "/" + job.getWorkPath(), 
					"agave://" + job.getArchiveSystem().getSystemId() + "/" +job.getArchivePath(), 
					job.getOwner(), 
					null, 
					null);
			JobDao.refresh(job);
			job.addEvent(new JobEvent(
					job.getStatus(), 
					"Archiving " + rootTask.getSource() + " to " + rootTask.getDest(), 
					rootTask, 
					job.getOwner()));
			
			JobDao.persist(job);
			
			for (RemoteFileInfo outputFile: outputFiles) 
			{
				if (StringUtils.equals(outputFile.getName(), ".") || StringUtils.equals(outputFile.getName(), "..")) continue;
				
				String workFileName = job.getWorkPath() + File.separator + outputFile.getName();
				String archiveFileName = job.getArchivePath() + File.separator + outputFile.getName();
				if (!jobFileList.contains(outputFile.getName())) {
					URLCopy urlCopy = new URLCopy(executionDataClient, workFileName, archiveDataClient, archiveFileName);
					TransferTask childTransferTask = new TransferTask(
							"agave://" + job.getSystem() + "/" + workFileName, 
							"agave://" + job.getArchiveSystem().getSystemId() + "/" + archiveFileName, 
							job.getOwner(), 
							rootTask, 
							rootTask);
					TransferTaskDao.persist(childTransferTask);
					urlCopy.copy(workFileName, archiveFileName, childTransferTask);
				}
			}
			
			// if it all worked as expected, then delete the job work directory
			executionDataClient.delete(job.getWorkPath());
			JobManager.updateStatus(job, JobStatusType.ARCHIVING_FINISHED, 
					"Job output archiving completed successfully.");
		}
		finally 
		{
			// clean up the local archive file
			FileUtils.deleteQuietly(archiveFile);
			try {
				if (archiveDataClient.isPermissionMirroringRequired() && StringUtils.isEmpty(job.getInternalUsername())) {
					archiveDataClient.setOwnerPermission(job.getOwner(), job.getArchivePath(), true);
				}
			} catch (Exception e) {}
			try { archiveDataClient.disconnect(); } catch (Exception e) {}
			try { executionDataClient.disconnect(); } catch (Exception e) {}
		}
	}

	public static JobStatusType checkStatus(Job job) throws RemoteJobMonitoringException
	{
		JobMonitor jobMonitor = JobMonitorFactory.getInstance(job);
		
		log.debug("Checking status of job " + job.getUuid());
		
		jobMonitor.updateStatus();
		
		return job.getStatus();
	}
	
	/**
	 * Takes a JsonNode representing a job request and parses it into a job object.
	 * 
	 * @param json a JsonNode containing the job request
	 * @return validated job object ready for submission
	 * @throws JobProcessingException
	 */
	public static Job processJob(JsonNode json, String username, String internalUsername) 
	throws JobProcessingException
	{
		Form form = new Form();
		String currentKey = null;
		
		try
		{
			Iterator<String> fields = json.fieldNames();
   			while(fields.hasNext()) {
				String key = fields.next();
				
				if (StringUtils.isEmpty(key)) continue;
				
				currentKey = key;
				
				if (key.equals("notifications")) {
					continue;
				}
				
				if (key.equals("dependencies")) 
				{
					throw new JobProcessingException(400,
							"Job dependencies are not yet supported.");
				}
				
				JsonNode child = json.get(key);
				
				if (child.isNull()) {
					form.add(key, null);
				}
				else if (child.isNumber()) 
				{
					form.add(key, child.asText());
				}
				else if (child.isObject())
				{
					Iterator<String> childFields = child.fieldNames();
					while(childFields.hasNext()) 
					{
						String childKey = childFields.next();
						JsonNode childchild = child.get(childKey);
						if (StringUtils.isEmpty(childKey) || childchild.isNull()) {
							continue;
						}
						else if (childchild.isDouble()) {
							form.add(childKey, childchild.decimalValue().toPlainString());
						}
						else if (childchild.isNumber()) 
						{
							form.add(childKey, new Long(childchild.longValue()).toString());
						}
						else {
							form.add(childKey, childchild.asText());
						}
					}
				}
				else 
				{
					form.add(key, json.get(key).asText());
				} 
			}
   			
   			List<Notification> notifications = new ArrayList<Notification>();
			
			if (json.has("notifications")) 
			{	
				if (!json.get("notifications").isArray())
				{
					throw new NotificationException("Invalid " + currentKey + " value given. "
							+ "notifications must be an array of notification objects specifying a "
							+ "valid url, event, and an optional boolean persistence attribute.");
				}
				else
				{
					currentKey = "notifications";
					
					ArrayNode jsonNotifications = (ArrayNode)json.get("notifications");
					for (int i=0; i<jsonNotifications.size(); i++) 
					{
						currentKey = "notifications["+i+"]";
						JsonNode jsonNotif = jsonNotifications.get(i);
						if (!jsonNotif.isObject())
						{
							throw new NotificationException("Invalid " + currentKey + " value given. "
								+ "Each notification objects should specify a "
								+ "valid url, event, and an optional boolean persistence attribute.");
						}
						else
						{
							Notification notification = new Notification();
							currentKey = "notifications["+i+"].url";
							if (!jsonNotif.has("url")) {
								throw new NotificationException("No " + currentKey + " attribute given. "
										+ "Notifications must have valid url and event attributes.");
							}
							else 
							{
								notification.setCallbackUrl(jsonNotif.get("url").textValue());
							}
							
							currentKey = "notifications["+i+"].event";
							if (!jsonNotif.has("event")) {
								throw new NotificationException("No " + currentKey + " attribute given. "
										+ "Notifications must have valid url and event attributes.");
							}
							else
							{
								String event = jsonNotif.get("event").textValue();
								try {
									if (!StringUtils.equals("*", event)) {
										try {
											JobStatusType.valueOf(event.toUpperCase());
										} catch (IllegalArgumentException e) {
											JobMacroType.valueOf(event.toUpperCase());
										}
										notification.setNotificationEvent(StringUtils.upperCase(event));
									}
									else {
										notification.setNotificationEvent("*");
									}
								} catch (Throwable e) {
									throw new NotificationException("Valid values are: *, " + 
											ServiceUtils.explode(", ", Arrays.asList(JobStatusType.values())) + ", " + 
											ServiceUtils.explode(", ", Arrays.asList(JobMacroType.values())));
								}
							}
							
							
							if (jsonNotif.has("persistent")) 
							{
								currentKey = "notifications["+i+"].persistent";
								if (jsonNotif.get("persistent").isNull()) {
									throw new NotificationException(currentKey + " cannot be null");
								}
								else if (!jsonNotif.get("persistent").isBoolean()) 
								{
									throw new NotificationException("Invalid value for " + currentKey + ". "
											+ "If provided, " + currentKey + " must be a boolean value.");
								} else {
									notification.setPersistent(jsonNotif.get("persistent").asBoolean());
								}
							}
							notifications.add(notification);
							Thread.sleep(5);
						}
					}
				}
			}
		
			Job job = processJob(form, username, internalUsername);
			
			for (Notification notification: notifications) {
				job.addNotification(notification);
			}
			
			return job;
		} 
		catch (JobProcessingException e) {
			throw e;
		}
		catch (SoftwareException e) {
			throw new JobProcessingException(400, e.getMessage());
		}
		catch (Throwable e) {
			throw new JobProcessingException(400,
					"Failed to parse json job description. Invalid value for " + 
							currentKey + ". " + e.getMessage());
		}
	}
	
	/**
	 * Takes a Form representing a job request and parses it into a job object. This is a
	 * stripped down, unstructured version of the other processJob method.
	 * 
	 * @param json a JsonNode containing the job request
	 * @return validated job object ready for submission
	 * @throws JobProcessingException
	 */
	public static Job processJob(Form form, String username, String internalUsername)
	throws JobProcessingException
	{
		Map<String, String> pTable = form.getValuesMap();
		SystemManager systemManager = new SystemManager();
		
		String name = null;
		if (pTable.containsKey("name")) {
			name = pTable.get("name");
		} else {
			name = pTable.get("jobName");
		}
	
		if (StringUtils.isEmpty(name) || StringUtils.length(name) > 64)
		{
			throw new JobProcessingException(400, 
					"Job name cannot be empty.");
		} 
		else if (StringUtils.length(name) > 64) {
			throw new JobProcessingException(400, 
					"Job name must be less than 64 characters.");
		}
		else 
		{
			name = name.trim();
		}

		String softwareName = null;
		if (pTable.containsKey("appId")) {
			softwareName = pTable.get("appId");
		} else {
			softwareName = pTable.get("softwareName");
		}
		
		if (StringUtils.isEmpty(softwareName))
		{
			throw new JobProcessingException(400, 
					"appId cannot be empty");
		} 
		else if (StringUtils.length(name) > 80) {
			throw new JobProcessingException(400, 
					"appId must be less than 80 characters");
		}
		else if (!softwareName.contains("-") || softwareName.endsWith("-")) 
		{
			throw new JobProcessingException(400, 
					"Invalid appId. " +
					"Please specify an app using its unique id. " +
					"The unique id is defined by the app name " +
					"and version separated by a hyphen. eg. example-1.0");
		}
		
		Software software = SoftwareDao.getSoftwareByUniqueName(softwareName.trim());
		
		if (!ApplicationManager.isInvokableByUser(software, username)) {
			throw new JobProcessingException(403, "Permission denied. You do not have permission to access this app");
		}
		
		// validate the optional execution system matches the software execution system
		String exeSystem = pTable.get("executionSystem");
		if (pTable.containsKey("executionSystem")) {
			softwareName = pTable.get("executionSystem");
		} else {
			softwareName = pTable.get("executionHost");
		}
		ExecutionSystem executionSystem = software.getExecutionSystem();
		if (StringUtils.length(exeSystem) > 80) {
			throw new JobProcessingException(400, 
					"executionSystem must be less than 80 characters");
		}
		else if (!StringUtils.isEmpty(exeSystem) && !StringUtils.equals(exeSystem, executionSystem.getSystemId())) {
			throw new JobProcessingException(403, 
					"Invalid execution system. Apps are registered to run on a specific execution system. If specified, " +
					"the execution system must match the execution system in the app description. The execution system " +
					"for " + software.getName() + " is " + software.getExecutionSystem().getSystemId() + ".");
		}
		
		/***************************************************************************
		 **						Batch Parameter Selection 						  **
		 ***************************************************************************/
				
		String currentParameter = null;
		String queueName = null;
		BatchQueue jobQueue = null;
		Long nodeCount = null;
		Double memoryPerNode = null;
		String requestedTime = null;
		Long processorsPerNode = null;

		try 
		{
			/********************************** Queue Selection *****************************************/
			
			currentParameter = "batchQueue";
			String userBatchQueue = (String)pTable.get("batchQueue");
			if (StringUtils.isEmpty(userBatchQueue)) {
				userBatchQueue = (String)pTable.get("queue");
			}
			
			if (StringUtils.length(userBatchQueue) > 128) {
				throw new JobProcessingException(400, 
						"batchQueue must be less than 128 characters");
			}
			else if (StringUtils.isEmpty(userBatchQueue)) 
			{
				// use the software default queue if present, otherwise we'll pick it for them in a bit
				if (!StringUtils.isEmpty(software.getDefaultQueue()))
				{
					jobQueue = executionSystem.getQueue(software.getDefaultQueue());
					queueName = software.getDefaultQueue();
					if (jobQueue == null) 
					{
						throw new JobProcessingException(400, 
								"Invalid default batchQueue. No batchQueue named " + userBatchQueue + 
								" is defined on system " + executionSystem.getSystemId());
					}
				}
			} 
			else 
			{
				// user gave a queue. see if it's a valid one
				jobQueue = executionSystem.getQueue(userBatchQueue);
				queueName = userBatchQueue;
				if (jobQueue == null) {
					throw new JobProcessingException(400, 
							"Invalid batchQueue. No batchQueue named " + userBatchQueue + 
							" is defined on system " + executionSystem.getSystemId());
				}
			}
			
			/********************************** Node Count Selection *****************************************/
			
			currentParameter = "nodeCount";
			String userNodeCount = (String)pTable.get("nodeCount");
			if (StringUtils.isEmpty(userNodeCount))
			{
				// use the software default queue if present
				if (software.getDefaultNodes() != null && software.getDefaultNodes() != -1) {
					nodeCount = software.getDefaultNodes();
				} 
				else 
				{ 
					// use a single node otherwise
					nodeCount = new Long(1);
				}
			}
			else
			{
				nodeCount = NumberUtils.toLong(userNodeCount);
			}
			
			if (nodeCount < 1)
			{
				throw new JobProcessingException(400,
						"Invalid " + (StringUtils.isEmpty(userNodeCount) ? "" : "default ") + 
						"nodeCount. If specified, nodeCount must be a positive integer value.");
			}
			
//			nodeCount = pTable.containsKey("nodeCount") ? Long.parseLong(pTable.get("nodeCount")) : software.getDefaultNodes();
//			if (nodeCount < 1) {
//				throw new JobProcessingException(400, 
//						"Invalid nodeCount value. nodeCount must be a positive integer value.");
//			}
			
			// if the queue wasn't specified by the user or app, pick a queue with just node count info
			if (jobQueue == null) {
				jobQueue = selectQueue(executionSystem, nodeCount, -1.0, (long)-1, BatchQueue.DEFAULT_MIN_RUN_TIME);
			}
			
			if (jobQueue == null) {
				throw new JobProcessingException(400, "Invalid " + 
						(StringUtils.isEmpty(userNodeCount) ? "" : "default ") + 
						"nodeCount. No queue found on " + 
						executionSystem.getSystemId() + " that support jobs with " +
						nodeCount + " nodes.");
			} else if (!validateBatchSubmitParameters(jobQueue, nodeCount, (long)-1, -1.0, BatchQueue.DEFAULT_MIN_RUN_TIME)) {
				throw new JobProcessingException(400, "Invalid " + 
						(StringUtils.isEmpty(userNodeCount) ? "" : "default ") + 
						"nodeCount. The " + jobQueue.getName() + " queue on " + 
						executionSystem.getSystemId() + " does not support jobs with " + nodeCount + " nodes.");
			}
			
			/********************************** Max Memory Selection *****************************************/
			
			currentParameter = "memoryPerNode";
			String userMemoryPerNode = (String)pTable.get("memoryPerNode");
			if (StringUtils.isEmpty(userMemoryPerNode)) {
				userMemoryPerNode = (String)pTable.get("maxMemory");
			}
			
			if (StringUtils.isEmpty(userMemoryPerNode))
			{
				if (software.getDefaultMemoryPerNode() != null) {
					memoryPerNode = software.getDefaultMemoryPerNode();
				} 
				else if (jobQueue.getMaxMemoryPerNode() != null && jobQueue.getMaxMemoryPerNode() > 0) {
					memoryPerNode = jobQueue.getMaxMemoryPerNode();
				}
				else {
					memoryPerNode = (double)0;
				}
			}
			else // memory was given, validate 
			{
				try {
					// try to parse it as a number in GB first
					memoryPerNode = Double.parseDouble(userMemoryPerNode);
				} 
				catch (Throwable e) 
				{
					// Otherwise parse it as a string matching ###.#[EPTGM]B
					try 
					{
						memoryPerNode = BatchQueue.parseMaxMemoryPerNode(userMemoryPerNode);
					} 
					catch (NumberFormatException e1)
					{
						memoryPerNode = (double)0;
					}
				}
			}
			
			if (memoryPerNode <= 0) {
				throw new JobProcessingException(400, 
						"Invalid " + (StringUtils.isEmpty(userMemoryPerNode) ? "" : "default ") + 
						"memoryPerNode. memoryPerNode should be a postive value specified in ###.#[EPTGM]B format.");
			}
			
			// if the queue wasn't specified by the user or app, reselect with node and memory info
			if (StringUtils.isEmpty(queueName)) {
				jobQueue = selectQueue(executionSystem, nodeCount, memoryPerNode, (long)-1, BatchQueue.DEFAULT_MIN_RUN_TIME);
			}
			
			if (jobQueue == null) {
				throw new JobProcessingException(400, "Invalid " + 
						(StringUtils.isEmpty(userMemoryPerNode) ? "" : "default ") + 
						"memoryPerNode. No queue found on " + 
						executionSystem.getSystemId() + " that support jobs with " + nodeCount + " nodes and " + 
						memoryPerNode + "GB memory per node");
			} else if (!validateBatchSubmitParameters(jobQueue, nodeCount, (long)-1, memoryPerNode, BatchQueue.DEFAULT_MIN_RUN_TIME)) {
				throw new JobProcessingException(400, "Invalid " + 
						(StringUtils.isEmpty(userMemoryPerNode) ? "" : "default ") + 
						"memoryPerNode. The " + jobQueue.getName() + " queue on " + 
						executionSystem.getSystemId() + " does not support jobs with " + nodeCount + " nodes and " + 
						memoryPerNode + "GB memory per node");
			}
			
			/********************************** Run Time Selection *****************************************/
			
			currentParameter = "requestedTime";
			//requestedTime = pTable.containsKey("requestedTime") ? pTable.get("requestedTime") : software.getDefaultMaxRunTime();
			
			String userRequestedTime = (String)pTable.get("maxRunTime");
			if (StringUtils.isEmpty(userRequestedTime)) {
				// legacy compatibility
				userRequestedTime = (String)pTable.get("requestedTime");
			}
			
			if (StringUtils.isEmpty(userRequestedTime))
			{
				if (!StringUtils.isEmpty(software.getDefaultMaxRunTime())) {
					requestedTime = software.getDefaultMaxRunTime();
				} else if (!StringUtils.isEmpty(jobQueue.getMaxRequestedTime())) {
					requestedTime = jobQueue.getMaxRequestedTime();
				} 
			}
			else
			{
				requestedTime = userRequestedTime;
			}
			
			if (!org.iplantc.service.systems.util.ServiceUtils.isValidRequestedJobTime(requestedTime)) {
				throw new JobProcessingException(400,
						"Invalid maxRunTime. maxRunTime should be the maximum run time " + 
							"time for this job in hh:mm:ss format.");
			} else if (org.iplantc.service.systems.util.ServiceUtils.compareRequestedJobTimes(requestedTime, BatchQueue.DEFAULT_MIN_RUN_TIME) == -1) {
				throw new JobProcessingException(400,
						"Invalid maxRunTime. maxRunTime should be greater than 00:00:00.");
			}
			
			// if the queue wasn't specified by the user or app, reselect with node and memory info
			if (StringUtils.isEmpty(queueName)) {
				jobQueue = selectQueue(executionSystem, nodeCount, memoryPerNode, (long)-1, requestedTime);
			}
			
			if (jobQueue == null) {
				throw new JobProcessingException(400, "Invalid " + 
						(StringUtils.isEmpty(userRequestedTime) ? "" : "default ") + 
						"maxRunTime. No queue found on " + 
						executionSystem.getSystemId() + " that supports jobs with " + nodeCount + " nodes, " + 
						memoryPerNode + "GB memory per node, and a run time of " + requestedTime);
			} else if (!validateBatchSubmitParameters(jobQueue, nodeCount, (long)-1, memoryPerNode, requestedTime)) {
				throw new JobProcessingException(400, "Invalid " + 
						(StringUtils.isEmpty(userRequestedTime) ? "" : "default ") + 
						"maxRunTime. The " + jobQueue.getName() + " queue on " + 
						executionSystem.getSystemId() + " does not support jobs with " + nodeCount + " nodes, " + 
						memoryPerNode + "GB memory per node, and a run time of " + requestedTime);
			}
			
			/********************************** Max Processors Selection *****************************************/
			
			currentParameter = "processorsPerNode";
			String userProcessorsPerNode = (String)pTable.get("processorsPerNode");
			if (StringUtils.isEmpty(userProcessorsPerNode)) {
				userProcessorsPerNode = (String)pTable.get("processorCount");
			}
			if (StringUtils.isEmpty(userProcessorsPerNode))
			{
				if (software.getDefaultProcessorsPerNode() != null) {
					processorsPerNode = software.getDefaultProcessorsPerNode();
				} else if (jobQueue.getMaxProcessorsPerNode() != null && jobQueue.getMaxProcessorsPerNode() > 0) {
					processorsPerNode = jobQueue.getMaxProcessorsPerNode();
				} else {
					processorsPerNode = new Long(1);
				}
			}
			else
			{
				processorsPerNode = NumberUtils.toLong(userProcessorsPerNode);
			}
			
			if (processorsPerNode < 1) {
				throw new JobProcessingException(400, 
						"Invalid " + (StringUtils.isEmpty(userProcessorsPerNode) ? "" : "default ") + 
						"processorsPerNode value. processorsPerNode must be a positive integer value.");
			}
			
			// if the queue wasn't specified by the user or app, reselect with node and memory info
			if (StringUtils.isEmpty(queueName)) {
				jobQueue = selectQueue(executionSystem, nodeCount, memoryPerNode, processorsPerNode, requestedTime);
			}
			
			if (jobQueue == null) {
				throw new JobProcessingException(400, "Invalid " + 
						(StringUtils.isEmpty(userProcessorsPerNode) ? "" : "default ") + 
						"processorsPerNode. No queue found on " + 
						executionSystem.getSystemId() + " that supports jobs with " + nodeCount + " nodes, " + 
						memoryPerNode + "GB memory per node, a run time of " + requestedTime + " and " + 
						processorsPerNode + " processors per node");
			} else if (!validateBatchSubmitParameters(jobQueue, nodeCount, processorsPerNode, memoryPerNode, requestedTime)) {
				throw new JobProcessingException(400, "Invalid " + 
						(StringUtils.isEmpty(userProcessorsPerNode) ? "" : "default ") + 
						"processorsPerNode. The " + jobQueue.getName() + " queue on " + 
						executionSystem.getSystemId() + " does not support jobs with " + nodeCount + " nodes, " + 
						memoryPerNode + "GB memory per node, a run time of " + requestedTime + " and " + 
						processorsPerNode + " processors per node");
			}
		}
		catch (JobProcessingException e) 
		{
			throw e;
		}
		catch (Exception e) {
			throw new JobProcessingException(400, "Invalid " + currentParameter + " value.", e);
		}
		
		/***************************************************************************
		 **						End Batch Queue Selection 						  **
		 ***************************************************************************/
		
		
		String defaultNotificationCallback = null;
		List<Notification> notifications = new ArrayList<Notification>();
		if (pTable.containsKey("callbackUrl")) { 
			defaultNotificationCallback = pTable.get("callbackUrl");
		} else if (pTable.containsKey("callbackURL")) {
//			throw new JobProcessingException(400,
//					"The callbackUrl attribute is no longer supported. Please specify " +
//					"one or more notification objects, including valid url and event attributes, " +
//					"in a notifications array instead.");
			defaultNotificationCallback = pTable.get("callbackURL");
		} else if (pTable.containsKey("notifications")) {
			defaultNotificationCallback = pTable.get("notifications");
		}
		
		if (!StringUtils.isEmpty(defaultNotificationCallback))
		{
			try {
				notifications.add(new Notification(JobStatusType.FINISHED.name(), defaultNotificationCallback));
				// uuid generation was happening too fast here. we need to pause since this runs in the same
				// thread and processor.
				Thread.sleep(5);
				notifications.add(new Notification(JobStatusType.FAILED.name(), defaultNotificationCallback));
			} catch (NotificationException e) {
				throw new JobProcessingException(400, e.getMessage());
			} catch (InterruptedException e) {
				throw new JobProcessingException(500, "Failed to verify notication callback url");
			}
		}
		
		/***************************************************************************
		 **						Verifying remote connectivity 					  **
		 ***************************************************************************/
		
		AuthConfig authConfig = executionSystem.getLoginConfig().getAuthConfigForInternalUsername(internalUsername);
		String salt = executionSystem.getEncryptionKeyForAuthConfig(authConfig);
		if (authConfig.isCredentialExpired(salt))
		{
			throw new JobProcessingException(412,
					(authConfig.isSystemDefault() ? "Default " : "Internal user " + internalUsername) + 
					" credential for " + software.getExecutionSystem().getSystemId() + " is not active." +
					" Please add a valid " + software.getExecutionSystem().getLoginConfig().getType() + 
					" execution credential for the execution system and resubmit the job.");
		}
		
		try
		{
			if (!executionSystem.getRemoteSubmissionClient(internalUsername).canAuthentication()) {
				throw new RemoteExecutionException("Unable to authenticate to " + executionSystem.getSystemId());
			}
		}
		catch (Exception e)
		{
			throw new JobProcessingException(412,
					"Unable to authenticate to " + executionSystem.getSystemId() + " with the " +
					(authConfig.isSystemDefault() ? "default " : "internal user " + internalUsername) + 
					"credential. Please check the " + executionSystem.getLoginConfig().getType() + 
					" execution credential for the execution system and resubmit the job.");
		}
	
		authConfig = executionSystem.getStorageConfig().getAuthConfigForInternalUsername(internalUsername);
		salt = executionSystem.getEncryptionKeyForAuthConfig(authConfig);
		if (authConfig.isCredentialExpired(salt))
		{
			throw new JobProcessingException(412,
					"Credential for " + software.getExecutionSystem().getSystemId() + " is not active." +
					" Please add a valid " + software.getExecutionSystem().getStorageConfig().getType() + 
					" storage credential for the execution system and resubmit the job.");
		}
		
		RemoteDataClient remoteExecutionDataClient = null;
		try {
			remoteExecutionDataClient = executionSystem.getRemoteDataClient(internalUsername);
			remoteExecutionDataClient.authenticate();
		} catch (Exception e) {
			throw new JobProcessingException(412,
					"Unable to authenticate to " + executionSystem.getSystemId() + " with the " +
					(authConfig.isSystemDefault() ? "default " : "internal user " + internalUsername) + 
					"credential. Please check the " + executionSystem.getLoginConfig().getType() + 
					" execution credential for the execution system and resubmit the job.");
		} finally {
			try { remoteExecutionDataClient.disconnect(); } catch (Exception e) {}
		}
		
		/***************************************************************************
		 **						Verifying Input Parmaeters						  **
		 ***************************************************************************/
		
		// Verify the inputs by their keys given in the SoftwareInputs
		// in the Software object. We should also be inserting any other 
		// hidden inputs here
		HashMap<String, String> inputTable = new HashMap<String, String>();

		for (SoftwareInput softwareInput : software.getInputs())
		{
			String inputValue = null;
			try
			{
				inputValue = pTable.get(softwareInput.getKey());
				
				// add hidden inputs into the input array so we have a full record
				// of all inputs for this job in the history.
				if (!softwareInput.isVisible()) 
				{
					if (pTable.containsKey(softwareInput.getKey())) {
						throw new JobProcessingException(400,
								"Invalid value for " + softwareInput.getKey() + 
								". " + softwareInput.getKey() + " is a fixed value that "
								+ "cannot be set manually. ");
					} else {
						inputValue = softwareInput.getDefaultValue();
					}
					
				}
				else if (!pTable.containsKey(softwareInput.getKey()))
				{
					if (softwareInput.isRequired()) 
					{
						throw new JobProcessingException(400,
								"No input specified for " + softwareInput.getKey());
					}
					else
					{
						continue;
					}
				}
				else if (StringUtils.isEmpty(inputValue))
				{
					throw new JobProcessingException(400, "No input specified for " + softwareInput.getKey());
				}
				else if(!StringUtils.isEmpty(softwareInput.getValidator())) 
				{
					inputValue = inputValue.replaceAll(";;", "");
					String[] explodedInputs = inputValue.split(";");
					if (softwareInput.getMinCardinality() > explodedInputs.length) 
					{
						throw new JobProcessingException(400,
								softwareInput.getKey() + " requires at least " + 
								softwareInput.getMinCardinality() + " semi-colon separated values");
					} 
					else 
					{
						for(String singleInput: explodedInputs) 
						{	
//							Regex r = new Regex();
//							r.compile(softwareInput.getValidator());
//							if (!r.search(singleInput)) 
//							{
							Pattern pattern = Pattern.compile(softwareInput.getValidator());
							Matcher matcher = pattern.matcher(singleInput);
							if (!StringUtils.isEmpty(softwareInput.getValidator()) && !matcher.find())
							{
								throw new JobProcessingException(400,
										"Invalid input value, \"" + singleInput + "\" for " + softwareInput.getKey() + 
										". Value must match the following expression: \"" +
										softwareInput.getValidator() + "\"");
							} else {
								URI inputUri = new URI(singleInput);
								if (!RemoteDataClientFactory.isSchemeSupported(inputUri)) {
									throw new JobProcessingException(400,
											"Invalid value for " + softwareInput.getKey() + 
											".URI with the " + inputUri.getScheme() + " scheme are not currently supported. " +
											"Please specify your input as a relative path, an Agave Files service endpoint, " +
											"or a URL with one of the following schemes: http, https, sftp, or agave.");
								}
								
							}
						}
					}
				}
				else
				{
					URI inputUri = new URI(inputValue);
					if (!RemoteDataClientFactory.isSchemeSupported(inputUri)) {
						throw new JobProcessingException(400,
								"Invalid value for " + softwareInput.getKey() + 
								". URI with the " + inputUri.getScheme() + " scheme are not currently supported. " +
								"Please specify your input as a relative path, an Agave Files service endpoint, " +
								"or a URI with one of the following schemes: http, https, sftp, or agave.");
					}
				}
			}
			catch (JobProcessingException e)
			{
				throw e;
			}
			catch (Exception e)
			{
				throw new JobProcessingException(400,
						"Failed to parse input for " + softwareInput.getKey());
			}

			inputTable.put(softwareInput.getKey(), inputValue);
		}

		/***************************************************************************
		 **						Verifying  Parameters							  **
		 ***************************************************************************/
		
		// Verify the parameters by their keys given in the
		// SoftwareParameter in the Software object.
		HashMap<String, Object> parameterTable = new HashMap<String, Object>();

		for (SoftwareParameter softwareParameter : software
				.getParameters())
		{
			Object inputValue = null;
			try
			{
				inputValue = pTable.get(softwareParameter.getKey());
				
				// add hidden parameters into the input array so we have a full record
				// of all parameters for this job in the history.
				if (!softwareParameter.isVisible())
				{
					if (pTable.containsKey(softwareParameter.getKey())) {
						throw new JobProcessingException(400,
								"Invalid parameter value for " + softwareParameter.getKey() + 
								". " + softwareParameter.getKey() + " is a fixed value that "
								+ "cannot be set manually. ");
					} else {
						inputValue = softwareParameter.getDefaultValue();
					}
				}
				else if (!pTable.containsKey(softwareParameter.getKey()))
				{
					if (softwareParameter.isRequired()) 
					{
						throw new JobProcessingException(400,
								"No input parameter specified for " + softwareParameter.getKey());
					}
					else
					{
						continue;
					}
				}
				else
				{
					if (softwareParameter.getType().equals(SoftwareParameterType.enumeration))
					{
						inputValue = form.getValues(softwareParameter.getKey(), ";", false);
						String[] jobParams = inputValue.toString().split(";");
						List<String> validParamValues = softwareParameter.getValidatorAsList();
						
						if (validParamValues.isEmpty())
						{
							throw new JobProcessingException(400,
									"Invalid parameter value for " + softwareParameter.getKey() + 
									". Value must match the following expression: \"" +
									softwareParameter.getValidator() + "\"");
						} 
						else
						{
							for(String jobParam: jobParams)
							{
								if (!validParamValues.contains(jobParam))
								{
									throw new JobProcessingException(400,
											"Invalid parameter value for " + softwareParameter.getKey() + 
											". Value must match the following expression: \"" +
											softwareParameter.getValidator() + "\"");
								}
							}
						}
					}
					else if (softwareParameter.getType().equals(SoftwareParameterType.bool))
					{
						if (inputValue.toString().equalsIgnoreCase("true") 
								|| inputValue.toString().equals("1") 
								|| inputValue.toString().equalsIgnoreCase("on")) 
						{
							inputValue = new Integer(1);
						} 
						else if (inputValue.toString().equalsIgnoreCase("false") 
								|| inputValue.toString().equals("0") 
								|| inputValue.toString().equalsIgnoreCase("off")) 
						{
							inputValue = new Integer(0);
						}
						else
						{
							throw new JobProcessingException(400,
									"Invalid parameter value for " + softwareParameter.getKey() + 
									". Value must be a boolean value. Use 1,0 or true/false as available values.");
						}
					}
					else if (softwareParameter.getType().equals(SoftwareParameterType.number))
					{
						try {
							if (NumberUtils.isDigits(inputValue.toString())) {
								inputValue = new Long(inputValue.toString());
							} else if (NumberUtils.isNumber(inputValue.toString())) {
								inputValue = new BigDecimal(inputValue.toString()).toPlainString();
							}
						} catch (NumberFormatException e) {
							throw new JobProcessingException(400,
									"Invalid parameter value for " + softwareParameter.getKey() + 
									". Value must be a number.");
						}
						
						if (!StringUtils.isEmpty(softwareParameter.getValidator()) && !Pattern.matches(softwareParameter.getValidator(), inputValue.toString())) {
							throw new JobProcessingException(400,
									"Invalid parameter value for " + softwareParameter.getKey() + 
									". Value must match the following expression: \"" +
									softwareParameter.getValidator() + "\"");
						}
					}
					else
					{
						if (!StringUtils.isEmpty(softwareParameter.getValidator()))
						{
//							Regex r = new Regex();
//							r.compile(softwareParameter.getValidator());
//							if (!r.search(inputValue.toString())) 
//							{
							if (!Pattern.matches(softwareParameter
									.getValidator(), inputValue.toString()))
							{
								throw new JobProcessingException(400,
										"Invalid parameter value for " + softwareParameter.getKey() + 
										". Value must match the following expression: \"" +
										softwareParameter.getValidator() + "\"");
							}
						}
					}
				}
			}
			catch (JobProcessingException e) {
				throw e;
			}
			catch (Exception e)
			{
				throw new JobProcessingException(500,
						"Failed to parse parameter for "+ softwareParameter.getKey());
			}

			parameterTable.put(softwareParameter.getKey(), inputValue);
		}
		
		/***************************************************************************
		 **						Verifying archive configuration					  **
		 ***************************************************************************/
		
		Boolean archive = Boolean.FALSE;
		String archivePath = null;
		RemoteSystem archiveSystem = null;
		
		if (pTable.containsKey("archive"))
		{
			if (!StringUtils.isEmpty(pTable.get("archive"))) 
			{
				if (pTable.get("archive").equalsIgnoreCase("true") || pTable.get("archive").equals("1") 
						|| pTable.get("archive").equalsIgnoreCase("on")) {
					archive = Boolean.TRUE;
				} 
			}

			if (archive)
			{
				if (pTable.containsKey("archiveSystem")) 
				{
					// lookup the user system
					String archiveSystemId = pTable.get("archiveSystem");
					archiveSystem = new SystemDao().findUserSystemBySystemId(username, archiveSystemId, RemoteSystemType.STORAGE);
					if (archiveSystem == null) {
						throw new JobProcessingException(400,
								"No storage system found matching archiveSystem = '" + archiveSystem + "' for " + username);
					}
				}
				else
				{
					// grab the user's default storage system
					archiveSystem = systemManager.getUserDefaultStorageSystem(username);
				}
				
				if (pTable.containsKey("archivePath") && !StringUtils.isEmpty(pTable.get("archivePath")))
				{
					archivePath = pTable.get("archivePath");
					
					if (!archivePath.startsWith("/"))
						archivePath = "/" + archivePath;
						
					RemoteDataClient remoteDataClient = null;
					try 
					{
						remoteDataClient = archiveSystem.getRemoteDataClient(internalUsername);
						remoteDataClient.authenticate();
						
						LogicalFile logicalFile = LogicalFileDao.findByUserSystemPath(archiveSystem, username, archivePath);
		                PermissionManager pm = new PermissionManager(archiveSystem, remoteDataClient, logicalFile, username);
		                
						if (!pm.canWrite(archivePath)) 
						{
							throw new JobProcessingException(403,
									"User does not have permission to access the provided archive path " + archivePath);
						} 
						else 
						{ 
							if (!remoteDataClient.doesExist(archivePath))
							{
								remoteDataClient.mkdirs(archivePath);
								if (remoteDataClient.isPermissionMirroringRequired() && StringUtils.isEmpty(internalUsername)) {
									remoteDataClient.setOwnerPermission(username, archivePath, true);
								}
							}
							else
							{
								if (!remoteDataClient.isDirectory(archivePath))
								{
									throw new JobProcessingException(400,
											"Archive path is not a folder");
								}
							}
						}
					} 
					catch (JobProcessingException e) {
						throw e;
					} 
					catch (RemoteDataException e) {
						int httpcode = 500;
						if (e.getMessage().contains("No credentials associated")) {
							httpcode = 400;
						}
						throw new JobProcessingException(httpcode, e.getMessage(), e);
					} 
					catch (Exception e) {
						throw new JobProcessingException(500, "Could not verify archive path");
					} 
					finally {
						try { remoteDataClient.disconnect(); } catch (Exception e) {}
					}
				}
			} 
			else
			{
				// grab the user's default storage system
				archiveSystem = systemManager.getUserDefaultStorageSystem(username);
			}
		}
		else
		{
			archive = Boolean.TRUE;
			
			// grab the user's default storage system
			archiveSystem = systemManager.getUserDefaultStorageSystem(username);
		}
		
		/***************************************************************************
		 **					Persist job and assign archive path					  **
		 ***************************************************************************/
		
		try
		{
			// create a job object
			Job job = new Job();
			job.setName(name);
			job.setOwner(username);
			job.setSoftwareName(software.getUniqueName());
			job.setStatus(JobStatusType.PENDING, JobStatusType.PENDING.getDescription());
			job.setInternalUsername(internalUsername);
			job.setSystem(software.getExecutionSystem().getSystemId());
			job.setBatchQueue(jobQueue.getName());
			job.setNodeCount(nodeCount);
			job.setProcessorsPerNode(processorsPerNode);
			job.setMemoryPerNode(memoryPerNode);
			job.setMaxRunTime(requestedTime);
			// bridget between the old callback urls and the new multiple webhook support.
			for (Notification n: notifications) {
				job.addNotification(n);
			}
			job.setArchiveOutput(archive);
			job.setArchivePath(archivePath);
			job.setArchiveSystem(archiveSystem);
			job.setInputsAsMap(inputTable);
			job.setParametersAsMap(parameterTable);
			job.setSubmitTime(new Date());

			// persisting the job makes it available to the job queue
			// for submission
			JobDao.persist(job);
			
			if (job.getId() >= 0)
			{
				// now that we have a job id, assign an archive path, if not already present
				if (StringUtils.isEmpty(archivePath)) {
					job.setArchivePath(job.getOwner() + "/archive/jobs/job-" + job.getUuid());
				}
				
				JobDao.persist(job);
				
				return job;
			}
			else
			{
				throw new JobProcessingException(500,
						"Failed to submit job request to the queue.");
			}

		}
		catch (JobProcessingException e) {
			throw e;
		}
		catch (Exception e)
		{
			throw new JobProcessingException(500, e.getMessage(), e);
		}
	}
	
	/** 
	 * Finds queue on the given executionSystem that supports the given number of nodes and 
	 * memory per node given. 
	 * 
	 * @param nodes a positive integer value or -1 for no limit
	 * @param processors positive integer value or -1 for no limit 
	 * @param memory memory in GB or -1 for no limit
	 * @param requestedTime time in hh:mm:ss format
	 * @return a BatchQueue matching the given parameters or null if no match can be found
	 */
	public static BatchQueue selectQueue(ExecutionSystem executionSystem, Long nodes, Double memory, String requestedTime)
	{

		return selectQueue(executionSystem, nodes, memory, (long)-1, requestedTime);
	}
	
	/** 
	 * Finds queue on the given executionSystem that supports the given number of nodes and 
	 * memory per node given. 
	 * 
	 * @param nodes a positive integer value or -1 for no limit
	 * @param processors positive integer value or -1 for no limit 
	 * @param memory memory in GB or -1 for no limit
	 * @param requestedTime time in hh:mm:ss format
	 * @return a BatchQueue matching the given parameters or null if no match can be found
	 */
	public static BatchQueue selectQueue(ExecutionSystem executionSystem, Long nodes, Double memory, Long processors, String requestedTime)
	{

		if (validateBatchSubmitParameters(executionSystem.getDefaultQueue(), nodes, processors, memory, requestedTime))
		{
			return executionSystem.getDefaultQueue();
		}
		else
		{
			BatchQueue[] queues = executionSystem.getBatchQueues().toArray(new BatchQueue[]{});
			Arrays.sort(queues);
			for (BatchQueue queue: queues)
			{
				if (queue.isSystemDefault())
					continue;
				else if (validateBatchSubmitParameters(queue, nodes, processors, memory, requestedTime))
					return queue;
			}
		}
		
		return null;
	}

	
	/**
	 * Validates that the queue supports the number of nodes, processors per node, memory and
	 * requestedTime provided. If any of these values are null or the given values exceed the queue
	 * limits, it returns false.
	 *
	 * @param queue the BatchQueue to check against
	 * @param nodes a positive integer value or -1 for no limit
	 * @param processors positive integer value or -1 for no limit 
	 * @param memory memory in GB or -1 for no limit
	 * @param requestedTime time in hh:mm:ss format
	 * @return true if all the values are non-null and within the limits of the queue
	 */
	public static boolean validateBatchSubmitParameters(BatchQueue queue, Long nodes, Long processors, Double memory, String requestedTime)
	{
		if (queue == null || 
			nodes == null ||  nodes == 0 || nodes < -1 || 
			processors == null || processors == 0 || processors < -1 ||
			memory == null || memory == 0 || memory < -1 ||
			StringUtils.isEmpty(requestedTime) || StringUtils.equals("00:00:00", requestedTime))
		{
			return false;
		}
		
		if (queue.getMaxNodes() > 0 && queue.getMaxNodes() < nodes) {
			return false;
		}
		
		if (queue.getMaxProcessorsPerNode() > 0 && queue.getMaxProcessorsPerNode() < processors) {
			return false;
		}
		
		if (queue.getMaxMemoryPerNode() > 0 && queue.getMaxMemoryPerNode() < memory) {
			return false;
		}
			
		if (queue.getMaxRequestedTime() != null && 
				org.iplantc.service.systems.util.ServiceUtils.compareRequestedJobTimes(queue.getMaxRequestedTime(), requestedTime) == -1)
				
		{
			return false;
		}
			
		return true;
	}

	/**
	 * Returns a map of all inputs needed to run the job comprised of the user-supplied 
	 * inputs as well as the default values for hidden and unspecified, but required inputs.
	 * This is needed during staging and job submission because the original job submission
	 * may not contain all the inputs actually needed to run the job depending on whether
	 * or not there are hidden fields in the app description.
	 *  
	 * @param job
	 * @return
	 * @throws JobException 
	 * 
	 * @deprecated
	 */
	public static Map<String, String> getJobInputMap(Job job) throws JobException {
		try
		{
			Map<String, String> jobInputMap = job.getInputsAsMap();
			Software software = SoftwareDao.getSoftwareByUniqueName(job.getSoftwareName());
			for (SoftwareInput input: software.getInputs()) {
				if (!jobInputMap.containsKey(input.getKey()) && !input.isVisible()) {
					jobInputMap.put(input.getKey(), input.getDefaultValue());
				}
			}
			
			return jobInputMap;
		} 
		catch (Throwable e) 
		{
			throw new JobException("Unable to parse job and application inputs", e);
		}
		
	}

}