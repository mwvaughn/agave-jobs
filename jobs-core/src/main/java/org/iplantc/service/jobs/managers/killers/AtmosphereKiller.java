package org.iplantc.service.jobs.managers.killers;

import java.io.IOException;

import org.iplantc.service.apps.dao.SoftwareDao;
import org.iplantc.service.apps.model.Software;
import org.iplantc.service.jobs.exceptions.JobException;
import org.iplantc.service.jobs.model.Job;
import org.iplantc.service.remote.RemoteSubmissionClient;
import org.iplantc.service.remote.RemoteSubmissionClientFactory;

public class AtmosphereKiller implements JobKiller {

	private Job	job;

	public AtmosphereKiller(Job job)
	{
		this.job = job;
	}

	@Override
	public void attack() throws JobException, IOException
	{

		RemoteSubmissionClient client = null;
		try
		{
			Software software = SoftwareDao.getSoftwareByUniqueName(job.getSoftwareName());
			client = new RemoteSubmissionClientFactory().getInstance(
					software.getExecutionSystem(), 
					job.getInternalUsername());
			client.runCommand("kill -9 " + job.getLocalJobId());
		}
		catch (IOException e) {
			throw e;
		}
		catch (Exception e)
		{
			throw new JobException("Failed to kill job " + job.getId(), e);
		}
	}

}
