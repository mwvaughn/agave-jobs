/**
 * 
 */
package org.iplantc.service.jobs.model.scripts;

import org.apache.commons.lang3.StringUtils;
import org.iplantc.service.jobs.model.Job;

/**
 * Concreate class for SGE batch submit scripts.
 * 
 * @author dooley
 * 
 */
public class TorqueSubmitScript extends AbstractSubmitScript {

	/**
	 * 
	 */
	public TorqueSubmitScript(Job job)
	{
		super(job);
	}

	/**
	 * Serializes the object into a PBS submit script. Assumption made are that
	 * for PTHREAD applications, the processor value is the number of cores per
	 * node. i.e. 1 node, N cores. For serial jobs, an entire node is requested.
	 * For parallel applications, half the processor value of nodes is requested
	 * with two cores per node.
	 */
	public String getScriptText()
	{
		// #!/bin/bash
		// #PBS -q batch
		// # the queue to be used.
		// #
		// #PBS -A your_allocation
		// # specify your project allocation
		// #
		// #PBS -l nodes=1:ppn=32
		// # number of nodes and number of processors on each node to be used.
		// # Do NOT use ppn = 1. Note that there are 32 procs on each Trestles node
		// # with 64GB memory/nodegoing 
		// #
		// #PBS -l cput=20:00:00
		// # requested CPU time.
		// #
		// #PBS -l walltime=20:00:00
		// # requested Wall-clock time hh:mm:ss
		// #
		// #PBS -o myoutput2
		// # name of the standard out file to be "output-file".
		// #
		// #PBS -j oe
		// # standard error output merge to the standard output file.
		// #
		// #PBS -N s_type
		// # name of the job (that will appear on executing the qstat command).
		// #
		// # Following are non PBS commands. PLEASE ADOPT THE SAME EXECUTION
		// SCHEME
		// # i.e. execute the job by copying the necessary files from your home
		// directpory
		// # to the scratch space, execute in the scratch space, and copy back
		// # the necessary files to your home directory.
		// #
		// export WORK_DIR=/work/$USER/your_code_directory
		// cd $WORK_DIR
		// # changing to your working directory (we recommend you to use work
		// volume for batch job run)
		// #
		// export NPROCS=`wc -l $PBS_NODEFILE |gawk '//{print $1}'`
		// #
		// date
		// #timing the time job starts
		// #
		
		//String absoluteWorkDir = system.getRemoteDataClient(job.getInternalUsername()).resolvePath(job.getWorkPath());
		String prefix = "#PBS ";
		String result = "#!/bin/bash\n" 
				+ prefix + "-N " + name + "\n"
				+ prefix + "-o " + standardOutputFile + "\n" 
				+ prefix + "-e " + standardErrorFile + "\n" 
				+ prefix + "-l cput=" + time + "\n"
				+ prefix + "-l walltime=" + time + "\n"
				+ prefix + "-q " + queue.getName() + "\n"
				+ prefix + "-l nodes=" + nodes + ":ppn=" + processors + "\n";
				if (!StringUtils.isEmpty(queue.getCustomDirectives())) {
					result += prefix + queue.getCustomDirectives() + "\n";
				}
		
//		if (job.getSystem().contains("trestles"))
//		{
//			int nodes = (int)(Math.ceil((double) processors / 32.0));
//			int memory = job.getMemoryRequest().intValue();
//			
//			if ((64 * nodes) < memory) 
//			{
//				nodes = (int)Math.ceil((double)memory / 64.0);
//			}
//			result += prefix + "-l nodes=" + nodes + ":ppn=32\n";
//		} 
//		else if (job.getSystem().contains("blacklight"))
//		{
//			int nodes = (int)(Math.ceil((double) processors / 16.0)) * 16;
//			result += prefix + "-l ncpus=" + nodes + "\n";
//		}
//		
//		if (!StringUtils.isEmpty(system.getDefaultQueue().getCustomDirectives())) {
//			result += system.getDefaultQueue().getCustomDirectives() + "\n";
//		}
		
//		for (String directive : system.getCustomDirectives()) {
//			if (!StringUtils.isEmpty(directive)) {
//				result += prefix + directive + "\n";
//			}
//		}
		
		return result;
	}

}
