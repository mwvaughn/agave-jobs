package org.iplantc.service.jobs.managers.killers;

import java.io.IOException;

import org.iplantc.service.jobs.exceptions.JobException;
import org.iplantc.service.jobs.model.Job;
import org.iplantc.service.remote.RemoteSubmissionClient;
import org.iplantc.service.systems.dao.SystemDao;
import org.iplantc.service.systems.model.ExecutionSystem;
import org.iplantc.service.systems.model.enumerations.RemoteSystemType;
import org.iplantc.service.systems.model.enumerations.SystemStatusType;

public class CondorKiller implements JobKiller {

	private Job	job;

	public CondorKiller(Job job)
	{
		this.job = job;
	}

	@Override
	public void attack() throws JobException, IOException
	{

		RemoteSubmissionClient remoteSubmissionClient = null;
		
		try
		{
			// if the system is down throw an exception to let the user know
			ExecutionSystem system = (ExecutionSystem)new SystemDao().findUserSystemBySystemId(job.getOwner(), job.getSystem(), RemoteSystemType.EXECUTION);
			
			if (system == null || !system.isAvailable() || !system.getStatus().equals(SystemStatusType.UP))
			{
				throw new JobException("Failed to stop remote job " + job.getLocalJobId() + 
						". System " + system.getName() + " is not available."); 
			}
			
			remoteSubmissionClient = system.getRemoteSubmissionClient(job.getInternalUsername());
			
			String result = remoteSubmissionClient.runCommand(system.getScheduler().getBatchKillCommand() + " " + job.getLocalJobId());
			if (!result.contains("has been marked for removal")) 
			{ 
				throw new JobException(
					"Failed to stop remote job " + job.getLocalJobId() + " on " + job.getSystem() + 
					". Condor refused the request.\\n" + result); 
			}

		}
		catch (IOException e) {
			throw e;
		}
		catch (JobException e) {
			throw e;
		}
		catch (Exception e)
		{
			throw new JobException("Failed to kill job " + job.getUuid(), e);
		} finally {
			try { remoteSubmissionClient.close(); } catch (Exception e) {}
		}
	}

}
