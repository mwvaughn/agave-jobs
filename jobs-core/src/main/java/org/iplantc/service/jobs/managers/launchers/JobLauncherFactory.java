/**
 * 
 */
package org.iplantc.service.jobs.managers.launchers;

import org.apache.log4j.Logger;
import org.iplantc.service.apps.dao.SoftwareDao;
import org.iplantc.service.apps.model.Software;
import org.iplantc.service.common.exceptions.NotificationException;
import org.iplantc.service.jobs.exceptions.JobException;
import org.iplantc.service.jobs.model.Job;
import org.iplantc.service.jobs.util.EmailMessage;
import org.iplantc.service.systems.model.enumerations.ExecutionType;
import org.iplantc.service.transfer.RemoteDataClient;

/**
 * @author dooley
 * 
 */
public class JobLauncherFactory 
{
	private static final Logger log = Logger.getLogger(JobLauncherFactory.class);
	
	public static AbstractJobLauncher getInstance(Job job) throws JobException
	{
		Software software = SoftwareDao.getSoftwareByUniqueName(job.getSoftwareName());

		// Verify the software application is valid and available since it 
		// could have changed status during the time the job was queued up.
		if (software == null) 
		{ 
			throw new JobException(job.getSoftwareName()
				+ " is not a recognized application."); 
		}
		else if (!software.isAvailable())
		{
			throw new JobException("Application is not available for execution"); 
		}
		else 
		{
			RemoteDataClient remoteDataClient = null;
			try {
				
				remoteDataClient = software.getStorageSystem().getRemoteDataClient();
				remoteDataClient.authenticate();
				
				if (software.isPubliclyAvailable())
				{	
					if (!remoteDataClient.doesExist(software.getDeploymentPath()))
					{
						software.setAvailable(false);
						SoftwareDao.persist(software);
						EmailMessage.send("Rion Dooley", 
								"dooley@tacc.utexas.edu", 
								"Public app " + software.getUniqueName() + " is missing.", 
								"While submitting a job, the Job Service noticed that the app bundle " +
								"of the public app " + software.getUniqueName() + " was missing. This " +
								"will impact provenance and could impact experiment reproducability. " +
								"Please restore the application zip bundle from archive and re-enable " + 
								"the application via the admin console.\n\n" +
								"Name: " + software.getUniqueName() + "\n" + 
								"User: " + job.getOwner() + "\n" +
								"Job: " + job.getUuid() + "\n" +
								"Time: " + job.getCreated().toString() + "\n\n");
						throw new JobException("Application executable is missing. Software is not available.");
					} 
				}
				else if (!remoteDataClient.doesExist(software.getDeploymentPath() + '/' + software.getExecutablePath())) 
				{
					software.setAvailable(false);
					SoftwareDao.persist(software);
					throw new JobException("Application executable is missing. Software is not available.");
				}
			} catch (NotificationException e) {
				log.error("Public app bundle for " + software.getUniqueName() + " is missing."); 
			} catch (Exception e) {
				throw new JobException("Unable to verify the availability of the application executable. Software is not available.");
			} 
			finally {
				try { remoteDataClient.disconnect(); } catch(Exception e) {}
			}
		}

		// now submit the job to the target system using the correct launcher.
		if (software.getExecutionSystem().getExecutionType().equals(ExecutionType.HPC))
		{
			return new HPCLauncher(job);
		}
		else if (software.getExecutionSystem().getExecutionType().equals(ExecutionType.CONDOR))
		{
			return new CondorLauncher(job);
		}
		else
		{
			return new CLILauncher(job);
		}
	}
}
