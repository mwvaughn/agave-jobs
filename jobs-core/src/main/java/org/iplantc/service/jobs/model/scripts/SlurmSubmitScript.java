/**
 * 
 */
package org.iplantc.service.jobs.model.scripts;

import org.apache.commons.lang3.StringUtils;
import org.iplantc.service.jobs.model.Job;

/**
 * Concreate class for SLURM batch submit scripts.
 * 
 * @author dooley
 * 
 */
public class SlurmSubmitScript extends AbstractSubmitScript {

	/**
	 * Create a batch submit script for SLURM
	 */
	public SlurmSubmitScript(Job job)
	{
		super(job);
	}

	/**
	 * Serializes the object into a SLURM submit script. Assumptions made are
	 * that the number of nodes used will be the ceiling of the number of 
	 * processors requested divided by 16. For serial jobs, an entire node is requested.
	 */
	public String getScriptText()
	{
		String prefix = "#SBATCH ";
		String result = "#!/bin/bash\n" 
//				+ prefix + "-A " + Settings.IPLANT_CHARGE_NUMBER  + "\n"  // moved to individual system custom directives 
				+ prefix + "-J " + name + "\n"
				+ prefix + "-o " + standardOutputFile + "\n" 
				+ prefix + "-e " + standardErrorFile + "\n" 
				+ prefix + "-t " + time + "\n"
				+ prefix + "-p " + queue.getName() + "\n"
				+ prefix + "-N " + nodes + " -n " + processors + "\n";
				if (!StringUtils.isEmpty(queue.getCustomDirectives())) {
					result += prefix + queue.getCustomDirectives() + "\n";
				}

		//				+ prefix + "-n " + processors + "\n";
			
//		double procsPerNode = Math.floor(system.getQueue(job.getBatchQueue()).getMaxMemoryPerNode() / job.getMemoryRequest());
//		BatchQueue jobQueue = system.getDefaultQueue();
//		
//		if (StringUtils.isEmpty(job.getBatchQueue())) 
//		{
//			if (procsPerNode < 1) {
//				result += prefix + "-p largemem\n";
//				result += prefix + "-n " + processors + "\n";
//			} else {
//				result += prefix + "-p " + system.getDefaultQueue().getName() + "\n";
//				if (procsPerNode > 15) {
//					result += prefix + "-n " + processors + "\n";
//				} else {
//					double totalNodes = Math.ceil(processors / procsPerNode);
//					result += prefix + "-N " + totalNodes + " -n " + procsPerNode;
//				}
//			}
//		} 
//		else 
//		{
//			for(BatchQueue queue: system.getBatchQueues()) {
//				if (StringUtils.equals(queue.getName(), job.getBatchQueue())) {
//					jobQueue = queue;
//					break;
//				}
//			}
//			
//			result += prefix + "-p " + jobQueue.getName() + "\n";
//			result += prefix + "-n " + processors + "\n";
//			
//		}
//		
//		if (!StringUtils.isEmpty(jobQueue.getCustomDirectives())) {
//			result += jobQueue.getCustomDirectives() + "\n";
//		}
		
		return result;
	}

}
