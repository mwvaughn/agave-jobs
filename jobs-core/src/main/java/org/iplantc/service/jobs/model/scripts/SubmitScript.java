/**
 * 
 */
package org.iplantc.service.jobs.model.scripts;

/**
 * @author dooley
 * 
 */
public interface SubmitScript {

	public String getScriptText();
}
