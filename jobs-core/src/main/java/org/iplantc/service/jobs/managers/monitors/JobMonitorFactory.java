/**
 * 
 */
package org.iplantc.service.jobs.managers.monitors;

import org.iplantc.service.apps.dao.SoftwareDao;
import org.iplantc.service.apps.model.Software;
import org.iplantc.service.apps.model.enumerations.ExecutionType;
import org.iplantc.service.jobs.exceptions.RemoteJobMonitoringException;
import org.iplantc.service.jobs.model.Job;

/**
 * Factory class to get monitoring class for different execution
 * system types.
 * 
 * @author dooley
 *
 */
public class JobMonitorFactory {

	public static JobMonitor getInstance(Job job) throws RemoteJobMonitoringException
	{
		Software software = SoftwareDao.getSoftwareByUniqueName(job.getSoftwareName());
		
		if (software == null ) {
			throw new RemoteJobMonitoringException("Job software no longer available");
		} 
		else
		{
			if (software.getExecutionType().equals(ExecutionType.CONDOR)) {
				return new CondorJobMonitor(job);
			} else if (software.getExecutionType().equals(ExecutionType.HPC)) {
				return new HPCMonitor(job);
			} else if (software.getExecutionType().equals(ExecutionType.CLI)) {
				return new ProcessMonitor(job);
			} else {
				throw new RemoteJobMonitoringException("Unable to monitor unknown execution type.");
			}
		}
	}

}
