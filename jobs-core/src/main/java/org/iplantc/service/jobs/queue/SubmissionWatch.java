package org.iplantc.service.jobs.queue;

import org.apache.log4j.Logger;
import org.hibernate.StaleObjectStateException;
import org.iplantc.service.apps.dao.SoftwareDao;
import org.iplantc.service.apps.model.Software;
import org.iplantc.service.common.persistence.TenancyHelper;
import org.iplantc.service.jobs.Settings;
import org.iplantc.service.jobs.dao.JobDao;
import org.iplantc.service.jobs.exceptions.JobException;
import org.iplantc.service.jobs.managers.JobManager;
import org.iplantc.service.jobs.model.Job;
import org.iplantc.service.jobs.model.enumerations.JobStatusType;
import org.iplantc.service.systems.model.enumerations.LoginProtocolType;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * Class to pull a job from the db queue and attempt to submit it to iplant
 * resources using one of the appropriate execution factory instances.
 * 
 * @author dooley
 * 
 */
@DisallowConcurrentExecution
public class SubmissionWatch implements org.quartz.Job 
{
	private static final Logger	log	= Logger.getLogger(SubmissionWatch.class);
	
	public SubmissionWatch() {}

	public void execute(JobExecutionContext context)
			throws JobExecutionException
	{
		// pull the oldest job with JobStatusType.PENDING from the db and submit
		// it to the remote scheduler.
		Job job = null;
		try
		{
			job = JobDao.getNextQueuedJob(JobStatusType.STAGED);

			if (job != null)
			{
				// this is a new thread and thus has no tenant info loaded. we set it up
				// here so things like app and system lookups will stay local to the 
				// tenant
				TenancyHelper.setCurrentTenantId(job.getTenantId());
				TenancyHelper.setCurrentEndUser(job.getOwner());
				
				// if it's a condor job, then it has to be submitted from a condor node.
				// we check to see if this is a submit node. if not, we pass.
				Software software = SoftwareDao.getSoftwareByUniqueName(job.getSoftwareName());
				
				// if the execution system login config is local, then we cannot submit
				// jobs to this system remotely. In this case, a worker will be running
				// dedicated to that system and will submitting jobs locally. All workers
				// other that this will should pass on accepting this job.
				if (software.getExecutionSystem().getLoginConfig().getProtocol().equals(LoginProtocolType.LOCAL) && 
						!Settings.LOCAL_SYSTEM_ID.equals(job.getSystem()))
				{
					return;
				}
				else // otherwise, throw it in remotely
				{
					// mark the job as submitting so no other process claims it
					// note: we should have jpa optimistic locking enabled, so
					// no race conditions should exist at this point.
					JobManager.updateStatus(job, JobStatusType.SUBMITTING, 
							"Attempt [" + (job.getRetries() + 1) + "] Preparing job for execution and staging binaries to execution system");
					
					JobManager.submit(job);
				}
			}
		}
		catch (StaleObjectStateException e) {
			log.debug("Just avoided a job submission race condition from worker " 
					+ context.getTrigger().getDescription());
		}
		catch (Exception e)
		{
			if (job == null)
			{
				log.error("Failed to retrieve job information from db", e);
			}
			else
			{
				log.error("Failed to submit job " + job.getUuid(), e);

				try
				{
					JobManager.updateStatus(job, JobStatusType.FAILED,
							"Failed to submit job " + job.getUuid() + ": "
									+ e.getMessage());
				}
				catch (Exception e1)
				{
					log.error("Failed to update job (" + job.getUuid() + ") "
									+ job.getName() + " status to failed");
					try {
						job.setErrorMessage("Failed to submit job " + job.getUuid() + ": "
										+ e.getMessage());
					}
					catch (JobException e2) {}
				}
			}
		}
	}
}