/**
 * 
 */
package org.iplantc.service.jobs.managers.monitors;

import java.io.File;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.iplantc.service.apps.dao.SoftwareDao;
import org.iplantc.service.apps.model.Software;
import org.iplantc.service.jobs.Settings;
import org.iplantc.service.jobs.dao.JobDao;
import org.iplantc.service.jobs.exceptions.JobException;
import org.iplantc.service.jobs.exceptions.RemoteJobMonitoringException;
import org.iplantc.service.jobs.managers.JobManager;
import org.iplantc.service.jobs.managers.killers.JobKiller;
import org.iplantc.service.jobs.managers.killers.JobKillerFactory;
import org.iplantc.service.jobs.model.Job;
import org.iplantc.service.jobs.model.enumerations.JobStatusType;
import org.iplantc.service.jobs.util.CondorJobLogParser;
import org.iplantc.service.remote.RemoteSubmissionClient;
import org.iplantc.service.systems.dao.SystemDao;
import org.iplantc.service.systems.model.ExecutionSystem;
import org.iplantc.service.transfer.RemoteDataClient;
import org.iplantc.service.transfer.exceptions.RemoteDataException;

/**
 * @author dooley
 *
 */
public class CondorJobMonitor extends AbstractJobMonitor 
{
	private static final Logger log = Logger.getLogger(CondorJobMonitor.class);
	
	public CondorJobMonitor(Job job)
	{
		super(job);
	}

	@Override
	public void updateStatus() throws RemoteJobMonitoringException
	{
		File retrievedCondorLogFile = null;
//		File tempAppDir;
        String remoteLogFilePath;
        RemoteDataClient client = null;
        CondorJobLogParser condorJobLogParser;
        File monitoringDir = null;
        
        try 
        { 
        	Software software = SoftwareDao.getSoftwareByUniqueName(job.getSoftwareName());
	        
        	//finished has to be determined by log file in tempAppDir
//            boolean jobDone = false;

            remoteLogFilePath = job.getWorkPath() + "/runtime.log";
            client = software.getExecutionSystem().getRemoteDataClient(job.getInternalUsername());
            client.authenticate();
            
            job.setStatusChecks(job.getStatusChecks() + 1);
        	
            log.debug("Checking runtime.log on " + client.getHost());
            if (client.doesExist(remoteLogFilePath))
            {
            	monitoringDir = FileUtils.getFile(FileUtils.getTempDirectory(), "job-" + job.getUuid());
            	if (!monitoringDir.exists()) {
            		monitoringDir.mkdirs();
            	}
            	
            	if (!client.doesExist(remoteLogFilePath)) 
            	{
            		// log file is created at submission, so something went wrong
            		JobManager.updateStatus(job, JobStatusType.FAILED, "Log file no longer present on remote system.");
            	}
            	else
            	{
            		client.get(remoteLogFilePath, monitoringDir.getAbsolutePath());
	            	
	            	retrievedCondorLogFile = new File(monitoringDir, "runtime.log");
	            	if (retrievedCondorLogFile.exists()) 
	            	{
	                    condorJobLogParser = new CondorJobLogParser(retrievedCondorLogFile);
	                    // here we know the job is done, so we can update the status.
			            // once we do that, the ArchiveWatch will kick in and handle things.
			            if (condorJobLogParser.isFinished())
			            {
			            	if (job.isArchiveOutput()) {
			            		job.setStatus(JobStatusType.CLEANING_UP, "Job completed execution");
			            	} else {
			            		job.setStatus(JobStatusType.FINISHED, 
			            				"Job completed execution. Skipping archiving at user request.");
			            	}
			            	job.setEndTime(new Date());
			            	job.setLastUpdated(new Date());
			            } 
			            else if (condorJobLogParser.isFailed())
			            {
			            	try {
			            		job.setStatus(JobStatusType.FAILED, "Job failed during execution");
			            		
			            		JobKiller killer = JobKillerFactory.getInstance(job);
			            		int retries = 0;
			            		while (retries < Settings.MAX_SUBMISSION_RETRIES) 
			            		{ 
			            			try 
			            			{
			            				killer.attack();
			            				job.setStatus(JobStatusType.FAILED, "Killed remaining processes and clean up the remote job");
			            				break;
			            			} 
			            			catch (JobException e) {
			            				throw e;
			            			} 
			            			catch (Exception e) {
			            				job.setStatus(job.getStatus(), "Failed to stop job " + job.getUuid() + 
			            						" on attempt " + retries);
			            				retries++;
			            			}
			            		}
			            	} 
			            	catch (Exception e) 
			            	{
			            		job.setStatus(JobStatusType.FAILED, "Job failed during execution and further " +
			            				"attempts to explicitly clean up the process failed.\\n" + e.getMessage());
			            	}
			            }
			            else 
			            {
			                // if job is not done, update the timestamp and move on
			            	job.setLastUpdated(new Date());
			            }
			            JobDao.persist(job);
	            	}
	            	else
	            	{
	            		// if job is not done, update the timestamp and move on
	            		JobManager.updateStatus(job, job.getStatus(), "Failed to retrieve log file to monitor status");
	            	}
            	}
            }
            else // log file not there, so check status in condor_q or condor_monitor
            {
            	
            	RemoteSubmissionClient remoteSubmissionClient = null;
				
				ExecutionSystem system = (ExecutionSystem) new SystemDao().findBySystemId(job.getSystem());
				remoteSubmissionClient = system.getRemoteSubmissionClient(job.getInternalUsername());
				
				String queryCommand = system.getScheduler().getBatchQueryCommand() + " " + job.getLocalJobId();
				
				String result = remoteSubmissionClient.runCommand(queryCommand);
				if (StringUtils.isEmpty(result)) 
				{
					// nothing returned from condor_q, so try condor_history since it will give us info on any job submitted
					result = remoteSubmissionClient.runCommand("condor_history -format '%d' JobStatus " + job.getLocalJobId());
					
					if (StringUtils.isEmpty(result)) {
						JobManager.updateStatus(job, JobStatusType.FAILED, "No record of the job on the remote Condor system. Assuming job deleted forcefully on the Condor system.");
					} 
					else if (StringUtils.equals("0", result)) {
						JobManager.updateStatus(job, JobStatusType.STOPPED, "Job failed to checkpoint. When it starts running, it will start running from the beginning");
					}
					else if (StringUtils.equals("1", result)) {
						JobManager.updateStatus(job, JobStatusType.STOPPED, "Job is current in queue");
					}
					else if (StringUtils.equals("2", result)) {
						JobManager.updateStatus(job, JobStatusType.STOPPED, "Job removed forcefully on the Condor system");
					}
					else if (StringUtils.equals("3", result)) {
						JobManager.updateStatus(job, JobStatusType.STOPPED, "Job removed forcefully on the Condor system");
					}
					else if (StringUtils.equals("4", result)) {
						JobManager.updateStatus(job, JobStatusType.CLEANING_UP, "Job completed, but no callback received");
					}
					else if (StringUtils.equals("5", result)) {
						JobManager.updateStatus(job, JobStatusType.PAUSED, "Job paused");
					}
					else if (StringUtils.equals("6", result)) {
						JobManager.updateStatus(job, JobStatusType.FAILED, "Job failed during submission by the Condor server");
					}
				} 
				else 
				{
					// condor_q returned info. now we just need to parse the numeric value
					if (StringUtils.equals("0", result)) {
						JobManager.updateStatus(job, JobStatusType.QUEUED, "Job failed to checkpoint. When it starts running, it will start running from the beginning");
					}
					else if (StringUtils.equals("1", result)) {
						JobManager.updateStatus(job, JobStatusType.QUEUED, "Job is current in queue");
					}
					else if (StringUtils.equals("2", result)) {
						JobManager.updateStatus(job, JobStatusType.STOPPED, "Job removed forcefully on the Condor system");
					}
					else if (StringUtils.equals("3", result)) {
						JobManager.updateStatus(job, JobStatusType.STOPPED, "Job removed forcefully on the Condor system");
					}
					else if (StringUtils.equals("4", result)) {
						JobManager.updateStatus(job, JobStatusType.CLEANING_UP, "Job completed, but no callback received");
					}
					else if (StringUtils.equals("5", result)) {
						JobManager.updateStatus(job, JobStatusType.PAUSED, "Job paused");
					}
					else if (StringUtils.equals("6", result)) {
						JobManager.updateStatus(job, JobStatusType.FAILED, "Job failed during submission by the Condor server");
					}
				}
            }
        } 
        catch (RemoteDataException e) {
        	throw new RemoteJobMonitoringException("Failed to retrieve remote condor log file for job " + job.getUuid(), e);
        }
        catch (Exception e) 
        {
        	throw new RemoteJobMonitoringException("Failed to retrieve condor job status for job " + job.getUuid(), e);
        }
        finally {
        	FileUtils.deleteQuietly(monitoringDir);
        	try { client.disconnect(); } catch (Exception e) {}
        }
	}

}