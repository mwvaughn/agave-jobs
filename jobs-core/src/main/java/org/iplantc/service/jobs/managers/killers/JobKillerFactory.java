/**
 * 
 */
package org.iplantc.service.jobs.managers.killers;

import org.iplantc.service.apps.dao.SoftwareDao;
import org.iplantc.service.apps.model.Software;
import org.iplantc.service.jobs.exceptions.JobException;
import org.iplantc.service.jobs.model.Job;
import org.iplantc.service.systems.model.enumerations.ExecutionType;

/**
 * Factor to init job killers
 * 
 * @author dooley
 * 
 */
public class JobKillerFactory {

	public static JobKiller getInstance(Job job) throws JobException
	{
		Software software = SoftwareDao.get(job.getSoftwareName());

		if (software == null) { 
			//throw new JobException(job.getSoftwareName()
			//		+ " is not a recognized application."); 
			// we're only supporting hpc jobs now.
			return new HPCKiller(job);
		}
		
		if (software.getExecutionSystem() == null) {
			// should not be possible
			throw new JobException("No execution host specified. Unable to kill the job");
		}
		else if (software.getExecutionSystem().getExecutionType().equals(ExecutionType.CONDOR))
		{ 
			// serial hpc app
			return new CondorKiller(job);
		}
		else if (software.getExecutionSystem().getExecutionType().equals(ExecutionType.CLI) ||
				software.getExecutionSystem().getExecutionType().equals(ExecutionType.ATMOSPHERE))
		{ 
			// serial hpc app
			return new AtmosphereKiller(job);
		}
		else
		{
			return new HPCKiller(job);
		}
//		
//			if (software.getParallelism().equals(ParallelismType.PARALLEL))
//			{
//				return new HPCKiller(job);
//			}
//			else if (software.getParallelism().equals(ParallelismType.SERIAL))
//			{
//				if (software.getExecutionType().equals(ExecutionType.HPC))
//				{
//					return new HPCKiller(job);
//				}
//				else if (software.getExecutionType().equals(
//						ExecutionType.ATMOSPHERE))
//				{
//					return new AtmosphereKiller(job);
//				}
//				else
//				{
//					throw new JobException(
//							"Unsupported execution type for software "
//									+ job.getUuid());
//				}
//			}
//			else
//			{
//				throw new JobException("Unsupported application type for job "
//						+ job.getUuid());
//			}
//		}
//		else if (!software.getExecutionSystem().getScheduler().equals(SchedulerType.UNKNOWN))
//		{ 
//			// serial hpc app
//			return new HPCKiller(job);
//		}
//		else
//		{
//			return new AtmosphereKiller(job);
//		}
	}
}
