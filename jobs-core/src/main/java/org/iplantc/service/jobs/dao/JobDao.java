/**
 * 
 */
package org.iplantc.service.jobs.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.LockOptions;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.StaleObjectStateException;
import org.iplantc.service.common.persistence.HibernateUtil;
import org.iplantc.service.common.persistence.TenancyHelper;
import org.iplantc.service.jobs.exceptions.JobException;
import org.iplantc.service.jobs.model.Job;
import org.iplantc.service.jobs.model.enumerations.JobStatusType;
import org.iplantc.service.jobs.model.enumerations.PermissionType;

/**
 * @author dooley
 * 
 */
public class JobDao
{
	
	protected static Session getSession() {
		HibernateUtil.beginTransaction();
		Session session = HibernateUtil.getSession();
		//session.clear();
		session.enableFilter("jobTenantFilter").setParameter("tenantId", TenancyHelper.getCurrentTenantId());
		return session;
	}
	
	/**
	 * Returns all jobs for a given username.
	 * 
	 * @param username
	 * @return
	 * @throws JobException
	 */
	@SuppressWarnings("unchecked")
	public static List<Job> getByUsername(String username) throws JobException
	{
		try
		{
			Session session = getSession();
			session.clear();
			String sql = "select distinct j.* from jobs j left join job_permissions p on j.id = p.job_id "
					+ "where (j.owner = :owner or (p.username = :owner and " +
							"(p.permission = :read or p.permission = :readwrite))) and j.visible = :visible " +
							"and j.tenant_id = :tenantid "
					+ "order by j.id desc";
			
			List<Job> jobs = session.createSQLQuery(sql).addEntity(Job.class)
					.setString("owner",username)
					.setParameter("read",PermissionType.READ_PERMISSION)
					.setParameter("readwrite", PermissionType.READ_WRITE_PERMISSION)
					.setBoolean("visible", Boolean.TRUE)
					.setString("tenantid", TenancyHelper.getCurrentTenantId())
					.list();
			
			session.flush();
			
			return jobs;

		}
		catch (ObjectNotFoundException e) {
			return new ArrayList<Job>();
		}
		catch (HibernateException ex)
		{
			throw new JobException(ex);
		}
		finally {
			try { HibernateUtil.commitTransaction();} catch (Exception e) {}
		}
	}

	/**
	 * Gets a job by its unique id.
	 * @param jobId
	 * @return
	 * @throws JobException
	 */
	public static Job getById(long jobId) throws JobException
	{
		try
		{
			Session session = getSession();
			session.disableFilter("jobTenantFilter");
			Job job = (Job) session.get(Job.class, jobId);
			
			session.flush();
			
			return job;

		}
		catch (ObjectNotFoundException ex)
		{
			return null;
		}
		catch (HibernateException ex)
		{
			throw new JobException(ex);
		}
		finally {
			try { HibernateUtil.commitTransaction();} catch (Exception e) {}
		}
	}
	
	/**
	 * Gets a job by its uuid.
	 * @param uuid
	 * @return Job object
	 * @throws JobException
	 */
	public static Job getByUuid(String uuid) throws JobException
	{
		try
		{
			Session session = getSession();
			session.clear();
			Job job = (Job) session.createQuery("from Job j where j.uuid = :uuid")
					.setString("uuid",  uuid)
					.uniqueResult();
			
			session.flush();
			
			return job;

		}
		catch (ObjectNotFoundException ex)
		{
			return null;
		}
		catch (HibernateException ex)
		{
			throw new JobException(ex);
		}
		finally {
			try { HibernateUtil.commitTransaction();} catch (Exception e) {}
		}
	}
	
	/**
	 * Refreshes a stale job
	 * @param job
	 * @return
	 * @throws JobException
	 */
	public static void refresh(Job job) throws JobException
	{
		try
		{
			Session session = getSession();
			session.clear();
			session.refresh(job);
			session.flush();
		}
		catch (HibernateException ex)
		{
			throw new JobException(ex);
		}
		finally {
			try { HibernateUtil.commitTransaction();} catch (Exception e) {}
		}
	}
	
	/**
	 * Returns a job owned by the user with teh given job id. This is essentially
	 * just a security check to make sure the owner matches the job id. ACL are 
	 * not taken into consideration here, just ownership.
	 * 
	 * @param username
	 * @param jobId
	 * @return
	 * @throws JobException
	 */
	public static Job getByUsernameAndId(String username, long jobId) throws JobException
	{
		try
		{
			Session session = getSession();
			session.clear();
			String hql = "from Job where owner = :owner and id = :jid and visible = :visible";
			Job job = (Job)session.createQuery(hql)
					.setString("owner",username)
					.setLong("jid", jobId)
					.setBoolean("visible", Boolean.TRUE)
					.setMaxResults(1)
					.uniqueResult();
			
			session.flush();
			
			if (job == null) {
				throw new JobException("No job found matching the given owner and id.");
			} else {
				return job;
			}

		}
		catch (ObjectNotFoundException ex)
		{
			return null;
		}
		catch (HibernateException ex)
		{
			throw new JobException(ex);
		}
		finally {
			try { HibernateUtil.commitTransaction();} catch (Exception e) {}
		}
	}


    /**
     * Gets the least recently updated job for a given system id with the given
     * status. 
     * @param system
     * @param jobStatus
     * @return
     * @throws JobException
     */
    public static Job getNextJobOnSystemByStatus(String system, JobStatusType jobStatus) throws JobException
    {
        try
        {
        	Session session = getSession();
        	session.clear();
			String sql = "select distinct * from jobs where (execution_system = :system" +
                    " and status = :status) order by last_updated desc";

            Job job = (Job) session.createSQLQuery(sql).addEntity(Job.class)
                    .setParameter("system",system)
                    .setParameter("status", jobStatus.toString())
                    .setMaxResults(1)
                    .uniqueResult();

            session.flush();
            
            return job;
        } 
        catch (HibernateException ex) {
            throw new JobException(ex);
        }
        finally {
			try { HibernateUtil.commitTransaction();} catch (Exception e) {}
		}
	}
    
	/**
	 * Returns a set of jobs belonging to the given user with the given status.
	 * Permissions are observed in this query.
	 * 
	 * @param username
	 * @param jobStatus
	 * @return
	 * @throws JobException
	 */
	@SuppressWarnings("unchecked")
	public static List<Job> getByUsernameAndStatus(String username, JobStatusType jobStatus) 
	throws JobException
	{
		try
		{
			Session session = getSession();
			session.clear();
			String sql = "select distinct j.* from jobs j " +
					"left join job_permissions p on j.id = p.job_id " +
					"where (j.owner = :owner or " +
					"			(p.username = :owner and " +
					"				(p.permission = :read or " +
					"					p.permission = :readpem or " +
					"					p.permission = :readwrite or " +
					"					p.permission = :readwritepem) " +
					"				) " +
					"			) and " +
					"		j.visible = :visible and " +
					"		j.status = :status " + 
					"order by j.last_updated desc ";
			
			List<Job> jobs = session.createSQLQuery(sql).addEntity(Job.class)
					.setString("owner",username)
					.setString("read",PermissionType.READ_PERMISSION.name())
					.setString("readpem",PermissionType.READ.name())
					.setString("readwrite", PermissionType.READ_WRITE_PERMISSION.name())
					.setString("readwritepem", PermissionType.READ_WRITE.name())
					.setBoolean("visible", true)
					.setString("status", jobStatus.name())
					.list();

			session.flush();
			
			return jobs;

		}
		catch (ObjectNotFoundException e) {
			return new ArrayList<Job>();
		}
		catch (HibernateException ex)
		{
			throw new JobException(ex);
		}
		finally {
			try { HibernateUtil.commitTransaction();} catch (Exception e) {}
		}
	}
	
	/**
	 * Returns the number of jobs in an active state for a given user 
	 * on a given system. Active states are defined by 
	 * JobStatusType.getActiveStatuses().
	 * 
	 * @param username
	 * @param system
	 * @return
	 * @throws JobException
	 */
	public static long countActiveUserJobsOnSystem(String username, String system) throws JobException
	{
		try
		{
			Session session = getSession();
			session.clear();
			String hql = "select count(*) from Job where owner = :jobowner " +
					"and system = :jobsystem and status in " +
					"(" + JobStatusType.getActiveStatusValues() + ")";
			
			long currentUserJobCount = ((Long)session.createQuery(hql)
					.setString("jobowner",username)
					.setString("jobsystem", system)
					.uniqueResult()).longValue();
			
			session.flush();
			
			return currentUserJobCount;

		}
		catch (ObjectNotFoundException ex)
		{
			throw new JobException(ex);
		}
		catch (HibernateException ex)
		{
			throw new JobException(ex);
		}
		finally {
			try { HibernateUtil.commitTransaction();} catch (Exception e) {}
		}
	}
	
	/**
	 * Returns the least recently checked active job for monitoring.
	 * 
	 * @return
	 * @throws JobException
	 */
	public static Job getNextActiveJob() throws JobException
	{
		try
		{
			Session session = getSession();
			session.clear();
			String hql = "from Job where "
					+ "lastUpdated > :refreshRate and "
					+ "status in (" + JobStatusType.getActiveStatusValues() + ") " +
					"order by rand()";
			
			Job job = (Job)session.createQuery(hql)
					.setTimestamp("refreshRate", new Date(System.currentTimeMillis() - 300000))
					.setMaxResults(1)
					.uniqueResult();
			
            if (job != null) {
                session.refresh(job, LockOptions.UPGRADE);
            }
			session.flush();
			
			return job;
		}
		catch (StaleObjectStateException ex) {
			return null;
		}
		catch (ObjectNotFoundException ex)
		{
			return null;
		}
		catch (HibernateException ex)
		{
			throw new JobException(ex);
		}
		finally {
			try { HibernateUtil.commitTransaction();} catch (Exception e) {}
		}
	}
	
	/**
	 * Returns the least recently checked active job for monitoring.
	 * 
	 * @return
	 * @throws JobException
	 */
	public static Job getNextRunningJob() throws JobException
	{
		try
		{
			Session session = getSession();
			session.clear();
			String hql = "select * from jobs where current_timestamp >= `last_updated` + INTERVAL format(floor(power(2, (`status_checks` - 1))),0) SECOND and "
					+ "(`status` = :queuedstatus "
					+ "or `status` = :runningstatus "
					+ "or `status` = :pausedstatus "
					+ "or `status` = :pendingstatus) "
					+ "and `local_job_id` is not null "
					+ "order by rand()";
			
			Job job = (Job)session.createSQLQuery(hql)
					.addEntity(Job.class)
					.setString("queuedstatus", JobStatusType.QUEUED.name())
					.setString("runningstatus", JobStatusType.RUNNING.name())
					.setString("pausedstatus", JobStatusType.PAUSED.name())
					.setString("pendingstatus", JobStatusType.PENDING.name())
					.setMaxResults(1)
					.uniqueResult();
			
            if (job != null) {
                session.refresh(job, LockOptions.UPGRADE);
            }
			session.flush();
			
			return job;
		}
		catch (StaleObjectStateException ex) {
			return null;
		}
		catch (ObjectNotFoundException ex)
		{
			return null;
		}
		catch (HibernateException ex)
		{
			throw new JobException(ex);
		}
		finally {
			try { HibernateUtil.commitTransaction();} catch (Exception e) {}
		}
	}
	
	/**
	 * Returns the total number of jobs in an active state 
	 * on a given system. Active states are defined by 
	 * JobStatusType.getActiveStatuses().
	 * 
	 * @param username
	 * @param system
	 * @return jobCount 
	 * @throws JobException
	 */
	public static long countActiveJobsOnSystem(String system) throws JobException
	{
		try
		{
			Session session = getSession();
			session.clear();
			String hql = "select count(*) from Job " +
					"where system = :jobsystem and " +
					"status in (" + JobStatusType.getActiveStatusValues() + ")";
			
			long currentUserJobCount = ((Long)session.createQuery(hql)
					.setString("jobsystem", system)
					.uniqueResult()).longValue();
			
			session.flush();
			
			return currentUserJobCount;

		}
		catch (ObjectNotFoundException ex)
		{
			throw new JobException(ex);
		}
		catch (HibernateException ex)
		{
			throw new JobException(ex);
		}
		finally {
			try { HibernateUtil.commitTransaction();} catch (Exception e) {}
		}
	}

	

	public static void persist(Job job) throws JobException
	{
		if (job == null)
			throw new JobException("Job cannot be null");

		try
		{
			Session session = getSession();
			
			session.saveOrUpdate(job);
			session.flush();
		}
		catch (HibernateException ex)
		{
			try
			{
				if (HibernateUtil.getSession().isOpen())
				{
					HibernateUtil.rollbackTransaction();
					HibernateUtil.getSession().close();
				}
			}
			catch (Exception e)
			{
			}
			throw new JobException(ex);
		}
		finally {
			try { HibernateUtil.commitTransaction();} catch (Exception e) {}
		}
	}

	/**
	 * Deletes a job from the db.
	 * @param job
	 * @throws JobException
	 */
	public static void delete(Job job) throws JobException
	{
		if (job == null)
			throw new JobException("Job cannot be null");

		try
		{
			Session session = getSession();
			session.clear();
			session.delete(job);
			session.flush();
		}
		catch (HibernateException ex)
		{
			try
			{
				if (HibernateUtil.getSession().isOpen()) {
					HibernateUtil.rollbackTransaction();
				}
			}
			catch (Throwable e) {}
				
			throw new JobException(ex);
		}
		finally {
			try { HibernateUtil.commitTransaction();} catch (Exception e) {}
		}
	}

	/**
	 * Finds next most recent job in the database with the given
	 * status.
	 * 
	 * @param status
	 * @return
	 * @throws JobException
	 */
	public static Job getNextQueuedJob(JobStatusType status)
			throws JobException
	{
		try
		{
			Session session = getSession();
			session.clear();
			session.disableFilter("jobTenantFilter");
			
			String sql = "select * from jobs j left join (select jc.owner, jc.execution_system, sys.max_system_jobs, count(id) as system_job_count " + 
					"from jobs jc left join (select s.system_id, ex.max_system_jobs from systems s left join executionsystems ex on s.id = ex.id) as sys on jc.execution_system = sys.system_id " + 
					" 	where jc.status in ( " + JobStatusType.getActiveStatusValues() + " )  " + 
					" 	group by jc.owner, jc.execution_system ) as jj on j.owner = jj.owner and j.execution_system = jj.execution_system " + 
					"where j.status = :jobstatus and (jj.system_job_count is NULL or jj.system_job_count < jj.max_system_jobs) " + 
					"order by rand()";
			
			Job job = (Job) session.createSQLQuery(sql).addEntity(Job.class)
					.setString("jobstatus", status.name())
					.setMaxResults(1)
					.uniqueResult();

			session.flush();
			
			return job;
			
		}
		catch (StaleObjectStateException ex) {
			return null;
		}
		catch (ObjectNotFoundException ex)
		{
			return null;
		}
		catch (HibernateException ex)
		{
			throw new JobException(ex);
		}
		finally {
			try { HibernateUtil.commitTransaction();} catch (Exception e) {}
		}
	}

	/**
	 * Performs a string replacement and update on the job with the 
	 * given id. This is only needed because job inputs are not a 
	 * separate table.
	 * 
	 * @param jobId
	 * @param source
	 * @param dest
	 * @throws JobException
	 */
	public static void updateInputs(long jobId, String source, String dest)
	throws JobException
	{
		try
		{
			Session session = getSession();
			session.clear();
			String sql = "update jobs set inputs = replace(inputs, :source, :dest)";
			session.createQuery(sql).setString("source", source).setString(
					"dest", dest).executeUpdate();
			
			session.flush();
		}
		catch (HibernateException ex)
		{
			throw new JobException(ex);
		}
		finally {
			try { HibernateUtil.commitTransaction();} catch (Exception e) {}
		}

	}

	/**
	 * Searches for jobs by the given user who matches the given set of 
	 * parameters. Permissions are honored in this query.
	 * 
	 * @param username
	 * @param queryParameters Hashtable of key value pairs by which to query.
	 * @return
	 * @throws JobException
	 */
	@SuppressWarnings("unchecked")
	public static List<Job> findMatching(String username,
			Hashtable<String, String> queryParameters) throws JobException
	{
		try
		{
			Session session = getSession();
			session.clear();
			String sql = "select distinct j.* from jobs j " +
					"left join job_permissions p on j.id = p.job_id " +
					"where (j.owner = :owner or " +
					"			(p.username = :owner and " +
					"				(p.permission = :read or " +
					"					p.permission = :readpem or " +
					"					p.permission = :readwrite or " +
					"					p.permission = :readwritepem) " +
					"				) " +
					"			) and " +
					"		j.visible = :visible and " +
					"		j.tenant_id = :tenantid "; 
			
			for(String attribute: queryParameters.keySet()) {
				sql += "		and " + attribute + " = :" + attribute + " ";
			}
			
			sql +=	" order by j.last_updated desc";
			
			SQLQuery query = session.createSQLQuery(sql);
		 	query.addEntity(Job.class)
				.setString("owner",username)
				.setString("read",PermissionType.READ.name())
				.setString("readpem",PermissionType.READ_PERMISSION.name())
				.setString("readwritepem", PermissionType.READ_WRITE_PERMISSION.name())
				.setString("readwrite", PermissionType.READ_WRITE.name())
				.setString("tenantid", TenancyHelper.getCurrentTenantId())
				.setBoolean("visible", true);
			
			for(String attribute: queryParameters.keySet()) {
				query.setParameter(attribute, queryParameters.get(attribute));
			}
			
			List<Job> jobs = query.list();

			session.flush();
			
			return jobs;

		}
		catch (HibernateException ex)
		{
			throw new JobException(ex);
		}
		finally {
			try { HibernateUtil.commitTransaction();} catch (Exception e) {}
		}
	}

	@SuppressWarnings("unchecked")
	public static List<Job> getAll() throws JobException
	{
		try
		{
			Session session = getSession();
			session.clear();
			List<Job> jobs = (List<Job>)session.createQuery("FROM Job").list();
			
			session.flush();
			
			return jobs;
		}
		catch (HibernateException ex)
		{
			throw new JobException(ex);
		}
		finally {
			try { HibernateUtil.commitTransaction();} catch (Exception e) {}
		}
	}

	public static Integer countTotalActiveJobs() throws JobException
	{
		try
		{
			Session session = getSession();
			session.clear();
			String hql = "select count(*) from Job " +
					"where status in (" + JobStatusType.getActiveStatusValues() + ")";
			
			int currentJobCount = ((Long)session.createQuery(hql)
					.uniqueResult()).intValue();
			
			session.flush();
			
			return currentJobCount;

		}
		catch (Throwable ex)
		{
			throw new JobException(ex);
		}
		finally {
			try { HibernateUtil.commitTransaction();} catch (Exception e) {}
		}
	}

	public static Long countActiveJobsOnSystemQueue(String system, String queueName) 
	throws JobException
	{
		try
		{
			Session session = getSession();
			session.clear();
			String hql = "select count(*) from Job where "
					+ "queue_request = :queuerequest and "
					+ "system = :jobsystem and "
					+ "status in " + "(" + JobStatusType.getActiveStatusValues() + ")";
			
			long currentUserJobCount = ((Long)session.createQuery(hql)
					.setString("queuerequest", queueName)
					.setString("jobsystem", system)
					.uniqueResult()).longValue();
			
			session.flush();
			
			return currentUserJobCount;

		}
		catch (ObjectNotFoundException ex)
		{
			throw new JobException(ex);
		}
		catch (HibernateException ex)
		{
			throw new JobException(ex);
		}
		finally {
			try { HibernateUtil.commitTransaction();} catch (Exception e) {}
		}
	}

	public static Long countActiveUserJobsOnSystemQueue(String owner, String system, String queueName) 
	throws JobException
	{
		try
		{
			Session session = getSession();
			session.clear();
			String hql = "select count(*) from Job where "
					+ "queue_request = :queuerequest and "
					+ "owner = :jobowner and "
					+ "system = :jobsystem and "
					+ "status in " + "(" + JobStatusType.getActiveStatusValues() + ")";
			
			long jobCount = ((Long)session.createQuery(hql)
					.setString("jobowner",owner)
					.setString("queuerequest", queueName)
					.setString("jobsystem", system)
					.uniqueResult()).longValue();
			
			session.flush();
			
			return jobCount;

		}
		catch (ObjectNotFoundException ex)
		{
			throw new JobException(ex);
		}
		catch (HibernateException ex)
		{
			throw new JobException(ex);
		}
		finally {
			try { HibernateUtil.commitTransaction();} catch (Exception e) {}
		}
	}

}
