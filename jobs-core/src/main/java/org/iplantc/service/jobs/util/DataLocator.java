/**
 * 
 */
package org.iplantc.service.jobs.util;

import java.util.ArrayList;
import java.util.List;

import org.iplantc.service.apps.dao.SoftwareDao;
import org.iplantc.service.apps.model.Software;
import org.iplantc.service.apps.model.enumerations.ExecutionType;
import org.iplantc.service.apps.util.ServiceUtils;
import org.iplantc.service.jobs.dao.JobDao;
import org.iplantc.service.jobs.exceptions.JobException;
import org.iplantc.service.jobs.exceptions.MissingDataException;
import org.iplantc.service.jobs.model.FileBean;
import org.iplantc.service.jobs.model.Job;
import org.iplantc.service.jobs.model.enumerations.JobStatusType;
import org.iplantc.service.systems.dao.SystemDao;
import org.iplantc.service.systems.model.RemoteSystem;
import org.iplantc.service.transfer.RemoteDataClient;
import org.iplantc.service.transfer.RemoteFileInfo;
import org.iplantc.service.transfer.Settings;
import org.iplantc.service.transfer.exceptions.RemoteDataException;

/**
 * @author dooley
 * 
 */
public class DataLocator {

	private Job					job;
	private RemoteDataClient 	client;

	/**
	 * 
	 */
	public DataLocator(Job job)
	{
		this.job = job;
	}
	
	public RemoteSystem findOutputSystemForJobData() throws RemoteDataException
	{
		if (job.isFailed() || job.isRunning() || !job.isArchiveOutput() || 
				job.getStatus().equals(JobStatusType.STOPPED))
		{ 	// get files from remote system
			return new SystemDao().findUserSystemBySystemId(job.getOwner(), job.getSystem());
		}
		else
		{ 
			return job.getArchiveSystem();
		}
	}

	public List<FileBean> listOutputDirectory(String path) throws RemoteDataException,
			MissingDataException, JobException
	{
		try {
			if (job.isFailed() || job.isRunning() || !job.isArchiveOutput() || 
					job.getStatus().equals(JobStatusType.STOPPED))
			{ 	// get files from remote system
				Software software = SoftwareDao.getSoftwareByUniqueName(job.getSoftwareName());
	
				if (software == null) { throw new JobException(
						"Failed to find the software record for this job"); }
	
				if (software.getExecutionType().equals(ExecutionType.HPC))
				{ // job is an hpc job, use gridftp
	
					RemoteDataClient client = null;
					RemoteSystem system = null;
					try
					{
						system = software.getExecutionSystem();
						
						client = system.getRemoteDataClient(job.getInternalUsername());
						
						if (ServiceUtils.isValid(job.getWorkPath()))
						{
							client.authenticate();
							
							if (client.doesExist(job.getWorkPath() + path))
							{
								RemoteFileInfo fileInfo = client.getFileInfo(job.getWorkPath() + path);
								
								if (fileInfo.isDirectory()) {
									return parseRemoteListing(client.ls(job.getWorkPath() + path), path, system.getSystemId());
								} else {
									List<RemoteFileInfo> remoteFileList = new ArrayList<RemoteFileInfo>();
									remoteFileList.add(fileInfo);
									return parseRemoteListing(remoteFileList, path, system.getSystemId());
								}
							} else {
								throw new RemoteDataException(
									"Unable to locate job data. Work folder no longer exists.");
							}
						}
						else
						{
							throw new RemoteDataException("No work directory specified for this job. Usually this occurs when a job failed during submission.");
						}
	
					}
					catch (RemoteDataException e)
					{
						throw e;
					}
					catch (Exception e)
					{
						throw new RemoteDataException(
								"Failed to list output folder " + job.getWorkPath() + path
										+ " for job " + job.getUuid(), e);
					}
				}
				else if (software.getExecutionType().equals(ExecutionType.ATMOSPHERE))
				{ // job is an atmo job
					throw new RemoteDataException(
							"Cannot retrieve data from running Atmosphere jobs");
				}
				else if (software.getExecutionType().equals(ExecutionType.CONDOR))
				{
					if (job.isFinished() && !job.isArchiveOutput()) {
						throw new RemoteDataException(
							"Job was not archived and data cannot be retrieved from condor systems.");
					} else {
						throw new RemoteDataException(
							"Data cannot be retrieved from condor systems.");
					}
				}
				else
				{
					throw new RemoteDataException("Unknown execution system type.");
				}
	
			}
			else if (job.isArchiveOutput())
			{ 
				// look in the user's archive
				RemoteDataClient client = null;
				RemoteSystem system = null;
				try
				{
					// no permission checks for the user on the system because those
					// happened at submit time. Once the job runs, their data is 
					// their data and should be accessible after the fact. This may,
					// however change in the future.
					system = job.getArchiveSystem();
					
					if (system == null) {
						throw new RemoteDataException("The archive system for this job " +
								"is not longer available.");
					} 
					else 
					{
						client = system.getRemoteDataClient(job.getInternalUsername());
						
						if (ServiceUtils.isValid(job.getArchivePath()))
						{
							client.authenticate();
							
							if (client.doesExist(job.getArchivePath() + path))
							{
								RemoteFileInfo fileInfo = client.getFileInfo(job.getArchivePath() + path);
								
								if (fileInfo.isDirectory()) {
									return parseRemoteListing(client.ls(job.getArchivePath() + path), path, system.getSystemId());
								} else {
									List<RemoteFileInfo> remoteFileList = new ArrayList<RemoteFileInfo>();
									remoteFileList.add(fileInfo);
									return parseRemoteListing(remoteFileList, path, system.getSystemId());
								}
							} else {
								throw new RemoteDataException(
									"Unable to locate job data. Work folder no longer exists.");
							}
						}
						else
						{
							throw new RemoteDataException("No work directory specified for this job. Usually this occurs when a job failed during submission.");
						}
						
						
//						client.authenticate();
//						return parseRemoteListing(client.ls(job.getArchivePath() + path), path, system.getSystemId());
					}
				}
				catch (Exception e)
				{
					throw new RemoteDataException("Failed to list output folder "
							+ job.getWorkPath() + " for job " + job.getUuid(), e);
				}
			}
			else
			{
				throw new MissingDataException(
						"Unable to locate job data because the job was not archived.");
			}
		
		} finally {
			try { client.disconnect(); } catch (Exception e) {}
		}

	}
	
	private List<FileBean> parseRemoteListing(List<RemoteFileInfo> listing, String path, String systemId)
	{
		List<FileBean> beans = new ArrayList<FileBean>();

		for (RemoteFileInfo entry : listing)
		{
			FileBean bean = new FileBean();
			bean.setName(entry.getName());
			bean.setPath((path + "/" + entry.getName()).replaceAll("//", "/"));
			bean.setOwner(entry.getOwner());
			try
			{
				bean.setLastModified(entry.getLastModified());
			}
			catch (Exception e)
			{
				bean.setLastModified(null);
			}
			
			bean.setLength(entry.getSize());
			bean.setDirectory(entry.isDirectory());
			bean.setReadable(true);
			bean.setWritable(entry.userCanWrite());
			bean.setSystemId(systemId);
			bean.setJobId(job.getUuid());
			bean.setUrl(Settings.IPLANT_JOB_SERVICE + job.getUuid() + "/outputs/media" + bean.getPath());
			bean.setParent(path);

			beans.add(bean);
		}

		return beans;
	}

//	private List<FileBean> parseIrodsListing(
//			List<CollectionAndDataObjectListingEntry> listing, String path)
//	{
//		List<FileBean> beans = new ArrayList<FileBean>();
//
//		for (CollectionAndDataObjectListingEntry entry : listing)
//		{
//			FileBean bean = new FileBean();
//			bean.setName(entry.getPathOrName());
//			bean.setLastModified(entry.getModifiedAt());
//			bean.setLength(entry.getDataSize());
//			bean.setDirectory(entry.isCollection());
//			bean.setOwner(entry.getOwnerName());
//			bean.setPath(entry.getParentPath().replaceAll(Settings.IRODS_STAGING_DIRECTORY, ""));
//			bean.setReadable(true);
//			bean.setUrl(Settings.IPLANT_IO_SERVICE + "io" + job.getArchivePath()
//					+ "/" + bean.getName());
//			bean.setParent(path);
//
//			beans.add(bean);
//		}
//
//		return beans;
//	}
//
//	private List<FileBean> parseGridFTPListing(Vector<MlsxEntry> listing, String path)
//	{
//		List<FileBean> beans = new ArrayList<FileBean>();
//
//		for (MlsxEntry entry : listing)
//		{
//			beans.add(parseMlsxEntryToFileBean(entry, path));
//		}
//
//		return beans;
//	}
//
//	private List<FileBean> parseSFTPListing(
//			List<CollectionAndDataObjectListingEntry> listing, String path)
//	{
//		List<FileBean> beans = new ArrayList<FileBean>();
//
//		return beans;
//	}
//
//	private FileBean parseMlsxEntryToFileBean(MlsxEntry entry, String path)
//	{
//
//		FileBean bean = new FileBean();
//
//		bean.setName(entry.getFileName());
//		bean.setPath((path + "/" + entry.getFileName()).replaceAll("//", "/"));
//
//		try
//		{
//			SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
//			bean.setLastModified(formatter.parse(entry.get("modify")));
//		}
//		catch (Exception e)
//		{
//			bean.setLastModified(null);
//		}
//
//		bean.setReadable(true);
//		bean.setWritable(false);
//		bean.setOwner(job.getOwner());
//		bean.setDirectory(entry.get(MlsxEntry.TYPE).equals(MlsxEntry.TYPE_DIR));
//		bean.setLength(new Long(entry.get(MlsxEntry.SIZE)).longValue());
//		bean.setUrl(Settings.IPLANT_JOB_SERVICE + ("job/" + job.getUuid() + (bean.isDirectory() ? "/output/list" : "/output") 
//				+ path + "/" + entry.getFileName()).replaceAll("//", "/"));
//		bean.setParent(path);
//
//		return bean;
//	}
	
	public static void main(String[] args)
	{
		try 
		{
			Job job = JobDao.getById(64);
			
			DataLocator dl = new DataLocator(job);
			
			for (FileBean bean: dl.listOutputDirectory("/uniq-1.0"))
			{
				System.out.println(bean.toJSON());
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
