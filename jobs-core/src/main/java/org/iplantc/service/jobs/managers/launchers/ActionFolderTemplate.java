package org.iplantc.service.jobs.managers.launchers;

import java.io.File;

import org.iplantc.service.apps.dao.SoftwareDao;
import org.iplantc.service.apps.model.Software;
import org.iplantc.service.apps.util.ServiceUtils;
import org.iplantc.service.jobs.managers.JobManager;
import org.iplantc.service.jobs.model.Job;
import org.iplantc.service.systems.model.ExecutionSystem;
import org.iplantc.service.systems.model.enumerations.SchedulerType;

class ActionFolderTemplate {
	private String	baseUrl;
	private Job		job;

	public ActionFolderTemplate(Job job, String baseUrl)
	{
		this.job = job;
		this.baseUrl = baseUrl;
	}

	public String toString()
	{
		Software software = SoftwareDao.getSoftwareByUniqueName(job.getSoftwareName());
		ExecutionSystem system = software.getExecutionSystem();
		
		return ServiceUtils.getContents(
				new File(JobManager.class.getClassLoader().getResource(
						"submit_action.template").getPath())).replaceAll(
				"CALLBACK_URL", baseUrl).replaceAll("IPLANT_JOB_ID",
				job.getUuid() + "").replaceAll("IPLANT_JOB_TOKEN",
				job.getUpdateToken()).replaceAll("SCHEDULER_TYPE",
						system.getScheduler().name().toLowerCase()).replaceAll(
				"SUBMIT_COMMAND",
				getSubmitCommandForScheduler(system.getScheduler()));
	}

	private String getSubmitCommandForScheduler(SchedulerType schedulerType)
	{
		if (schedulerType.equals(SchedulerType.SGE)
				|| schedulerType.equals(SchedulerType.PBS)
				|| schedulerType.equals(SchedulerType.COBALT))
		{

			return "qsub";

		}
		else if (schedulerType.equals(SchedulerType.LOADLEVELER))
		{

			return "llsubmit";

		}
		else if (schedulerType.equals(SchedulerType.LSF))
		{

			return "bsub";

		}
		else if (schedulerType.equals(SchedulerType.SGE))
		{

			return "condor-submit";

		}
		else
		{ // fork scheduler
			return "sh";
		}
	}
}
