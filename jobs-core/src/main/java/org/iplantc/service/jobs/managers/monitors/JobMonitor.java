package org.iplantc.service.jobs.managers.monitors;

import org.iplantc.service.jobs.exceptions.RemoteJobMonitoringException;

public interface JobMonitor {

	public void updateStatus() throws RemoteJobMonitoringException;

	//public JobStatusType isRunning() throws RemoteJobMonitoringException;
}