package org.iplantc.service.jobs.queue;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.StaleObjectStateException;
import org.iplantc.service.apps.dao.SoftwareDao;
import org.iplantc.service.apps.model.Software;
import org.iplantc.service.common.persistence.TenancyHelper;
import org.iplantc.service.jobs.Settings;
import org.iplantc.service.jobs.dao.JobDao;
import org.iplantc.service.jobs.managers.JobManager;
import org.iplantc.service.jobs.model.Job;
import org.iplantc.service.jobs.model.enumerations.JobStatusType;
import org.iplantc.service.systems.model.enumerations.StorageProtocolType;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * Class to pull a job from the db queue and attempt to submit it to iplant
 * resources using one of the appropriate execution factory instances.
 * 
 * @author dooley
 * 
 */
@DisallowConcurrentExecution
public class ArchiveWatch implements org.quartz.Job 
{
	private static final Logger	log	= Logger.getLogger(ArchiveWatch.class);
	
	public ArchiveWatch() {}

	public void execute(JobExecutionContext context)
			throws JobExecutionException
	{

		// pull the oldest job with JobStatusType.CLEANING_UP from the db
		Job job = null;
		try
		{
			// Job.status should be set to JobStatusType.CLEANING_UP upon completion. Once 
			// it hits that state, the ArchiveWatch should see the job, if it has 
			// Job.archive set to true, it should be archived and the Job.status set to
			// ARCHIVING_FINISHED or ARCHIVING_FAILED depending on the outcome. Otherwise,
			// Job.status should be set to FINISHED indicating the terminal state of the job.
			job = JobDao.getNextQueuedJob(JobStatusType.CLEANING_UP);
			
			if (job != null) 
            {
				// this is a new thread and thus has no tenant info loaded. we set it up
				// here so things like app and system lookups will stay local to the 
				// tenant
				TenancyHelper.setCurrentTenantId(job.getTenantId());
				TenancyHelper.setCurrentEndUser(job.getOwner());
				
				if (!job.isArchiveOutput()) 
				{
					log.debug("Job " + job.getUuid() + " completed. Skipping archiving at user request.");
					JobManager.updateStatus(job, JobStatusType.FINISHED, 
							"Job completed. Skipping archiving at user request.");
				} 
				else
				{
	            	Software software = SoftwareDao.getSoftwareByUniqueName(job.getSoftwareName());
	                
	            	// if the execution system for this job has a local storage config,
	            	// all other transfer workers will pass on it.
	                if (!StringUtils.equals(Settings.LOCAL_SYSTEM_ID, job.getSystem()) &&
	                		software.getExecutionSystem().getStorageConfig().getProtocol().equals(StorageProtocolType.LOCAL)) 
	                {
	                    return;
	                } 
	                else 
	                {
						// mark the job as submitting so no other process claims it
						JobManager.updateStatus(job, JobStatusType.ARCHIVING, "Beginning to archive output.");
						
						int attempts = 0;
            			boolean archived = false;
            			
            			// attempt to stage the job several times
            			while (!archived && attempts <= Settings.MAX_SUBMISSION_RETRIES)
            			{
            				attempts++;
            				
            				job.setRetries(attempts-1);
            			
            				log.debug("Attempt " + attempts + " to archive job " + job.getUuid() + " output");
            				try 
							{
	            				JobManager.archive(job);
	            				archived = true;
	            				JobManager.updateStatus(job, JobStatusType.FINISHED, "Job completed.");
	            				log.debug("Finished archiving job " + job.getUuid() + " output");
							}
							catch (Exception e) {
								if (attempts > Settings.MAX_SUBMISSION_RETRIES ) {
									e.printStackTrace();
									log.error("Failed to archive job " + job.getUuid() + " inputs on attempt " + attempts, e);
									throw e;
								} else {
									try {
										e.printStackTrace();
										JobManager.updateStatus(job, JobStatusType.CLEANING_UP, "Attempt " 
											+ attempts + " failed to archive job output.\\n" + e.getMessage());
									} catch (Exception e1) {}
								}
							}
            			}
	                }
				}
			}
		}
		catch (StaleObjectStateException e) {
			log.debug("Just avoided a job archive race condition from worker " 
					+ context.getTrigger().getDescription());
		}
		catch (HibernateException e) {
			log.error("Failed to retrieve job information from db", e);
		}
		catch (Exception e)
		{
			String message = "Failed to archive job " + job.getUuid() + " " + e.getMessage();
			log.error("Failed to archive output for job " + job.getUuid(), e);
				
			try {
				JobManager.updateStatus(job, JobStatusType.ARCHIVING_FAILED, message);
				JobManager.updateStatus(job, JobStatusType.FAILED, JobStatusType.FAILED.getDescription());
			} catch (Exception e1) {}
		} 
	}
}