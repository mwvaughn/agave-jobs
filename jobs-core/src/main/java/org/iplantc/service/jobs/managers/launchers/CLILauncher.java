/**
 * 
 */
package org.iplantc.service.jobs.managers.launchers;

import org.iplantc.service.apps.util.ServiceUtils;
import org.iplantc.service.jobs.exceptions.JobException;
import org.iplantc.service.jobs.managers.parsers.RemoteJobIdParser;
import org.iplantc.service.jobs.managers.parsers.RemoteJobIdParserFactory;
import org.iplantc.service.jobs.model.Job;
import org.iplantc.service.jobs.util.Slug;

/**
 * Class to fork a background task on a remote linux system. The process
 * id will be stored as the Job.localJobId for querying by the monitoring
 * queue.
 *  
 * @author dooley
 * 
 */
public class CLILauncher extends HPCLauncher 
{
//	private static final Logger log = Logger.getLogger(CondorLauncher.class);
	
	/**
	 * Creates an instance of a JobLauncher capable of submitting jobs to
	 * Atmosphere VMs.
	 */
	public CLILauncher(Job job)
	{
		super(job);
	}

	@Override
	protected String submitJobToQueue() throws JobException
	{
		try
		{
			submissionClient = executionSystem.getRemoteSubmissionClient(job.getInternalUsername());
			
			String cdCommand = "source ~/.bashrc; cd " + remoteExecutionDataClient.resolvePath(job.getWorkPath());

			String chmodCommand = "chmod +x " + batchScriptName;
			
			String logFileBaseName = Slug.toSlug(job.getName());
			
			String submitCommand = executionSystem.getScheduler().getBatchSubmitCommand() + " ./"
					+ batchScriptName + " 2> " + logFileBaseName + ".err 1&> " + logFileBaseName + ".out ";
			
			String submissionResponse = submissionClient.runCommand(
					cdCommand + "; " + chmodCommand + "; " + submitCommand + " & echo $!");
					
			if (!ServiceUtils.isValid(submissionResponse.trim())) 
			{
				// retry once just in case it was a flickr
				submissionResponse = submissionClient.runCommand(
						cdCommand + "; " + chmodCommand + "; " + submitCommand + " & echo $!");
				
				if (!ServiceUtils.isValid(submissionResponse.trim())) 
					throw new JobException("Failed to submit cli job. " + submissionResponse);
			}
			
			RemoteJobIdParser jobIdParser = 
					new RemoteJobIdParserFactory().getInstance(executionSystem.getScheduler());
			
			return jobIdParser.getJobId(submissionResponse);
		}
		catch (JobException e) {
			throw e;
		}
		catch (Exception e)
		{
			throw new JobException("Failed to submit job to condor queue", e);
		}
		finally
		{
			try { submissionClient.close(); } catch (Exception e){}
		}
	}
}