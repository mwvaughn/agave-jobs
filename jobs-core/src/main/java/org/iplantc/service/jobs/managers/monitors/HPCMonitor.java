package org.iplantc.service.jobs.managers.monitors;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.iplantc.service.apps.dao.SoftwareDao;
import org.iplantc.service.apps.model.Software;
import org.iplantc.service.jobs.Settings;
import org.iplantc.service.jobs.dao.JobDao;
import org.iplantc.service.jobs.exceptions.RemoteJobMonitoringException;
import org.iplantc.service.jobs.managers.JobManager;
import org.iplantc.service.jobs.model.Job;
import org.iplantc.service.jobs.model.enumerations.JobStatusType;
import org.iplantc.service.remote.RemoteSubmissionClient;
import org.iplantc.service.systems.dao.SystemDao;
import org.iplantc.service.systems.model.ExecutionSystem;
import org.iplantc.service.systems.model.enumerations.LoginProtocolType;

/**
 * Monitors a batch job by querying the remote scheduler for the local job id. 
 * Some work is probably needed to ascertain false negatives and use the
 * job history information available on some systems.
 * 
 * @author dooley
 *
 */
public class HPCMonitor extends AbstractJobMonitor {
	private static final Logger log = Logger.getLogger(HPCMonitor.class);
	
	public HPCMonitor(Job job)
	{
		super(job);
	}

	@Override
	public void updateStatus() throws RemoteJobMonitoringException
	{
		try 
		{
			if (job != null)
			{
				// if it's a condor job, then it has to be submitted from a condor node.
				// we check to see if this is a submit node. if not, we pass.
				Software software = SoftwareDao.getSoftwareByUniqueName(job.getSoftwareName());
				
				// if the execution system login config is local, then we cannot submit
				// jobs to this system remotely. In this case, a worker will be running
				// dedicated to that system and will submitting jobs locally. All workers
				// other that this will should pass on accepting this job.
				if (software.getExecutionSystem().getLoginConfig().getProtocol().equals(LoginProtocolType.LOCAL) && 
						!Settings.LOCAL_SYSTEM_ID.equals(job.getSystem()))
				{
					return;
				}
				else // otherwise, throw it in remotely
				{
					job.setStatusChecks(job.getStatusChecks() + 1);
		        	JobDao.persist(job);
		        	
					RemoteSubmissionClient remoteSubmissionClient = null;
					
					ExecutionSystem system = (ExecutionSystem) new SystemDao().findBySystemId(job.getSystem());
					remoteSubmissionClient = system.getRemoteSubmissionClient(job.getInternalUsername());
					
					String queryCommand = system.getScheduler().getBatchQueryCommand() + " " + job.getLocalJobId();
					
					String result = remoteSubmissionClient.runCommand(queryCommand);
					if (StringUtils.isEmpty(result) || result.toLowerCase().contains("unknown") 
							|| result.toLowerCase().contains("error") || result.toLowerCase().contains("not ")) {
						// job not found.
						JobManager.updateStatus(job, JobStatusType.CLEANING_UP, "Job completed, but no callback received");
					} else {
						JobManager.updateStatus(job, job.getStatus(), job.getErrorMessage());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			try {
				JobManager.updateStatus(job, job.getStatus(), "Failed to query job status\n" + e.getMessage());
			} catch (Exception e1) {
				log.warn("Failed to query job status for job " + job.getUuid(), e);
			}
		}
	}

}
