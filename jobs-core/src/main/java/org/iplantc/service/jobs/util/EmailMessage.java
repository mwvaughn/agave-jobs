/*Copyright (c) 2004,University of Illinois at Urbana-Champaign.  All rights reserved.
 * 
 * Created on May 10, 2007
 * 
 * Developed by: CCT, Center for Computation and Technology, 
 * 				NCSA, University of Illinois at Urbana-Champaign
 * 				OSC, Ohio Supercomputing Center
 * 				TACC, Texas Advanced Computing Center
 * 				UKy, University of Kentucky
 * 
 * https://www.gridchem.org/
 * 
 * Permission is hereby granted, free of charge, to any person 
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal with the Software without 
 * restriction, including without limitation the rights to use, 
 * copy, modify, merge, publish, distribute, sublicense, and/or 
 * sell copies of the Software, and to permit persons to whom 
 * the Software is furnished to do so, subject to the following conditions:
 * 1. Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 *    this list of conditions and the following disclaimers in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the names of Chemistry and Computational Biology Group , NCSA, 
 *    University of Illinois at Urbana-Champaign, nor the names of its contributors 
 *    may be used to endorse or promote products derived from this Software without 
 *    specific prior written permission.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS WITH THE SOFTWARE.
*/

package org.iplantc.service.jobs.util;

import java.util.Properties;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.iplantc.service.common.exceptions.NotificationException;
import org.iplantc.service.jobs.Settings;

import com.sun.mail.smtp.SMTPTransport;

/**
 * Simple email class using the javamail API to send an email.
 * 
 * @author Rion Dooley < dooley [at] tacc [dot] utexas [dot] edu >
 *
 */
public class EmailMessage {
    
    public static Logger log = Logger.getLogger(EmailMessage.class.getName());
    
    public static void send(String name, String address, String subject, String body) throws NotificationException {
        Session session = null;
        
        Properties props = new Properties();
        
        try {
            props.put("mail.smtp.host", Settings.MAIL_SERVER);
            props.put("mail.smtp.auth", Settings.MAILPASSWORD);
            
            session = Session.getInstance(props);
            
            MimeMessage message = createMessageObject(session, 
                    subject, body, name, address);
            
            SMTPTransport transport = (SMTPTransport)session.getTransport("smtp");
            
            transport.connect(Settings.MAIL_SERVER,Settings.MAILLOGIN, Settings.MAILPASSWORD);
            transport.sendMessage(message, message.getRecipients(Message.RecipientType.TO));
            
        } catch (Exception e) {
            
            log.error(e);
            throw new NotificationException("Email notification failed.",e);
            
        }
    }
   
    private static MimeMessage createMessageObject(Session session, 
                                                    String subject, 
                                                    String body, 
                                                    String name,
                                                    String address) 
    throws Exception {
        
        MimeMessage message = new MimeMessage(session);
        
        message.setText(body);
        
        message.setSubject(subject);
        
        Address fromAddress = new InternetAddress(Settings.MAILLOGIN, "iPlant Job Service");
        
        Address toAddress = new InternetAddress(address, name);
        
        message.setFrom(fromAddress);
        
        message.setRecipient(Message.RecipientType.TO, toAddress);
        
        return message;
    }    
}