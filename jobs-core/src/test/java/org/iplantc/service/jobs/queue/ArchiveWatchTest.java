package org.iplantc.service.jobs.queue;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.iplantc.service.apps.dao.SoftwareDao;
import org.iplantc.service.apps.model.Software;
import org.iplantc.service.apps.model.SoftwareInput;
import org.iplantc.service.apps.model.SoftwareParameter;
import org.iplantc.service.common.exceptions.PermissionException;
import org.iplantc.service.jobs.dao.JobDao;
import org.iplantc.service.jobs.exceptions.JobException;
import org.iplantc.service.jobs.model.JSONTestDataUtil;
import org.iplantc.service.jobs.model.Job;
import org.iplantc.service.jobs.model.enumerations.JobStatusType;
import org.iplantc.service.jobs.submission.AbstractJobSubmissionTest;
import org.iplantc.service.systems.exceptions.SystemException;
import org.iplantc.service.systems.manager.SystemManager;
import org.iplantc.service.systems.model.ExecutionSystem;
import org.iplantc.service.systems.model.StorageSystem;
import org.iplantc.service.transfer.RemoteDataClient;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * Tests end to end integration of a job submission by manually pushing
 * through each stage of each queue.
 */
public class ArchiveWatchTest extends AbstractJobSubmissionTest
{
	// this is the root directory for creating job specific
	private Job job;
	private JSONTestDataUtil jtd;
	private SystemManager systemManager = new SystemManager();
	private StorageSystem storageSystem;
	private ExecutionSystem executionSystem;
	private String workPath = "archivetest-workdir-"+System.currentTimeMillis();
	private String archivePath = "archivetest-archivedir-"+System.currentTimeMillis();
	
	@BeforeClass
	private void beforeClass() throws Exception 
	{
		clearSystems();
		clearSoftware();
		clearJobs();
		
		jtd = JSONTestDataUtil.getInstance();
		
		initSystems();
		
		initSoftware();
	}
	
	@AfterClass 
	public void afterClass() throws Exception {
		
		clearSystems();
		clearSoftware();
		clearJobs();
		
		RemoteDataClient remoteDataClient = null;
		try {
			remoteDataClient = executionSystem.getRemoteDataClient(job.getInternalUsername());
			remoteDataClient.authenticate();
			remoteDataClient.delete(workPath);
//			Assert.assertFalse(remoteDataClient.doesExist(workPath), 
//					"Failed to delete work folder from remote system");
		} catch (Exception e) {
			//Assert.fail("Failed delete work directory", e);
		} finally {
			remoteDataClient.disconnect();
		}
		
		try {
			remoteDataClient = storageSystem.getRemoteDataClient(job.getInternalUsername());
			remoteDataClient.authenticate();
			remoteDataClient.delete(archivePath);
//			Assert.assertFalse(remoteDataClient.doesExist(archivePath), 
//					"Failed to delete archive folder from remote system");
		} catch (Exception e) {
//			Assert.fail("Failed delete archive data", e);
		} finally {
			remoteDataClient.disconnect();
		}
	}
	
	@BeforeMethod
	private void beforeMethod() throws Exception {
		// create job work directory on local system and put input file there
		clearJobs();
	}
	
	@AfterMethod
	private void afterMethod() throws Exception {
		// create job work directory on local system and put input file there
		clearJobs();
	}
	
	protected void initSoftware() throws Exception {
		File softwareDir = new File(SOFTWARE_SYSTEM_TEMPLATE_DIR, "/wc-condor.example.com.json");
		JSONObject json = jtd.getTestDataObject(softwareDir.getPath());
		Software software = Software.fromJSON(json, SYSTEM_OWNER);
		software.setOwner(SYSTEM_OWNER);
		
		SoftwareDao.persist(software);
	}
	
	protected void initSystems() throws JSONException, IOException, SystemException, PermissionException {
		JSONObject json = jtd.getTestDataObject(STORAGE_SYSTEM_TEMPLATE_DIR + 
				File.separator + "storage.example.com.json");
		storageSystem = (StorageSystem) systemManager.parseSystem(json, SYSTEM_OWNER, null);
		storageSystem.setOwner(SYSTEM_OWNER);
		storageSystem.getUsersUsingAsDefault().add(SYSTEM_OWNER);
		systemDao.persist(storageSystem);
		
		File executionDir = new File(EXECUTION_SYSTEM_TEMPLATE_DIR, "condor.example.com.json");;
		json = jtd.getTestDataObject(executionDir.getPath());
		executionSystem  = (ExecutionSystem) systemManager.parseSystem(json, SYSTEM_OWNER, null);
		executionSystem.setOwner(SYSTEM_OWNER);
		systemDao.persist(executionSystem);
	}
	
	private Job createJob(Software software) throws JobException, JSONException {
		Job job = new Job();
		job.setName( software.getExecutionSystem().getName() + " test");
		job.setArchiveOutput(true);
		job.setArchivePath(archivePath);
		job.setArchiveSystem(storageSystem);
		job.setCreated(new Date());
		job.setMemoryPerNode((double)4);
		job.setOwner(software.getOwner());
		job.setProcessorsPerNode((long)1);
		job.setMaxRunTime("1:00:00");
		job.setSoftwareName(software.getUniqueName());
		job.setStatus(JobStatusType.CLEANING_UP, JobStatusType.CLEANING_UP.getDescription());
		job.setSystem(software.getExecutionSystem().getSystemId());
		job.setWorkPath(workPath);
		
		JSONObject inputs = new JSONObject();
		for(SoftwareInput input: software.getInputs()) {
			inputs.put(input.getKey(), input.getDefaultValue());
		}
		job.setInputs(inputs.toString());
		
		JSONObject parameters = new JSONObject();
		for (SoftwareParameter parameter: software.getParameters()) {
			parameters.put(parameter.getKey(), parameter.getDefaultValue());
		}
		job.setParameters(parameters.toString());
		
		JobDao.persist(job);
		
		return job;
	}
	
	protected void stageTestData(String localArchiveFile) throws Exception {
		
		RemoteDataClient storageClient = null;
		try {
			storageClient = executionSystem.getRemoteDataClient(job.getInternalUsername());
			storageClient.authenticate();
			// set up the work folder like it's a completed job
			storageClient.mkdirs(job.getWorkPath());
			for (File localFile: new File(TEST_DATA_DIR).listFiles()) {
				storageClient.put(localFile.getAbsolutePath(), job.getWorkPath() + "/" + localFile.getName());
			}
			for (File localFile: new File(TEST_DATA_DIR).listFiles()) {
				storageClient.put(localFile.getAbsolutePath(), job.getWorkPath() + "/beta-" + localFile.getName());
			}
			// copy over the .agave.archive file
			storageClient.put(localArchiveFile, job.getWorkPath() + "/.agave.archive");
			Assert.assertTrue(storageClient.doesExist(job.getWorkPath()), 
					"Failed to copy work files to execution system");
		} catch (Exception e) {
			Assert.fail("Failed to work files to execution system");
		} finally {
			storageClient.disconnect();
		}
	}
	
	@DataProvider(name="submitJobProvider")
	public Object[][] submitJobProvider() throws Exception
	{
		List<Software> testApps = SoftwareDao.getUserApps(SYSTEM_OWNER, false);
		File tmpArchiveFile = new File(System.currentTimeMillis() + "");
		tmpArchiveFile.createNewFile();
		
		FileUtils.write(tmpArchiveFile, ".agave.archive\n", true);
		for(File workFile: new File(TEST_DATA_DIR).listFiles()) {
			FileUtils.write(tmpArchiveFile, workFile.getName() + "\n", true);
		}
		
		Object[][] testData = new Object[testApps.size()][3];
		for(int i=0; i< testApps.size(); i++) {
			testData[i] = new Object[] { testApps.get(i), tmpArchiveFile, "Archiving of job output failed.", false };
		}
		
		return testData;
	}
	
	@Test (groups={"submission"}, dataProvider="submitJobProvider")
	public void submitJob(Software software, File tmpArchiveFile, String message, boolean shouldThrowException) throws Exception
	{
		RemoteDataClient remoteDataClient = null;
		
		job = createJob(software);
		
		// archive the job when it's done
		try 
		{
			stageTestData(tmpArchiveFile.getAbsolutePath());
			
			ArchiveWatch archiveWatch = new ArchiveWatch();
			archiveWatch.execute(null);
			Assert.assertEquals(job.getStatus(), JobStatusType.FINISHED, 
					"Job status was not FINISHED after ArchiveWatch completed.");
			
			remoteDataClient = job.getArchiveSystem().getRemoteDataClient(job.getInternalUsername());
			remoteDataClient.authenticate();
			Assert.assertTrue(remoteDataClient.doesExist(job.getArchivePath()), 
					"Archive folder does not exist on remote system.");
			
			String[] excludedFiles = FileUtils.readFileToString(tmpArchiveFile).split("\n");
			
			for (String localFile: excludedFiles) {
				Assert.assertFalse(remoteDataClient.doesExist(job.getArchivePath() + "/" + localFile), 
						"Excluded file was archived " + job.getArchivePath() + "/" + localFile);
			}
		} 
		catch (Exception e) {
			Assert.fail("Failed to archive job data to " + job.getArchiveSystem().getSystemId(), e);
		} 
		finally {
			try { tmpArchiveFile.delete(); } catch (Exception e) {}
			try { remoteDataClient.disconnect(); } catch (Exception e) {}
		}
	}
}