package org.iplantc.service.jobs.queue;

import java.io.File;
import java.net.URLEncoder;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.iplantc.service.apps.dao.SoftwareDao;
import org.iplantc.service.apps.model.Software;
import org.iplantc.service.apps.model.SoftwareInput;
import org.iplantc.service.apps.model.SoftwareParameter;
import org.iplantc.service.jobs.Settings;
import org.iplantc.service.jobs.dao.JobDao;
import org.iplantc.service.jobs.exceptions.JobException;
import org.iplantc.service.jobs.model.JSONTestDataUtil;
import org.iplantc.service.jobs.model.Job;
import org.iplantc.service.jobs.model.enumerations.JobStatusType;
import org.iplantc.service.jobs.submission.AbstractJobSubmissionTest;
import org.iplantc.service.jobs.util.Slug;
import org.iplantc.service.profile.dao.InternalUserDao;
import org.iplantc.service.profile.exceptions.ProfileException;
import org.iplantc.service.profile.model.InternalUser;
import org.iplantc.service.systems.dao.SystemDao;
import org.iplantc.service.systems.manager.SystemManager;
import org.iplantc.service.systems.model.ExecutionSystem;
import org.iplantc.service.systems.model.RemoteSystem;
import org.iplantc.service.systems.model.StorageSystem;
import org.iplantc.service.transfer.RemoteDataClient;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class StagingWatchTest extends AbstractJobSubmissionTest 
{
	protected static String LOCAL_TXT_FILE = "src/test/resources/transfer/test_upload.txt";
	
	private JSONTestDataUtil jtd;
	private SystemDao systemDao = new SystemDao();
	private SystemManager systemManager = new SystemManager();
	
	@BeforeClass
	public void beforeClass() throws Exception
	{
		clearInternalUsers();
		clearSoftware();
		clearSystems();
		clearJobs();
		
		jtd = JSONTestDataUtil.getInstance();
		
		JSONObject json = null;
		
		//File storageDir = new File(STORAGE_SYSTEM_TEMPLATE_DIR);
		for(String protocol: new String[]{"irods","gridftp","sftp"}) {
			json = jtd.getTestDataObject(STORAGE_SYSTEM_TEMPLATE_DIR + File.separator + protocol + ".example.com.json");
			StorageSystem storageSystem = (StorageSystem) systemManager.parseSystem(json, SYSTEM_OWNER, null);
			storageSystem.setOwner(SYSTEM_OWNER);
			if (protocol.equals("irods")) {
				storageSystem.setGlobalDefault(true);
				storageSystem.setPubliclyAvailable(true);
			}
			systemDao.persist(storageSystem);
			
			// copy the test file to each remote system
			RemoteDataClient client = storageSystem.getRemoteDataClient();
			client.authenticate();
			client.put(LOCAL_TXT_FILE, "");
			client.disconnect();
		}
		
		
		
		//File executionDir = new File(EXECUTION_SYSTEM_TEMPLATE_DIR);
		//for(File jsonFile: executionDir.listFiles()) {
		for(String protocol: new String[]{"ssh"}) {//,"gsissh","condor"}) {
			json = jtd.getTestDataObject(EXECUTION_SYSTEM_TEMPLATE_DIR + File.separator + protocol + ".example.com.json");
			//json = jtd.getTestDataObject(jsonFile.getPath());
			ExecutionSystem system = (ExecutionSystem) systemManager.parseSystem(json, SYSTEM_OWNER, null);
			system.setOwner(SYSTEM_OWNER);
			systemDao.persist(system);
			
			// register an app on the system
			json = jtd.getTestDataObject(SOFTWARE_SYSTEM_TEMPLATE_DIR + "/system-software.json");
			json.put("name", "test-" + protocol);
			json.put("executionSystem", system.getSystemId());
			json.put("deploymentSystem", systemManager.getDefaultStorageSystem().getSystemId());
			Software software = Software.fromJSON(json, SYSTEM_OWNER);
			software.setOwner(SYSTEM_OWNER);
			SoftwareDao.persist(software);
		}
		
//		File softwareDir = new File(SOFTWARE_SYSTEM_TEMPLATE_DIR);
//		for(File jsonFile: softwareDir.listFiles()) {
//			json = jtd.getTestDataObject(jsonFile.getPath());
//			Software software = Software.fromJSON(json, SYSTEM_OWNER);
//			software.setOwner(SYSTEM_OWNER);
//			SoftwareDao.persist(software);
//		}
	}
	
	public void clearSystems()
	{
		for (RemoteSystem system: systemDao.getAll()) {
			systemDao.remove(system);
		}
	}

	public void clearSoftware()
	{
		for (Software software: SoftwareDao.getAll()) {
			SoftwareDao.delete(software);
		}
	}

	public void clearJobs() throws JobException
	{
		for (Job job: JobDao.getAll()) {
			JobDao.delete(job);
		}
	}
	
	private void clearInternalUsers() throws ProfileException
	{
		InternalUserDao internalUserDao = new InternalUserDao();
		
		for (InternalUser internalUser: internalUserDao.getAll()) {
			internalUserDao.delete(internalUser);
		}
	}

	@AfterClass
	public void afterClass() throws Exception
	{
		clearInternalUsers();
		clearJobs();
		clearSoftware();
		clearSystems();
	}
	
	
	public Job createJob(Software software, String inputUri) throws JobException, JSONException 
	{
		Job job = new Job();
		job.setName( software.getExecutionSystem().getName() + " test");
		job.setArchiveOutput(false);
		job.setArchivePath("/");
		job.setArchiveSystem(software.getStorageSystem());
		job.setCreated(new Date());
		job.setMemoryPerNode((double)512);
		job.setOwner(software.getExecutionSystem().getOwner());
		job.setProcessorsPerNode((long)1);
		job.setMaxRunTime("1:00:00");
		job.setSoftwareName(software.getUniqueName());
		job.setStatus(JobStatusType.STAGED, "Job inputs staged to execution system");
		job.setSystem(software.getExecutionSystem().getSystemId());
		
		String remoteWorkPath = null;
		if (StringUtils.isEmpty(software.getExecutionSystem().getScratchDir())) {
			remoteWorkPath = job.getOwner() +
				"/job-" + job.getUuid() + "-" + Slug.toSlug(job.getName()) + 
				"/" + FilenameUtils.getName(software.getDeploymentPath());
		} else {
			remoteWorkPath = software.getExecutionSystem().getScratchDir() + 
					job.getOwner() + 
					"/job-" + job.getUuid() + "-" + Slug.toSlug(job.getName()) +
					"/" + FilenameUtils.getName(software.getDeploymentPath());
		}
		job.setWorkPath(remoteWorkPath);
		
		JSONObject inputs = new JSONObject();
		for (SoftwareInput input: software.getInputs()) {
			inputs.put(input.getKey(), inputUri);
		}
		job.setInputs(inputs.toString());
		
		JSONObject parameters = new JSONObject();
		for (SoftwareParameter param: software.getParameters()) {
			parameters.put(param.getKey(), param.getDefaultValue());
		}
		job.setParameters(parameters.toString());
		
		return job;
	}
	
	@DataProvider(name="stageJobDataFromRegisteredSystemsProvider")
	public Object[][] stageJobDataFromRegisteredSystemsProvider() throws JobException, JSONException
	{
		String remoteFileName = FilenameUtils.getName(LOCAL_TXT_FILE);
		
		String irodsInternalUriInput = Settings.IPLANT_IO_SERVICE + "media/system/irods.example.com/" + remoteFileName;
		String irodsAgaveUriInput = "agave://irods.example.com/" + remoteFileName;
		String sftpInternalUriInput = Settings.IPLANT_IO_SERVICE + "media/system/sftp.example.com/" + remoteFileName;
		String sftpAgaveUriInput = "agave://sftp.example.com/" + remoteFileName;
		String gridftpInternalUriInput = Settings.IPLANT_IO_SERVICE + "media/system/gridftp.example.com/" + remoteFileName;
		String gridftpAgaveUriInput = "agave://gridftp.example.com/" + remoteFileName;
		
		List<Software> testApps = SoftwareDao.getAll();
		Object[][] testData = new Object[testApps.size() * 6][2];
		int i=0;
		for (Software software: testApps) 
		{
			testData[i++] = new Object[] { createJob(software, irodsInternalUriInput), "Failed to stage irods internal uri input to job execution system " + software.getExecutionSystem().getSystemId() };
			testData[i++] = new Object[] { createJob(software, irodsAgaveUriInput), "Failed to stage irods agave uri input to job execution system " + software.getExecutionSystem().getSystemId() };
			testData[i++] = new Object[] { createJob(software, sftpInternalUriInput), "Failed to stage sftp internal uri input to job execution system " + software.getExecutionSystem().getSystemId() };
			testData[i++] = new Object[] { createJob(software, sftpAgaveUriInput), "Failed to stage sftp agave uri input to job execution system " + software.getExecutionSystem().getSystemId() };
			testData[i++] = new Object[] { createJob(software, gridftpInternalUriInput), "Failed to stage gridftp internal uri input to job execution system " + software.getExecutionSystem().getSystemId() };
			testData[i++] = new Object[] { createJob(software, gridftpAgaveUriInput), "Failed to stage gridftp agave uri input to job execution system " + software.getExecutionSystem().getSystemId() };
		}
		
		return testData;
	}
	
	//@Test(dataProvider="stageJobDataFromRegisteredSystemsProvider")
	public void stageJobDataFromRegisteredSystems(Job job, String message)
	{
		RemoteDataClient remoteDataClient = null;
		try {
			StagingWatch watch = new StagingWatch();
			Map<String, String> jobInputMap = job.getInputsAsMap();
			watch.stageJobData(job, jobInputMap);
			
			Assert.assertEquals(job.getStatus(), JobStatusType.STAGED, "Job status was not changed to staged after staging watch completed.");
			
			remoteDataClient = systemDao.findBySystemId(job.getSystem()).getRemoteDataClient(job.getInternalUsername());
			remoteDataClient.authenticate();
			Assert.assertTrue(remoteDataClient.doesExist(job.getWorkPath() + File.separator + FilenameUtils.getName(LOCAL_TXT_FILE)), message);
		} catch (Exception e) {
			Assert.fail(message, e);
		} finally {
			try { remoteDataClient.disconnect(); } catch (Exception e) {}
		}
	}
	
	public String createGenericUri(String systemId) throws Exception
	{
		String remoteFileName = FilenameUtils.getName(LOCAL_TXT_FILE);
		
		RemoteSystem system = systemDao.findBySystemId(systemId);
		RemoteDataClient client = system.getRemoteDataClient();
		String salt = system.getEncryptionKeyForAuthConfig(system.getStorageConfig().getDefaultAuthConfig());
		String sUri = system.getStorageConfig().getProtocol().name().toLowerCase() + 
				"://" + 
				URLEncoder.encode(system.getStorageConfig().getDefaultAuthConfig().getUsername(), "UTF-8") + 
				":" +
				URLEncoder.encode(system.getStorageConfig().getDefaultAuthConfig().getClearTextPassword(salt), "UTF-8") +
				"@" + 
				system.getStorageConfig().getHost() + ":" + system.getStorageConfig().getPort() + 
				"/" + client.resolvePath(remoteFileName);
		return sUri;
	}
	
	@DataProvider(name="stageJobDataFromGenericUriProvider")
	public Object[][] stageJobDataFromGenericUriProvider() throws Exception
	{	
		String sftpUriInput = createGenericUri("sftp.example.com");
		String gridftpUriInput = createGenericUri("gridftp.example.com");
		String irodsUriInput = createGenericUri("irods.example.com");
		String httpInput = "http://a2.espncdn.com/prod/assets/espn_top_nav_logo_109x27.png";
		String httpsInput = "https://www.google.com/images/srpr/logo4w.png";
		
		List<Software> testApps = SoftwareDao.getAll();
		Object[][] testData = new Object[testApps.size() * 5][2];
		int i=0;
		for (Software software: testApps) 
		{
			testData[i++] = new Object[] { createJob(software, irodsUriInput), true, "Failed to stage irods generic uri input to job execution system " + software.getExecutionSystem().getSystemId() };
			testData[i++] = new Object[] { createJob(software, sftpUriInput), false, "Failed to stage sftp generic uri input to job execution system " + software.getExecutionSystem().getSystemId() };
			testData[i++] = new Object[] { createJob(software, gridftpUriInput), true, "Failed to stage gridftp generic uri input to job execution system " + software.getExecutionSystem().getSystemId() };
			testData[i++] = new Object[] { createJob(software, httpInput), false, "Failed to stage http generic uri input to job execution system " + software.getExecutionSystem().getSystemId() };
			testData[i++] = new Object[] { createJob(software, httpsInput), false, "Failed to stage https generic uri input to job execution system " + software.getExecutionSystem().getSystemId() };
		}
		
		return testData;
	}
	
	@Test(dataProvider="stageJobDataFromGenericUriProvider")//, dependsOnMethods={"stageJobDataFromRegisteredSystems"})
	public void stageJobDataFromGenericUri(Job job, boolean shouldFail, String message)
	{
		RemoteDataClient remoteDataClient = null;
		try {
			StagingWatch watch = new StagingWatch();
			Map<String, String> jobInputMap = job.getInputsAsMap();
			watch.stageJobData(job, jobInputMap);
			
			if (!shouldFail)
			{
				Assert.assertEquals(job.getStatus(), JobStatusType.STAGED, "Job status was not changed to staged after staging watch completed.");
			
				remoteDataClient = systemDao.findBySystemId(job.getSystem()).getRemoteDataClient();
				remoteDataClient.authenticate();
				Assert.assertTrue(remoteDataClient.doesExist(job.getWorkPath() + File.separator + FilenameUtils.getName(LOCAL_TXT_FILE)), message);
			} else {
				Assert.assertNotNull(job.getErrorMessage(), message);
			}
		} catch (Exception e) {
			Assert.fail(message, e);
		} finally {
			try { remoteDataClient.disconnect(); } catch (Exception e) {}
		}
	}
}
