package org.iplantc.service.jobs.managers;

import static org.iplantc.service.jobs.model.JSONTestDataUtil.TEST_EXECUTION_SYSTEM_FILE;
import static org.iplantc.service.jobs.model.JSONTestDataUtil.TEST_OWNER;
import static org.iplantc.service.jobs.model.JSONTestDataUtil.TEST_SOFTWARE_SYSTEM_FILE;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.iplantc.service.apps.dao.SoftwareDao;
import org.iplantc.service.apps.model.Software;
import org.iplantc.service.apps.model.SoftwareInput;
import org.iplantc.service.apps.model.SoftwareParameter;
import org.iplantc.service.apps.model.enumerations.SoftwareParameterType;
import org.iplantc.service.common.Settings;
import org.iplantc.service.common.exceptions.MessagingException;
import org.iplantc.service.common.messaging.Message;
import org.iplantc.service.common.messaging.MessageClientFactory;
import org.iplantc.service.common.messaging.MessageQueueClient;
import org.iplantc.service.jobs.dao.AbstractDaoTest;
import org.iplantc.service.jobs.dao.JobDao;
import org.iplantc.service.jobs.dao.JobEventDao;
import org.iplantc.service.jobs.exceptions.JobException;
import org.iplantc.service.jobs.exceptions.JobProcessingException;
import org.iplantc.service.jobs.model.JSONTestDataUtil;
import org.iplantc.service.jobs.model.Job;
import org.iplantc.service.jobs.model.JobEvent;
import org.iplantc.service.jobs.model.enumerations.JobStatusType;
import org.iplantc.service.notification.dao.NotificationDao;
import org.iplantc.service.notification.model.Notification;
import org.iplantc.service.systems.dao.SystemDao;
import org.iplantc.service.systems.model.BatchQueue;
import org.iplantc.service.systems.model.ExecutionSystem;
import org.iplantc.service.systems.model.StorageSystem;
import org.iplantc.service.systems.model.enumerations.RemoteSystemType;
import org.iplantc.service.systems.model.enumerations.SystemStatusType;
import org.iplantc.service.transfer.RemoteDataClient;
import org.json.JSONException;
import org.json.JSONObject;
import org.restlet.data.Form;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.surftools.BeanstalkClientImpl.ClientImpl;

public class JobManagerTest extends AbstractDaoTest {
	private boolean pass = false;
	private boolean fail = true;
	private ObjectMapper mapper = new ObjectMapper();
	
	@BeforeClass
	public void beforeClass() throws Exception {
		super.beforeClass();
		SoftwareDao.persist(software);
		drainQueue();
	}

	@AfterClass
	public void afterClass() throws Exception {
		clearJobs();
		clearSoftware();
		clearSystems();
		drainQueue();
	}

	/**
	 * Flushes the messaging tube of any and all existing jobs.
	 * @param queueName
	 */
	@AfterMethod
	public void drainQueue() 
	{
		ClientImpl client = null;
	
		try {
			// drain the message queue
			client = new ClientImpl(Settings.MESSAGING_SERVICE_HOST,
					Settings.MESSAGING_SERVICE_PORT);
			client.watch(Settings.NOTIFICATION_QUEUE);
			client.useTube(Settings.NOTIFICATION_QUEUE);
			client.kick(Integer.MAX_VALUE);
			
			com.surftools.BeanstalkClient.Job beanstalkJob = null;
			do {
				try {
					beanstalkJob = client.peekReady();
					if (beanstalkJob != null)
						client.delete(beanstalkJob.getJobId());
				} catch (Throwable e) {
					e.printStackTrace();
				}
			} while (beanstalkJob != null);
			do {
				try {
					beanstalkJob = client.peekBuried();
					if (beanstalkJob != null)
						client.delete(beanstalkJob.getJobId());
				} catch (Throwable e) {
					e.printStackTrace();
				}
			} while (beanstalkJob != null);
			do {
				try {
					beanstalkJob = client.peekDelayed();
					
					if (beanstalkJob != null)
						client.delete(beanstalkJob.getJobId());
				} catch (Throwable e) {
					e.printStackTrace();
				}
			} while (beanstalkJob != null);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		finally {
			try { client.ignore(Settings.NOTIFICATION_QUEUE); } catch (Throwable e) {}
			try { client.close(); } catch (Throwable e) {}
			client = null;
		}
	}
	
	/**
	 * Counts number of messages in the queue.
	 * 
	 * @param queueName
	 * @return int totoal message count
	 */
	public int getMessageCount(String queueName) throws MessagingException
	{
		ClientImpl client = null;
		
		try {
			// drain the message queue
			client = new ClientImpl(Settings.MESSAGING_SERVICE_HOST,
					Settings.MESSAGING_SERVICE_PORT);
			client.watch(queueName);
			client.useTube(queueName);
			Map<String,String> stats = client.statsTube(queueName);
			String totalJobs = stats.get("current-jobs-ready");
			if (NumberUtils.isNumber(totalJobs)) {
				return NumberUtils.toInt(totalJobs);
			} else {
				throw new MessagingException("Failed to find total job count for queue " + queueName);
			}
		} catch (MessagingException e) {
			throw e;
		} catch (Throwable e) {
			throw new MessagingException("Failed to read jobs from queue " + queueName, e);
		}
		finally {
			try { client.ignore(Settings.NOTIFICATION_QUEUE); } catch (Throwable e) {}
			try { client.close(); } catch (Throwable e) {}
			client = null;
		}
	}
	
	/**
	 * Adds or updates a value in a Restlet form.
	 * 
	 * @param form
	 * @param field
	 * @param value
	 * @return
	 */
	private Form updateForm(Form form, String field, Object value) 
	{
		if (form.getNames().contains(field)) {
			form.removeAll(field);
		}
		if (value == null) {
			form.add(field, null);
		}
		else if (value instanceof String) {
			form.add(field, (String)value);
		}
		else
		{
			form.add(field, ObjectUtils.toString(value));
		}
		
		return form;
	}
	
	/**
	 * Sets an field in a ObjectNode object, determining the proper type on the fly.
	 * 
	 * @param json
	 * @param field
	 * @param value
	 * @return the updated ObjectNode
	 */
	private ObjectNode updateObjectNode(ObjectNode json, String field, Object value)
	{
		if (value == null)
			json.putNull(field);
		else if (value instanceof ArrayNode)
			json.putArray(field).addAll((ArrayNode)value);
		else if (value instanceof ObjectNode)
			json.putObject(field);
		else if (value instanceof Long)
			json.put(field, (Long)value);
		else if (value instanceof Integer)
			json.put(field, (Integer)value);
		else if (value instanceof Float)
			json.put(field, (Float)value);
		else if (value instanceof Double)
			json.put(field, (Double)value);
		else if (value instanceof BigDecimal)
			json.put(field, (BigDecimal)value);
		else if (value instanceof Boolean)
			json.put(field, (Boolean)value);
		else if (value instanceof Collection) {
			ArrayNode arrayNode = new ObjectMapper().createArrayNode();
			for (Object o: (Collection)value) {
				if (o instanceof ArrayNode)
					arrayNode.addArray().addAll((ArrayNode)o);
				else if (o instanceof ObjectNode)
					arrayNode.add((ObjectNode)value);
				else if (o instanceof Long)
					arrayNode.add((Long)value);
				else if (o instanceof Integer)
					arrayNode.add((Long)value);
				else if (o instanceof Float)
					arrayNode.add((Long)value);
				else if (o instanceof Double)
					arrayNode.add((Long)value);
				else if (o instanceof Boolean)
					arrayNode.add((Boolean)value);
				else if (o instanceof String)
					arrayNode.add((String)value);
				else
					arrayNode.addObject();
			}
			json.putArray(field).addAll(arrayNode);
		}
		else if (value instanceof Map) {
			for (String key: ((Map<String,Object>)value).keySet()) {
				json = updateObjectNode(json, key, ((Map<String,Object>)value).get(key));
			}
		}
		else if (value instanceof String)
			json.put(field, (String)value);
		else 
			json.putObject(field);
		
		return json;
	}
	
	/**
	 * Creates a JsonNode representation of a job notification without the persistent field
	 * @param url
	 * @param event
	 * @return
	 */
	private JsonNode createJsonNotification(Object url, Object event) 
	{
		ObjectNode json = updateObjectNode(mapper.createObjectNode(), "url", url);
		json = updateObjectNode(json, "event", event);
		return json;
	}
	
	/**
	 * Creates a JsonNode representation of a job notification using the supplied values 
	 * and determining the types on the fly.
	 * 
	 * @param url
	 * @param event
	 * @param persistent
	 * @return
	 */
	private JsonNode createJsonNotification(Object url, Object event, boolean persistent) 
	{
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode json = updateObjectNode(mapper.createObjectNode(), "url", url);
		json = updateObjectNode(json, "event", event);
		json.put("persistent", persistent);
		
		return json;
	}
	
	
	/**
	 * Creates a bare bones ObjectNode representing a job submission.
	 * @return ObjectNode with minimal set of job attributes.
	 * @throws IOException 
	 * @throws JsonProcessingException 
	 */
	private ObjectNode createJobJsonNode()
	{
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode json = mapper.createObjectNode();
		
		try 
		{
			json.put("name", "processJsonJobWithNotifications");
			json.put("appId", software.getUniqueName());
			ObjectNode jsonInput = mapper.createObjectNode();
			for (SoftwareInput input: software.getInputs()) {
				jsonInput.put(input.getKey(), input.getDefaultValue());
			}
			json.put("inputs", jsonInput);
			
			ObjectNode jsonParameter = mapper.createObjectNode();
			for (SoftwareParameter parameter: software.getParameters()) {
				if (parameter.getType().equals(SoftwareParameterType.enumeration))
				{
					ArrayNode defaultEnumerations = (ArrayNode)mapper.readTree(parameter.getDefaultValue());
					String[] defaults = new String[defaultEnumerations.size()];
					for (int i=0; i<defaultEnumerations.size(); i++) {
						defaults[i] = defaultEnumerations.get(i).textValue();
					}
					jsonParameter.put(parameter.getKey(), StringUtils.join(defaults, ';'));
				} else {
					jsonParameter.put(parameter.getKey(), parameter.getDefaultValue());
				}
			}
			json.put("parameters", jsonParameter);
		} catch (Exception e) {
			Assert.fail("Failed to read in software description to create json job object", e);
		}
		
		return json;
	}
	
	
	/**
	 * Creates a bare bones Form representing a job submission.
	 * @return ObjectNode with minimal set of job attributes.
	 */
	private Form createJobForm()
	{
		Form form = new Form();
		try 
		{
			form.add("name", "processJsonJobWithNotifications");
			form.add("appId", software.getUniqueName());
			for (SoftwareInput input: software.getInputs()) {
				form.add(input.getKey(), input.getDefaultValue());
			}
			
			for (SoftwareParameter parameter: software.getParameters()) {
				if (parameter.getType().equals(SoftwareParameterType.enumeration))
				{
					ArrayNode defaultEnumerations = (ArrayNode)mapper.readTree(parameter.getDefaultValue());
					String[] defaults = new String[defaultEnumerations.size()];
					for (int i=0; i<defaultEnumerations.size(); i++) {
						defaults[i] = defaultEnumerations.get(i).textValue();
					}
					form.add(parameter.getKey(), StringUtils.join(defaults, ';'));
				} else {
					form.add(parameter.getKey(), parameter.getDefaultValue());
				}
			}
		} catch (Exception e) {
			Assert.fail("Failed to read in software description to create json job object", e);
		}
		
		return form;
	}
	
	private SoftwareParameter createParameter(String key, String type, String defaultValue, String validator, boolean required, boolean visible)
	{
		SoftwareParameter param = new SoftwareParameter();
		param.setKey(key);
		param.setType(type);
		param.setDefaultValue(defaultValue);
		param.setRequired(required);
		param.setVisible(visible);
		if (type.equals(SoftwareParameterType.enumeration.name())) {
			ArrayNode parameterEnumValues = mapper.createArrayNode()
					.add(mapper.createObjectNode().put(defaultValue, defaultValue))
					.add(mapper.createObjectNode().put("BETA", "BETA"))
					.add(mapper.createObjectNode().put("GAMMA", "GAMMA"))
					.add(mapper.createObjectNode().put("DELTA", "DELTA"));
			param.setValidator(parameterEnumValues.toString());
		} else {
			param.setValidator(validator);
		}
		return param;
	}
	
	private SoftwareInput createInput(String key, String defaultValue, String validator, boolean required, boolean visible)
	{
		SoftwareInput input = new SoftwareInput();
		input.setKey(key);
		input.setDefaultValue(defaultValue);
		input.setRequired(required);
		input.setVisible(visible);
		input.setValidator(validator);
		
		return input;
	}
	
	@DataProvider
	public Object[][] validateBatchSubmitParametersProvider() {
		// name maxJobs userJobs nodes memory, procs, time, cstm, default
		BatchQueue queueUnbounded = new BatchQueue("queueMaximum", (long) -1,
				(long) -1, (long) -1, (double) -1.0, (long) -1,
				BatchQueue.DEFAULT_MAX_RUN_TIME, null, false);
		BatchQueue queueMinimal = new BatchQueue("queueMinimal", (long) 2,
				(long) 2, (long) 2, (double) 2.0, (long) 2, "00:01:00", null,
				false);

		boolean pass = true;
		boolean fail = false;

		return new Object[][] {
				// fixed limit queue
				{ queueMinimal, queueMinimal.getMaxNodes(),
						queueMinimal.getMaxProcessorsPerNode(),
						queueMinimal.getMaxMemoryPerNode(),
						queueMinimal.getMaxRequestedTime(), pass,
						"Everything at limits should pass" },
				{ queueMinimal, (queueMinimal.getMaxNodes() - (long) 1),
						(queueMinimal.getMaxProcessorsPerNode() - (long) 1),
						(queueMinimal.getMaxMemoryPerNode() - (double) 1),
						"00:00:30", pass, 
						"Everything under limits should pass" },
				{ queueMinimal, (queueMinimal.getMaxNodes() + (long) 1),
						(queueMinimal.getMaxProcessorsPerNode() + (long) 1),
						(queueMinimal.getMaxMemoryPerNode() + (double) 1),
						"00:03:00", fail, 
						"Everything over limits should fail" },
				{ queueMinimal, null, null, null, null, fail,
						"Everything null should fail" },
				// node checks
				{ queueMinimal, new Long(-1),
						queueMinimal.getMaxProcessorsPerNode(),
						queueMinimal.getMaxMemoryPerNode(),
						queueMinimal.getMaxRequestedTime(), pass,
						"Nodes unbounded, everything else at limits should pass" },
				{ queueMinimal, new Long(-2),
						queueMinimal.getMaxProcessorsPerNode(),
						queueMinimal.getMaxMemoryPerNode(),
						queueMinimal.getMaxRequestedTime(), fail,
						"Nodes negative, everything else at limits should fail" },
				{ queueMinimal, new Long(0),
						queueMinimal.getMaxProcessorsPerNode(),
						queueMinimal.getMaxMemoryPerNode(),
						queueMinimal.getMaxRequestedTime(), fail,
						"Nodes zero, everything else at limits should fail" },
				{ queueMinimal, null, queueMinimal.getMaxProcessorsPerNode(),
						queueMinimal.getMaxMemoryPerNode(),
						queueMinimal.getMaxRequestedTime(), fail,
						"Nodes null, everything else at limits should fail" },
				{ queueMinimal, (queueMinimal.getMaxNodes() + (long) 1),
						queueMinimal.getMaxProcessorsPerNode(),
						queueMinimal.getMaxMemoryPerNode(),
						queueMinimal.getMaxRequestedTime(), fail,
						"Nodes over, everything else at limits should fail" },
				// proc checks
				{ queueMinimal, queueMinimal.getMaxNodes(), new Long(-1),
						queueMinimal.getMaxMemoryPerNode(),
						queueMinimal.getMaxRequestedTime(), pass,
						"Procs unbounded, everything else at limits should pass" },
				{ queueMinimal, queueMinimal.getMaxNodes(), new Long(-2),
						queueMinimal.getMaxMemoryPerNode(),
						queueMinimal.getMaxRequestedTime(), fail,
						"Procs negative, everything else at limits should fail" },
				{ queueMinimal, queueMinimal.getMaxNodes(), new Long(0),
						queueMinimal.getMaxMemoryPerNode(),
						queueMinimal.getMaxRequestedTime(), fail,
						"Procs zero, everything else at limits should fail" },
				{ queueMinimal, queueMinimal.getMaxNodes(), null,
						queueMinimal.getMaxMemoryPerNode(),
						queueMinimal.getMaxRequestedTime(), fail,
						"Procs null, everything else at limits should fail" },
				{ queueMinimal, queueMinimal.getMaxNodes(),
						(queueMinimal.getMaxNodes() + (long) 1),
						queueMinimal.getMaxMemoryPerNode(),
						queueMinimal.getMaxRequestedTime(), fail,
						"Procs over, everything else at limits should fail" },
				// memory checks
				{ queueMinimal, queueMinimal.getMaxNodes(),
						queueMinimal.getMaxProcessorsPerNode(), new Double(-1),
						queueMinimal.getMaxRequestedTime(), pass,
						"Memory unbounded, everything else at limits should pass" },
				{ queueMinimal, queueMinimal.getMaxNodes(),
						queueMinimal.getMaxProcessorsPerNode(), new Double(-2),
						queueMinimal.getMaxRequestedTime(), fail,
						"Memory negative, everything else at limits should fail" },
				{ queueMinimal, queueMinimal.getMaxNodes(),
						queueMinimal.getMaxProcessorsPerNode(), new Double(0),
						queueMinimal.getMaxRequestedTime(), fail,
						"Memory zero, everything else at limits should fail" },
				{ queueMinimal, queueMinimal.getMaxNodes(),
						queueMinimal.getMaxProcessorsPerNode(), null,
						queueMinimal.getMaxRequestedTime(), fail,
						"Memory null, everything else at limits should fail" },
				{ queueMinimal, queueMinimal.getMaxNodes(),
						queueMinimal.getMaxProcessorsPerNode(),
						(queueMinimal.getMaxMemoryPerNode() + (double) 1),
						queueMinimal.getMaxRequestedTime(), fail,
						"Memory over, everything else at limits should fail" },
				// time checks
				{ queueMinimal, 
						queueMinimal.getMaxNodes(),
						queueMinimal.getMaxProcessorsPerNode(),
						queueMinimal.getMaxMemoryPerNode(),
						"00:01:00", pass,
						"Time equal, everything else at limits should pass" },
				{ queueMinimal, 
						queueMinimal.getMaxNodes(),
						queueMinimal.getMaxProcessorsPerNode(), 
						queueMinimal.getMaxMemoryPerNode(),
						"00:00:30", pass,
						"Time under, everything else at limits should pass" },
				{ queueMinimal, 
						queueMinimal.getMaxNodes(),
						queueMinimal.getMaxProcessorsPerNode(), 
						queueMinimal.getMaxMemoryPerNode(),
						"00:03:00", fail,
						"Time over, everything else at limits should fail" },
				{ queueMinimal, 
						queueMinimal.getMaxNodes(),
						queueMinimal.getMaxProcessorsPerNode(), 
						queueMinimal.getMaxMemoryPerNode(),
						null, fail,
						"Time null, everything else at limits should fail" },

				// unbounded queue
				{ queueUnbounded, queueUnbounded.getMaxNodes(),
						queueUnbounded.getMaxProcessorsPerNode(),
						queueUnbounded.getMaxMemoryPerNode(),
						queueUnbounded.getMaxRequestedTime(), pass,
						"Everything at unbounded limits should pass" },
				{ queueUnbounded, new Long(1), new Long(1), new Double(1),
						"00:01:00", pass,
						"Everything under unbounded limits should pass" },
				{ queueUnbounded, null, null, null, null, fail,
						"Everything null should fail" },
				{ queueUnbounded, new Long(-2), new Long(-2), new Double(-2),
						"00:01:00", fail, "Everything negative should fail" },
				// node checks
				{ queueUnbounded, new Long(1),
						queueUnbounded.getMaxProcessorsPerNode(),
						queueUnbounded.getMaxMemoryPerNode(),
						queueUnbounded.getMaxRequestedTime(), pass,
						"Nodes under, everything else unbounded should pass" },
				{ queueUnbounded, new Long(-2),
						queueUnbounded.getMaxProcessorsPerNode(),
						queueUnbounded.getMaxMemoryPerNode(),
						queueUnbounded.getMaxRequestedTime(), fail,
						"Nodes negative, everything else unbounded should fail" },
				{ queueUnbounded, new Long(0),
						queueUnbounded.getMaxProcessorsPerNode(),
						queueUnbounded.getMaxMemoryPerNode(),
						queueUnbounded.getMaxRequestedTime(), fail,
						"Nodes zero, everything else unbounded should fail" },
				{ queueUnbounded, null,
						queueUnbounded.getMaxProcessorsPerNode(),
						queueUnbounded.getMaxMemoryPerNode(),
						queueUnbounded.getMaxRequestedTime(), fail,
						"Nodes null, everything else unbounded should fail" },
				// proc checks
				{ queueUnbounded, 
						queueUnbounded.getMaxNodes(), 
						new Long(1),
						queueUnbounded.getMaxMemoryPerNode(),
						queueUnbounded.getMaxRequestedTime(), pass,
						"Procs under, everything else unbounded should pass" },
				{ queueUnbounded, 
						queueUnbounded.getMaxNodes(), 
						new Long(-2),
						queueUnbounded.getMaxMemoryPerNode(),
						queueUnbounded.getMaxRequestedTime(), fail,
						"Procs negative, everything else unbounded should fail" },
				{ queueUnbounded, 
						queueUnbounded.getMaxNodes(), 
						new Long(0),
						queueUnbounded.getMaxMemoryPerNode(),
						queueUnbounded.getMaxRequestedTime(), fail,
						"Procs zero, everything else unbounded should fail" },
				{ queueUnbounded, 
						queueUnbounded.getMaxNodes(), 
						null,
						queueUnbounded.getMaxMemoryPerNode(),
						queueUnbounded.getMaxRequestedTime(), fail,
						"Procs null, everything else unbounded should fail" },
				// memory checks
				{ queueUnbounded, 
						queueUnbounded.getMaxNodes(),
						queueUnbounded.getMaxProcessorsPerNode(),
						new Double(1), 
						queueUnbounded.getMaxRequestedTime(), pass,
						"Memory under, everything else unbounded should fail" },
				{ queueUnbounded, 
						queueUnbounded.getMaxNodes(),
						queueUnbounded.getMaxProcessorsPerNode(),
						new Double(-2), 
						queueUnbounded.getMaxRequestedTime(), fail,
						"Memory negative, everything else unbounded should fail" },
				{ queueUnbounded, 
						queueUnbounded.getMaxNodes(),
						queueUnbounded.getMaxProcessorsPerNode(),
						new Double(0), 
						queueUnbounded.getMaxRequestedTime(), fail,
						"Memory zero, everything else unbounded should fail" },
				{ queueUnbounded, 
						queueUnbounded.getMaxNodes(),
						queueUnbounded.getMaxProcessorsPerNode(), 
						null,
						queueUnbounded.getMaxRequestedTime(), fail,
						"Memory null, everything else unbounded should fail" },
				// time checks
				{ queueUnbounded, 
						queueUnbounded.getMaxNodes(),
						queueUnbounded.getMaxProcessorsPerNode(),
						queueUnbounded.getMaxMemoryPerNode(), 
						"00:01:00", pass,
						"Time equal, everything else unbounded should pass" },
				{ queueUnbounded, 
						queueUnbounded.getMaxNodes(),
						queueUnbounded.getMaxProcessorsPerNode(),
						queueUnbounded.getMaxMemoryPerNode(), 
						"00:00:00", fail,
						"Time zero, everything else unbounded should fail" },
				{ queueUnbounded, 
						queueUnbounded.getMaxNodes(),
						queueUnbounded.getMaxProcessorsPerNode(),
						queueUnbounded.getMaxMemoryPerNode(),  
						null, fail,
						"Time null, everything else unbounded should fail" }, };
	}
	
	/**
	 * Tests that batch submit parameters given in a job description are validated 
	 * properly against the limits of a given queue.
	 * 
	 * @param queue
	 * @param nodes
	 * @param processors
	 * @param memory
	 * @param requestedTime
	 * @param shouldPass
	 * @param message
	 */
	@Test(dataProvider = "validateBatchSubmitParametersProvider")
	public void validateBatchSubmitParameters(BatchQueue queue, Long nodes, Long processors, Double memory, String requestedTime, boolean shouldPass, String message) 
	{
		Assert.assertEquals(shouldPass, 
				JobManager.validateBatchSubmitParameters(queue, nodes, processors, memory, requestedTime), 
				message);
	}
	
	@DataProvider
	public Object[][] selectQueueLimitTestProvider() throws Exception {
		//TODO: isolate combinations of queues, systems, apps, etc to test
		
		ExecutionSystem system = new ExecutionSystem();
		// name maxJobs userJobs nodes memory, procs, time, cstm, default
		
		BatchQueue queueDefault = new BatchQueue("queueDefault", (long) 1, (long) 1, (long) 1, (double) 1.0, (long) 1, "01:00:00", null, true);
		BatchQueue queueTwo = new BatchQueue("queueTwo", (long) 2, (long) 2, (long) 2, (double) 2.0, (long) -1, "02:00:00", null, false);
		BatchQueue queueTen = new BatchQueue("queueTen", (long) 10, (long) 10, (long) 10, (double) 10, (long) 1, "10:00:00", null, false);
		BatchQueue queueHundred = new BatchQueue("queueHundred", (long) 100, (long) 100, (long) 100, (double) 100, (long) 100, "100:00:00", null, false);
		BatchQueue queueUnbounded = new BatchQueue("queueMax", (long) -1, (long) -1, (long) -1, (double) -1.0, (long) -1, BatchQueue.DEFAULT_MAX_RUN_TIME, null, false);
		
		BatchQueue[] allQueues = {queueDefault, queueTwo, queueTen, queueHundred, queueUnbounded };
		return new Object[][] {
				{ new BatchQueue[] { queueDefault, queueUnbounded }, (long)-1, (double)-1, "00:00:01", queueDefault.getName(), "Default queue picked if specs fit." },
				
				{ allQueues, (long) 1, (double)-1, "00:00:01", queueDefault.getName(), "First matching matching queue was not selected" },
				{ allQueues, (long)-1, (double) 1, "00:00:01", queueDefault.getName(), "First matching matching queue was not selected" },
				{ allQueues, (long)-1, (double)-1, "01:00:00", queueDefault.getName(), "First matching matching queue was not selected" },
				
				{ allQueues, (long) 2, (double)-1, "00:00:01", queueTwo.getName(), "First matching matching queue was not selected" },
				{ allQueues, (long)-1, (double) 2, "00:00:01", queueTwo.getName(), "First matching matching queue was not selected" },
				{ allQueues, (long)-1, (double)-1, "02:00:00", queueTwo.getName(), "First matching matching queue was not selected" },
				
				{ allQueues, (long)10, (double)-1, "00:00:01", queueTen.getName(), "First matching matching queue was not selected" },
				{ allQueues, (long)-1, (double)10, "00:00:01", queueTen.getName(), "First matching matching queue was not selected" },
				{ allQueues, (long)-1, (double)-1, "10:00:00", queueTen.getName(), "First matching matching queue was not selected" },
				
				{ allQueues, (long)100, (double)-1, "00:00:01", queueHundred.getName(), "First matching matching queue was not selected" },
				{ allQueues, (long)-1, (double)100, "00:00:01", queueHundred.getName(), "First matching matching queue was not selected" },
				{ allQueues, (long)-1, (double)-1, "100:00:00", queueHundred.getName(), "First matching matching queue was not selected" },
				
				{ allQueues, (long)101, (double)-1, "00:00:01", queueUnbounded.getName(), "First matching matching queue was not selected" },
				{ allQueues, (long)-1, (double)101, "00:00:01", queueUnbounded.getName(), "First matching matching queue was not selected" },
				{ allQueues, (long)-1, (double)-1, "101:00:00", queueUnbounded.getName(), "First matching matching queue was not selected" },
		};
	}

	/**
	 * Tests whether the JobManager.selectQueue method returns the expected queue
	 * given a set of inputs.
	 * 
	 * @param executionSystem
	 * @param nodes
	 * @param memory
	 * @param requestedTime
	 * @param queueName
	 * @param message
	 */
	@Test(dataProvider = "selectQueueLimitTestProvider", dependsOnMethods = { "validateBatchSubmitParameters" })
	public void selectQueueLimitTest(BatchQueue[] testQueues, Long nodes, Double memory, String requestedTime, String expectedQueueName, String message) 
	{
		try 
		{
			ExecutionSystem testSystem = privateExecutionSystem.clone();
			testSystem.getBatchQueues().clear();
			for (BatchQueue testQueue: testQueues) {
				testSystem.addBatchQueue(testQueue);
			}
			
			BatchQueue selectedQueue = JobManager.selectQueue(testSystem, nodes, memory, requestedTime);
			String selectedQueueName = (selectedQueue == null ? null : selectedQueue.getName());
			
			Assert.assertEquals(selectedQueueName, expectedQueueName, message);
		} 
		catch (Exception e) {
			Assert.fail(message, e);
		}
	}
	
	/**
	 * Generic method to run the JobManager.processJob(JsonNode, String, String) method.
	 * @param json
	 * @param shouldThrowException
	 * @param message
	 */
	private Job genericProcessJsonJob(ObjectNode json, boolean shouldThrowException, String message)
	{
		Job job = null;
		try 
		{
			job = JobManager.processJob(json, JSONTestDataUtil.TEST_OWNER, null);
			Assert.assertNotNull(job.getId(), "Job was not saved after processing.");
		} catch (JobProcessingException e) {
			if (!shouldThrowException) {
				Assert.fail(message, e);
			}
		} catch (Exception e) {
			Assert.fail("Unexpected failed to process job", e);
		}
		finally {
			try { clearJobs(); } catch (Throwable t) {} 
		}
		
		return job;
	}
	
	
	/**
	 * Generic method to run the JobManager.processJob(Form, String, String) method.
	 * @param form
	 * @param shouldThrowException
	 * @param message
	 */
	private Job genericProcessFormJob(Form form, boolean shouldThrowException, String message)
	{
		Job job = null;
		
		try 
		{
			job = JobManager.processJob(form, JSONTestDataUtil.TEST_OWNER, null);
			Assert.assertNotNull(job.getId(), "Job was not saved after processing.");
		} catch (JobProcessingException e) {
			if (!shouldThrowException) {
				Assert.fail(message, e);
			}
		} catch (Exception e) {
			Assert.fail("Unexpected failed to process job", e);
		}
		finally {
			try { clearJobs(); } catch (Throwable t) {} 
		}
		
		return job;
	}
	
	@DataProvider
	public Object[][] processJsonJobProvider()
	{
		boolean pass = false;
		boolean fail = true;
		return new Object[][] {
				{ "name", null, fail, "Null name should throw exception" },
				{ "name", "", fail, "Empty name should throw exception" },
				{ "name", new Object(), fail, "Object for name should throw exception" },
				{ "name", new ArrayList<String>(), fail, "Array for name should throw exception" },
				{ "name", new Long(1), fail, "Long for name should throw exception" },
				{ "name", new Float(1.0), fail, "Float for name should throw exception" },
				{ "name", new Double(1.0), fail, "Double for name should throw exception" },
				{ "name", new Integer(1), fail, "Integer for name should throw exception" },
				{ "name", new BigDecimal(1), fail, "BigDecimal  for name should throw exception" },
				{ "name", StringUtils.rightPad("h", 64, "h"), fail, "name must be under 64 characters" },
				{ "name", new Boolean(false), fail, "Boolean for name should throw exception" },
				{ "name", new Boolean(true), fail, "Boolean for name should throw exception" },
				// maybe add tests for special characters, chinese characters, etc.
				
				{ "jobName", null, fail, "Null jobName should throw exception" },
				{ "jobName", "", fail, "Empty jobName should throw exception" },
				{ "jobName", new Object(), fail, "Object for jobName should throw exception" },
				{ "jobName", new ArrayList<String>(), fail, "Array for jobName should throw exception" },
				{ "jobName", new Long(1), fail, "Long for jobName should throw exception" },
				{ "jobName", new Float(1.0), fail, "Float for jobName should throw exception" },
				{ "jobName", new Double(1.0), fail, "Double for jobName should throw exception" },
				{ "jobName", new Integer(1), fail, "Integer for jobName should throw exception" },
				{ "jobName", new BigDecimal(1), fail, "BigDecimal  for jobName should throw exception" },
				{ "jobName", StringUtils.rightPad("h", 65, "h"), fail, "jobName must be under 64 characters" },
				{ "jobName", new Boolean(false), fail, "Boolean for jobName should throw exception" },
				{ "jobName", new Boolean(true), fail, "Boolean for jobName should throw exception" },
				
				{ "appId", null, fail, "Null appId should throw exception" },
				{ "appId", "", fail, "Empty appId should throw exception" },
				{ "appId", new Object(), fail, "Object for appId should throw exception" },
				{ "appId", new ArrayList<String>(), fail, "Array for appId should throw exception" },
				{ "appId", new Long(1), fail, "Long for appId should throw exception" },
				{ "appId", new Float(1.0), fail, "Float for appId should throw exception" },
				{ "appId", new Double(1.0), fail, "Double for appId should throw exception" },
				{ "appId", new Integer(1), fail, "Integer for appId should throw exception" },
				{ "appId", new BigDecimal(1), fail, "BigDecimal  for appId should throw exception" },
				{ "appId", StringUtils.rightPad("h", 81, "h"), fail, "appId must be under 80 characters" },
				{ "appId", new Boolean(false), fail, "Boolean for appId should throw exception" },
				{ "appId", new Boolean(true), fail, "Boolean for appId should throw exception" },
				
				{ "softwareName", null, fail, "Null softwareName should throw exception" },
				{ "softwareName", "", fail, "Empty softwareName should throw exception" },
				{ "softwareName", new Object(), fail, "Object for softwareName should throw exception" },
				{ "softwareName", new ArrayList<String>(), fail, "Array for softwareName should throw exception" },
				{ "softwareName", new Long(1), fail, "Long for softwareName should throw exception" },
				{ "softwareName", new Float(1.0), fail, "Float for softwareName should throw exception" },
				{ "softwareName", new Double(1.0), fail, "Double for softwareName should throw exception" },
				{ "softwareName", new Integer(1), fail, "Integer for softwareName should throw exception" },
				{ "softwareName", new BigDecimal(1), fail, "BigDecimal for softwareName should throw exception" },
				{ "softwareName", StringUtils.rightPad("h", 81, "h"), fail, "softwareName must be under 80 characters" },
				{ "softwareName", new Boolean(false), fail, "Boolean for softwareName should throw exception" },
				{ "softwareName", new Boolean(true), fail, "Boolean for softwareName should throw exception" },
				
				{ "executionSystem", null, fail, "Null executionSystem should throw exception" },
				{ "executionSystem", "", fail, "Empty executionSystem should throw exception" },
				{ "executionSystem", new Object(), fail, "Object for executionSystem should throw exception" },
				{ "executionSystem", new ArrayList<String>(), fail, "Array for executionSystem should throw exception" },
				{ "executionSystem", new Long(1), fail, "Long for executionSystem should throw exception" },
				{ "executionSystem", new Float(1.0), fail, "Float for executionSystem should throw exception" },
				{ "executionSystem", new Double(1.0), fail, "Double for executionSystem should throw exception" },
				{ "executionSystem", new Integer(1), fail, "Integer for executionSystem should throw exception" },
				{ "executionSystem", new BigDecimal(1), fail, "BigDecimal for executionSystem should throw exception" },
				{ "executionSystem", StringUtils.rightPad("h", 81, "h"), fail, "executionSystem must be under 80 characters" },
				{ "executionSystem", new Boolean(false), fail, "Boolean for executionSystem should throw exception" },
				{ "executionSystem", new Boolean(true), fail, "Boolean for executionSystem should throw exception" },
				
				{ "executionHost", null, fail, "Null executionHost should throw exception" },
				{ "executionHost", "", fail, "Empty executionHost should throw exception" },
				{ "executionHost", new Object(), fail, "Object for executionHost should throw exception" },
				{ "executionHost", new ArrayList<String>(), fail, "Array for executionHost should throw exception" },
				{ "executionHost", new Long(1), fail, "Long for executionHost should throw exception" },
				{ "executionHost", new Float(1.0), fail, "Float for executionHost should throw exception" },
				{ "executionHost", new Double(1.0), fail, "Double for executionHost should throw exception" },
				{ "executionHost", new Integer(1), fail, "Integer for executionHost should throw exception" },
				{ "executionHost", new BigDecimal(1), fail, "BigDecimal for executionHost should throw exception" },
				{ "executionHost", StringUtils.rightPad("h", 81, "h"), fail, "executionHost must be under 80 characters" },
				{ "executionHost", new Boolean(false), fail, "Boolean for executionHost should throw exception" },
				{ "executionHost", new Boolean(true), fail, "Boolean for executionHost should throw exception" },
				
				{ "batchQueue", null, fail, "Null batchQueue should throw exception" },
				{ "batchQueue", "", fail, "Empty batchQueue should throw exception" },
				{ "batchQueue", new Object(), fail, "Object for batchQueue should throw exception" },
				{ "batchQueue", new ArrayList<String>(), fail, "Array for batchQueue should throw exception" },
				{ "batchQueue", new Long(1), fail, "Long for batchQueue should throw exception" },
				{ "batchQueue", new Float(1.0), fail, "Float for batchQueue should throw exception" },
				{ "batchQueue", new Double(1.0), fail, "Double for batchQueue should throw exception" },
				{ "batchQueue", new Integer(1), fail, "Integer for batchQueue should throw exception" },
				{ "batchQueue", new BigDecimal(1), fail, "BigDecimal for batchQueue should throw exception" },
				{ "batchQueue", new Boolean(false), fail, "Boolean for batchQueue should throw exception" },
				{ "batchQueue", new Boolean(true), fail, "Boolean for batchQueue should throw exception" },
				
				{ "queue", null, fail, "Null queue should throw exception" },
				{ "queue", "", fail, "Empty queue should throw exception" },
				{ "queue", new Object(), fail, "Object for queue should throw exception" },
				{ "queue", new ArrayList<String>(), fail, "Array for queue should throw exception" },
				{ "queue", new Long(1), fail, "Long for queue should throw exception" },
				{ "queue", new Float(1.0), fail, "Float for queue should throw exception" },
				{ "queue", new Double(1.0), fail, "Double for queue should throw exception" },
				{ "queue", new Integer(1), fail, "Integer for queue should throw exception" },
				{ "queue", new BigDecimal(1), fail, "BigDecimal for queue should throw exception" },
				{ "queue", StringUtils.rightPad("h", 129, "h"),fail, "queue must be under 128 characters" },
				{ "queue", new Boolean(false), fail, "Boolean for queue should throw exception" },
				{ "queue", new Boolean(true), fail, "Boolean for queue should throw exception" },
				
				{ "nodeCount", null, fail, "Null nodeCount should throw exception" },
				{ "nodeCount", "", fail, "Empty nodeCount should throw exception" },
				{ "nodeCount", new Object(), fail, "Object for nodeCount should throw exception" },
				{ "nodeCount", new ArrayList<String>(), fail, "Array for nodeCount should throw exception" },
				{ "nodeCount", new Long(1), pass, "Long for nodeCount should pass" },
				{ "nodeCount", new Float(1.0), fail, "Float for nodeCount should fail" },
				{ "nodeCount", new Double(1.0), fail, "Double for nodeCount should fail" },
				{ "nodeCount", new Integer(1), pass, "Integer for nodeCount should pass" },
				{ "nodeCount", new BigDecimal(1), pass, "BigDecimal for nodeCount should pass" },
				{ "nodeCount", new Boolean(false), fail, "Boolean for nodeCount should throw exception" },
				{ "nodeCount", new Boolean(true), fail, "Boolean for nodeCount should throw exception" },
				
				{ "processorsPerNode", null, fail, "Null processorsPerNode should throw exception" },
				{ "processorsPerNode", "", fail, "Empty processorsPerNode should throw exception" },
				{ "processorsPerNode", new Object(), fail, "Object for processorsPerNode should throw exception" },
				{ "processorsPerNode", new ArrayList<String>(), fail, "Array for processorsPerNode should throw exception" },
				{ "processorsPerNode", new Long(1), pass, "Long for processorsPerNode should throw exception" },
				{ "processorsPerNode", new Float(1.0), fail, "Float for processorsPerNode should fail" },
				{ "processorsPerNode", new Double(1.0), fail, "Double for processorsPerNode should fail" },
				{ "processorsPerNode", new Integer(1), pass, "Integer for processorsPerNode should pass" },
				{ "processorsPerNode", new BigDecimal(1), pass, "BigDecimal for processorsPerNode should pass" },
				{ "processorsPerNode", new Boolean(false), fail, "Boolean for processorsPerNode should throw exception" },
				{ "processorsPerNode", new Boolean(true), fail, "Boolean for processorsPerNode should throw exception" },
				
				{ "processorCount", null, fail, "Null processorCount should throw exception" },
				{ "processorCount", "", fail, "Empty processorCount should throw exception" },
				{ "processorCount", new Object(), fail, "Object for processorCount should throw exception" },
				{ "processorCount", new ArrayList<String>(), fail, "Array for processorCount should throw exception" },
				{ "processorCount", new Long(1), pass, "Long for processorCount should throw exception" },
				{ "processorCount", new Float(1.0), fail, "Float for processorCount should fail" },
				{ "processorCount", new Double(1.0), fail, "Double for processorCount should fail" },
				{ "processorCount", new Integer(1), pass, "Integer for processorCount should pass" },
				{ "processorCount", new BigDecimal(1), pass, "BigDecimal for processorCount should pass" },
				{ "processorCount", new Boolean(false), fail, "Boolean for processorCount should throw exception" },
				{ "processorCount", new Boolean(true), fail, "Boolean for processorCount should throw exception" },
				
				{ "memoryPerNode", null, fail, "Null memoryPerNode should throw exception" },
				{ "memoryPerNode", "", fail, "Empty memoryPerNode should throw exception" },
				{ "memoryPerNode", "abracadabra", fail, "Invalid memoryPerNode string should throw exception" },
				{ "memoryPerNode", "1GB", pass, "Vaid string memoryPerNode string should pass" },
				{ "memoryPerNode", new Object(), fail, "Object for memoryPerNode should throw exception" },
				{ "memoryPerNode", new ArrayList<String>(), fail, "Array for memoryPerNode should throw exception" },
				{ "memoryPerNode", new Long(1), pass, "Long for memoryPerNode should pass" },
				{ "memoryPerNode", new Float(1.0), pass, "Float for memoryPerNode should pass" },
				{ "memoryPerNode", new Double(1.0), pass, "Double for memoryPerNode should pass" },
				{ "memoryPerNode", new Integer(1), pass, "Integer for memoryPerNode should pass" },
				{ "memoryPerNode", new BigDecimal(1), pass, "BigDecimal for memoryPerNode should pass" },
				{ "memoryPerNode", new Boolean(false), fail, "Boolean for memoryPerNode should throw exception" },
				{ "memoryPerNode", new Boolean(true), fail, "Boolean for memoryPerNode should throw exception" },
				
				{ "maxMemory", null, fail, "Null maxMemory should throw exception" },
				{ "maxMemory", "", fail, "Empty maxMemory should throw exception" },
				{ "maxMemory", "abracadabra", fail, "Invalid maxMemory string should throw exception" },
				{ "maxMemory", "1GB", pass, "Vaid string maxMemory string should oass" },
				{ "maxMemory", new Object(), fail, "Object for maxMemory should throw exception" },
				{ "maxMemory", new ArrayList<String>(), fail, "Array for maxMemory should throw exception" },
				{ "maxMemory", new Long(1), pass, "Long for maxMemory should pass" },
				{ "maxMemory", new Float(1.0), pass, "Float for maxMemory should pass" },
				{ "maxMemory", new Double(1.0), pass, "Double for maxMemory should pass" },
				{ "maxMemory", new Integer(1), pass, "Integer for maxMemory should pass" },
				{ "maxMemory", new BigDecimal(1), pass, "BigDecimal for maxMemory pass" },
				{ "maxMemory", new Boolean(false), fail, "Boolean for maxMemory should throw exception" },
				{ "maxMemory", new Boolean(true), fail, "Boolean for maxMemory should throw exception" },
				
				{ "maxRunTime", null, fail, "Null maxRunTime should throw exception" },
				{ "maxRunTime", "", fail, "Empty maxRunTime should throw exception" },
				{ "maxRunTime", "asdfasdfasd", fail, "Invalid string maxRunTime should throw exception" },
				{ "maxRunTime", "00:00:01", pass, "Invalid string maxRunTime should pass" },
				{ "maxRunTime", new Object(), fail, "Object for maxRunTime should throw exception" },
				{ "maxRunTime", new ArrayList<String>(), fail, "Array for maxRunTime should throw exception" },
				{ "maxRunTime", new Long(1), fail, "Long for maxRunTime should throw exception" },
				{ "maxRunTime", new Float(1.0), fail, "Float for maxRunTime should throw exception" },
				{ "maxRunTime", new Double(1.0), fail, "Double for maxRunTime should throw exception" },
				{ "maxRunTime", new Integer(1), fail, "Integer for maxRunTime should throw exception" },
				{ "maxRunTime", new BigDecimal(1), fail, "BigDecimal for maxRunTime should throw exception" },
				{ "maxRunTime", new Boolean(false), fail, "Boolean for maxRunTime should throw exception" },
				{ "maxRunTime", new Boolean(true), fail, "Boolean for maxRunTime should throw exception" },
				
				{ "requestedTime", null, fail, "Null requestedTime should throw exception" },
				{ "requestedTime", "", fail, "Empty requestedTime should throw exception" },
				{ "requestedTime", "asdfasdfasd", fail, "Invalid string requestedTime should throw exception" },
				{ "requestedTime", "00:00:01", pass, "Valid string requestedTime should pass" },
				{ "requestedTime", new Object(), fail, "Object for requestedTime should throw exception" },
				{ "requestedTime", new ArrayList<String>(), fail, "Array for requestedTime should throw exception" },
				{ "requestedTime", new Long(1), fail, "Long for requestedTime should throw exception" },
				{ "requestedTime", new Float(1.0), fail, "Float for requestedTime should throw exception" },
				{ "requestedTime", new Double(1.0), fail, "Double for requestedTime should throw exception" },
				{ "requestedTime", new Integer(1), fail, "Integer for requestedTime should throw exception" },
				{ "requestedTime", new BigDecimal(1), fail, "BigDecimal for requestedTime should throw exception" },
				{ "requestedTime", new Boolean(false), fail, "Boolean for requestedTime should throw exception" },
				{ "requestedTime", new Boolean(true), fail, "Boolean for requestedTime should throw exception" },
				
				{ "dependencies", null, fail, "Null dependencies should throw exception" },
				{ "dependencies", "", fail, "Empty dependencies should throw exception" },
				{ "dependencies", "dependencies", fail, "String dependencies should throw exception" },
				{ "dependencies", new Object(), fail, "Object for dependencies should throw exception" },
				{ "dependencies", new ArrayList<String>(), fail, "Array for dependencies should throw exception" },
				{ "dependencies", new Long(1), fail, "Long for dependencies should throw exception" },
				{ "dependencies", new Float(1.0), fail, "Float for dependencies should throw exception" },
				{ "dependencies", new Double(1.0), fail, "Double for dependencies should throw exception" },
				{ "dependencies", new Integer(1), fail, "Integer for dependencies should throw exception" },
				{ "dependencies", new BigDecimal(1), fail, "BigDecimal for dependencies should throw exception" },
				{ "dependencies", new Boolean(false), fail, "Boolean for dependencies should throw exception" },
				{ "dependencies", new Boolean(true), fail, "Boolean for dependencies should throw exception" },
				
				{ "archive", null, fail, "Null archive should throw exception" },
				{ "archive", "", fail, "Empty archive should throw exception" },
				{ "archive", "archive", fail, "String archive should throw exception" },
				{ "archive", new Object(), fail, "Object for archive should throw exception" },
				{ "archive", new ArrayList<String>(), fail, "Array for archive should throw exception" },
				{ "archive", new Long(1), fail, "Long for archive should throw exception" },
				{ "archive", new Float(1.0), fail, "Float for archive should throw exception" },
				{ "archive", new Double(1.0), fail, "Double for archive should throw exception" },
				{ "archive", new Integer(1), fail, "Integer for archive should throw exception" },
				{ "archive", new BigDecimal(1), fail, "BigDecimal for archive should throw exception" },
				{ "archive", new Boolean(false), pass, "Boolean for archive should pass" },
				{ "archive", new Boolean(true), pass, "Boolean for archive should pass" },
				
				{ "archiveSystem", null, fail, "Null archiveSystem should throw exception" },
				{ "archiveSystem", "", fail, "Empty archiveSystem should throw exception" },
				{ "archiveSystem", new Object(), fail, "Object for archiveSystem should throw exception" },
				{ "archiveSystem", new ArrayList<String>(), fail, "Array for archiveSystem should throw exception" },
				{ "archiveSystem", new Long(1), fail, "Long for archiveSystem should throw exception" },
				{ "archiveSystem", new Float(1.0), fail, "Float for archiveSystem should throw exception" },
				{ "archiveSystem", new Double(1.0), fail, "Double for archiveSystem should throw exception" },
				{ "archiveSystem", new Integer(1), fail, "Integer for archiveSystem should throw exception" },
				{ "archiveSystem", new BigDecimal(1), fail, "BigDecimal for archiveSystem should throw exception" },
				{ "archiveSystem", new Boolean(false), fail, "Boolean for archiveSystem should throw exception" },
				{ "archiveSystem", new Boolean(true), fail, "Boolean for archiveSystem should throw exception" },
				
				{ "archivePath", null, fail, "Null archivePath should throw exception" },
				{ "archivePath", "", fail, "Empty archivePath should throw exception" },
				{ "archivePath", new Object(), fail, "Object for archivePath should throw exception" },
				{ "archivePath", new ArrayList<String>(), fail, "Array for archivePath should throw exception" },
				{ "archivePath", new Long(1), fail, "Long for archivePath should throw exception" },
				{ "archivePath", new Float(1.0), fail, "Float for archivePath should throw exception" },
				{ "archivePath", new Double(1.0), fail, "Double for archivePath should throw exception" },
				{ "archivePath", new Integer(1), fail, "Integer for archivePath should throw exception" },
				{ "archivePath", new BigDecimal(1), fail, "BigDecimal for archivePath should throw exception" },
				{ "archivePath", StringUtils.rightPad("h", 81, "h"), fail, "archivePath must be under 80 characters" },
				{ "archivePath", new Boolean(false), fail, "Boolean for archivePath should throw exception" },
				{ "archivePath", new Boolean(true), fail, "Boolean for archivePath should throw exception" },
				
				{ "inputs", null, fail, "Null inputs should throw exception" },
				{ "inputs", "", fail, "Empty inputs should throw exception" },
				{ "inputs", new ArrayList<String>(), fail, "Array for inputs should throw exception" },
				{ "inputs", new Long(1), fail, "Long for inputs should throw exception" },
				{ "inputs", new Float(1.0), fail, "Float for inputs should throw exception" },
				{ "inputs", new Double(1.0), fail, "Double for inputs should throw exception" },
				{ "inputs", new Integer(1), fail, "Integer for inputs should throw exception" },
				{ "inputs", new BigDecimal(1), fail, "BigDecimal for inputs should throw exception" },
				{ "inputs", new Boolean(false), fail, "Boolean for inputs should throw exception" },
				{ "inputs", new Boolean(true), fail, "Boolean for inputs should throw exception" },
				
				{ "parameters", null, fail, "Null parameters should throw exception" },
				{ "parameters", "", fail, "Empty parameters should throw exception" },
				{ "parameters", new ArrayList<String>(), fail, "Array for parameters should throw exception" },
				{ "parameters", new Long(1), fail, "Long for parameters should throw exception" },
				{ "parameters", new Float(1.0), fail, "Float for parameters should throw exception" },
				{ "parameters", new Double(1.0), fail, "Double for parameters should throw exception" },
				{ "parameters", new Integer(1), fail, "Integer for parameters should throw exception" },
				{ "parameters", new BigDecimal(1), fail, "BigDecimal for parameters should throw exception" },
				{ "parameters", new Boolean(false), fail, "Boolean for parameters should throw exception" },
				{ "parameters", new Boolean(true), fail, "Boolean for parameters should throw exception" },
				
		};
	}
	
	/**
	 * Tests basic field validation on jobs submitted as json
	 * 
	 * @param field
	 * @param value
	 * @param internalUsername
	 * @param shouldThrowException
	 * @param message
	 */
	@Test(dataProvider = "processJsonJobProvider", dependsOnMethods = { "selectQueueLimitTest" })
	public void processJsonJob(String field, Object value, boolean shouldThrowException, String message) 
	{
		try
		{
			ObjectNode json = createJobJsonNode();
			
			json = updateObjectNode(json, field, value);
		
			genericProcessJsonJob(json, shouldThrowException, message);
		} 
		finally {
			try { clearJobs(); } catch (Throwable e) {}
		}
	}
	
	@DataProvider
	public Object[][] processJsonJobInputsProvider() 
	{
		ObjectMapper mapper = new ObjectMapper();
		
		SoftwareInput input = software.getInputs().iterator().next();
		boolean pass = false;
		boolean fail = true;
		// need to verify cardinality
		return new Object[][] {
				{ mapper.createObjectNode().put(input.getKey(), "/path/to/folder"), pass, "absolute path should pass" },
				{ mapper.createObjectNode().put(input.getKey(), "path/to/folder"), pass, "relative path should pass" },
				{ mapper.createObjectNode().put(input.getKey(), "folder"), pass, "relative file should pass" },
				{ mapper.createObjectNode().put(input.getKey(), String.format("%1$s;%1$s", input.getDefaultValue())), pass, "Semicolon separated lists of inputs are allowed" },
				{ mapper.createObjectNode().put(input.getKey(), String.format("%1$s;;%1$s", input.getDefaultValue(), input.getDefaultValue())), pass, "Double semicolons should be ignored and treated as a single semicolon" },
				{ mapper.createObjectNode().put(input.getKey(), "http://example.com"), pass, "HTTP uri schema should pass" },
				{ mapper.createObjectNode().put(input.getKey(), "https://example.com"), pass, "HTTPS uri schema should pass" },
				{ mapper.createObjectNode().put(input.getKey(), "http://foo:bar@example.com"), pass, "HTTP with basic auth uri schema should pass" },
				{ mapper.createObjectNode().put(input.getKey(), "https://foo:bar@example.com"), pass, "HTTP with basic auth uri schema should pass" },
				{ mapper.createObjectNode().put(input.getKey(), "sftp://example.com"), pass, "SFTP uri schema should pass" },
				{ mapper.createObjectNode().put(input.getKey(), "sftp://foo:bar@example.com/"), pass, "SFTP with basic auth uri schema should pass" },
				{ mapper.createObjectNode().put(input.getKey(), "agave://example.com"), pass, "agave uri schema should pass" },
				{ mapper.createObjectNode().put(input.getKey(), "agave://" + privateExecutionSystem.getSystemId() + "/"), pass, "agave uri schema should pass" },
				
				{ mapper.createObjectNode().put(input.getKey(), "HTTP://example.com"), pass, "HTTP uri schema is case insensitive and should pass" },
				{ mapper.createObjectNode().put(input.getKey(), "HTTPS://example.com"), pass, "HTTPS uri schema should pass" },
				{ mapper.createObjectNode().put(input.getKey(), "HTTP://foo:bar@example.com"), pass, "HTTP with basic auth uri schema is case insensitive and should pass" },
				{ mapper.createObjectNode().put(input.getKey(), "HTTPS://foo:bar@example.com"), pass, "HTTP with basic auth uri schema is case insensitive and should pass" },
				{ mapper.createObjectNode().put(input.getKey(), "SFTP://example.com"), pass, "SFTP uri schema is case insensitive and should pass" },
				{ mapper.createObjectNode().put(input.getKey(), "SFTP://foo:bar@example.com/"), pass, "SFTP with basic auth uri schema is case insensitive and should pass" },
				{ mapper.createObjectNode().put(input.getKey(), "AGAVE://example.com"), pass, "gridftp uri schema is case insensitive and should pass" },
				{ mapper.createObjectNode().put(input.getKey(), "AGAVE://" + privateExecutionSystem.getSystemId() + "/"), pass, "agave uri schema is case insensitive and should pass" },
				
				// unsupported schemas
				{ mapper.createObjectNode().put(input.getKey(), "file:///path/to/folder"), fail, "FILE uri schema should fail" },
				{ mapper.createObjectNode().put(input.getKey(), "ftp://example.com"), fail, "FTP uri schema should fail" },
				{ mapper.createObjectNode().put(input.getKey(), "ftp://foo:bar@example.com/"), fail, "FTP with basic auth uri schema should fail" },
				{ mapper.createObjectNode().put(input.getKey(), "ftps://example.com"), fail, "FTPS uri schema should fail" },
				{ mapper.createObjectNode().put(input.getKey(), "ftps://foo:bar@example.com/"), fail, "FTPS with basic auth uri schema should fail" },
				{ mapper.createObjectNode().put(input.getKey(), "gsiftp://example.com"), fail, "GSIFTP uri schema should fail" },
				{ mapper.createObjectNode().put(input.getKey(), "gsiftp://foo:bar@example.com/"), fail, "GSIFTP with basic auth uri schema should fail" },
				{ mapper.createObjectNode().put(input.getKey(), "gridftp://example.com"), fail, "gridftp uri schema should fail" },
				{ mapper.createObjectNode().put(input.getKey(), "gridftp://foo:bar@example.com/"), fail, "GRIDFTP with basic auth uri schema should fail" },
				{ mapper.createObjectNode().put(input.getKey(), "s3://s3.amazon.com/abced"), fail, "s3 uri schema should fail" },
				{ mapper.createObjectNode().put(input.getKey(), "s3://foo:bar@s3.amazon.com/abced"), fail, "s3 with basic auth uri schema should fail" },
				
				{ mapper.createObjectNode().put(input.getKey(), "FILE:///path/to/folder"), fail, "FILE uri schema should fail" },
				{ mapper.createObjectNode().put(input.getKey(), "FTP://example.com"), fail, "FTP uri schema is case insensitive and should fail" },
				{ mapper.createObjectNode().put(input.getKey(), "FTP://foo:bar@example.com/"), fail, "FTP with basic auth uri schema is case insensitive and should fail" },
				{ mapper.createObjectNode().put(input.getKey(), "FTPS://example.com"), fail, "FTPS uri schema is case insensitive and should fail" },
				{ mapper.createObjectNode().put(input.getKey(), "FTPS://foo:bar@example.com/"), fail, "FTPS with basic auth uri schema is case insensitive and should fail" },
				{ mapper.createObjectNode().put(input.getKey(), "GSIFTP://example.com"), fail, "GSIFTP uri schema is case insensitive and should fail" },
				{ mapper.createObjectNode().put(input.getKey(), "GSIFTP://foo:bar@example.com/"), fail, "GSIFTP with basic auth uri schema is case insensitive and should fail" },
				{ mapper.createObjectNode().put(input.getKey(), "GRIDFTP://example.com"), fail, "gridftp uri schema is case insensitive and should fail" },
				{ mapper.createObjectNode().put(input.getKey(), "GRIDFTP://foo:bar@example.com/"), fail, "GRIDFTP with basic auth uri schema is case insensitive and should fail" },
				{ mapper.createObjectNode().put(input.getKey(), "S3://s3.amazon.com/abced"), fail, "s3 uri schema is case insensitive and should fail" },
				{ mapper.createObjectNode().put(input.getKey(), "S3://foo:bar@s3.amazon.com/abced"), fail, "s3 with basic auth uri schema is case insensitive and should fail" },
				
				{ mapper.createObjectNode().put(input.getKey(), "abba://example.com"), fail, "Unknown uri schema should fail" },
				{ mapper.createObjectNode().put(input.getKey(), "ABBA://example.com"), fail, "Unknown uri schema is case insensitive and should fail" },
		};
		
	}
	
	/**
	 * Tests job app input validation on jobs submitted as json
	 * 
	 * @param jobInputs
	 * @param shouldThrowException
	 * @param message
	 */
	@Test(dataProvider = "processJsonJobInputsProvider", dependsOnMethods={"processJsonJob"})
	public void processJsonJobInputs(ObjectNode jobInputs, boolean shouldThrowException, String message) 
	{
		ObjectNode json = createJobJsonNode();
		json.put("inputs", jobInputs);
		genericProcessJsonJob(json, shouldThrowException, message);
	}

	/**
	 * Tests job app input validation on jobs submitted as json
	 * 
	 * @param jobInputs
	 * @param shouldThrowException
	 * @param message
	 */
	@Test(dependsOnMethods={"processJsonJobInputs"})
	public void processJsonJobInputDefaults() 
	{
		Software testSoftware = null;
		
		try {
			testSoftware = software.clone();
			testSoftware.setOwner(JSONTestDataUtil.TEST_OWNER);
			testSoftware.setName("processJsonJobInputDefaults");
			testSoftware.getInputs().clear();
			
//			SoftwareInput input1 = new SoftwareInput();
//			input1.setDefaultValue("/usr/bin/date");
//			input1.setKey("requiredKey");
//			input1.setRequired(true);
//			testSoftware.addInput(input1);
			
			SoftwareInput input2 = new SoftwareInput();
			input2.setDefaultValue("/usr/bin/time");
			input2.setKey("hiddenTime");
			input2.setVisible(false);
			testSoftware.addInput(input2);
			
			SoftwareInput input3 = new SoftwareInput();
			input3.setDefaultValue("/usr/bin/mkdir");
			input3.setKey("optionalKey1");
			input3.setRequired(false);
			testSoftware.addInput(input3);
			
			SoftwareInput input4 = new SoftwareInput();
			input4.setDefaultValue("/usr/bin/top");
			input4.setKey("requiredKey2");
			input4.setRequired(true);
			testSoftware.addInput(input4);
			
			SoftwareInput input5 = new SoftwareInput();
			input5.setDefaultValue("/usr/bin/ls");
			input5.setKey("optionalKey2");
			input5.setRequired(false);
			testSoftware.addInput(input5);
			
			SoftwareDao.persist(testSoftware);
			
			ObjectNode json = createJobJsonNode();
			json.put("appId", testSoftware.getUniqueName());
			json.put("inputs", new ObjectMapper().createObjectNode().put(input5.getKey(), "wazzup").put(input4.getKey(), "top").put("dummyfield", "something"));
			
			Job job = genericProcessJsonJob(json, false, "Hidden and required inputs should be added by default.");
			
//			Assert.assertTrue(job.getInputsAsMap().containsKey(input1.getKey()), "Required fields should be added if not specified and a default exists");
//			Assert.assertEquals(job.getInputsAsMap().get(input1.getKey()), input1.getDefaultValue(), "Required fields should be added if not specified and a default exists");
			
			Assert.assertTrue(job.getInputsAsMap().containsKey(input2.getKey()), "Hidden fields should always be added");
			Assert.assertEquals(job.getInputsAsMap().get(input2.getKey()), input2.getDefaultValue(), "Hidden fields should always be added");
			
			Assert.assertFalse(job.getInputsAsMap().containsKey(input3.getKey()), "Optional fields should not be added if user does not supply value");
			Assert.assertFalse(job.getInputsAsMap().containsKey("dummyfield"), "User supplied fields not part of the job should not be persisted.");
			
			Assert.assertTrue(job.getInputsAsMap().containsKey(input4.getKey()), "User supplied required fields should be persisted");
			Assert.assertEquals(job.getInputsAsMap().get(input4.getKey()), "top", "Required field that user supplies as input should be the value persisted with the job");
			
			Assert.assertTrue(job.getInputsAsMap().containsKey(input5.getKey()), "User supplied optional fields should be persisted");
			Assert.assertEquals(job.getInputsAsMap().get(input5.getKey()), "wazzup", "Option field that user supplies as input should be the value persisted with the job");
			
		} catch (Exception e) {
			Assert.fail("Failed to process job", e);
		}
		finally {
			try { SoftwareDao.delete(testSoftware); } catch (Throwable t) {}
			try { clearJobs(); } catch (Throwable t) {}
		}
	}
	
	@DataProvider
	public Object[][] processJsonJobParametersProvider() throws JSONException
	{
		
		SoftwareParameter parameter = software.getParameters().iterator().next();
		String paramName = parameter.getKey();
		// need to verify cardinality
		Map<SoftwareParameterType, String> defaultValues = new HashMap<SoftwareParameterType, String>();
		defaultValues.put(SoftwareParameterType.bool, "1");
		defaultValues.put(SoftwareParameterType.enumeration, "ALPHA");
		defaultValues.put(SoftwareParameterType.number, "512");
		defaultValues.put(SoftwareParameterType.string, "somedefaultvalue");
		
		Map<SoftwareParameterType, String> validTestValues = new HashMap<SoftwareParameterType, String>();
		validTestValues.put(SoftwareParameterType.bool, "0");
		validTestValues.put(SoftwareParameterType.enumeration, "BETA");
		validTestValues.put(SoftwareParameterType.number, "215");
		validTestValues.put(SoftwareParameterType.string, "anoteruservalue");
		
		Map<SoftwareParameterType, List<String>> invalidTestValues = new HashMap<SoftwareParameterType, List<String>>();
		
		for (SoftwareParameterType type: SoftwareParameterType.values()) {
			List<String> invalidValues = new ArrayList<String>();
			for (SoftwareParameterType validType: validTestValues.keySet()) {
				if (type.equals(validType)) 
					continue;
				else
					invalidValues.add(validTestValues.get(validType));
			}
			invalidTestValues.put(type, invalidValues);
		}
		
		List<Object[]> testData = new ArrayList<Object[]>();
		for (SoftwareParameterType type: SoftwareParameterType.values()) 
		{
													//	name			type		default					validator	required	visible
			testData.add(new Object[] { createParameter(paramName, type.name(), defaultValues.get(type), 	null, 		false, 		false), mapper.createObjectNode().put(paramName, validTestValues.get(type)), null										  , fail, "Hidden param should fail if user provides value" });
			testData.add(new Object[] { createParameter(paramName, type.name(), defaultValues.get(type), 	null, 		true, 		false), mapper.createObjectNode().put(paramName, validTestValues.get(type)), null										  , fail, "Hidden param should fail if user provides value" });
			testData.add(new Object[] { createParameter(paramName, type.name(), defaultValues.get(type), 	null, 		true, 		false), null, 														 		 mapper.createObjectNode().put(paramName, defaultValues.get(type)), pass, "Hidden param should use default." });
			testData.add(new Object[] { createParameter(paramName, type.name(), defaultValues.get(type), 	null, 		false, 		false), null, 																 mapper.createObjectNode().put(paramName, defaultValues.get(type)), pass, "Hidden param should use default." });
			
			testData.add(new Object[] { createParameter(paramName, type.name(), defaultValues.get(type), 	null, 		false, 		true), null, 																 null, pass, "Visible param not required should not use default when no user data given" });
			testData.add(new Object[] { createParameter(paramName, type.name(), defaultValues.get(type), 	null, 		true, 		true), null, 																 mapper.createObjectNode().put(paramName, defaultValues.get(type)), fail, "Required param should fail if not supplied" });
			testData.add(new Object[] { createParameter(paramName, type.name(), defaultValues.get(type), 	null, 		false, 		true), mapper.createObjectNode().put(paramName, validTestValues.get(type)),  mapper.createObjectNode().put(paramName, validTestValues.get(type)), pass, "User supplied value should be used for visible optional values of " + type.name() + " parameter" });
			testData.add(new Object[] { createParameter(paramName, type.name(), defaultValues.get(type), 	null, 		true, 		true), mapper.createObjectNode().put(paramName, validTestValues.get(type)),  mapper.createObjectNode().put(paramName, validTestValues.get(type)), pass, "User supplied value should be used for visible required values of " + type.name() + " parameter" });
			
			
			
			if (type.equals(SoftwareParameterType.enumeration))
			{
				// validate all enumerated values will work
				SoftwareParameter p1 = createParameter(paramName, type.name(), defaultValues.get(type), null, 		true, 		true);
				for (String enumValue: p1.getValidatorAsList()) 
				{
					testData.add(new Object[] { p1, mapper.createObjectNode().put(paramName, enumValue),  mapper.createObjectNode().put(paramName, enumValue), pass, "Valid required enumerated value of " + enumValue + " is within the available values of " + p1.getValidator() + " and should pass" });
				}
				
				SoftwareParameter p2 = createParameter(paramName, type.name(), defaultValues.get(type), null, 		true, 		true);
				for (String enumValue: p2.getValidatorAsList()) 
				{
					testData.add(new Object[] { p2, mapper.createObjectNode().put(paramName, enumValue),  mapper.createObjectNode().put(paramName, enumValue), pass, "Valid optinal enumerated value of " + enumValue + " is within the available values of " + p2.getValidator() + " and should pass" });
				}
			}
			else if (!type.equals(SoftwareParameterType.string))
			{
				if (type.equals(SoftwareParameterType.bool))
				{
					testData.add(new Object[] { createParameter(paramName, type.name(), defaultValues.get(type), 	null, 		false, 		true), mapper.createObjectNode().put(paramName, "true"),  mapper.createObjectNode().put(paramName, "1"), pass, "User supplied value 1 should be used for visible optional values of " + type.name() + " parameter" });
					testData.add(new Object[] { createParameter(paramName, type.name(), defaultValues.get(type), 	null, 		true, 		true), mapper.createObjectNode().put(paramName, "true"),  mapper.createObjectNode().put(paramName, "1"), pass, "User supplied value 1 should be used for visible required values of " + type.name() + " parameter" });
					testData.add(new Object[] { createParameter(paramName, type.name(), defaultValues.get(type), 	null, 		false, 		true), mapper.createObjectNode().put(paramName, "false"),  mapper.createObjectNode().put(paramName, "0"), pass, "User supplied value 0 should be used for visible optional values of " + type.name() + " parameter" });
					testData.add(new Object[] { createParameter(paramName, type.name(), defaultValues.get(type), 	null, 		true, 		true), mapper.createObjectNode().put(paramName, "false"),  mapper.createObjectNode().put(paramName, "0"), pass, "User supplied value 0 should be used for visible required values of " + type.name() + " parameter" });
				}
				
				for (String invalidValue: invalidTestValues.get(type)) {
					if (!type.equals(SoftwareParameterType.number)) {
						testData.add(new Object[] { createParameter(paramName, type.name(), defaultValues.get(type), 	null, 		true, 		true), mapper.createObjectNode().put(paramName, invalidValue),  null, fail, "Invalid required value " + invalidValue + " should fail for " + type.name() + " parameter" });
						testData.add(new Object[] { createParameter(paramName, type.name(), defaultValues.get(type), 	null, 		false, 		true), mapper.createObjectNode().put(paramName, invalidValue),  null, fail, "Invalid optional value " + invalidValue + " should fail for " + type.name() + " parameter" });
					}
				}
			}
		}
		return testData.toArray(new Object[][] {});
	}
	
	/**
	 * Tests job app parameter validation on jobs submitted as json
	 * 
	 * @param jobParameters
	 * @param shouldThrowException
	 * @param message
	 */
	@Test(dataProvider = "processJsonJobParametersProvider", dependsOnMethods={"processJsonJobInputDefaults"})
	public void processJsonJobParameters(SoftwareParameter appParameter, ObjectNode jobParameters, ObjectNode expectedParameters, boolean shouldThrowException, String message) 
	{
		Software testSoftware = null;
		
		try {
			testSoftware = software.clone();
			testSoftware.setOwner(JSONTestDataUtil.TEST_OWNER);
			testSoftware.setName("processJsonJobInputDefaults");
			testSoftware.getParameters().clear();
			SoftwareDao.persist(testSoftware);
			if (appParameter != null) {
				testSoftware.addParameter(appParameter);
			}
			
			SoftwareDao.merge(testSoftware);
			
			ObjectNode json = createJobJsonNode();
			json.put("appId", testSoftware.getUniqueName());
			if (jobParameters == null) {
				json.remove("parameters");
			} else {
				json.put("parameters", jobParameters);
			}
			
			Job job = JobManager.processJob(json, JSONTestDataUtil.TEST_OWNER, null);
			
			Assert.assertNotNull(job.getId(), "Job was not saved after processing.");
			
			
			if (expectedParameters != null) {
				Iterator<String> fieldNamesIterator = expectedParameters.fieldNames();
				while (fieldNamesIterator.hasNext())
				{
					String fieldName = fieldNamesIterator.next();
					Assert.assertTrue(job.getParametersAsMap().containsKey(fieldName), message);
					String foundParameter = (String)job.getParametersAsMap().get(appParameter.getKey());
					String expectedParameter = expectedParameters.get(fieldName).asText();
					if (appParameter.getType().equals(SoftwareParameterType.number)) {
						foundParameter = new Double(foundParameter).toString();
						expectedParameter = new Double(expectedParameter).toString();
					} 
					Assert.assertEquals(foundParameter, expectedParameter, 
							"Unexpected value for field " + fieldName + " found. Expected " + expectedParameter + 
							" found " + foundParameter);
				}
			}
			else
			{
				Assert.assertTrue(job.getParametersAsMap().isEmpty(), message);
			}
		} catch (JobProcessingException e) {
			if (!shouldThrowException) {
				Assert.fail(message, e);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Failed to process job", e);
		}
		finally {
			try { SoftwareDao.delete(testSoftware); } catch (Throwable t) {}
			try { clearJobs(); } catch (Throwable t) {}
		}
	}
	
	/**
	 * Tests job app parameter validation on jobs submitted as json
	 * 
	 * @param jobParameters
	 * @param shouldThrowException
	 * @param message
	 */
	@Test(dependsOnMethods={"processJsonJobParameters"})
	public void processJsonJobHiddenParametersThrowExceptionWhenProvided() 
	{
		Software testSoftware = null;
		
		try {
			testSoftware = software.clone();
			testSoftware.setOwner(JSONTestDataUtil.TEST_OWNER);
			testSoftware.setName("processJsonJobInputDefaults");
			testSoftware.getParameters().clear();
			SoftwareParameter param = createParameter("hiddenparam", SoftwareParameterType.bool.name(), "true", 	null, 		true, 		false);
			testSoftware.addParameter(param);
			
			SoftwareDao.persist(testSoftware);
			
			ObjectNode json = createJobJsonNode();
			json.put("appId", testSoftware.getUniqueName());
			json.put("parameters", mapper.createObjectNode().put(param.getKey(), true));
			
			JobManager.processJob(json, JSONTestDataUtil.TEST_OWNER, null);
					
			Assert.fail("User supplied value for a hidden parameter should fail.");
		}
		catch (JobProcessingException e) {
			// this is expected;
		} catch (Exception e) {
			Assert.fail("Unexpected exception parsing job", e);
		}
		finally {
			try { SoftwareDao.delete(testSoftware); } catch (Throwable t) {}
			try { clearJobs(); } catch (Throwable t) {}
		}
	}
	
	@DataProvider
	public Object[][] processJsonJobWithNotificationsProvider()
	{
		ObjectMapper mapper = new ObjectMapper();
		Object[] validUrls = { "http://example.com", "http://foo@example.com", "http://foo:bar@example.com", "http://example.com/job/${JOB_ID}/${JOB_STATUS}", "http://example.com?foo=${JOB_ID}", "foo@example.com"};
		Object[] invalidWebhookUrls = { "example.com", "example", "", null, new Long(1), mapper.createArrayNode(), mapper.createObjectNode() };
		Object[] invalidEmailAddresses = { "@example.com", "@example", "foo@example", "foo@", "@.com", "foo@.com" };
		Object[] validEvents = { JobStatusType.RUNNING.name(), JobStatusType.RUNNING.name().toLowerCase() };
		Object[] invalidEvents = { "", null, new Long(1),  mapper.createArrayNode(), mapper.createObjectNode() };
		
		boolean pass = false;
		boolean fail = true;
		
		JsonNode validNotificationJsonNode = createJsonNotification(validUrls[0], JobStatusType.FINISHED.name(), false);
		
		List<Object[]> testCases = new ArrayList<Object[]>();
		for (Object url: validUrls) {
			for (Object event: validEvents) {
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(url, event, false)), pass, "Valid notifications should pass" });
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(url, event, true)), pass, "Valid notifications should pass" });
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(url, event)), pass, "Valid notifications without persistence field should pass" });
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(url, event)), pass, "Valid notifications without persistence field should pass" });
				
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(url, event, false)).add(validNotificationJsonNode), pass, "Valid multiple notifications should pass" });
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(url, event, true)).add(validNotificationJsonNode), pass, "Valid multiple notifications should pass" });
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(url, event)).add(validNotificationJsonNode), pass, "Valid multiple notifications without persistence field should pass" });
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(url, event)).add(validNotificationJsonNode), pass, "Valid multiple notifications without persistence field should pass" });
			}
			
			for (Object invalidEvent: invalidEvents) {
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(url, invalidEvent, false)), fail, "Invalid notifications event " + invalidEvent + " should fail" });
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(url, invalidEvent, true)), fail, "Invalid notifications event " + invalidEvent + " should fail" });
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(url, invalidEvent)), fail, "Invalid notifications event " + invalidEvent + " without persistence field should fail" });
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(url, invalidEvent)), fail, "Invalid notifications event " + invalidEvent + " without persistence field should fail" });

				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(url, invalidEvent, false)).add(validNotificationJsonNode), fail, "One valid notification and a second with an invalid event " + invalidEvent + " should fail" });
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(url, invalidEvent, true)).add(validNotificationJsonNode), fail, "One valid notification and a second with an invalid event " + invalidEvent + " should fail" });
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(url, invalidEvent)).add(validNotificationJsonNode), fail, "One valid notification and a second with an invalid event " + invalidEvent + " without persistence field should fail" });
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(url, invalidEvent)).add(validNotificationJsonNode), fail, "One valid notification and a second with an invalid event " + invalidEvent + " without persistence field should fail" });
			}
		}
		
		for (Object invalidWebhookUrl: invalidWebhookUrls) {
			for (Object event: validEvents) {
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(invalidWebhookUrl, event, false)), fail, "Invalid notifications invalidWebhookUrl " + invalidWebhookUrl + " notifications should fail" });
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(invalidWebhookUrl, event, true)), fail, "Invalid notifications invalidWebhookUrl " + invalidWebhookUrl + " notifications should fail" });
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(invalidWebhookUrl, event)), fail, "Invalid notifications invalidWebhookUrl " + invalidWebhookUrl + " notifications without persistence field should fail" });
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(invalidWebhookUrl, event)), fail, "Invalid notifications invalidWebhookUrl " + invalidWebhookUrl + " notifications without persistence field should fail" });

				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(invalidWebhookUrl, event, false)).add(validNotificationJsonNode), fail, "One valid notification and a second with an invalidWebhookUrl " + invalidWebhookUrl + " notifications should fail" });
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(invalidWebhookUrl, event, true)).add(validNotificationJsonNode), fail, "One valid notification and a second with an invalidWebhookUrl " + invalidWebhookUrl + " notifications should fail" });
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(invalidWebhookUrl, event)).add(validNotificationJsonNode), fail, "One valid notification and a second with an invalidWebhookUrl " + invalidWebhookUrl + " notifications without persistence field should fail" });
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(invalidWebhookUrl, event)).add(validNotificationJsonNode), fail, "One valid notification and a second with an invalidWebhookUrl " + invalidWebhookUrl + " notifications without persistence field should fail" });
			}
			
			for (Object invalidEvent: invalidEvents) {
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(invalidWebhookUrl, invalidEvent, false)), fail, "Invalid notifications invalidWebhookUrl " + invalidWebhookUrl + " and event " + invalidEvent + " should fail" });
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(invalidWebhookUrl, invalidEvent, true)), fail, "Invalid notifications invalidWebhookUrl " + invalidWebhookUrl + " and event " + invalidEvent + " should fail" });
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(invalidWebhookUrl, invalidEvent)), fail, "Invalid notifications invalidWebhookUrl " + invalidWebhookUrl + " and event " + invalidEvent + " without persistence field should fail" });
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(invalidWebhookUrl, invalidEvent)), fail, "Invalid notifications invalidWebhookUrl " + invalidWebhookUrl + " and event " + invalidEvent + " without persistence field should fail" });
				
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(invalidWebhookUrl, invalidEvent, false)).add(validNotificationJsonNode), fail, "One valid notification and a second with an invalidWebhookUrl " + invalidWebhookUrl + " and event " + invalidEvent + " should fail" });
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(invalidWebhookUrl, invalidEvent, true)).add(validNotificationJsonNode), fail, "One valid notification and a second with an invalidWebhookUrl " + invalidWebhookUrl + " and event " + invalidEvent + " should fail" });
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(invalidWebhookUrl, invalidEvent)).add(validNotificationJsonNode), fail, "One valid notification and a second with an invalidWebhookUrl " + invalidWebhookUrl + " and event " + invalidEvent + " without persistence field should fail" });
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(invalidWebhookUrl, invalidEvent)).add(validNotificationJsonNode), fail, "One valid notification and a second with an invalidWebhookUrl " + invalidWebhookUrl + " and event " + invalidEvent + " without persistence field should fail" });
			}
		}
		
		for (Object invalidEmailAddress: invalidEmailAddresses) {
			for (Object event: validEvents) {
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(invalidEmailAddress, event, false)), fail, "Invalid notifications invalidEmailAddress " + invalidEmailAddress + " notifications should fail" });
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(invalidEmailAddress, event, true)), fail, "Invalid notifications invalidEmailAddress " + invalidEmailAddress + " notifications should fail" });
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(invalidEmailAddress, event)), fail, "Invalid notifications invalidEmailAddress " + invalidEmailAddress + " notifications without persistence field should fail" });
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(invalidEmailAddress, event)), fail, "Invalid notifications invalidEmailAddress " + invalidEmailAddress + " notifications without persistence field should fail" });
				
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(invalidEmailAddress, event, false)).add(validNotificationJsonNode), fail, "One valid notification and a second with an invalidEmailAddress " + invalidEmailAddress + " notifications should fail" });
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(invalidEmailAddress, event, true)).add(validNotificationJsonNode), fail, "One valid notification and a second with an invalidEmailAddress " + invalidEmailAddress + " notifications should fail" });
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(invalidEmailAddress, event)).add(validNotificationJsonNode), fail, "One valid notification and a second with an invalidEmailAddress " + invalidEmailAddress + " notifications without persistence field should fail" });
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(invalidEmailAddress, event)).add(validNotificationJsonNode), fail, "One valid notification and a second with an invalidEmailAddress " + invalidEmailAddress + " notifications without persistence field should fail" });
			}
			
			for (Object invalidEvent: invalidEvents) {
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(invalidEmailAddress, invalidEvent, false)), fail, "Invalid notifications invalidEmailAddress " + invalidEmailAddress + " and event " + invalidEvent + " should fail" });
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(invalidEmailAddress, invalidEvent, true)), fail, "Invalid notifications invalidEmailAddress " + invalidEmailAddress + " and event " + invalidEvent + " should fail" });
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(invalidEmailAddress, invalidEvent)), fail, "Invalid notifications invalidEmailAddress " + invalidEmailAddress + " and event " + invalidEvent + " without persistence field should fail" });
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(invalidEmailAddress, invalidEvent)), fail, "Invalid notifications invalidEmailAddress " + invalidEmailAddress + " and event " + invalidEvent + " without persistence field should fail" });
				
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(invalidEmailAddress, invalidEvent, false)).add(validNotificationJsonNode), fail, "One valid notification and a second with an invalidEmailAddress " + invalidEmailAddress + " and event " + invalidEvent + " should fail" });
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(invalidEmailAddress, invalidEvent, true)).add(validNotificationJsonNode), fail, "One valid notification and a second with an invalidEmailAddress " + invalidEmailAddress + " and event " + invalidEvent + " should fail" });
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(invalidEmailAddress, invalidEvent)).add(validNotificationJsonNode), fail, "One valid notification and a second with an invalidEmailAddress " + invalidEmailAddress + " and event " + invalidEvent + " without persistence field should fail" });
				testCases.add(new Object[]{ mapper.createArrayNode().add(createJsonNotification(invalidEmailAddress, invalidEvent)).add(validNotificationJsonNode), fail, "One valid notification and a second with an invalidEmailAddress " + invalidEmailAddress + " and event " + invalidEvent + " without persistence field should fail" });
			}
		}
		
		return testCases.toArray(new Object[][] {});
	}
	
	/**
	 * Tests job notifications validation on jobs submitted as json
	 * 
	 * @param notificationsJsonArray
	 * @param shouldThrowException
	 * @param message
	 */
	@Test(dataProvider = "processJsonJobWithNotificationsProvider", dependsOnMethods={"processJsonJobHiddenParametersThrowExceptionWhenProvided"})
	public void processJsonJobWithNotifications(ArrayNode notificationsJsonArray, boolean shouldThrowException, String message) 
	{
		
		ObjectNode json = createJobJsonNode();
		json.putArray("notifications").addAll(notificationsJsonArray);
		Job job = null;
		try {
			try 
			{
				job = JobManager.processJob(json, JSONTestDataUtil.TEST_OWNER, null);
				Assert.assertNotNull(job.getId(), "Job was not saved after processing.");
			} catch (JobProcessingException e) {
				if (!shouldThrowException) {
					Assert.fail(message, e);
				}
			} catch (Exception e) {
				Assert.fail("Unexpected failed to process job", e);
			}
			
			try {
				if (job != null && notificationsJsonArray != null) 
				{
					List<Notification> notifications = new NotificationDao().getActiveUserNotificationsForUuid(job.getOwner(), job.getUuid());
					Assert.assertEquals(notifications.size(), notificationsJsonArray.size(), "Unexpected notification count. Found " + notifications.size() + " expected " + notificationsJsonArray.size());
					
					// this won't correctly check for multiple notifications to the same event
					for (int i=0; i<notificationsJsonArray.size();i++)
					{
						JsonNode notificationJson = notificationsJsonArray.get(i);
						
						notifications = new NotificationDao().getActiveForAssociatedUuidAndEvent(job.getUuid(), notificationJson.get("event").textValue());
						Assert.assertEquals(notifications.size(), 1, "Provided " + notificationJson.get("event").textValue() + " notification not found for job.");
						Assert.assertEquals(notifications.get(0).getCallbackUrl(), notificationJson.get("url").textValue(), 
								"Saved " + notificationJson.get("event").textValue() + " notification had the wrong callback url. Expected " + 
								notificationJson.get("url").textValue() + " found " + notifications.get(0).getCallbackUrl());
						
						if (notificationJson.has("persistent")) {
							Assert.assertEquals(notifications.get(0).isPersistent(), notificationJson.get("persistent").asBoolean(), 
									"Saved " + notificationJson.get("event").textValue() + " notification had the wrong persistent value. Expected " + 
									notificationJson.get("persistent").asBoolean() + " found " + notifications.get(0).isPersistent());
						} else {
							Assert.assertEquals(notifications.get(0).isPersistent(), false, 
									"Saved " + notificationJson.get("event").textValue() + " notification defaulted to the wrong persistent value. Expected " + 
									Boolean.FALSE + " found " + notifications.get(0).isPersistent());
						}
					}	
					
				}
			} catch (Exception e) {
				Assert.fail("Error verifying notifications for job: " + e.getMessage(), e);
			}
		}
		finally {
			try { clearJobs(); } catch (Throwable e) {}
		}
		
	}
	
	/**
	 * Tests basic field validation on jobs submitted as form
	 * 
	 * @param field
	 * @param value
	 * @param internalUsername
	 * @param shouldThrowException
	 * @param message
	 */
	@Test(dataProvider = "processJsonJobProvider", dependsOnMethods={"processJsonJobWithNotifications"})
	public void processFormJob(String field, Object value, boolean shouldThrowException, String message) 
	{
		Form form = createJobForm();
		
		form = updateForm(form, field, value);
		
		genericProcessFormJob(form, shouldThrowException, message);
	}
	
	@DataProvider
	public Object[][] processFormJobWithNotificationsProvider()
	{
		ObjectMapper mapper = new ObjectMapper();
		Object[] validUrls = { "http://example.com", "http://foo@example.com", "http://foo:bar@example.com", "http://example.com/job/${JOB_ID}/${JOB_STATUS}", "http://example.com?foo=${JOB_ID}"};
		Object[] invalidWebhookUrls = { "example.com", "example", "", null, new Long(1), mapper.createArrayNode(), mapper.createObjectNode() };
		Object[] validEmailAddresses = { "foo@example.com", "foo+bar@example.com", "foo.bar@example.com" };
		Object[] invalidEmailAddresses = { "@example.com", "@example", "foo@example", "foo@", "@.com", "foo@.com" };
		
		List<Object[]> testCases = new ArrayList<Object[]>();
		for (Object url: validUrls) {
			testCases.add(new Object[] { url, pass, "Valid url " + url + " should pass" });
		}
		
		for (Object url: invalidWebhookUrls) {
			testCases.add(new Object[] { url, fail, "Invalid url " + url + " should fail;" });
		}
		
		for (Object url: validEmailAddresses) {
			testCases.add(new Object[] { url, pass, "Valid email address " + url + " should pass" });
		}
		
		for (Object url: invalidEmailAddresses) {
			testCases.add(new Object[] { url, fail, "Invalid email address " + url + " should fail" });
		}
		
		return testCases.toArray(new Object[][] {});
	}
	
	/** 
	 * Tests job notifications validation on jobs submitted as form
	 * 
	 * @param notificationsJsonArray
	 * @param shouldThrowException
	 * @param message
	 */
	@Test(dataProvider = "processFormJobWithNotificationsProvider", dependsOnMethods={"processFormJob"})
	public void processFormJobWithNotifications(Object webhookUrlOrEmail, boolean shouldThrowException, String message) 
	{
		Form form = createJobForm();
		form = updateForm(form, "notifications", webhookUrlOrEmail);
		Job job = null;
		try {
			try 
			{
				job = JobManager.processJob(form, JSONTestDataUtil.TEST_OWNER, null);
				Assert.assertNotNull(job.getId(), "Job was not saved after processing.");
			} catch (JobProcessingException e) {
				if (!shouldThrowException) {
					Assert.fail(message, e);
				}
			} catch (Throwable e) {
				Assert.fail("Unexpected failed to process job", e);
			}
			
			try {
				if (job != null) 
				{
					List<Notification> notifications = new NotificationDao().getActiveUserNotificationsForUuid(job.getOwner(), job.getUuid());
					Assert.assertEquals(notifications.size(), 2, "Unexpected notification count. Found " + notifications.size() + " expected 2");
					
					notifications = new NotificationDao().getActiveForAssociatedUuidAndEvent(job.getUuid(), JobStatusType.FINISHED.name());
					Assert.assertEquals(notifications.size(), 1, "No FINISHED notification found for job.");
					Assert.assertEquals(notifications.get(0).getCallbackUrl(), webhookUrlOrEmail, 
							"Persisted FINISHED notification had the wrong callback url. Expected " + 
							webhookUrlOrEmail + " found " + notifications.get(0).getCallbackUrl());
					Assert.assertFalse(notifications.get(0).isPersistent(), "Saved FINISHED notification defaulted to the wrong persistent value. "
							+ "Expected " + Boolean.FALSE + " found " + notifications.get(0).isPersistent());
					
					notifications = new NotificationDao().getActiveForAssociatedUuidAndEvent(job.getUuid(), JobStatusType.FAILED.name());
					Assert.assertEquals(notifications.size(), 1, "No FAILED notification found for job.");
					Assert.assertEquals(notifications.get(0).getCallbackUrl(), webhookUrlOrEmail, 
							"Persisted FAILED notification had the wrong callback url. Expected " + 
							webhookUrlOrEmail + " found " + notifications.get(0).getCallbackUrl());
					Assert.assertFalse(notifications.get(0).isPersistent(), "Saved FAILED notification defaulted to the wrong persistent value. "
							+ "Expected " + Boolean.FALSE + " found " + notifications.get(0).isPersistent());
				}
			} catch (Exception e) {
				Assert.fail("Error verifying notifications for job", e);
			}
		}
		finally {
			try { clearJobs(); } catch (Throwable e) {}
		}
	}
	
	/** 
	 * Tests job notifications validation on jobs submitted as form
	 * 
	 * @param notificationsJsonArray
	 * @param shouldThrowException
	 * @param message
	 */
	@Test(dataProvider = "processFormJobWithNotificationsProvider", dependsOnMethods={"processFormJobWithNotifications"})
	public void processFormJobCallbackUrlTest(Object webhookUrlOrEmail, boolean shouldThrowException, String message) 
	{
		Form form = createJobForm();
		form = updateForm(form, "notifications", webhookUrlOrEmail);
		genericProcessFormJob(form, fail, "Use of callbackUrl in a job description should fail.");
	}
	
	@DataProvider
	public Object[][] processFormJobInputsProvider()
	{
		Object[][] validTestValues = { 
				{ "path/to/folder", "relative path should pass" },
				{ "folder", "relative file should pass" },
				{ "http://example.com", "HTTP uri schema should pass" },
				{ "https://example.com", "HTTPS uri schema should pass" },
				{ "http://foo:bar@example.com", "HTTP with basic auth uri schema should pass" },
				{ "https://foo:bar@example.com", "HTTP with basic auth uri schema should pass" },
				{ "sftp://example.com", "SFTP uri schema should pass" },
				{ "sftp://foo:bar@example.com/", "SFTP with basic auth uri schema should pass" },
				{ "agave://example.com", "agave uri schema should pass" },
				{ "agave://" + privateExecutionSystem.getSystemId() + "/", "agave uri schema should pass" },
				{ "http://example.com;http://example.com", "Semicolon separated lists of inputs are allowed" },
				{ "http://example.com;;http://example.com", "Double semicolons should be ignored and treated as a single semicolon" },
				
				{ "HTTP://example.com", "HTTP uri schema is case insensitive and should pass" },
				{ "HTTPS://example.com", "HTTPS uri schema should pass" },
				{ "HTTP://foo:bar@example.com", "HTTP with basic auth uri schema is case insensitive and should pass" },
				{ "HTTPS://foo:bar@example.com", "HTTP with basic auth uri schema is case insensitive and should pass" },
				{ "SFTP://example.com", "SFTP uri schema is case insensitive and should pass" },
				{ "SFTP://foo:bar@example.com/", "SFTP with basic auth uri schema is case insensitive and should pass" },
				{ "AGAVE://example.com", "gridftp uri schema is case insensitive and should pass" },
				{ "AGAVE://" + privateExecutionSystem.getSystemId() + "/", "agave uri schema is case insensitive and should pass" },
		};
		
		Object[][] invalidTestValues = { 		
				// unsupported schemas
				{ "file:///path/to/folder", "FILE uri schema should fail" },
				{ "ftp://example.com", "FTP uri schema should pass" },
				{ "ftp://foo:bar@example.com/", "FTP with basic auth uri schema should pass" },
				{ "ftps://example.com", "FTPS uri schema should pass" },
				{ "ftps://foo:bar@example.com/", "FTPS with basic auth uri schema should pass" },
				{ "gsiftp://example.com", "GSIFTP uri schema should fail" },
				{ "gsiftp://foo:bar@example.com/", "GSIFTP with basic auth uri schema should fail" },
				{ "gridftp://example.com", "gridftp uri schema should fail" },
				{ "gridftp://foo:bar@example.com/", "GRIDFTP with basic auth uri schema should fail" },
				{ "s3://s3.amazon.com/abced", "s3 uri schema should fail" },
				{ "s3://foo:bar@s3.amazon.com/abced", "s3 with basic auth uri schema should fail" },
				
				{ "FILE:///path/to/folder", "FILE uri schema should fail" },
				{ "FTP://example.com", "FTP uri schema is case insensitive and should pass" },
				{ "FTP://foo:bar@example.com/", "FTP with basic auth uri schema is case insensitive and should pass" },
				{ "FTPS://example.com", "FTPS uri schema is case insensitive and should pass" },
				{ "FTPS://foo:bar@example.com/", "FTPS with basic auth uri schema is case insensitive and should pass" },
				{ "GSIFTP://example.com", "GSIFTP uri schema is case insensitive and should fail" },
				{ "GSIFTP://foo:bar@example.com/", "GSIFTP with basic auth uri schema is case insensitive and should fail" },
				{ "GRIDFTP://example.com", "gridftp uri schema is case insensitive and should fail" },
				{ "GRIDFTP://foo:bar@example.com/", "GRIDFTP with basic auth uri schema is case insensitive and should fail" },
				{ "S3://s3.amazon.com/abced", "s3 uri schema is case insensitive and should fail" },
				{ "S3://foo:bar@s3.amazon.com/abced", "s3 with basic auth uri schema is case insensitive and should fail" },
				
				{ "abba://example.com", "Unknown uri schema should fail" },
				{ "ABBA://example.com", "Unknown uri schema is case insensitive and should fail" },
		};
		
		List<Object[]> testData = new ArrayList<Object[]>();
		String inputKey = "testinputkey";
		String defaultValue = "http://default.example.com";
		
		testData.add(new Object[] { createInput(inputKey, defaultValue, 	null, 		false, 		false), mapper.createObjectNode().put(inputKey, "path/to/folder"), 	null										  							, fail, "Hidden input should fail if user provides value" });
		testData.add(new Object[] { createInput(inputKey, defaultValue, 	null, 		true, 		false), mapper.createObjectNode().put(inputKey, "path/to/folder"), 	null										  							, fail, "Hidden input should fail if user provides value" });
		testData.add(new Object[] { createInput(inputKey, defaultValue, 	null, 		true, 		false), null, 													  				mapper.createObjectNode().put(inputKey, defaultValue)						, pass, "Hidden input should use default." });
		testData.add(new Object[] { createInput(inputKey, defaultValue, 	null, 		false, 		false), null, 													  				mapper.createObjectNode().put(inputKey, defaultValue)						, pass, "Hidden input should use default." });
		
		testData.add(new Object[] { createInput(inputKey, defaultValue, 	null, 		false, 		true), null, 													  				null																		, pass, "Visible input not required should not use default when no user data given" });
		testData.add(new Object[] { createInput(inputKey, defaultValue, 	null, 		true, 		true), null, 													  				mapper.createObjectNode().put(inputKey, defaultValue)						, fail, "Required input should fail if not supplied" });
		testData.add(new Object[] { createInput(inputKey, defaultValue, 	null, 		false, 		true), mapper.createObjectNode().put(inputKey, "path/to/folder"),  	mapper.createObjectNode().put(inputKey, "path/to/folder")	, pass, "User supplied value should be used for visible optional input values" });
		testData.add(new Object[] { createInput(inputKey, defaultValue, 	null, 		true, 		true), mapper.createObjectNode().put(inputKey, "path/to/folder"),  	mapper.createObjectNode().put(inputKey, "path/to/folder")	, pass, "User supplied value should be used for visible required input values" });
		
		testData.add(new Object[] { createInput(inputKey, defaultValue, 	null, 		true, 		true), mapper.createObjectNode().putNull(inputKey),  							null																		, fail, "Visible required input should fail when null user input value given" });
		testData.add(new Object[] { createInput(inputKey, defaultValue, 	null, 		false, 		true), mapper.createObjectNode().putNull(inputKey),  							null																		, fail, "Visible optional input should fail when null user input value given" });
		
		for (Object[] validTestValue: validTestValues) 
		{
			testData.add(new Object[] { createInput(inputKey, defaultValue, 	null, 		false, 		true), mapper.createObjectNode().put(inputKey, (String)validTestValue[0]),  	mapper.createObjectNode().put(inputKey, (String)validTestValue[0])	, pass, (String)validTestValue[1] });
			testData.add(new Object[] { createInput(inputKey, defaultValue, 	null, 		true, 		true), mapper.createObjectNode().put(inputKey, (String)validTestValue[0]),  	mapper.createObjectNode().put(inputKey, (String)validTestValue[0])	, pass, (String)validTestValue[1] });
		}
		
		for (Object[] invalidTestValue: invalidTestValues) 
		{
			testData.add(new Object[] { createInput(inputKey, defaultValue, 	null, 		false, 		true), mapper.createObjectNode().put(inputKey, (String)invalidTestValue[0]),  	null	, fail, (String)invalidTestValue[1] });
			testData.add(new Object[] { createInput(inputKey, defaultValue, 	null, 		true, 		true), mapper.createObjectNode().put(inputKey, (String)invalidTestValue[0]),  	null	, fail, (String)invalidTestValue[1] });
		}
		
		return testData.toArray(new Object[][] {});
	}
	
	/**
	 * Tests job app input validation on jobs submitted as form
	 * 
	 * @param jobInputs
	 * @param shouldThrowException
	 * @param message
	 */
	@Test(dataProvider = "processFormJobInputsProvider", dependsOnMethods={"processFormJobCallbackUrlTest"})
	public void processFormJobInputs(SoftwareInput appInput, ObjectNode jobInputs, ObjectNode expectedInputs, boolean shouldThrowException, String message) 
	{
		Software testSoftware = null;
		
		try {
			testSoftware = software.clone();
			testSoftware.setOwner(JSONTestDataUtil.TEST_OWNER);
			testSoftware.setName("processJsonJobInputDefaults");
			testSoftware.getInputs().clear();
			SoftwareDao.persist(testSoftware);
			if (appInput != null) {
				testSoftware.addInput(appInput);
			}
			
			SoftwareDao.merge(testSoftware);
			
			Form form = createJobForm();
			for (SoftwareInput input: software.getInputs()) {
				form.removeAll(input.getKey());
			}
			form = updateForm(form, "appId", testSoftware.getUniqueName());
			
			if (jobInputs != null) {
				for (Iterator<String> inputKeys = jobInputs.fieldNames(); inputKeys.hasNext();){
					String key = inputKeys.next();
					form = updateForm(form, key, jobInputs.get(key).textValue());
				}
			}
			
			Job job = JobManager.processJob(form, JSONTestDataUtil.TEST_OWNER, null);
			
			Assert.assertNotNull(job.getId(), "Job was not saved after processing.");
			
			if (expectedInputs != null) {
				for (Iterator<String> fieldNamesIterator = expectedInputs.fieldNames(); fieldNamesIterator.hasNext();)
				{
					String fieldName = fieldNamesIterator.next();
					Assert.assertTrue(job.getInputsAsMap().containsKey(fieldName), message);
					String foundValue = job.getInputsAsMap().get(fieldName);
					String expectedValue = expectedInputs.get(fieldName).textValue();
					Assert.assertEquals(foundValue, expectedValue, 
							"Unexpected value for field " + fieldName + " found. Expected " + expectedValue + 
							" found " + foundValue);
				}
			}
			else
			{
				Assert.assertTrue(job.getInputsAsMap().isEmpty(), message);
			}
		} catch (JobProcessingException e) {
			if (!shouldThrowException) {
				Assert.fail(message, e);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Failed to process job", e);
		}
		finally {
			try { SoftwareDao.delete(testSoftware); } catch (Throwable t) {}
			try { clearJobs(); } catch (Throwable t) {}
		}
	}
	
	/**
	 * Tests job app parameter validation on jobs submitted as form
	 * 
	 * @param jobParameters
	 * @param shouldThrowException
	 * @param message
	 */
	@Test(dataProvider = "processJsonJobParametersProvider", dependsOnMethods={"processFormJobInputs"})
	public void processFormJobParameters(SoftwareParameter appParameter, ObjectNode jobParameters, ObjectNode expectedParameters, boolean shouldThrowException, String message) 
	{
		Software testSoftware = null;
		
		try {
			testSoftware = software.clone();
			testSoftware.setOwner(JSONTestDataUtil.TEST_OWNER);
			testSoftware.setName("processJsonJobInputDefaults");
			testSoftware.getParameters().clear();
			SoftwareDao.persist(testSoftware);
			if (appParameter != null) {
				testSoftware.addParameter(appParameter);
			}
			
			SoftwareDao.merge(testSoftware);
			
			Form form = createJobForm();
			form = updateForm(form, "appId", testSoftware.getUniqueName());
			for (SoftwareParameter parameter: software.getParameters()) {
				form.removeAll(parameter.getKey());
			}
			
			if (jobParameters != null) {
				for (Iterator<String> inputKeys = jobParameters.fieldNames(); inputKeys.hasNext();){
					String key = inputKeys.next();
					form = updateForm(form, key, jobParameters.get(key).textValue());
				}
			}
			
			Job job = JobManager.processJob(form, JSONTestDataUtil.TEST_OWNER, null);
			
			Assert.assertNotNull(job.getId(), "Job was not saved after processing.");
			
			if (expectedParameters != null) {
				Iterator<String> fieldNamesIterator = expectedParameters.fieldNames();
				while (fieldNamesIterator.hasNext())
				{
					String fieldName = fieldNamesIterator.next();
					Assert.assertTrue(job.getParametersAsMap().containsKey(fieldName), message);
					String foundParameter = (String)job.getParametersAsMap().get(appParameter.getKey());
					String expectedParameter = expectedParameters.get(fieldName).asText();
					if (appParameter.getType().equals(SoftwareParameterType.number)) {
						foundParameter = new Double(foundParameter).toString();
						expectedParameter = new Double(expectedParameter).toString();
					} 
					Assert.assertEquals(foundParameter, expectedParameter, 
							"Unexpected value for field " + fieldName + " found. Expected " + expectedParameter + 
							" found " + foundParameter);
				}
			}
			else
			{
				Assert.assertTrue(job.getParametersAsMap().isEmpty(), message);
			}
		} catch (JobProcessingException e) {
			if (!shouldThrowException) {
				Assert.fail(message, e);
			}
		} catch (Exception e) {
			Assert.fail("Failed to process job", e);
		}
		finally {
			try { SoftwareDao.delete(testSoftware); } catch (Throwable t) {}
			try { clearJobs(); } catch (Throwable t) {}
		}
	}
	
	@DataProvider
	public Object[][] processJsonJobBatchQueueParametersProvider()
	{
		ExecutionSystem system = new ExecutionSystem();
		// name maxJobs userJobs nodes memory, procs, time, cstm, default
		
		BatchQueue queueDefault = new BatchQueue("queueDefault", (long) 1, (long) 1, (long) 1, (double) 1.0, (long) 1, "01:00:00", null, true);
		BatchQueue queueTwo = new BatchQueue("queueTwo", (long) 2, (long) 2, (long) 2, (double) 2.0, (long) 2, "02:00:00", null, false);
		BatchQueue queueTen = new BatchQueue("queueTen", (long) 10, (long) 10, (long) 10, (double) 10, (long) 10, "10:00:00", null, false);
		BatchQueue queueHundred = new BatchQueue("queueHundred", (long) 100, (long) 100, (long) 100, (double) 100, (long) 100, "100:00:00", null, false);
		//BatchQueue queueUnbounded = new BatchQueue("queueMax", (long) -1, (long) -1, (long) -1, (double) -1.0, (long) -1, BatchQueue.DEFAULT_MAX_RUN_TIME, null, false);
		
		BatchQueue[] allQueues = {queueDefault, queueTwo, queueTen, queueHundred };
		List<Object[]> testData = new ArrayList<Object[]>();
		String[] jobQueues = new String[] { null };
		
		// no job queue specified, no app defaults
		testData.add(new Object[]{ new BatchQueue[] { queueDefault }, null, null, null, null, null, null, null, null, null, null, queueDefault.getName(), fail, "No specs and no default should fail" });
		
		// no app defaults, job exceeds default when defalut is only
													//												job specs								||||					app default specs
									// queues						//     q			nodes					mem			time		procs	  	q				nodes			mem			time		procs	   	expected					pass		message
		testData.add(new Object[]{ allQueues,   					 "thissinotaqueue", null, 					null, 		null, 		null,		null, 			null, 			null, 		null, 		null, 		null,						fail, "Non-existent queue fails" });
		testData.add(new Object[]{ new BatchQueue[] { queueDefault }, null, 			(long)2, 				null, 		null, 		null,     	null, 			null, 			null, 		null, 		null,     	null,						fail, "Default queue only, out of bounds job nodes fails" });
		testData.add(new Object[]{ new BatchQueue[] { queueDefault }, null, 			null, 					(double)2, 	null, 		null, 		null,			null, 			null, 		null, 		null, 		null,						fail, "Default queue only, out of bounds job memory fails" });
		testData.add(new Object[]{ new BatchQueue[] { queueDefault }, null, 			null, 					null, 		"02:00:00", null,  		null, 			null, 			null, 		null, 		null, 		null,						fail, "Default queue only, out of bounds job time fails" });
		testData.add(new Object[]{ new BatchQueue[] { queueDefault }, null, 			null, 					null,		null, 		(long)2,    null, 			null, 			null, 		null, 		null, 		null,						fail, "Default queue only, out of bounds job procs fails" });
		
		testData.add(new Object[]{ new BatchQueue[] { queueDefault }, null, 			null, 					null, 		null, 		null, 		null, 			(long)2, 		null, 		null, 		null,     	null, 						fail, "Default queue only, out of bounds app default nodes fails" });
		testData.add(new Object[]{ new BatchQueue[] { queueDefault }, null, 			null, 					null, 		null, 		null,		null, 		 	null, 			(double)2, 	null, 		null, 		null, 						fail, "Default queue only, out of bounds app default memory fails" });
		testData.add(new Object[]{ new BatchQueue[] { queueDefault }, null, 			null, 					null,		null, 		null, 		null, 			null, 			null, 		"02:00:00", null,  		null, 						fail, "Default queue only, out of bounds app default time fails" });
		testData.add(new Object[]{ new BatchQueue[] { queueDefault }, null, 			null, 					null, 		null, 		null, 		null, 		 	null, 			null,		null, 		(long)2,    null, 						fail, "Default queue only, out of bounds app default procs fails" });
		
		// no app defaults, job exceeds max queue
		testData.add(new Object[]{ allQueues, 						  null, 			(long)101, 				null, 		null, 		null,     	null, 			null, 			null, 		null, 		null,     	null,						fail, "All queues, out of bounds nodes fails" });
		testData.add(new Object[]{ allQueues, 						  null, 			null, 					(double)101,null, 		null, 		null,			null, 			null, 		null, 		null, 		null,						fail, "All queues, out of bounds memory fails" });
		testData.add(new Object[]{ allQueues, 						  null, 			null, 					null, 		"101:00:00",null,  		null, 			null, 			null, 		null, 		null, 		null,						fail, "All queues, out of bounds time fails" });
		testData.add(new Object[]{ allQueues, 						  null, 			null, 					null,		null, 		(long)101,  null, 			null, 			null, 		null, 		null, 		null,						fail, "All queues, out of bounds procs fails" });
		
		// no job specs, app defaults exceeds max queue
		testData.add(new Object[]{ allQueues,  						  null, 			null,  					null,		null, 		null, 		null, 			(long)101, 		null, 		null, 		null,     	null,					 	fail, "All queues, out of bounds app default nodes fails" });
		testData.add(new Object[]{ allQueues,  						  null, 			null,  					null,		null, 		null, 		null, 			null, 			(double)101,null, 		null, 		null, 						fail, "All queues, out of bounds app default memory fails" });
		testData.add(new Object[]{ allQueues,  						  null, 			null,  					null,		null, 		null, 		null, 			null, 			null, 		"101:00:00",null,  		null, 						fail, "All queues, out of bounds app default time fails" });
		testData.add(new Object[]{ allQueues,  						  null, 			null, 					null,		null, 		null,		null, 			null,			null, 		null, 		(long)101,  null, 						fail, "All queues, out of bounds app default procs fails" });
		
		for (BatchQueue jobQueue: allQueues) {
			testData.add(new Object[]{ allQueues,  					  null, 			null,  					null,		null, 		null, 		jobQueue.getName(),	null, 					null, 							null, 							null, 									jobQueue.getName(),	pass, "No job specs and default app queue " + jobQueue.getName() + " did not select " + jobQueue.getName() });
			testData.add(new Object[]{ allQueues,   				  null, 			null,  					null,		null, 		null, 		null, 				jobQueue.getMaxNodes(), null, 							null, 							null,     								jobQueue.getName(),	pass, "No job specs and default queue nodes " + jobQueue.getMaxNodes() + " did not select " + jobQueue.getName() });
			testData.add(new Object[]{ allQueues,   				  null, 			null,  					null,		null, 		null, 		null, 				null, 					jobQueue.getMaxMemoryPerNode(), null, 							null, 									jobQueue.getName(),	pass, "No job specs and default queue memory " + jobQueue.getMaxMemoryPerNode() + " did not select " + jobQueue.getName() });
			testData.add(new Object[]{ allQueues,   				  null, 			null,  					null,		null, 		null, 		null, 				null, 					null, 							jobQueue.getMaxRequestedTime(), null,  									jobQueue.getName(),	pass, "No job specs and default queue run time " + jobQueue.getMaxRequestedTime() + " did not select " + jobQueue.getName() });
			testData.add(new Object[]{ allQueues,   				  null, 			null,  					null,		null, 		null, 		null, 				null, 					null,							null, 							jobQueue.getMaxProcessorsPerNode(),    	jobQueue.getName(),	pass, "No job specs and default queue procs " + jobQueue.getMaxProcessorsPerNode() + " did not select " + jobQueue.getName() });
			
			if (!jobQueue.equals(queueHundred)) 
			{				
				testData.add(new Object[]{ allQueues,   jobQueue.getName(),	null, 						null, 								null, 								null,									null, 					null, 						null, 								null, 								null,									jobQueue.getName(),		pass, "Specifying " + jobQueue.getName() + " did not select " + jobQueue.getName() });
				testData.add(new Object[]{ allQueues,   jobQueue.getName(),	queueHundred.getMaxNodes(), null,								null, 								null, 									null, 					null, 						null, 								null, 								null,									null,					fail, "Specifying " + jobQueue.getName() + " and trumping with " + queueHundred.getName() + " nodes should fail" });
				testData.add(new Object[]{ allQueues,   jobQueue.getName(),	null, 						queueHundred.getMaxMemoryPerNode(), null, 								null,									null, 					null, 						null, 								null, 								null,									null,					fail, "Specifying " + jobQueue.getName() + " and trumping with " + queueHundred.getName() + " memory should fail" });
				testData.add(new Object[]{ allQueues,   jobQueue.getName(),	null, 						null, 								queueHundred.getMaxRequestedTime(), null,									null, 					null, 						null, 								null, 								null,									null,					fail, "Specifying " + jobQueue.getName() + " and trumping with " + queueHundred.getName() + " run time should fail" });
				testData.add(new Object[]{ allQueues,   jobQueue.getName(),	null, 						null, 								null, 								queueHundred.getMaxProcessorsPerNode(),	null, 					null, 						null, 								null, 								null,									null,					fail, "Specifying " + jobQueue.getName() + " and trumping with " + queueHundred.getName() + " procs did should fail" });

				testData.add(new Object[]{ allQueues,   jobQueue.getName(),	null, 						null, 								null, 								null,									queueHundred.getName(), null, 						null, 								null, 								null, 									jobQueue.getName(),		pass, "Specifying " + jobQueue.getName() + " and trumping with app default queue of " + queueHundred.getName() + " nodes did not select " + jobQueue.getName() });
				testData.add(new Object[]{ allQueues,   jobQueue.getName(),	null, 						null,								null, 								null, 									null,					queueHundred.getMaxNodes(), null, 								null, 								null, 									null,					fail, "Specifying default max nodes greater than " + jobQueue.getName() + " limit should fail" });
				testData.add(new Object[]{ allQueues,   jobQueue.getName(),	null, 						null,								null, 								null,									null, 					null, 						queueHundred.getMaxMemoryPerNode(), null, 								null, 									null,					fail, "Specifying default max memory greater than " + jobQueue.getName() + " limit should fail" });
				testData.add(new Object[]{ allQueues,   jobQueue.getName(),	null, 						null, 								null, 								null,									null, 					null, 						null, 								queueHundred.getMaxRequestedTime(), null, 									null,					fail, "Specifying default max run time greater than " + jobQueue.getName() + " limit should fail" });
				testData.add(new Object[]{ allQueues,   jobQueue.getName(),	null, 						null, 								null, 								null,									null, 					null, 						null, 								null, 								queueHundred.getMaxProcessorsPerNode(), null,					fail, "Specifying default max procs greater than " + jobQueue.getName() + " limit should fail" });
				
				testData.add(new Object[]{ allQueues,   jobQueue.getName(),	jobQueue.getMaxNodes(), 	null,								null, 								null, 									null,					queueHundred.getMaxNodes(), null, 								null, 								null, 									jobQueue.getName(),		pass, "Specifying user supplied node count value overrides app default" });
				testData.add(new Object[]{ allQueues,   jobQueue.getName(),	null, 						jobQueue.getMaxMemoryPerNode(), 	null, 								null,									null, 					null, 						queueHundred.getMaxMemoryPerNode(), null, 								null, 									jobQueue.getName(),		pass, "Specifying user supplied memory value overrides app default" });
				testData.add(new Object[]{ allQueues,   jobQueue.getName(),	null, 						null, 								jobQueue.getMaxRequestedTime(), 	null,									null, 					null, 						null, 								queueHundred.getMaxRequestedTime(), null, 									jobQueue.getName(),		pass, "Specifying user supplied run time value overrides app default" });
				testData.add(new Object[]{ allQueues,   jobQueue.getName(),	null, 						null, 								null, 								jobQueue.getMaxProcessorsPerNode(),		null, 					null, 						null, 								null, 								queueHundred.getMaxProcessorsPerNode(), jobQueue.getName(),		pass, "Specifying user supplied procs value overrides app default" });
			}
			else
			{
				testData.add(new Object[]{ allQueues,   jobQueue.getName(),	null, 						null, 								null, 								null,									null, 			null, 			null, 		null, 		null, 		jobQueue.getName(),		pass, "Specifying " + jobQueue.getName() + " did not select " + jobQueue.getName() });
				testData.add(new Object[]{ allQueues, 	null, 				jobQueue.getMaxNodes(),	 	null, 								null, 								null, 									null, 			null, 			null, 		null, 		null,     	jobQueue.getName(),		pass, "Specifying max nodes for " + jobQueue.getName() + " did not select that queue" });
				testData.add(new Object[]{ allQueues, 	null, 				null, 						jobQueue.getMaxMemoryPerNode(), 	null, 								null, 									null,			null, 			null, 		null, 		null, 		jobQueue.getName(),		pass, "Selecting max memory for " + jobQueue.getName() + " did not select that queue" });
				testData.add(new Object[]{ allQueues, 	null, 				null, 						null, 								jobQueue.getMaxRequestedTime(), 	null,  									null, 			null, 			null, 		null, 		null, 		jobQueue.getName(),		pass, "Selecting max run time for " + jobQueue.getName() + " did not select that queue" });
				testData.add(new Object[]{ allQueues, 	null, 				null, 						null,								null, 								jobQueue.getMaxProcessorsPerNode(),    	null, 			null, 			null, 		null, 		null, 		jobQueue.getName(),		pass, "Selecting max processors for " + jobQueue.getName() + " did not select that queue" });
			}
		}
		
		// add user setting overriding app settings
		
		return testData.toArray(new Object[][]{});
	}
					
	/**
	 * Tests job batch queue parameter validation on jobs submitted as json. This should provide coverage over
	 * all possible permutations of user parameters, app defaults, and batch queue limits.
	 * 
	 * @param batchQueueParameters
	 * @param shouldThrowException
	 * @param message
	 */
	@Test(dataProvider = "processJsonJobBatchQueueParametersProvider", dependsOnMethods={"processFormJobInputs"})
	public void processJsonJobBatchQueueParameters(BatchQueue[] batchQueues, 
			String jobQueue, Long jobNodes, Double jobMemory, String jobRequestedTime, Long jobProcessors, 
			String appQueue, Long appNodes, Double appMemory, String appRequestedTime, Long appProcessors,
			String expectedJobQueueName, boolean shouldThrowException, String message) 
	{
		Software testSoftware = null;
		ExecutionSystem testSystem = null;
		SystemDao systemDao = new SystemDao();
		try 
		{
			testSystem = (ExecutionSystem)systemDao.findBySystemId("BatchQueueTest");
			if (testSystem == null )
			{
				JSONObject systemJson = jtd.getTestDataObject(TEST_EXECUTION_SYSTEM_FILE);
				systemJson.remove("id");
				systemJson.remove("type");
				systemJson.put("id", "BatchQueueTest");
				systemJson.put("type", RemoteSystemType.EXECUTION.name());
				
				testSystem = ExecutionSystem.fromJSON(systemJson);
				testSystem.getUsersUsingAsDefault().add(TEST_OWNER);
				testSystem.setType(RemoteSystemType.EXECUTION);
				testSystem.setOwner(JSONTestDataUtil.TEST_OWNER);
			}
			
			testSystem.getBatchQueues().clear();
			systemDao.persist(testSystem);
			for (BatchQueue testQueue: batchQueues) {
				testSystem.addBatchQueue(testQueue);
			}
			systemDao.merge(testSystem);
			Assert.assertNotNull(testSystem.getId(), "Execution system was not saved.");
			
			testSoftware = Software.fromJSON(jtd.getTestDataObject(TEST_SOFTWARE_SYSTEM_FILE), TEST_OWNER);
			testSoftware.setExecutionSystem(testSystem);
			testSoftware.setOwner(TEST_OWNER);
			testSoftware.setVersion(software.getVersion());
			testSoftware.setOwner(JSONTestDataUtil.TEST_OWNER);
			testSoftware.setName("processJsonJobInputDefaults");
			testSoftware.setDefaultQueue(appQueue);
			testSoftware.setDefaultNodes(appNodes);
			testSoftware.setDefaultMemoryPerNode(appMemory);
			testSoftware.setDefaultProcessorsPerNode(appProcessors);
			testSoftware.setDefaultMaxRunTime(appRequestedTime);
			testSoftware.setExecutionSystem(testSystem);
			SoftwareDao.persist(testSoftware);
			Assert.assertNotNull(testSoftware.getId(), "Software was not saved.");
			
			
			// set up queue(s) on executionsystem
			// set up app defaults and map to the execution system
			// create job for the app with test fields
			ObjectNode json = createJobJsonNode();
			json = updateObjectNode(json, "appId", testSoftware.getUniqueName());
			if (!StringUtils.isEmpty(jobQueue))
				json = updateObjectNode(json, "batchQueue", jobQueue);
			if (jobNodes != null)
				json = updateObjectNode(json, "nodeCount", jobNodes);
			if (jobMemory != null)
				json = updateObjectNode(json, "memoryPerNode", jobMemory);
			if (jobProcessors != null)
				json = updateObjectNode(json, "processorsPerNode", jobProcessors);
			if (jobRequestedTime != null)
				json = updateObjectNode(json, "maxRunTime", jobRequestedTime);
			
			Job job = JobManager.processJob(json, JSONTestDataUtil.TEST_OWNER, null);
			
			Assert.assertNotNull(job.getId(), "Job was not saved after processing.");
			
			Assert.assertEquals(expectedJobQueueName, job.getBatchQueue(), 
							"Unexpected batchQueue found for job. Expected " + expectedJobQueueName + 
							" found " + job.getBatchQueue());
		} 
		catch (JobProcessingException e) 
		{
			if (!shouldThrowException) {
				Assert.fail(message, e);
			}
		} 
		catch (Exception e) 
		{
			Assert.fail("Failed to process job", e);
		}
		finally {
			try { clearJobs(); } catch (Throwable t) {}
			try { SoftwareDao.delete(testSoftware); } catch (Throwable t) {}
			try { systemDao.remove(testSystem); } catch (Throwable t) {}
		}
	}
	
	
	
	@DataProvider
	public Object[][] updateStatusJobJobStatusTypeProvider()
	{
		List<Object[]> testData = new ArrayList<Object[]>();
	
		for (JobStatusType currentStatus: JobStatusType.values())
		{
			for (JobStatusType newStatus: JobStatusType.values()) {
			//JobStatusType newStatus = JobStatusType.RUNNING;
				testData.add(new Object[]{ currentStatus, newStatus, null, false, 
						String.format("Status update from %s to %s should not throw an exception", currentStatus.name(), newStatus.name()) } );
				testData.add(new Object[]{ currentStatus, newStatus, newStatus.name(), false, 
						String.format("Status update from %s to %s should not throw an exception", currentStatus.name(), newStatus.name()) } );
				testData.add(new Object[]{ currentStatus, newStatus, "NOTAREALEVENT", false, 
						String.format("Status update from %s to %s should not throw an exception", currentStatus.name(), newStatus.name()) } );
				testData.add(new Object[]{ currentStatus, newStatus, "*", false, 
						String.format("Status update from %s to %s should not throw an exception", currentStatus.name(), newStatus.name()) } );
				
			}	
			//break;
		}
		return testData.toArray(new Object[][]{});
	}
	

	/**
	 * Tests that the job status is updated (if not redundant), and a 
	 * notification is sent when the job has one.
	 * 
	 * @param job
	 * @param status
	 * @param shouldThrowException
	 * @param message
	 */
	@Test(dataProvider="updateStatusJobJobStatusTypeProvider", dependsOnMethods="selectQueueLimitTest")
	public void updateStatusJobJobStatusType(JobStatusType jobStatus, JobStatusType newStatus, String notificatonEvent, boolean shouldThrowException, String message) 
	{
		Job job = null;
		try 
		{
			NotificationDao notificationDao = new NotificationDao();
		
			job = createJob(jobStatus);
			JobDao.persist(job);
			
			Notification notification = null;
			if (!StringUtils.isEmpty(notificatonEvent)) {
				notification = new Notification(job.getUuid(), job.getOwner(), notificatonEvent, "http://example.com", false);
				notificationDao.persist(notification);
			}
			
			JobManager.updateStatus(job, newStatus);
			
			// verify status update
			Assert.assertEquals(job.getStatus(), newStatus,
					"Job status did not update after status update.");
			Assert.assertEquals(job.getErrorMessage(), newStatus.getDescription(),
					"Job description did not update after status update.");

			// verify event creation
			List<JobEvent> events = JobEventDao.getByJobIdAndStatus(job.getId(), job.getStatus());
			Assert.assertEquals(events.size(), 1,
					"Wrong number of events found. Expected " + 
					1 + ", found " + events.size());
			
			JobEvent event = events.get(0);
			Assert.assertEquals(
					event.getDescription(),
					newStatus.getDescription(),
					"Wrong event description found. Expected '"
							+ newStatus.getDescription() + "', found '"
							+ event.getDescription() + "'");

			
			if (jobStatus != newStatus && 
					(StringUtils.equals(notificatonEvent, newStatus.name()) || 
							StringUtils.equals(notificatonEvent, "*")))
			{
				int messageCount = getMessageCount(Settings.NOTIFICATION_QUEUE);
				Assert.assertEquals(messageCount, 1, "Wrong number of messages found");
				
				// verify notification message was sent
				MessageQueueClient client = MessageClientFactory.getMessageClient();
				Message queuedMessage = null;
				try {
					queuedMessage = client.pop(Settings.NOTIFICATION_TOPIC,
							Settings.NOTIFICATION_QUEUE);
				} catch (Throwable e) { 
					Assert.fail("Failed to remove message from the queue. Further tests will fail.", e);
				}
				finally {
					client.delete(Settings.NOTIFICATION_TOPIC,
							Settings.NOTIFICATION_QUEUE, queuedMessage.getId());
				}
				Assert.assertNotNull(queuedMessage,
						"Null message found on the queue");
				JsonNode json = mapper.readTree(queuedMessage.getMessage());
				Assert.assertEquals(notification.getUuid(), json.get("uuid").textValue(),
						"Notification message has wrong uuid");
				Assert.assertEquals(job.getStatus().name(), json.get("event").textValue(),
						"Notification message has wrong event");
			}
			else
			{
				// check for messages in the queue?
				Assert.assertEquals(getMessageCount(Settings.NOTIFICATION_QUEUE), 0, 
						"Messages found in the queue when none should be there.");
			}
		} catch (Exception e) {
			if (!shouldThrowException) {
				Assert.fail(message, e);
			}
		} finally {
			try { clearJobs(); } catch (Exception e) {}
		}
	}
	

	@DataProvider
	public Object[][] updateStatusJobJobStatusTypeStringProvider()
	{
		List<Object[]> testData = new ArrayList<Object[]>();
		String customStatusMessage = "This is a different new status message, so the same status should update";
		for (JobStatusType currentStatus: JobStatusType.values())
		{
			for (JobStatusType newStatus: JobStatusType.values()) {
				testData.add(new Object[]{ currentStatus, newStatus, newStatus.getDescription(), null, false, 
						String.format("Status update from %s to %s same message should not throw an exception", currentStatus.name(), newStatus.name()) } );
				testData.add(new Object[]{ currentStatus, newStatus, newStatus.getDescription(), newStatus.name(), false, 
						String.format("Status update from %s to %s same message should not throw an exception", currentStatus.name(), newStatus.name()) } );
				testData.add(new Object[]{ currentStatus, newStatus, newStatus.getDescription(), "NOTAREALEVENT", false, 
						String.format("Status update from %s to %s same message should not throw an exception", currentStatus.name(), newStatus.name()) } );
				testData.add(new Object[]{ currentStatus, newStatus, newStatus.getDescription(), "*", false, 
						String.format("Status update from %s to %s same message should not throw an exception", currentStatus.name(), newStatus.name()) } );
				
				if (currentStatus.equals(newStatus)) 
				{
					testData.add(new Object[]{ currentStatus, newStatus, customStatusMessage, null, false, 
							String.format("Status update from %s to %s different message should not throw an exception", currentStatus.name(), newStatus.name()) } );
					testData.add(new Object[]{ currentStatus, newStatus, customStatusMessage, newStatus.name(), false, 
							String.format("Status update from %s to %s different message should not throw an exception", currentStatus.name(), newStatus.name()) } );
					testData.add(new Object[]{ currentStatus, newStatus, customStatusMessage, "NOTAREALEVENT", false, 
							String.format("Status update from %s to %s different message should not throw an exception", currentStatus.name(), newStatus.name()) } );
					testData.add(new Object[]{ currentStatus, newStatus, customStatusMessage, "*", false, 
							String.format("Status update from %s to %s different message should not throw an exception", currentStatus.name(), newStatus.name()) } );
					
				}
			}	
		}
		return testData.toArray(new Object[][]{});
	}
	
	/**
	 * Tests that the job status is updated when a new status or message value is given. Also verifies a message
	 * is added to the queue if a notification for the job status is set. 
	 * @param jobStatus
	 * @param newStatus
	 * @param statusMessage
	 * @param addNotification
	 * @param shouldThrowException
	 * @param message
	 */
	//@Test(dataProvider = "updateStatusJobJobStatusTypeStringProvider", dependsOnMethods = { "updateStatusJobJobStatusType" })
	@Test(dataProvider="updateStatusJobJobStatusTypeStringProvider", dependsOnMethods="updateStatusJobJobStatusType")
	public void updateStatusJobJobStatusTypeString(JobStatusType jobStatus, JobStatusType newStatus, String statusMessage, String notificatonEvent, boolean shouldThrowException, String message) 
	{
		Job job = null;
		try 
		{
			NotificationDao notificationDao = new NotificationDao();
			
			job = createJob(jobStatus);
			JobDao.persist(job);
			
			Notification notification = null;
			if (!StringUtils.isEmpty(notificatonEvent)) {
				notification = new Notification(job.getUuid(), job.getOwner(), notificatonEvent, "http://example.com", false);
				notificationDao.persist(notification);
			}
			
			JobManager.updateStatus(job, newStatus, statusMessage);

			// verify status update
			Assert.assertEquals(job.getStatus(), newStatus,
					"Job status did not update after status update.");
			Assert.assertEquals(job.getErrorMessage(), statusMessage,
					"Job description did not update after status update.");

			// verify event creation
			List<JobEvent> events = JobEventDao.getByJobIdAndStatus(job.getId(), job.getStatus());
			int expectedEvents = (jobStatus.equals(newStatus) && !jobStatus.getDescription().equals(statusMessage)) ? 2 : 1;
			Assert.assertEquals(expectedEvents, events.size(),
					"Wrong number of events found. Expected " + expectedEvents + ", found " + events.size());

			// this test will fail if the events do not come back ordered by created asc 
			JobEvent event = events.get(events.size() -1);
			Assert.assertEquals(event.getDescription(), statusMessage,
					"Wrong event description found. Expected '" + statusMessage
							+ "', found '" + event.getDescription() + "'");

			if (!(jobStatus.equals(newStatus) && jobStatus.getDescription().equals(statusMessage)) && 
					(StringUtils.equals(notificatonEvent, newStatus.name()) || StringUtils.equals(notificatonEvent, "*"))) 
			{
				// verify notification message was sent
				MessageQueueClient client = MessageClientFactory.getMessageClient();
				Message queuedMessage = null;
				try {
					queuedMessage = client.pop(Settings.NOTIFICATION_TOPIC,
							Settings.NOTIFICATION_QUEUE);
				} catch (Throwable e) { 
					Assert.fail("Failed to remove message from the queue. Further tests will fail.", e);
				}
				finally {
					client.delete(Settings.NOTIFICATION_TOPIC,
							Settings.NOTIFICATION_QUEUE, queuedMessage.getId());
				}
				Assert.assertNotNull(queuedMessage,
						"Null message found on the queue");
				JsonNode json = mapper.readTree(queuedMessage.getMessage());
				Assert.assertEquals(notification.getUuid(), json.get("uuid").textValue(),
						"Notification message has wrong uuid");
				Assert.assertEquals(job.getStatus().name(), json.get("event").textValue(),
						"Notification message has wrong event");
			}
			else
			{
				// check for messages in the queue?
				Assert.assertEquals(getMessageCount(Settings.NOTIFICATION_QUEUE), 0, 
						"Messages found in the queue when none should be there.");
			}
		} catch (Exception e) {
			if (!shouldThrowException) {
				Assert.fail(message, e);
			}
		}
		finally {
			try { clearJobs(); } catch (Exception e) {}
		}
	}
	
	/*********************************************************************************
	 * 								NOT YET IMPLEMENTED
	 *********************************************************************************/
	
	

	/**
	 * Generic submission test used by all the methods testing job submission is some
	 * form or fashion.
	 * 
	 * @param job
	 * @param shouldThrowException
	 * @param message
	 */
	private void genericSubmitTest(Job job, boolean shouldThrowException, String message) 
	{
		try 
		{
			JobManager.submit(job);

			Assert.assertNotNull(job.getLocalJobId(),
					"Local job id was not obtained during submission");
			Assert.assertEquals(job.getStatus(), JobStatusType.QUEUED,
					"Job status was not updated to QUEUED after submission");
			Assert.assertNotNull(job.getSubmitTime(),
					"Job submit time was not updated during job submission");
		} 
		catch (Exception e) 
		{
			Assert.assertTrue(shouldThrowException, message);
		}
		finally {
			try { cleanupJob(job); } catch (Throwable e) {}
		}
	}
	
	private void cleanupJob(Job job) throws JobException
	{
		SystemDao systemDao = new SystemDao();
		ExecutionSystem executionSystem = (ExecutionSystem)systemDao.findBySystemId(job.getSystem());
		
		try 
		{
			JobManager.kill(job);
		} 
		catch (Throwable e) 
		{
			throw new JobException("Failed to kill test job submission.\n"
					+ "System: " + job.getSystem() + " \n"
					+ "Host: " + executionSystem.getLoginConfig().getHost() + ":" + executionSystem.getLoginConfig().getPort() + "\n"
					+ "Username: " + executionSystem.getLoginConfig().getDefaultAuthConfig().getUsername() + "\n"
					+ "Work directory: " + job.getWorkPath() + "\n"
					+ "Local id: " + job.getLocalJobId() + "s\n", e);
		}
		
		RemoteDataClient dataClient = null;
		try 
		{
			dataClient = executionSystem.getRemoteDataClient(job.getInternalUsername());
			dataClient.authenticate();
			dataClient.delete(job.getWorkPath());
		}
		catch (Throwable e) 
		{
			throw new JobException("Failed to clean up after killing job submission.\n"
					+ "System: " + job.getSystem() + " \n"
					+ "Host: " + executionSystem.getLoginConfig().getHost() + ":" + executionSystem.getLoginConfig().getPort() + "\n"
					+ "Username: " + executionSystem.getLoginConfig().getDefaultAuthConfig().getUsername() + "\n"
					+ "Work directory: " + job.getWorkPath() + "\n"
					+ "Local id: " + job.getLocalJobId() + "s\n", e);
		}
		finally 
		{
			try { dataClient.disconnect(); } catch (Throwable e) {}
		}
	}
	
	@DataProvider
	public Object[][] submitProvider() throws Exception
	{
		// for each execution system
		// 		create software for that system
		//		create job for that system
		// 		stage data to that system's work folder
		//		pass in job 
		
		// this should cover all combinations of protocols and schedulers
		return new Object[][] {
				{ createJob(JobStatusType.STAGED), false, "Job submission should succeed" }	
		};
	}
	
	/**
	 * Test job submission using all combinations of protocols and schedulers
	 * 
	 * @param job
	 * @param shouldThrowException
	 * @param message
	 */
	//@Test(dataProvider = "submitProvider", dependsOnMethods = { "processJobFormStringString" })
	public void submit(Job job, boolean shouldThrowException, String message) 
	{
		genericSubmitTest(job, shouldThrowException, message);
	}
	

	/**
	 * Tests that submission fails and the job requeues when the execution system is not available.
	 */
	//@Test(dependsOnMethods = { "submit" })
	public void submitTestExecutionSystemChecks() 
	{
		SystemDao systemDao = new SystemDao();
		privateExecutionSystem.setStatus(SystemStatusType.DOWN);
		systemDao.persist(privateExecutionSystem);
		
		try {
			Job job = createJob(JobStatusType.STAGED);
			genericSubmitTest(job, true, "Job submission should fail when the execution system is down");
		} catch (Throwable e) {
			Assert.fail("Job submission should fail when the execution system is down");
		}
		
		privateExecutionSystem.setStatus(SystemStatusType.UNKNOWN);
		systemDao.persist(privateExecutionSystem);
		
		try {
			Job job = createJob(JobStatusType.STAGED);
			genericSubmitTest(job, true, "Job submission should fail when the execution system is unknown");
		} catch (Throwable e) {
			Assert.fail("Job submission should fail when the execution system is unknown");
		}
	}
	
	/**
	 * Tests that submission fails and the job requeues when the job archive system is not available.
	 */
	//@Test(dependsOnMethods = { "submitTestExecutionSystemChecks" })
	public void submitTestArchiveSystemChecks() 
	{
		SystemDao systemDao = new SystemDao();
		privateStorageSystem.setStatus(SystemStatusType.DOWN);
		systemDao.persist(privateStorageSystem);
		
		try {
			Job job = createJob(JobStatusType.STAGED);
			genericSubmitTest(job, true, "Job submission should fail when the archive system is down");
			Assert.assertEquals(job.getStatus(), JobStatusType.PENDING, "Job should remain in a pending state until the sytems become available again.");
		} catch (Throwable e) {
			Assert.fail("Job submission should fail when the archive system is down");
		}
		
		privateStorageSystem.setStatus(SystemStatusType.UNKNOWN);
		systemDao.persist(privateStorageSystem);
		
		try {
			Job job = createJob(JobStatusType.STAGED);
			genericSubmitTest(job, true, "Job submission should fail when the archive system is unknown");
			Assert.assertEquals(job.getStatus(), JobStatusType.PENDING, "Job should remain in a pending state until the sytems become available again.");
		} catch (Throwable e) {
			Assert.fail("Job submission should fail when the archive system is unknown");
		}
	}
	
	/**
	 * Tests that submission fails and the job requeues when the app deployment system is not available.
	 */
	//@Test(dependsOnMethods = { "submitTestArchiveSystemChecks" })
	public void submitTestApplicationSystemChecks() 
	{
		SystemDao systemDao = new SystemDao();
		StorageSystem softwareSystem = software.getStorageSystem();
		softwareSystem.setStatus(SystemStatusType.DOWN);
		systemDao.persist(softwareSystem);
		
		try {
			Job job = createJob(JobStatusType.STAGED);
			genericSubmitTest(job, true, "Job submission should fail when the application storage system is down");
			Assert.assertEquals(job.getStatus(), JobStatusType.PENDING, "Job should remain in a pending state until the sytems become available again.");
		} catch (Throwable e) {
			Assert.fail("Job submission should fail when the archive system is down");
		}
		
		softwareSystem.setStatus(SystemStatusType.UNKNOWN);
		systemDao.persist(softwareSystem);
		
		try {
			Job job = createJob(JobStatusType.STAGED);
			genericSubmitTest(job, true, "Job submission should fail when the application storage system is unknown");
			Assert.assertEquals(job.getStatus(), JobStatusType.PENDING, "Job should remain in a pending state until the sytems become available again.");
		} catch (Throwable e) {
			Assert.fail("Job submission should fail when the archive system is unknown");
		}
	}
	
	/**
	 * Tests that submission fails and the job requeues when the app is not available.
	 */
	//@Test(dependsOnMethods = { "submitTestApplicationSystemChecks" })
	public void submitTestApplicationChecks() 
	{
		software.setAvailable(false);
		SoftwareDao.persist(software);
		
		try {
			Job job = createJob(JobStatusType.STAGED);
			genericSubmitTest(job, true, "Job submission should fail when the application is not available");
			Assert.assertEquals(job.getStatus(), JobStatusType.PENDING, "Job should remain in a pending state until the sytems become available again.");
		} catch (Throwable e) {
			Assert.fail("Job submission should fail when the archive system is down");
		}
	}

	//@Test(dataProvider = "checkStatusProvider", dependsOnMethods = { "submitTestApplicationChecks" })
	public void checkStatus(Job job, boolean shouldThrowException, String message) {
		throw new RuntimeException("Test not implemented");
	}

	//@Test
	public void archive() {
		throw new RuntimeException("Test not implemented");
	}

	//@Test
	public void hide(Job job, boolean shouldThrowException, String message) {
		throw new RuntimeException("Test not implemented");
	}

	//@Test
	public void kill(Job job, boolean shouldThrowException, String message) {
		throw new RuntimeException("Test not implemented");
	}

}
