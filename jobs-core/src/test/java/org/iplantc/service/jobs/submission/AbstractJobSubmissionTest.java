package org.iplantc.service.jobs.submission;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.iplantc.service.apps.exceptions.SoftwareException;
import org.iplantc.service.apps.model.Software;
import org.iplantc.service.common.persistence.HibernateUtil;
import org.iplantc.service.jobs.dao.JobDao;
import org.iplantc.service.jobs.exceptions.JobException;
import org.iplantc.service.jobs.exceptions.JobProcessingException;
import org.iplantc.service.jobs.managers.JobManager;
import org.iplantc.service.jobs.model.Job;
import org.iplantc.service.jobs.model.enumerations.JobStatusType;
import org.iplantc.service.jobs.util.Slug;
import org.iplantc.service.systems.dao.SystemDao;
import org.iplantc.service.systems.model.RemoteSystem;
import org.restlet.data.Form;
import org.restlet.data.Parameter;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class AbstractJobSubmissionTest {

	public static String EXECUTION_SYSTEM_TEMPLATE_DIR = "src/test/resources/systems/execution";
	public static String STORAGE_SYSTEM_TEMPLATE_DIR = "src/test/resources/systems/storage";
	public static String SOFTWARE_SYSTEM_TEMPLATE_DIR = "src/test/resources/software";
	public static String INTERNAL_USER_TEMPLATE_DIR = "src/test/resources/internal_users";
	public static String CREDENTIALS_TEMPLATE_DIR = "src/test/resources/credentials";
	public static String TEST_DATA_DIR = "src/test/resources/transfer";
	public static String TEST_INPUT_FILE = TEST_DATA_DIR + "/test_upload.txt";
	
	public static final String SYSTEM_OWNER = "api_sample_user";
	public static final String SYSTEM_OWNER_SHARED = "ipctestshare";
	
	protected Form form;
	protected Job job;
	protected SystemDao systemDao = new SystemDao();
	
	/**
	 * Initalizes the test db and adds the test app 
	 */
	@BeforeClass
	public void initDb() throws Exception {}
	
	protected void clearSystems()
	{	
		for(RemoteSystem s: systemDao.getAll()) {
			systemDao.remove(s);
		}
	}
	
	@SuppressWarnings("unchecked")
	protected void clearSoftware() throws Exception
	{
		Session session = null;
		try
		{
			HibernateUtil.beginTransaction();
			session = HibernateUtil.getSession();
			for (Software software: (List<Software>)session.createQuery("from Software").list()) {
				session.delete(software);
			}
			session.flush();
		}
		catch (HibernateException ex) {
			throw new SoftwareException(ex);
		}
		finally {
			try { HibernateUtil.commitTransaction();} catch (Exception e) {}
		}
	}
	
	@SuppressWarnings("unchecked")
	protected void clearJobs() throws Exception
	{
		Session session = null;
		try
		{
			HibernateUtil.beginTransaction();
			session = HibernateUtil.getSession();
			for (Job job: (List<Job>)session.createQuery("from Job").list()) {
				session.delete(job);
			}
			session.flush();
		}
		catch (HibernateException ex) {
			throw new JobException(ex);
		}
		finally {
			try { HibernateUtil.commitTransaction();} catch (Exception e) {}
		}
	}
	
	@AfterClass
	public void afterClass() throws Exception
	{
		clearJobs();
		clearSoftware();
		clearSystems();
	}
	
	/**
	 * Generic test case for job submission tests. Tries to submit the form, asserts that an 
	 * exception was thrown if shouldThrowExceptino was true.
	 *
     * @param attribute
	 * @param value
	 * @param message
	 * @param shouldThrowException
	 */
	protected void genericJobSubmissionTestCase(String attribute, String value, String message, boolean shouldThrowException) 
	{
		boolean actuallyThrewException = false;
		String exceptionMsg = message;
		
		Form testForm = updateJobSubmissionForm(form, attribute, value);
        
		try
        {
        	job = JobManager.processJob(testForm, SYSTEM_OWNER, null);
        }
        catch(JobProcessingException se)
        {
        	actuallyThrewException = true;
            exceptionMsg = "Error submitting job: " + message;
            if (actuallyThrewException != shouldThrowException) se.printStackTrace();
        }
		
        System.out.println(" exception thrown?  expected " + shouldThrowException + " actual " + actuallyThrewException);
		Assert.assertTrue(actuallyThrewException == shouldThrowException, exceptionMsg);
		
	}
	
	/**
	 * Generic test case for job submission tests. Tries to submit the form, asserts that an 
	 * exception was thrown if shouldThrowExceptino was true.
	 * 
	 * @param job
	 * @param message
	 * @param shouldThrowException
	 */
	protected void genericRemoteSubmissionTestCase(Job job, String message, boolean shouldThrowException) 
	{
		boolean actuallyThrewException = false;
		String exceptionMsg = message;
		
		try
        {
			JobDao.persist(job);
			
			job.setArchivePath("/" + job.getOwner() + "/archive/jobs/job-" + job.getId() + "-" + Slug.toSlug(job.getName()));
			
			JobDao.persist(job);
			
			JobManager.submit(job);
			
			job = JobDao.getById(job.getId());
			
			Assert.assertEquals(job.getStatus(), JobStatusType.QUEUED, 
					"Job was not placed into queue on " + job.getSystem());
			
			Assert.assertTrue(!StringUtils.isEmpty(job.getLocalJobId()), 
					"Local job id not obtained from submission to " + 
					job.getSystem() + "\n" + job.getErrorMessage());
        }
        catch (Exception e)
		{
			actuallyThrewException = true;
            exceptionMsg = "Error placing job into queue on " + job.getSystem() + ": " + message;
            if (actuallyThrewException != shouldThrowException) e.printStackTrace();
		}
		finally
		{
			try { JobDao.delete(job); } catch (Exception e) {}
		}
		
        System.out.println(" exception thrown?  expected " + shouldThrowException + " actual " + actuallyThrewException);
		Assert.assertTrue(actuallyThrewException == shouldThrowException, exceptionMsg);
		
	}
	
	/**
	 * Updates the given name value pair in the form and returns a copy of the updated form.
	 * 
	 * @param form
	 * @param name
	 * @param value
	 * @return
	 */
	protected Form updateJobSubmissionForm(Form form, String name, String value)
	{
		Form newForm = new Form();
		Iterator<Parameter> iterator = form.iterator();
		while(iterator.hasNext())
		{
			Parameter p = iterator.next();
			newForm.add(p);
		}
		
		newForm.set(name, value, true);
		
		return newForm;
	}
}
