package org.iplantc.service.jobs.dao;

import java.util.Date;

import org.iplantc.service.jobs.model.Job;
import org.iplantc.service.jobs.model.enumerations.JobStatusType;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class ExponentialBackoffTest extends AbstractDaoTest 
{

	@BeforeClass
	@Override
	public void beforeClass() throws Exception
	{
		super.beforeClass();
	}
	
	@AfterClass
	@Override
	public void afterClass() throws Exception
	{
		super.afterClass();
	}
	
	@BeforeMethod
	public void setUp() throws Exception {
		initSystems();
        initSoftware();
        clearJobs();
	}
	
	@AfterMethod
	public void tearDown() throws Exception {
		clearJobs();
		clearSoftware();
		clearSystems();
	}
	
	@DataProvider(name="getNextRunningJobProvider")
	public Object[][] getNextRunningJobProvider() {
		// exponential backoff algorithm is 2^(n-1) where n is the number of status checks to date
		Object[][] testCases = new Object[1][1];
		for (int i=0;i<testCases.length; i++) {
			testCases[i] = new Integer[]{i+1};
		}
		return testCases;
	}
	
	@Test(dataProvider="getNextRunningJobProvider")
	public void getNextRunningJob(int numberOfChecks) 
	{
		Date currentTime = new Date();
		// create job with status RUNNING
		Job jobPastDue, jobOnTime, jobNotYet;
		try {
			
			jobPastDue = createJob(JobStatusType.QUEUED);
			jobPastDue.setLastUpdated(new Date((long)(currentTime.getTime() - calculateBackoff(numberOfChecks) - 300000)));
			jobPastDue.setStatusChecks(numberOfChecks);
			JobDao.persist(jobPastDue);
			Assert.assertNotNull(jobPastDue.getId(), "Failed to persist past due job");
			
			Job runningJob = JobDao.getNextRunningJob();
			Assert.assertNotNull(runningJob, "Failed to retrieve past due job when number of checks was " + numberOfChecks);
			Assert.assertEquals(runningJob.getId(), jobPastDue.getId(), "Incorrect past due job retrieved.");
			JobDao.delete(jobPastDue);
			
			jobOnTime = createJob(JobStatusType.QUEUED);
			jobOnTime.setLastUpdated(new Date((long)(currentTime.getTime() - calculateBackoff(numberOfChecks))));
			jobOnTime.setStatusChecks(numberOfChecks);
			JobDao.persist(jobOnTime);
			Assert.assertNotNull(jobOnTime, "Failed to persist on time job");
			
			runningJob = JobDao.getNextRunningJob();
			Assert.assertNotNull(runningJob, "Failed to retrieve past due job when number of checks was " + numberOfChecks);
			Assert.assertEquals(runningJob.getId(), jobOnTime.getId(), "Incorrect on time job retrieved.");
			JobDao.delete(jobOnTime);
			
			jobNotYet = createJob(JobStatusType.QUEUED);
			jobNotYet.setLastUpdated(new Date(currentTime.getTime() - calculateBackoff(numberOfChecks) + 300000));
			jobNotYet.setStatusChecks(numberOfChecks);
			JobDao.persist(jobNotYet);
			Assert.assertNotNull(jobNotYet.getId(), "Failed to persist not yet ready job");
			
			runningJob = JobDao.getNextRunningJob();
			Assert.assertNull(runningJob, "Past due job found when none should be returned");
			
		} catch (Exception e) {
			Assert.fail("Unexpected error occurred running test for JobDao.getNextRunningJob()", e);
		}
	}
	
	@Test
	public void verifyBackoffProgression() 
	{
		Date currentTime = new Date();
		// create job with status RUNNING
		Job job;
		try {
			int numberOfChecks = 0;
			job = createJob(JobStatusType.QUEUED);
			job.setLastUpdated(new Date((long)(currentTime.getTime() - calculateBackoff(numberOfChecks))));
			job.setStatusChecks(numberOfChecks);
			JobDao.persist(job);
			Assert.assertNotNull(job, "Failed to persist on time job");
			
			Job runningJob = JobDao.getNextRunningJob();
			Assert.assertNotNull(runningJob, "Failed to retrieve past due job when number of checks was " + numberOfChecks);
			Assert.assertEquals(runningJob.getId(), job.getId(), "Incorrect on time job retrieved.");
			
			for (int i=3;i<6; i++)
			{
				// update. it should now fail
				numberOfChecks = i;
				runningJob.setLastUpdated(new Date());
				runningJob.setStatusChecks(numberOfChecks);
				JobDao.persist(runningJob);
				Job tooSoonRunningJob = JobDao.getNextRunningJob();
				Assert.assertNull(tooSoonRunningJob, "Retrieved job when time was now and checks was " + numberOfChecks);
				
				int shortOffset = calculateBackoff(numberOfChecks-1);
				int offset = calculateBackoff(numberOfChecks);
				Thread.sleep(shortOffset);
				Job nullJob = JobDao.getNextRunningJob();
				Assert.assertNull(nullJob, "Retrieved job when number of checks time was " + (shortOffset/1000) + " seconds after the last update and checks was " + numberOfChecks);
				
				// wait a second. it should now pass
				Thread.sleep(offset-shortOffset);
				runningJob = JobDao.getNextRunningJob();
				Assert.assertNotNull(runningJob, "Failed to retrieve past due job when number of checks was " + numberOfChecks);
			}
		} catch (Exception e) {
			Assert.fail("Unexpected error occurred running test for JobDao.getNextRunningJob()", e);
		}
	}
	
	private int calculateBackoff(int numberOfChecks) {
		int  backoff = (int)Math.pow(2, numberOfChecks-1) * 1000;
		System.out.println(backoff);
		return backoff;
	}
}
