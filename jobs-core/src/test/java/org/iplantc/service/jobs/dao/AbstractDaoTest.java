/**
 * 
 */
package org.iplantc.service.jobs.dao;

import static org.iplantc.service.jobs.model.JSONTestDataUtil.TEST_EXECUTION_SYSTEM_FILE;
import static org.iplantc.service.jobs.model.JSONTestDataUtil.TEST_OWNER;
import static org.iplantc.service.jobs.model.JSONTestDataUtil.TEST_SOFTWARE_SYSTEM_FILE;
import static org.iplantc.service.jobs.model.JSONTestDataUtil.TEST_STORAGE_SYSTEM_FILE;

import java.util.Date;
import java.util.List;

import org.codehaus.plexus.util.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.iplantc.service.apps.exceptions.SoftwareException;
import org.iplantc.service.apps.model.Software;
import org.iplantc.service.apps.model.SoftwareInput;
import org.iplantc.service.apps.model.SoftwareParameter;
import org.iplantc.service.common.persistence.HibernateUtil;
import org.iplantc.service.jobs.exceptions.JobException;
import org.iplantc.service.jobs.model.JSONTestDataUtil;
import org.iplantc.service.jobs.model.Job;
import org.iplantc.service.jobs.model.JobEvent;
import org.iplantc.service.jobs.model.enumerations.JobStatusType;
import org.iplantc.service.notification.dao.NotificationDao;
import org.iplantc.service.notification.model.Notification;
import org.iplantc.service.systems.dao.SystemDao;
import org.iplantc.service.systems.model.ExecutionSystem;
import org.iplantc.service.systems.model.RemoteSystem;
import org.iplantc.service.systems.model.StorageSystem;
import org.iplantc.service.systems.model.enumerations.RemoteSystemType;
import org.json.JSONObject;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

/**
 * @author dooley
 *
 */
public class AbstractDaoTest 
{	
	public static String EXECUTION_SYSTEM_TEMPLATE_DIR = "src/test/resources/systems/execution";
	public static String STORAGE_SYSTEM_TEMPLATE_DIR = "src/test/resources/systems/storage";
	public static String SOFTWARE_SYSTEM_TEMPLATE_DIR = "src/test/resources/software";
	public static String INTERNAL_USER_TEMPLATE_DIR = "src/test/resources/internal_users";
	public static String CREDENTIALS_TEMPLATE_DIR = "src/test/resources/credentials";
	
	protected JSONTestDataUtil jtd;
	protected SystemDao systemDao = new SystemDao();
	protected StorageSystem privateStorageSystem;
	protected ExecutionSystem privateExecutionSystem;
	public Software software;
	
	@BeforeClass
	public void beforeClass() throws Exception
	{
		systemDao = new SystemDao();
		
		jtd = JSONTestDataUtil.getInstance();

        initSystems();
        initSoftware();
        clearJobs();
	}
	
	protected void initSystems() throws Exception
	{
		clearSystems();
		
		privateExecutionSystem = ExecutionSystem.fromJSON(jtd.getTestDataObject(TEST_EXECUTION_SYSTEM_FILE));
		privateExecutionSystem.setOwner(TEST_OWNER);
		privateExecutionSystem.getUsersUsingAsDefault().add(TEST_OWNER);
		privateExecutionSystem.setType(RemoteSystemType.EXECUTION);
		systemDao.persist(privateExecutionSystem);
		
		privateStorageSystem = StorageSystem.fromJSON(jtd.getTestDataObject(TEST_STORAGE_SYSTEM_FILE));
		privateStorageSystem.setOwner(TEST_OWNER);
		privateStorageSystem.getUsersUsingAsDefault().add(TEST_OWNER);
		privateStorageSystem.setType(RemoteSystemType.STORAGE);
        systemDao.persist(privateStorageSystem);
	}
	
	protected void clearSystems()
	{
		for(RemoteSystem s: systemDao.findByExample("available", true)) {
			systemDao.remove(s);
		}
	}
	
	protected void initSoftware() throws Exception
	{
		clearSoftware(); 
		
		JSONObject json = jtd.getTestDataObject(TEST_SOFTWARE_SYSTEM_FILE);
		software = Software.fromJSON(json, TEST_OWNER);
		software.setExecutionSystem(privateExecutionSystem);
		software.setOwner(TEST_OWNER);
		software.setVersion(software.getVersion());
	}
	
	@SuppressWarnings("unchecked")
	protected void clearSoftware() throws Exception
	{
		Session session = null;
		try
		{
			HibernateUtil.beginTransaction();
			session = HibernateUtil.getSession();
			for (Software software: (List<Software>)session.createQuery("from Software").list()) {
				session.delete(software);
			}
			session.flush();
		}
		catch (HibernateException ex) {
			throw new SoftwareException(ex);
		}
		finally {
			try { HibernateUtil.commitTransaction();} catch (Throwable e) {}
		}
	}
	
	@SuppressWarnings("unchecked")
	protected void clearJobs() throws Exception
	{
		NotificationDao notificationDao = new NotificationDao();
		
		Session session = null;
		try
		{
			HibernateUtil.beginTransaction();
			session = HibernateUtil.getSession();
			session.clear();
			for (Job job: (List<Job>)session.createQuery("from Job").list()) {
				JobDao.delete(job);
			}
			
			for (JobEvent event: (List<JobEvent>)session.createQuery("from JobEvent").list()) {
				JobEventDao.delete(event);
			}
			
			for (Notification notification: (List<Notification>)session.createQuery("from Notification").list()) {
				notificationDao.delete(notification);
			}
			
			session.flush();
		}
		catch (Throwable ex) {
			throw new JobException(ex);
		}
		finally {
			try { HibernateUtil.commitTransaction();} catch (Throwable e) {}
		}
	}
	
	protected void clearJobPermissions() throws Exception
	{
		for(Job j: JobDao.getByUsername(TEST_OWNER)) {
        	JobDao.delete(j);
        }
	}
	
	protected void clearJobNotifications() throws Exception
	{
		NotificationDao ndao = new NotificationDao();
		
		for(Notification n: ndao.getAll()) {
        	ndao.delete(n);
        }
	}

	@AfterClass
	public void afterClass() throws Exception
	{
		clearJobs();
		clearSoftware();
		clearSystems();
	}
	
	public Job createJob(JobStatusType status) throws Exception
	{
		Job job = new Job();
		job.setName("test-job");
        job.setInternalUsername(TEST_OWNER + "_internaluser");
		job.setArchiveOutput(true);
		job.setArchivePath(TEST_OWNER + "/archive/test-job-999");
		job.setArchiveSystem(privateStorageSystem);
		job.setBatchQueue(StringUtils.isEmpty(software.getDefaultQueue()) ? software.getExecutionSystem().getDefaultQueue().getName() : software.getDefaultQueue());
		job.setCharge(Float.parseFloat("1001.5"));
		job.setEndTime(new Date());
		JSONObject inputs = new JSONObject();
		for(SoftwareInput swInput: software.getInputs()) {
			inputs.put(swInput.getKey(), swInput.getDefaultValue());
		}
		job.setInputs(inputs.toString());
		job.setLocalJobId("12345");
		job.setMaxRunTime(StringUtils.isEmpty(software.getDefaultMaxRunTime()) ? "00:30:00" : software.getDefaultMaxRunTime());
		job.setMemoryPerNode((software.getDefaultMemoryPerNode() == null) ? (double)1 : software.getDefaultMemoryPerNode());
		job.setNodeCount((software.getDefaultNodes() == null) ? (long)1 : software.getDefaultNodes());
		job.setOutputPath("/dev/null");
		job.setOwner(TEST_OWNER);
		JSONObject parameters = new JSONObject();
		for(SoftwareParameter swParameter: software.getParameters()) {
			parameters.put(swParameter.getKey(), swParameter.getDefaultValue());
		}
		job.setParameters(parameters.toString());
		job.setProcessorsPerNode((software.getDefaultProcessorsPerNode() == null) ? (long)1 : software.getDefaultProcessorsPerNode());
		job.setSchedulerJobId("12345");
		job.setSoftwareName(software.getUniqueName());
		job.setStatus(status, status.getDescription());
		job.setStartTime(new Date());
		job.setSubmitTime(new Date());
		job.setSystem(software.getExecutionSystem().getSystemId());
		job.setWorkPath("/dev/null");
		
		return job;
	}
}
