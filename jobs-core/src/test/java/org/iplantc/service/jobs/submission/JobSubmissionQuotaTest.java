/**
 * 
 */
package org.iplantc.service.jobs.submission;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.hibernate.cfg.Configuration;
import org.iplantc.service.apps.dao.SoftwareDao;
import org.iplantc.service.apps.model.Software;
import org.iplantc.service.apps.model.SoftwarePermission;
import org.iplantc.service.common.persistence.HibernateUtil;
import org.iplantc.service.jobs.dao.JobDao;
import org.iplantc.service.jobs.exceptions.JobException;
import org.iplantc.service.jobs.managers.JobManager;
import org.iplantc.service.jobs.model.JSONTestDataUtil;
import org.iplantc.service.jobs.model.Job;
import org.iplantc.service.jobs.model.enumerations.JobStatusType;
import org.iplantc.service.systems.dao.SystemDao;
import org.iplantc.service.systems.exceptions.SystemArgumentException;
import org.iplantc.service.systems.model.ExecutionSystem;
import org.iplantc.service.systems.model.SystemRole;
import org.iplantc.service.systems.model.enumerations.RoleType;
import org.iplantc.service.systems.model.enumerations.SystemStatusType;
import org.iplantc.service.transfer.model.enumerations.PermissionType;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
//import org.iplantc.service.systems.model.JSONTestDataUtil;

/**
 * Tests the user and overall job quotas enforced by the system.
 * 
 * @author dooley
 *
 */
public class JobSubmissionQuotaTest extends AbstractJobSubmissionTest {

	private static final String SYSTEM_OWNER = "testuser";
	private static final String SYSTEM_OWNER_SHARED = "shareduser";
	private SystemDao systemsDao;
	private JSONTestDataUtil jtd;
	private Software software;
	
	@BeforeClass
	public void initDb()
	{
		try
		{
			jtd = JSONTestDataUtil.getInstance();
		    File file = new File("src/test/resources/db/hibernate.cfg.xml");
			Configuration configuration = new Configuration().configure(file);
			HibernateUtil.rebuildSessionFactory(configuration);
			systemsDao = new SystemDao();
			
			JSONObject systemJson = jtd.getTestDataObject(JSONTestDataUtil.TEST_EXECUTION_SYSTEM_FILE);
			
			ExecutionSystem existingSystem = (ExecutionSystem)systemsDao.findBySystemId(systemJson.getString("id"));
			if (existingSystem == null) { 
				existingSystem =  ExecutionSystem.fromJSON(systemJson);
				existingSystem.setOwner(SYSTEM_OWNER);
				existingSystem.setPubliclyAvailable(false);
				existingSystem.setStatus(SystemStatusType.DOWN);
				existingSystem.getRoles().add(new SystemRole(SYSTEM_OWNER_SHARED, RoleType.PUBLISHER));
				systemsDao.persist(existingSystem);
				existingSystem = (ExecutionSystem)systemsDao.findById(existingSystem.getId());
				
			}
			
			JSONObject appJson = jtd.getTestDataObject(JSONTestDataUtil.TEST_SOFTWARE_SYSTEM_FILE);
	        appJson.put("executionHost", existingSystem.getSystemId());
	        software = Software.fromJSON(appJson, SYSTEM_OWNER);
	        
	        software.setExecutionSystem(existingSystem);
	        software.setOwner(SYSTEM_OWNER);
	        software.setPubliclyAvailable(false);
	        software.getPermissions().add(new SoftwarePermission(SYSTEM_OWNER_SHARED, PermissionType.ALL));
	        SoftwareDao.persist(software);
		}
		catch (Exception e)
		{	
			e.printStackTrace();
		}
	}
	
	@AfterClass
	public void clearDb()
	{
		try { systemsDao.remove(software.getExecutionSystem()); } catch (Exception e) {}
		
		try { SoftwareDao.delete(software); } catch (Exception e) {}
	}
	
	@BeforeMethod
	public void beforeMethod() throws SystemArgumentException, JSONException, IOException 
	{
		HibernateUtil.flush();
		
	}
	
	public Job getDefaultJob() throws JobException
	{
		Job job = new Job();
		job.setName("test-job");
		job.setArchiveOutput(true);
		job.setArchivePath("/iplant-test/archive/test-job-999");
		job.setCreated(new Date());
		JSONObject inputs = new JSONObject();
//		inputs.put("input1", "/iplant-test/test-data/foo.dat");
		job.setInputs(inputs.toString());
		job.setMemoryPerNode((double)512);
		job.setOwner(SYSTEM_OWNER);
		JSONObject outputs = new JSONObject();
		job.setParameters(outputs.toString());
		job.setProcessorsPerNode((long)1);
		job.setMaxRunTime("1:00");
		job.setSoftwareName(software.getUniqueName());
		job.setStatus(JobStatusType.STAGED, "Input data staged to execution system");
		job.setSystem(software.getExecutionSystem().getSystemId());
		job.setUpdateToken("asodfuasodu2342asdfar23rsdafa");
		job.setVisible(true);
		
		return job;
	}
	
	@Test
	public void jobSystemUnavailableStaysInQueue() throws Exception
	{
		Job job;
		try 
		{
			job = getDefaultJob();
			
			JobDao.persist(job);
			
			JobManager.submit(job);
			
			job = JobDao.getById(job.getId());
			
			Assert.assertEquals(job.getStatus(), JobStatusType.STAGED, "Job status is still at staged when system unavailable.");
			
			Assert.assertTrue(job.getErrorMessage().contains("unavailable"), "Unavailable system should queue job");
			
		} 
		catch(Exception e) 
		{
			throw e;
		}
		finally 
		{
			for(Job j: JobDao.getByUsername(SYSTEM_OWNER)) {
				try { JobDao.delete(j); } catch (Exception e) {}
			}
			
//			try {
//				executionSystem.setStatus(SystemStatusType.UP);
//				systemsDao.persist(executionSystem);
//			} catch (Exception e) {}
//			
		}
	}
	
	@Test
	public void jobSystemQueueQuotaStaysInQueue() throws Exception
	{
		Job job;
		
		try 
		{
			job = getDefaultJob();
			
			
			for (int i=0; i<software.getExecutionSystem().getDefaultQueue().getMaxJobs(); i++) {
				Job j = job.copy();
				j.setStatus(JobStatusType.RUNNING, "Job began running");
				j.setSystem(job.getSystem());
				JobDao.persist(j);
			}
			
			JobDao.persist(job);
			
			JobManager.submit(job);
			
			job = JobDao.getById(job.getId());
			
			Assert.assertEquals(job.getStatus(), JobStatusType.STAGED, "Job status is still at staged when system unavailable.");
			
			Assert.assertTrue(job.getErrorMessage().contains("capacity for new jobs"), "No new jobs ");
			
		} 
		catch(Exception e) 
		{
			throw e;
		}
		finally 
		{
			for(Job j: JobDao.getByUsername(SYSTEM_OWNER)) {
				try { JobDao.delete(j); } catch (Exception e) {}
			}
		}
	}
	
	@Test
	public void jobSystemSoftUserQuotaEnforced() throws Exception
	{
		Job job;
		
		try 
		{
			job = getDefaultJob();
			Software software = SoftwareDao.getSoftwareByUniqueName(job.getSoftwareName());
			ExecutionSystem executionSystem = software.getExecutionSystem();
			for (int i=0; i<Math.ceil(executionSystem.getMaxSystemJobsPerUser() * 1.2); i++) {
				Job j = job.copy();
				j.setStatus(JobStatusType.RUNNING, "Job began running");
				j.setSystem(job.getSystem());
				JobDao.persist(j);
			}
			
			JobDao.persist(job);
			
			JobManager.submit(job);
			
			job = JobDao.getById(job.getId());
			
			Assert.assertEquals(job.getStatus(), JobStatusType.STAGED, "Job status is still at staged when soft user quota enforced.");
			
			Assert.assertTrue(job.getErrorMessage().contains("simultaneous jobs"), "No new jobs ");
			
		} 
		catch(Exception e) 
		{
			throw e;
		}
		finally {
			for(Job j: JobDao.getByUsername(SYSTEM_OWNER)) {
				try { JobDao.delete(j); } catch (Exception e) {}
			}
		}
	}
	
	@Test
	public void jobSystemHardUserQuotaEnforced() throws Exception
	{
		Job job;
		
		try 
		{
			job = getDefaultJob();
			
			job.setStatus(JobStatusType.RUNNING, "Job began running");
			
			job.setName("test-test-2");
			
			double queueQuota = Math.ceil(software.getExecutionSystem().getDefaultQueue().getMaxJobs() / 2) + 1;
			
			for (int i=0; i<queueQuota; i++) {
				Job j = job.copy();
				job.setOwner(SYSTEM_OWNER_SHARED);
				j.setStatus(JobStatusType.RUNNING, "Job began running");
				j.setSystem(job.getSystem());
				JobDao.persist(j);
			}
			
			job.setName("test-user-2");
			Software software = SoftwareDao.getSoftwareByUniqueName(job.getSoftwareName());
			ExecutionSystem executionSystem = software.getExecutionSystem();
			for (int i=0; i<Math.ceil(executionSystem.getMaxSystemJobsPerUser() * 1.2); i++) {
				Job j = job.copy();
				j.setStatus(JobStatusType.RUNNING, "Job began running");
				j.setSystem(job.getSystem());
				JobDao.persist(j);
			}
			
			JobDao.persist(job);
			
			JobManager.submit(job);
			
			job = JobDao.getById(job.getId());
			
			Assert.assertEquals(job.getStatus(), JobStatusType.STAGED, "Job status is still at staged when hard user quota enforced.");
			Assert.assertNotNull(job.getErrorMessage(), "Job was not run. Error stored with job");
			Assert.assertTrue(job.getErrorMessage().contains("simultaneous jobs"), "No new jobs ");
			
		} 
		catch(Exception e) 
		{
			throw e;
		}
		finally {
			for(Job j: JobDao.getByUsername(SYSTEM_OWNER)) {
				try { JobDao.delete(j); } catch (Exception e) {}
			}
			
			for(Job j: JobDao.getByUsername(SYSTEM_OWNER_SHARED)) {
				try { JobDao.delete(j); } catch (Exception e) {}
			}
		}
	}
}
