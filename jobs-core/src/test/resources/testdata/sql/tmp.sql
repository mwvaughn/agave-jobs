 CREATE TABLE Jobs
(
    id BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    owner VARCHAR(32) NOT NULL,
    internalUser VARCHAR(32) NOT NULL,
    system VARCHAR(128),
    softwareName VARCHAR(255) NOT NULL,
    processorCount INT,
    requestedTime VARCHAR(9) NOT NULL,
    memoryRequest INT,
    callbackUrl VARCHAR(255),
    outputPath VARCHAR(255),
    archiveOutput BIT,
    archivePath VARCHAR(255),
    workPath VARCHAR(255),
    status VARCHAR(64),
    updateToken VARCHAR(128),
    inputs LONGTEXT,
    parameters LONGTEXT,
    localId VARCHAR(255),
    schedulerJobId VARCHAR(255),
    charge REAL,
    startTime DATETIME,
    submitTime DATETIME,
    endTime DATETIME,
    errorMessage VARCHAR(255),
    lastUpdated DATETIME,
    created DATETIME,
    visible BIT,
    archiveSystem BIGINT NOT NULL
);
CREATE INDEX FK2350763F8D90DC ON Jobs ( archiveSystem );


SELECT s FROM RemoteSystem s WHERE s.type = :type AND s.publiclyAvailable = :publiclyAvail AND s.globalDefault = :globalDefault AND s.available = :available AND :username in elements( s.usersUsingAsDefault)


INSERT INTO test.Jobs (id, name, owner, internalUser, system, softwareName, processorCount, requestedTime, memoryRequest, callbackUrl, outputPath, archiveOutput, archivePath, workPath, status, updateToken, inputs, parameters, localId, schedulerJobId, charge, startTime, submitTime, endTime, errorMessage, lastUpdated, created, visible, archiveSystem) VALUES (9, 'Stevetest',  'sterry1',             'testuser',                  'localhost',             'wc-1.00', 1, '1:00',   0, null, null, true,              '/sterry1/jobs/condor', null, 'PENDING', '7d7e5472e5159d726d905b4c06009c2f',      '{"query1":"wc-1.00/read1.fq"}', '{"printLongestLine":"1"}', null, null, null, null, null, null, 'Failed ', null, '2013-02-02 00:00:01', true, 765);


INSERT INTO test.Jobs (id, name, owner, internalUser, system, softwareName, processorCount, requestedTime, memoryRequest, callbackUrl, outputPath, archiveOutput, archivePath, workPath, status, updateToken, inputs, parameters, localId, schedulerJobId, charge, startTime, submitTime, endTime, errorMessage, lastUpdated, created, visible, archiveSystem) VALUES (22, 'test-job', 'testuser', 'sterry1_internaluser', 'lonestar4.tacc.teragrid.org', 'head-trestles-5.97', 1, '1:00', 512, null, null, true, '/iplant-test/archive/test-job-999', null, 'PENDING',    'asodfuasodu2342asdfar23rsdafa', '{"inputfile":"/dooley/raxml.json"}',                       '{}', null, null, null, null, null, null,      null, null, '2013-03-26 16:06:54', true, 740);
INSERT INTO test.Jobs (id, name, owner, internalUser, system, softwareName, processorCount, requestedTime, memoryRequest, callbackUrl, outputPath, archiveOutput, archivePath, workPath, status, updateToken, inputs, parameters, localId, schedulerJobId, charge, startTime, submitTime, endTime, errorMessage, lastUpdated, created, visible, archiveSystem) VALUES (23, 'test-job', 'testuser', 'sterry1_internaluser', 'lonestar4.tacc.teragrid.org', 'head-trestles-5.97', 1, '1:00', 512, null, null, true, '/iplant-test/archive/test-job-999', null, 'STAGED', '    asodfuasodu2342asdfar23rsdafa', '{"inputfile":"/dooley/raxml.json"}',                       '{}', null, null, null, null, null, null,      null, null, '2013-03-26 16:07:32', true, 740);








INSERT INTO test.SYSTEMS (id, available, created, description, globalDefault, lastUpdated, name, owner, publiclyAvailable, revision, site, status, systemId, type, storageConfig_id) VALUES (13, true, '2013-03-11 09:17:05', 'My example storage system used for testing', false, '2013-03-11 09:17:06', 'My Example Storage System', 'testuser', false, 1, 'my.site', 'UP', 'storage.example.com', 'STORAGE', 700);
