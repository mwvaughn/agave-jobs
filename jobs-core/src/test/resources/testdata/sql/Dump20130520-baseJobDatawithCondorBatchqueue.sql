CREATE DATABASE  IF NOT EXISTS `test` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `test`;
-- MySQL dump 10.13  Distrib 5.5.29, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: test
-- ------------------------------------------------------
-- Server version	5.5.29-0ubuntu0.12.10.1-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `AUTHCONFIGS`
--

DROP TABLE IF EXISTS `AUTHCONFIGS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AUTHCONFIGS` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `credential` longtext,
  `internalUsername` varchar(32) DEFAULT NULL,
  `lastUpdated` datetime NOT NULL,
  `password` varchar(128) DEFAULT NULL,
  `systemDefault` bit(1) DEFAULT NULL,
  `loginCredentialType` varchar(16) NOT NULL,
  `username` varchar(32) DEFAULT NULL,
  `authenticationSystemId` bigint(20) DEFAULT NULL,
  `remoteConfig_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `FK2BB96EA9A30289FB` (`authenticationSystemId`),
  KEY `FK2BB96EA994301D9` (`remoteConfig_id`),
  CONSTRAINT `FK2BB96EA994301D9` FOREIGN KEY (`remoteConfig_id`) REFERENCES `REMOTECONFIGS` (`id`),
  CONSTRAINT `FK2BB96EA9A30289FB` FOREIGN KEY (`authenticationSystemId`) REFERENCES `CREDENTIALSERVERS` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=175 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AUTHCONFIGS`
--

LOCK TABLES `AUTHCONFIGS` WRITE;
/*!40000 ALTER TABLE `AUTHCONFIGS` DISABLE KEYS */;
INSERT INTO `AUTHCONFIGS` (`id`, `created`, `credential`, `internalUsername`, `lastUpdated`, `password`, `systemDefault`, `loginCredentialType`, `username`, `authenticationSystemId`, `remoteConfig_id`) VALUES (144,'2013-05-17 12:43:18',NULL,NULL,'2013-05-17 12:43:18','6GNT0o33NeYrf+RIOotL0rVqZzC765+E','','PASSWORD','ipcservices',NULL,NULL),(145,'2013-05-17 12:43:21',NULL,NULL,'2013-05-17 12:43:21','9HY4GXXiA+AYw961S5S4+Q==','','PASSWORD','ipcservices',NULL,NULL),(146,'2013-05-17 12:43:21',NULL,NULL,'2013-05-17 12:43:21','Ug1Wd5ALv8JhtGqM4KyOp2BgP9BxoaJV','','PASSWORD','iplant',NULL,NULL),(147,'2013-05-17 12:43:21','',NULL,'2013-05-17 12:43:21','mS5DsWdTCLixMXDR1VkqUT9Y9vkyIsc8','','X509','iplant',92,NULL),(148,'2013-05-17 12:43:21',NULL,NULL,'2013-05-17 12:43:21',NULL,'','LOCAL',NULL,NULL,NULL),(149,'2013-05-17 12:43:21','',NULL,'2013-05-17 12:43:21','XIzvfEsgatmGil5OPaouRhUD20kGpKDw','','X509','iplant',93,NULL),(150,'2013-05-17 12:43:21','',NULL,'2013-05-17 12:43:21','1XJ2pn4Qm/ClzN/Tdjf/90cjgdCxlVpX','','X509','iplant',94,NULL),(151,'2013-05-17 12:43:21',NULL,NULL,'2013-05-17 12:43:21',NULL,'','LOCAL',NULL,NULL,NULL),(152,'2013-05-17 12:43:21',NULL,NULL,'2013-05-17 12:43:21','XdejbUYQ6sf0b11V5857CSu2J5ViUzJb','','PASSWORD','username',NULL,NULL),(153,'2013-05-17 12:43:21','',NULL,'2013-05-17 12:43:21','MjqBDqYEVWWr5GE9IeSev0UrsH61r8Sc','','X509','iplant',95,NULL),(154,'2013-05-17 12:43:21','',NULL,'2013-05-17 12:43:21','mwRwmxhdSM6oxV9ZZTa0DsweYtbqU0FS','','X509','iplant',96,NULL),(155,'2013-05-17 12:43:21',NULL,NULL,'2013-05-17 12:43:21',NULL,'','LOCAL',NULL,NULL,NULL),(156,'2013-05-17 12:43:21',NULL,NULL,'2013-05-17 12:43:21',NULL,'','LOCAL',NULL,NULL,NULL),(157,'2013-05-17 12:43:21','',NULL,'2013-05-17 12:43:21','E0Z0L7Z/pYcSGrKLMjTd0obFL28UCs5a','','X509','iplant',97,NULL),(158,'2013-05-17 12:43:21','',NULL,'2013-05-17 12:43:21','apBDB4fMjaJiYbZ914TnSXgnc7cfXMqO','','X509','iplant',98,NULL),(159,'2013-05-17 12:43:21','',NULL,'2013-05-17 12:43:21','KlTkrPgqDrVg/BYzK/orK0eMAakqB4DX','','X509','iplant',99,NULL),(160,'2013-05-17 12:43:21','',NULL,'2013-05-17 12:43:21','BCv5gbIZmXejRJu+IvCkUfPMAD4XL9bz','','X509','iplant',100,NULL),(161,'2013-05-17 12:43:21','',NULL,'2013-05-17 12:43:21','8LJSgs8fErStbopVGMmE87+BFzQg+E5d','','X509','iplant',101,NULL),(162,'2013-05-17 12:43:21','',NULL,'2013-05-17 12:43:21','Rreo9IGW3bSg6pC7oA1IBE1lUIRCtlLP','','X509','iplant',102,NULL),(163,'2013-05-17 12:43:22',NULL,NULL,'2013-05-17 12:43:22',NULL,'','LOCAL',NULL,NULL,NULL),(164,'2013-05-17 12:43:22',NULL,NULL,'2013-05-17 12:43:22',NULL,'','LOCAL',NULL,NULL,NULL),(165,'2013-05-17 12:43:22','',NULL,'2013-05-17 12:43:22','Sgn55NqrWZLZF+rFir4rIQIDHmXoQmtE','','X509','iplant',103,NULL),(166,'2013-05-17 12:43:22',NULL,NULL,'2013-05-17 12:43:22','G3DNnEJi1LFX6JUv9MvMYTw7/tArrztA','','X509','iplant',104,NULL),(167,'2013-05-17 12:43:22','',NULL,'2013-05-17 12:43:22','gsruhSuKg+mBlEnLEDijvjihFCRZ5WQr','','X509','iplant',105,NULL),(168,'2013-05-17 12:43:22',NULL,NULL,'2013-05-17 12:43:22','L7hW6TA0mSttsFRFLnQRYZPkxo0TPQRB','','X509','iplant',106,NULL),(169,'2013-05-17 12:43:22','',NULL,'2013-05-17 12:43:22','aKrhj1nhA5zrsXXMau3bSwDcxqb456Ey','','PASSWORD','iplant',NULL,NULL),(170,'2013-05-17 12:43:22','sadasdfasdfasdfasdfasdfa',NULL,'2013-05-17 12:43:22','JNfp9o9P4bFKXv8zu5qYLueBtsYUPo6p','','TOKEN','iplant',107,NULL),(171,'2013-05-17 12:43:22','',NULL,'2013-05-17 12:43:22','pvZEB6vY1jU+X/59lUPuWhoufhV8wI9A','','X509','iplant',108,NULL),(172,'2013-05-17 12:43:22','',NULL,'2013-05-17 12:43:22','Cs0GOY6q+Z8iYTI+gFoFBRRuAJ7F+QgD','','X509','iplant',109,NULL),(173,'2013-05-17 12:43:22','',NULL,'2013-05-17 12:43:22','zde1Zh3Hf7zRAD5IPb6DWP9gKbhWk65L','','PASSWORD','iplant',NULL,NULL),(174,'2013-05-17 12:43:22','',NULL,'2013-05-17 12:43:22','RXIDo41zVqpNMGf+QxK6B826Sxcm1dso','','PASSWORD','iplant',NULL,NULL);
/*!40000 ALTER TABLE `AUTHCONFIGS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AuthenicationTokens`
--

DROP TABLE IF EXISTS `AuthenicationTokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AuthenicationTokens` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `creator` varchar(32) NOT NULL,
  `expires_at` datetime NOT NULL,
  `internal_username` varchar(32) NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `renewed_at` datetime NOT NULL,
  `remaining_uses` int(11) NOT NULL,
  `token` varchar(64) NOT NULL,
  `username` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `token` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AuthenicationTokens`
--

LOCK TABLES `AuthenicationTokens` WRITE;
/*!40000 ALTER TABLE `AuthenicationTokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `AuthenicationTokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `BATCHQUEUES`
--

DROP TABLE IF EXISTS `BATCHQUEUES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BATCHQUEUES` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `customDirectives` longtext,
  `lastUpdated` datetime NOT NULL,
  `maxJobs` bigint(20) NOT NULL,
  `maxMemory` bigint(20) NOT NULL,
  `name` varchar(128) NOT NULL,
  `systemDefault` bit(1) NOT NULL,
  `executionSystemId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `FKAFC6A11CB3964AA` (`executionSystemId`),
  CONSTRAINT `FKAFC6A11CB3964AA` FOREIGN KEY (`executionSystemId`) REFERENCES `EXECUTIONSYSTEMS` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `BATCHQUEUES`
--

LOCK TABLES `BATCHQUEUES` WRITE;
/*!40000 ALTER TABLE `BATCHQUEUES` DISABLE KEYS */;
INSERT INTO `BATCHQUEUES` (`id`, `created`, `customDirectives`, `lastUpdated`, `maxJobs`, `maxMemory`, `name`, `systemDefault`, `executionSystemId`) VALUES (44,'2013-05-17 12:43:21',NULL,'2013-05-17 12:43:21',100,2048,'testqueue','\0',NULL),(45,'2013-05-17 12:43:21',NULL,'2013-05-17 12:43:21',100,2048,'normal','',NULL),(46,'2013-05-17 12:43:21',NULL,'2013-05-17 12:43:21',100,2048,'normal','',NULL),(47,'2013-05-17 12:43:22',NULL,'2013-05-17 12:43:22',100,2048,'normal','',NULL),(48,'2013-05-17 12:43:22',NULL,'2013-05-17 12:43:22',100,2048,'default','',NULL),(49,'2013-05-17 12:43:22',NULL,'2013-05-17 12:43:22',100,2048,'normal','',NULL),(50,'2013-05-17 12:43:22',NULL,'2013-05-17 12:43:22',100,2048,'normal','',NULL),(51,'2013-05-17 12:43:22',NULL,'2013-05-17 12:43:22',100,2048,'normal','',NULL),(52,'2013-05-17 12:43:22',NULL,'2013-05-17 12:43:22',100,2048,'normal','',NULL),(53,'2013-05-20 16:12:29','','2013-05-20 16:12:42',5,1,'condorqueue','',NULL);
/*!40000 ALTER TABLE `BATCHQUEUES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CREDENTIALSERVERS`
--

DROP TABLE IF EXISTS `CREDENTIALSERVERS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CREDENTIALSERVERS` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `endpoint` varchar(255) NOT NULL,
  `lastUpdated` datetime NOT NULL,
  `name` varchar(64) NOT NULL,
  `port` int(11) DEFAULT NULL,
  `protocol` varchar(16) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CREDENTIALSERVERS`
--

LOCK TABLES `CREDENTIALSERVERS` WRITE;
/*!40000 ALTER TABLE `CREDENTIALSERVERS` DISABLE KEYS */;
INSERT INTO `CREDENTIALSERVERS` (`id`, `created`, `endpoint`, `lastUpdated`, `name`, `port`, `protocol`) VALUES (92,'2013-05-17 12:43:21','myproxy.teragrid.org','2013-05-17 12:43:21','XSEDE MyProxy Server',7512,'MYPROXY'),(93,'2013-05-17 12:43:21','myproxy.teragrid.org','2013-05-17 12:43:21','XSEDE MyProxy Server',7512,'MYPROXY'),(94,'2013-05-17 12:43:21','myproxy.teragrid.org','2013-05-17 12:43:21','XSEDE MyProxy Server',7512,'MYPROXY'),(95,'2013-05-17 12:43:21','myproxy.my.example.com','2013-05-17 12:43:21','My Example Auth System',22,'MYPROXY'),(96,'2013-05-17 12:43:21','myproxy.my.example.com','2013-05-17 12:43:21','My Example Auth System',22,'MYPROXY'),(97,'2013-05-17 12:43:21','myproxy.teragrid.org','2013-05-17 12:43:21','XSEDE MyProxy Server',7512,'MYPROXY'),(98,'2013-05-17 12:43:21','myproxy.teragrid.org','2013-05-17 12:43:21','XSEDE MyProxy Server',7512,'MYPROXY'),(99,'2013-05-17 12:43:21','myproxy.teragrid.org','2013-05-17 12:43:21','XSEDE MyProxy Server',7512,'MYPROXY'),(100,'2013-05-17 12:43:21','myproxy.teragrid.org','2013-05-17 12:43:21','XSEDE MyProxy Server',7512,'MYPROXY'),(101,'2013-05-17 12:43:21','myproxy.teragrid.org','2013-05-17 12:43:21','XSEDE MyProxy Server',7512,'MYPROXY'),(102,'2013-05-17 12:43:21','myproxy.teragrid.org','2013-05-17 12:43:21','XSEDE MyProxy Server',7512,'MYPROXY'),(103,'2013-05-17 12:43:22','myproxy.teragrid.org','2013-05-17 12:43:22','XSEDE MyProxy Server',7512,'MYPROXY'),(104,'2013-05-17 12:43:22','myproxy.teragrid.org','2013-05-17 12:43:22','XSEDE MyProxy Server',7512,'MYPROXY'),(105,'2013-05-17 12:43:22','myproxy.teragrid.org','2013-05-17 12:43:22','XSEDE MyProxy Server',7512,'MYPROXY'),(106,'2013-05-17 12:43:22','myproxy.teragrid.org','2013-05-17 12:43:22','XSEDE MyProxy Server',7512,'MYPROXY'),(107,'2013-05-17 12:43:22','myproxy.teragrid.org','2013-05-17 12:43:22','XSEDE OA4MP Server',7512,'OAUTH2'),(108,'2013-05-17 12:43:22','myproxy.teragrid.org','2013-05-17 12:43:22','XSEDE MyProxy Server',7512,'MYPROXY'),(109,'2013-05-17 12:43:22','myproxy.teragrid.org','2013-05-17 12:43:22','XSEDE MyProxy Server',7512,'MYPROXY');
/*!40000 ALTER TABLE `CREDENTIALSERVERS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EXECUTIONSYSTEMS`
--

DROP TABLE IF EXISTS `EXECUTIONSYSTEMS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EXECUTIONSYSTEMS` (
  `environment` longtext,
  `exeuctionType` varchar(16) NOT NULL,
  `schedulerType` varchar(16) NOT NULL,
  `scratchDir` varchar(255) DEFAULT NULL,
  `startupScript` varchar(255) DEFAULT NULL,
  `type` varchar(16) NOT NULL,
  `workDir` varchar(255) DEFAULT NULL,
  `id` bigint(20) NOT NULL,
  `loginConfig_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK39BE7E0C7871F82F` (`id`),
  KEY `FK39BE7E0C58E1D51B` (`loginConfig_id`),
  CONSTRAINT `FK39BE7E0C58E1D51B` FOREIGN KEY (`loginConfig_id`) REFERENCES `LOGINCONFIGS` (`id`),
  CONSTRAINT `FK39BE7E0C7871F82F` FOREIGN KEY (`id`) REFERENCES `SYSTEMS` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EXECUTIONSYSTEMS`
--

LOCK TABLES `EXECUTIONSYSTEMS` WRITE;
/*!40000 ALTER TABLE `EXECUTIONSYSTEMS` DISABLE KEYS */;
INSERT INTO `EXECUTIONSYSTEMS` (`environment`, `exeuctionType`, `schedulerType`, `scratchDir`, `startupScript`, `type`, `workDir`, `id`, `loginConfig_id`) VALUES (NULL,'HPC','CONDOR','','./bashrc','EXECUTION','',110,170),(NULL,'HPC','SGE','','./bashrc','EXECUTION','',111,172),(NULL,'HPC','SGE','','./bashrc','EXECUTION','',112,174),(NULL,'HPC','SGE','','./bashrc','EXECUTION','',113,176),(NULL,'CONDOR','CONDOR','','./bashrc','EXECUTION','',114,178),(NULL,'HPC','SGE','','./bashrc','EXECUTION','',115,180),(NULL,'HPC','PBS','','./bashrc','EXECUTION','',117,184),(NULL,'CONDOR','CONDOR','','./bashrc','EXECUTION','',118,186),(NULL,'HPC','SLURM','','./bashrc','EXECUTION','',119,188),(NULL,'HPC','SGE','','./bashrc','EXECUTION','',120,190),(NULL,'HPC','SGE','','./bashrc','EXECUTION','',121,192),(NULL,'HPC','SGE','','./bashrc','EXECUTION','',122,194);
/*!40000 ALTER TABLE `EXECUTIONSYSTEMS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EXECUTIONSYSTEMS_BATCHQUEUES`
--

DROP TABLE IF EXISTS `EXECUTIONSYSTEMS_BATCHQUEUES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EXECUTIONSYSTEMS_BATCHQUEUES` (
  `EXECUTIONSYSTEMS_id` bigint(20) NOT NULL,
  `queues_id` int(11) NOT NULL,
  UNIQUE KEY `queues_id` (`queues_id`),
  KEY `FKC2C8CB69137FCA36` (`EXECUTIONSYSTEMS_id`),
  KEY `FKC2C8CB69837E3BAE` (`queues_id`),
  CONSTRAINT `FKC2C8CB69137FCA36` FOREIGN KEY (`EXECUTIONSYSTEMS_id`) REFERENCES `EXECUTIONSYSTEMS` (`id`),
  CONSTRAINT `FKC2C8CB69837E3BAE` FOREIGN KEY (`queues_id`) REFERENCES `BATCHQUEUES` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EXECUTIONSYSTEMS_BATCHQUEUES`
--

LOCK TABLES `EXECUTIONSYSTEMS_BATCHQUEUES` WRITE;
/*!40000 ALTER TABLE `EXECUTIONSYSTEMS_BATCHQUEUES` DISABLE KEYS */;
INSERT INTO `EXECUTIONSYSTEMS_BATCHQUEUES` (`EXECUTIONSYSTEMS_id`, `queues_id`) VALUES (110,44),(112,45),(113,46),(115,47),(117,48),(118,53),(119,49),(120,50),(121,51),(122,52);
/*!40000 ALTER TABLE `EXECUTIONSYSTEMS_BATCHQUEUES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EncodingTasks`
--

DROP TABLE IF EXISTS `EncodingTasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EncodingTasks` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `callbackKey` varchar(64) NOT NULL,
  `created` datetime NOT NULL,
  `destPath` varchar(255) NOT NULL,
  `eventId` varchar(64) DEFAULT NULL,
  `sourcePath` varchar(255) NOT NULL,
  `status` varchar(32) NOT NULL,
  `transformName` varchar(32) NOT NULL,
  `transformFilterName` varchar(32) NOT NULL,
  `logicalFileId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `FKD83E9DBE4242808` (`logicalFileId`),
  CONSTRAINT `FKD83E9DBE4242808` FOREIGN KEY (`logicalFileId`) REFERENCES `LogicalFiles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EncodingTasks`
--

LOCK TABLES `EncodingTasks` WRITE;
/*!40000 ALTER TABLE `EncodingTasks` DISABLE KEYS */;
/*!40000 ALTER TABLE `EncodingTasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `INTERNALUSERS`
--

DROP TABLE IF EXISTS `INTERNALUSERS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `INTERNALUSERS` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `currentlyActive` bit(1) DEFAULT NULL,
  `city` varchar(32) DEFAULT NULL,
  `country` varchar(32) DEFAULT NULL,
  `created` datetime NOT NULL,
  `createdBy` varchar(32) NOT NULL,
  `department` varchar(64) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `fax` varchar(32) DEFAULT NULL,
  `firstName` varchar(32) DEFAULT NULL,
  `gender` int(11) DEFAULT NULL,
  `institution` varchar(32) DEFAULT NULL,
  `lastName` varchar(32) DEFAULT NULL,
  `lastUpdated` datetime NOT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `position` varchar(32) DEFAULT NULL,
  `researchArea` varchar(64) DEFAULT NULL,
  `state` varchar(32) DEFAULT NULL,
  `username` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `username` (`username`,`createdBy`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `INTERNALUSERS`
--

LOCK TABLES `INTERNALUSERS` WRITE;
/*!40000 ALTER TABLE `INTERNALUSERS` DISABLE KEYS */;
/*!40000 ALTER TABLE `INTERNALUSERS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `JobPermissions`
--

DROP TABLE IF EXISTS `JobPermissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `JobPermissions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `jobId` bigint(20) NOT NULL,
  `lastUpdated` datetime NOT NULL,
  `permission` varchar(16) NOT NULL,
  `username` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `jobId` (`jobId`,`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `JobPermissions`
--

LOCK TABLES `JobPermissions` WRITE;
/*!40000 ALTER TABLE `JobPermissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `JobPermissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Jobs`
--

DROP TABLE IF EXISTS `Jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Jobs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `archiveOutput` bit(1) DEFAULT NULL,
  `archivePath` varchar(255) DEFAULT NULL,
  `callbackUrl` varchar(255) DEFAULT NULL,
  `charge` float DEFAULT NULL,
  `created` datetime NOT NULL,
  `endTime` datetime DEFAULT NULL,
  `errorMessage` longtext,
  `inputs` longtext,
  `internalUsername` varchar(32) NOT NULL,
  `lastUpdated` datetime NOT NULL,
  `localJobId` varchar(255) DEFAULT NULL,
  `memoryRequest` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `outputPath` varchar(255) DEFAULT NULL,
  `owner` varchar(32) NOT NULL,
  `parameters` longtext,
  `processorCount` int(11) NOT NULL,
  `requestedTime` varchar(19) DEFAULT NULL,
  `retries` int(11) DEFAULT NULL,
  `schedulerJobId` varchar(255) DEFAULT NULL,
  `softwareName` varchar(32) NOT NULL,
  `startTime` datetime DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `submitTime` datetime DEFAULT NULL,
  `system` varchar(32) NOT NULL,
  `updateToken` varchar(64) DEFAULT NULL,
  `visible` bit(1) DEFAULT NULL,
  `workPath` varchar(255) DEFAULT NULL,
  `systemId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `FK235076EE3A9A9E` (`systemId`),
  CONSTRAINT `FK235076EE3A9A9E` FOREIGN KEY (`systemId`) REFERENCES `SYSTEMS` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Jobs`
--

LOCK TABLES `Jobs` WRITE;
/*!40000 ALTER TABLE `Jobs` DISABLE KEYS */;
INSERT INTO `Jobs` (`id`, `archiveOutput`, `archivePath`, `callbackUrl`, `charge`, `created`, `endTime`, `errorMessage`, `inputs`, `internalUsername`, `lastUpdated`, `localJobId`, `memoryRequest`, `name`, `outputPath`, `owner`, `parameters`, `processorCount`, `requestedTime`, `retries`, `schedulerJobId`, `softwareName`, `startTime`, `status`, `submitTime`, `system`, `updateToken`, `visible`, `workPath`, `systemId`) VALUES (1,'','/iplant/home/sterry1/archive/test-job-999','http://example.com/callback',1001.5,'2013-05-17 13:41:02','2013-05-17 13:41:02','The target execution system for this application, \"condor.opensciencegrid.org,\" is currently unavailable. This job will remain in queue until the system becomes available. ','{\"query1\":\"read1.fq\"}','sterry1','2013-05-20 16:25:02','29',512,'testname','','sterry1','{}',1,'1:00',0,'','wca-1.00','2013-05-20 16:26:33','RUNNING','2013-05-20 16:26:27','condor.opensciencegrid.org','232a28d8930d43fbc4c58069eaae8bba','','/tmp/scratch/iplant/sterry1-1-1368816062000',NULL);
/*!40000 ALTER TABLE `Jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `LOGINCONFIGS`
--

DROP TABLE IF EXISTS `LOGINCONFIGS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LOGINCONFIGS` (
  `protocol` varchar(16) NOT NULL,
  `id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK4D4A65E85C950942` (`id`),
  CONSTRAINT `FK4D4A65E85C950942` FOREIGN KEY (`id`) REFERENCES `REMOTECONFIGS` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `LOGINCONFIGS`
--

LOCK TABLES `LOGINCONFIGS` WRITE;
/*!40000 ALTER TABLE `LOGINCONFIGS` DISABLE KEYS */;
INSERT INTO `LOGINCONFIGS` (`protocol`, `id`) VALUES ('GSISSH',170),('LOCAL',172),('GSISSH',174),('GRAM',176),('LOCAL',178),('GSISSH',180),('LOCAL',182),('GSISSH',184),('LOCAL',186),('GSISSH',188),('API',190),('UNICORE',192),('SSH',194);
/*!40000 ALTER TABLE `LOGINCONFIGS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `LogicalFiles`
--

DROP TABLE IF EXISTS `LogicalFiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LogicalFiles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `internalUsername` varchar(32) DEFAULT NULL,
  `lastUpdated` datetime NOT NULL,
  `name` varchar(64) NOT NULL,
  `nativeFormat` varchar(32) DEFAULT NULL,
  `owner` varchar(32) NOT NULL,
  `path` varchar(255) NOT NULL,
  `source` varchar(255) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  `uuid` varchar(255) NOT NULL,
  `systemId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `FKE52CA00EEE3A9A9E` (`systemId`),
  CONSTRAINT `FKE52CA00EEE3A9A9E` FOREIGN KEY (`systemId`) REFERENCES `SYSTEMS` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `LogicalFiles`
--

LOCK TABLES `LogicalFiles` WRITE;
/*!40000 ALTER TABLE `LogicalFiles` DISABLE KEYS */;
/*!40000 ALTER TABLE `LogicalFiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `REMOTECONFIGS`
--

DROP TABLE IF EXISTS `REMOTECONFIGS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `REMOTECONFIGS` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `host` longtext NOT NULL,
  `lastUpdated` datetime NOT NULL,
  `port` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=195 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `REMOTECONFIGS`
--

LOCK TABLES `REMOTECONFIGS` WRITE;
/*!40000 ALTER TABLE `REMOTECONFIGS` DISABLE KEYS */;
INSERT INTO `REMOTECONFIGS` (`id`, `created`, `host`, `lastUpdated`, `port`) VALUES (160,'2013-05-17 12:43:18','data.iplantcollaborative.org','2013-05-17 12:43:18',1247),(161,'2013-05-17 12:43:21','irods.corral.tacc.utexas.edu','2013-05-17 12:43:21',1247),(162,'2013-05-17 12:43:21','iplant-vm.tacc.utexas.edu','2013-05-17 12:43:21',22),(163,'2013-05-17 12:43:21','gridftp1.ranch.tacc.utexas.edu','2013-05-17 12:43:21',2811),(164,'2013-05-17 12:43:21','localhost','2013-05-17 12:43:21',22),(165,'2013-05-17 12:43:21','gridftp1.ranch.tacc.utexas.edu','2013-05-17 12:43:21',2811),(166,'2013-05-17 12:43:21','gridftp1.ranch.tacc.utexas.edu','2013-05-17 12:43:21',2811),(167,'2013-05-17 12:43:21','localhost','2013-05-17 12:43:21',22),(168,'2013-05-17 12:43:21','ftp.example.com','2013-05-17 12:43:21',21),(169,'2013-05-17 12:43:21','login.example.com','2013-05-17 12:43:21',8211),(170,'2013-05-17 12:43:21','login.example.com','2013-05-17 12:43:21',22),(171,'2013-05-17 12:43:21','localhost','2013-05-17 12:43:21',22),(172,'2013-05-17 12:43:21','login1.ls4.tacc.utexas.edu','2013-05-17 12:43:21',22),(173,'2013-05-17 12:43:21','gridftp1.ls4.tacc.utexas.edu','2013-05-17 12:43:21',2811),(174,'2013-05-17 12:43:21','login1.ls4.tacc.utexas.edu','2013-05-17 12:43:21',22),(175,'2013-05-17 12:43:21','gridftp1.ls4.tacc.utexas.edu','2013-05-17 12:43:21',2811),(176,'2013-05-17 12:43:21','gridftp1.ls4.tacc.utexas.edu','2013-05-17 12:43:21',2119),(177,'2013-05-17 12:43:21','localhost','2013-05-17 12:43:21',22),(178,'2013-05-17 12:43:21','localhost','2013-05-17 12:43:21',22),(179,'2013-05-17 12:43:21','gridftp1.ls4.tacc.utexas.edu','2013-05-17 12:43:21',2811),(180,'2013-05-17 12:43:21','login1.ls4.tacc.utexas.edu','2013-05-17 12:43:21',22),(181,'2013-05-17 12:43:22','localhost','2013-05-17 12:43:22',22),(182,'2013-05-17 12:43:22','login1.ls4.tacc.utexas.edu','2013-05-17 12:43:22',22),(183,'2013-05-17 12:43:22','trestles-dm.sdsc.xsede.org','2013-05-17 12:43:22',2811),(184,'2013-05-17 12:43:22','trestles.sdsc.edu','2013-05-17 12:43:22',22),(185,'2013-05-17 12:43:22','localhost','2013-05-17 12:43:22',22),(186,'2013-05-17 12:43:22','localhost','2013-05-17 12:43:22',22),(187,'2013-05-17 12:43:22','data3.stampede.tacc.utexas.edu','2013-05-17 12:43:22',2811),(188,'2013-05-17 12:43:22','stampede.tacc.utexas.edu','2013-05-17 12:43:22',2222),(189,'2013-05-17 12:43:22','login1.ls4.tacc.utexas.edu','2013-05-17 12:43:22',2811),(190,'2013-05-17 12:43:22','login1.ls4.tacc.utexas.edu','2013-05-17 12:43:22',22),(191,'2013-05-17 12:43:22','gridftp1.ls4.tacc.utexas.edu','2013-05-17 12:43:22',2811),(192,'2013-05-17 12:43:22','alamo.tacc.utexas.edu','2013-05-17 12:43:22',8080),(193,'2013-05-17 12:43:22','login1.ls4.tacc.utexas.edu','2013-05-17 12:43:22',2811),(194,'2013-05-17 12:43:22','login1.ls4.tacc.utexas.edu','2013-05-17 12:43:22',22);
/*!40000 ALTER TABLE `REMOTECONFIGS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `REMOTECONFIGS_AUTHCONFIGS`
--

DROP TABLE IF EXISTS `REMOTECONFIGS_AUTHCONFIGS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `REMOTECONFIGS_AUTHCONFIGS` (
  `REMOTECONFIGS_id` bigint(20) NOT NULL,
  `authConfigs_id` bigint(20) NOT NULL,
  PRIMARY KEY (`REMOTECONFIGS_id`,`authConfigs_id`),
  UNIQUE KEY `authConfigs_id` (`authConfigs_id`),
  KEY `FK658FEB545B09F96` (`REMOTECONFIGS_id`),
  KEY `FK658FEB5191DBA1A` (`authConfigs_id`),
  CONSTRAINT `FK658FEB5191DBA1A` FOREIGN KEY (`authConfigs_id`) REFERENCES `AUTHCONFIGS` (`id`),
  CONSTRAINT `FK658FEB545B09F96` FOREIGN KEY (`REMOTECONFIGS_id`) REFERENCES `REMOTECONFIGS` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `REMOTECONFIGS_AUTHCONFIGS`
--

LOCK TABLES `REMOTECONFIGS_AUTHCONFIGS` WRITE;
/*!40000 ALTER TABLE `REMOTECONFIGS_AUTHCONFIGS` DISABLE KEYS */;
INSERT INTO `REMOTECONFIGS_AUTHCONFIGS` (`REMOTECONFIGS_id`, `authConfigs_id`) VALUES (160,144),(161,145),(162,146),(163,147),(164,148),(165,149),(166,150),(167,151),(168,152),(169,153),(170,154),(171,155),(172,156),(173,157),(174,158),(175,159),(176,160),(179,161),(180,162),(183,165),(184,166),(187,167),(188,168),(189,169),(190,170),(191,171),(192,172),(193,173),(194,174);
/*!40000 ALTER TABLE `REMOTECONFIGS_AUTHCONFIGS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `REMOTEFILEPERMISSIONS`
--

DROP TABLE IF EXISTS `REMOTEFILEPERMISSIONS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `REMOTEFILEPERMISSIONS` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `internalUsername` varchar(32) DEFAULT NULL,
  `lastUpdated` datetime NOT NULL,
  `logicalFileId` bigint(20) NOT NULL,
  `permission` varchar(32) NOT NULL,
  `username` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `REMOTEFILEPERMISSIONS`
--

LOCK TABLES `REMOTEFILEPERMISSIONS` WRITE;
/*!40000 ALTER TABLE `REMOTEFILEPERMISSIONS` DISABLE KEYS */;
/*!40000 ALTER TABLE `REMOTEFILEPERMISSIONS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `STORAGECONFIGS`
--

DROP TABLE IF EXISTS `STORAGECONFIGS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `STORAGECONFIGS` (
  `homeDir` varchar(255) DEFAULT NULL,
  `protocol` varchar(16) NOT NULL,
  `resource` varchar(255) DEFAULT NULL,
  `rootDir` varchar(255) DEFAULT NULL,
  `zone` varchar(255) DEFAULT NULL,
  `id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK17C7D6965C950942` (`id`),
  CONSTRAINT `FK17C7D6965C950942` FOREIGN KEY (`id`) REFERENCES `REMOTECONFIGS` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `STORAGECONFIGS`
--

LOCK TABLES `STORAGECONFIGS` WRITE;
/*!40000 ALTER TABLE `STORAGECONFIGS` DISABLE KEYS */;
INSERT INTO `STORAGECONFIGS` (`homeDir`, `protocol`, `resource`, `rootDir`, `zone`, `id`) VALUES (NULL,'IRODS','bitol','/iplant/home','iplant',160),(NULL,'IRODS','corral','/tacc/home/ipcservices','tacc',161),(NULL,'SFTP',NULL,'/tmp',NULL,162),(NULL,'GRIDFTP',NULL,NULL,NULL,163),(NULL,'LOCAL',NULL,NULL,NULL,164),(NULL,'GRIDFTP',NULL,NULL,NULL,165),(NULL,'GRIDFTP',NULL,NULL,NULL,166),(NULL,'LOCAL',NULL,NULL,NULL,167),(NULL,'FTP',NULL,NULL,NULL,168),(NULL,'GRIDFTP',NULL,NULL,NULL,169),(NULL,'LOCAL',NULL,NULL,NULL,171),(NULL,'GRIDFTP',NULL,NULL,NULL,173),(NULL,'GRIDFTP',NULL,NULL,NULL,175),(NULL,'LOCAL',NULL,'/tmp',NULL,177),(NULL,'GRIDFTP',NULL,NULL,NULL,179),(NULL,'LOCAL',NULL,NULL,NULL,181),(NULL,'GRIDFTP',NULL,'/',NULL,183),(NULL,'LOCAL',NULL,'/tmp',NULL,185),(NULL,'GRIDFTP',NULL,'/',NULL,187),(NULL,'SFTP',NULL,NULL,NULL,189),(NULL,'GRIDFTP',NULL,NULL,NULL,191),(NULL,'SFTP',NULL,NULL,NULL,193);
/*!40000 ALTER TABLE `STORAGECONFIGS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `STORAGESYSTEMS`
--

DROP TABLE IF EXISTS `STORAGESYSTEMS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `STORAGESYSTEMS` (
  `type` varchar(16) NOT NULL,
  `id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK7788C5497871F82F` (`id`),
  CONSTRAINT `FK7788C5497871F82F` FOREIGN KEY (`id`) REFERENCES `SYSTEMS` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `STORAGESYSTEMS`
--

LOCK TABLES `STORAGESYSTEMS` WRITE;
/*!40000 ALTER TABLE `STORAGESYSTEMS` DISABLE KEYS */;
INSERT INTO `STORAGESYSTEMS` (`type`, `id`) VALUES ('STORAGE',101),('STORAGE',102),('STORAGE',103),('STORAGE',104),('STORAGE',105),('STORAGE',106),('STORAGE',107),('STORAGE',108),('STORAGE',109);
/*!40000 ALTER TABLE `STORAGESYSTEMS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SYSTEMPERMISSIONS`
--

DROP TABLE IF EXISTS `SYSTEMPERMISSIONS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SYSTEMPERMISSIONS` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `lastUpdated` datetime NOT NULL,
  `permission` varchar(32) NOT NULL,
  `username` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SYSTEMPERMISSIONS`
--

LOCK TABLES `SYSTEMPERMISSIONS` WRITE;
/*!40000 ALTER TABLE `SYSTEMPERMISSIONS` DISABLE KEYS */;
/*!40000 ALTER TABLE `SYSTEMPERMISSIONS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SYSTEMROLES`
--

DROP TABLE IF EXISTS `SYSTEMROLES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SYSTEMROLES` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `lastUpdated` datetime NOT NULL,
  `role` varchar(32) NOT NULL,
  `username` varchar(32) NOT NULL,
  `system_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `FK25DCB5CEBBBF083F` (`system_id`),
  CONSTRAINT `FK25DCB5CEBBBF083F` FOREIGN KEY (`system_id`) REFERENCES `SYSTEMS` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SYSTEMROLES`
--

LOCK TABLES `SYSTEMROLES` WRITE;
/*!40000 ALTER TABLE `SYSTEMROLES` DISABLE KEYS */;
/*!40000 ALTER TABLE `SYSTEMROLES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SYSTEMS`
--

DROP TABLE IF EXISTS `SYSTEMS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SYSTEMS` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `available` bit(1) DEFAULT NULL,
  `created` datetime NOT NULL,
  `description` longtext,
  `globalDefault` bit(1) DEFAULT NULL,
  `lastUpdated` datetime NOT NULL,
  `name` varchar(64) NOT NULL,
  `owner` varchar(32) NOT NULL,
  `publiclyAvailable` bit(1) DEFAULT NULL,
  `revision` int(11) DEFAULT NULL,
  `site` varchar(64) DEFAULT NULL,
  `status` varchar(8) NOT NULL,
  `systemId` varchar(64) NOT NULL,
  `type` varchar(32) NOT NULL,
  `storageConfig_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `systemId` (`systemId`),
  KEY `FKC33D60045DAACF9B` (`storageConfig_id`),
  CONSTRAINT `FKC33D60045DAACF9B` FOREIGN KEY (`storageConfig_id`) REFERENCES `STORAGECONFIGS` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=123 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SYSTEMS`
--

LOCK TABLES `SYSTEMS` WRITE;
/*!40000 ALTER TABLE `SYSTEMS` DISABLE KEYS */;
INSERT INTO `SYSTEMS` (`id`, `available`, `created`, `description`, `globalDefault`, `lastUpdated`, `name`, `owner`, `publiclyAvailable`, `revision`, `site`, `status`, `systemId`, `type`, `storageConfig_id`) VALUES (101,'','2013-05-17 12:43:18','The iPlant Data Store is where your data are stored. The Data Store is cloud-based and is the central repository from which data is accessed by all of iPlant\'s technologies.','','2013-05-17 12:43:18','iPlant Data Store','sterry1','\0',1,'iplantcollaborative.org','UP','data.iplantcollaborative.org','STORAGE',160),(102,'','2013-05-17 12:43:21','The TACC data repository known as Corral.','\0','2013-05-17 12:43:21','TACC Corral','sysowner','\0',1,'tacc.utexas.edu','UP','irods.corral.tacc.utexas.edu','STORAGE',161),(103,'','2013-05-17 12:43:21','Example storage sytem accessible via sftp.','\0','2013-05-17 12:43:21','SFTP Example','sysowner','\0',1,'example.com','UP','sftp.example.com','STORAGE',162),(104,'','2013-05-17 12:43:21','TACC disk archival system.','\0','2013-05-17 12:43:21','Ranch','sysowner','\0',1,'tacc.xsede.org','UP','ranch.tacc.utexas.edu','STORAGE',163),(105,'','2013-05-17 12:43:21','Example storage system accessible via local file system.','\0','2013-05-17 12:43:21','Local Example','sysowner','\0',1,'example.com','UP','local.storage.example.com','STORAGE',164),(106,'','2013-05-17 12:43:21','My example storage system used for testing','\0','2013-05-17 12:43:21','My Example Storage System','sysowner','\0',1,'my.site','UP','storage.example.com','STORAGE',165),(107,'','2013-05-17 12:43:21','Example gridftp system','\0','2013-05-17 12:43:21','GRIDFTP Example','sysowner','\0',1,'example.com','UP','gridftp.example.com','STORAGE',166),(108,'','2013-05-17 12:43:21','Example storage system accessible via local file system.','\0','2013-05-17 12:43:21','Local Example','sysowner','\0',1,'example.com','UP','local.example.com','STORAGE',167),(109,'','2013-05-17 12:43:21','Example storage sytem accessible via sftp.','\0','2013-05-17 12:43:21','FTP Example','sysowner','\0',1,'example.com','UP','ftp.example.com','STORAGE',168),(110,'','2013-05-17 12:43:21','My example system used for testing','\0','2013-05-17 12:43:21','My Second Example System','sysowner','\0',1,'my.site','UP','execute.example.com','EXECUTION',169),(111,'','2013-05-17 12:43:21','Sample local execution system.','\0','2013-05-17 12:43:21','API','sysowner','\0',1,'tacc.xsede.org','UP','local.execution.example.com','EXECUTION',171),(112,'','2013-05-17 12:43:21','The TACC Dell Linux Cluster (Lonestar) is a powerful, multi-use cyberinfrastructure HPC and remote visualization resource.\\n\\nLonestar contains 22,656 cores within 1,888 Dell PowerEdgeM610 compute blades (nodes), 16 PowerEdge R610 compute-I/Oserver-nodes, and 2 PowerEdge M610 (3.3GHz) login nodes. Each compute node has 24GB of memory, and the login/development nodes have 16GB. The system storage includes a 1000TB parallel (SCRATCH) Lustre file system, and 276TB of local compute-node disk space (146GB/node). Lonestar also provides access to five large memory (1TB) nodes, and eight nodes containing two NVIDIA GPU\'s, giving users access to high-throughput computing and remote visualization capabilities respectively.\\n\\nA QDR InfiniBand switch fabric interconnects the nodes (I/Oand compute) through a fat-tree topology, with a point-to-point bandwidth of 40GB/sec (unidirectional speed).\\n\\nCompute nodes have two processors, each a Xeon 5680 series 3.33GHz hex-core processor with a 12MB unified L3 cache. Peak performancefor the 12 cores is 160 GFLOPS. Eight GPU nodes contain two NVIDIA M2070 GPU\'s contained in two Dell C6100 servers. The new Westmere microprocessor (basically similar to the Nehalem processor family, but using 32nm technology) has the following features: hex-core, shared L3 cache per socket, Integrated Memory Controller, larger L1 caches, Macro Ops Fusion, double-speed integer units, Advanced Smart Cache, and new SSE4.2 instructions. The memory system has 3 channels and uses 1333 MHz DIMMS.','\0','2013-05-17 12:43:21','TACC Lonestar','sysowner','\0',1,'tacc.xsede.org','UP','lonestar4.tacc.teragrid.org','EXECUTION',173),(113,'','2013-05-17 12:43:21','Sample gram execution system.','\0','2013-05-17 12:43:21','GRAM','sysowner','\0',1,'tacc.xsede.org','UP','gram.example.com','EXECUTION',175),(114,'','2013-05-17 12:43:21','Sample Condor system config','\0','2013-05-17 12:43:21','Open Science Grid','sysowner','\0',1,'example.com','UP','condor.example.com','EXECUTION',177),(115,'','2013-05-17 12:43:21','Sample gsissh execution system.','\0','2013-05-17 12:43:21','GSISSH','sysowner','\0',1,'tacc.xsede.org','UP','gsissh.example.com','EXECUTION',179),(117,'','2013-05-17 12:43:22','Trestles is intended for moderately scalable parallel applications with an emphasis on improving productivity for a broad spectrum of users. The scheduling environment will be structured to optimize job productivity by: 1) predominantly supporting jobs with core counts of 1,024 or less, 2) allowing long-running jobs of up to 2 weeks, 3) enabling user-settable job reservations, 4) supporting on-demand computing and 5) providing fast turnaround for development and interactive work. Trestles will also support science gateways by having the relevant middleware and common TeraGrid software installed at the outset. Trestles will be ideal for applications with fast local I/O requirements that can benefit from the flash memory available on each compute node.','\0','2013-05-17 12:43:22','Trestles','sysowner','\0',1,'sdsc.teragrid.org','UP','trestles.sdsc.teragrid.org','EXECUTION',183),(118,'','2013-05-17 12:43:22','The Open Science Grid (OSG) advances science through open distributed computing. The OSG is a multi-disciplinary partnership to federate local, regional, community and national cyberinfrastructures to meet the needs of research and academic communities at all scales.','\0','2013-05-17 12:43:22','Open Science Grid','sterry1','\0',1,'opensciencegrid.org','UP','condor.opensciencegrid.org','EXECUTION',185),(119,'','2013-05-17 12:43:22','Stampede is intended primarily for parallel applications scalable to tens of thousands of cores.  Normal batch queues will enable users to run simulations up to 24 hours.  Jobs requiring run times and more cores than allowed by the normal queues will be run in a special queue after approval of TACC staff.  Serial and development queues will also be configured. In addition, users will be able to run jobs using thousands of the Intel Xeon Phi coprocessors via the same queues to support massively parallel workflows.','\0','2013-05-17 12:43:22','TACC Stampede','sysowner','\0',1,'tacc.xsede.org','UP','stampede.tacc.utexas.edu','EXECUTION',187),(120,'','2013-05-17 12:43:22','Sample api (oauth2) execution system.','\0','2013-05-17 12:43:22','API','sysowner','\0',1,'tacc.xsede.org','UP','api.example.com','EXECUTION',189),(121,'','2013-05-17 12:43:22','Sample unicore execution system.','\0','2013-05-17 12:43:22','UNICORE','sysowner','\0',1,'tacc.xsede.org','UP','unicore.example.com','EXECUTION',191),(122,'','2013-05-17 12:43:22','Sample ssh execution system.','\0','2013-05-17 12:43:22','SSH','sysowner','\0',1,'tacc.xsede.org','UP','ssh.example.com','EXECUTION',193);
/*!40000 ALTER TABLE `SYSTEMS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SYSTEMS_SYSTEMROLES`
--

DROP TABLE IF EXISTS `SYSTEMS_SYSTEMROLES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SYSTEMS_SYSTEMROLES` (
  `SYSTEMS_id` bigint(20) NOT NULL,
  `roles_id` bigint(20) NOT NULL,
  UNIQUE KEY `roles_id` (`roles_id`),
  KEY `FKAD028E139ED1662A` (`SYSTEMS_id`),
  KEY `FKAD028E13125859E1` (`roles_id`),
  CONSTRAINT `FKAD028E13125859E1` FOREIGN KEY (`roles_id`) REFERENCES `SYSTEMROLES` (`id`),
  CONSTRAINT `FKAD028E139ED1662A` FOREIGN KEY (`SYSTEMS_id`) REFERENCES `SYSTEMS` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SYSTEMS_SYSTEMROLES`
--

LOCK TABLES `SYSTEMS_SYSTEMROLES` WRITE;
/*!40000 ALTER TABLE `SYSTEMS_SYSTEMROLES` DISABLE KEYS */;
/*!40000 ALTER TABLE `SYSTEMS_SYSTEMROLES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SoftwareInputs`
--

DROP TABLE IF EXISTS `SoftwareInputs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SoftwareInputs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `defaultValue` varchar(255) NOT NULL,
  `description` longtext,
  `fileTypes` varchar(128) DEFAULT NULL,
  `outputKey` varchar(32) NOT NULL,
  `label` varchar(64) DEFAULT NULL,
  `lastUpdated` datetime NOT NULL,
  `minCardinality` int(11) DEFAULT NULL,
  `ontology` varchar(64) DEFAULT NULL,
  `required` bit(1) DEFAULT NULL,
  `validator` varchar(64) DEFAULT NULL,
  `visible` bit(1) DEFAULT NULL,
  `software_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `FK8E22A29041F2F66B` (`software_id`),
  CONSTRAINT `FK8E22A29041F2F66B` FOREIGN KEY (`software_id`) REFERENCES `Softwares` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=606 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SoftwareInputs`
--

LOCK TABLES `SoftwareInputs` WRITE;
/*!40000 ALTER TABLE `SoftwareInputs` DISABLE KEYS */;
INSERT INTO `SoftwareInputs` (`id`, `created`, `defaultValue`, `description`, `fileTypes`, `outputKey`, `label`, `lastUpdated`, `minCardinality`, `ontology`, `required`, `validator`, `visible`, `software_id`) VALUES (605,'2013-05-17 12:52:36','read1.fq','','text-0','query1','File to count words in: ','2013-05-17 12:52:36',1,'http://sswapmeet.sswap.info/util/TextDocument','\0','','',605);
/*!40000 ALTER TABLE `SoftwareInputs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SoftwareOutputs`
--

DROP TABLE IF EXISTS `SoftwareOutputs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SoftwareOutputs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `defaultValue` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `fileTypes` varchar(128) DEFAULT NULL,
  `outputKey` varchar(32) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `lastUpdated` datetime NOT NULL,
  `maxCardinality` int(11) DEFAULT NULL,
  `minCardinality` int(11) DEFAULT NULL,
  `ontology` varchar(64) DEFAULT NULL,
  `pattern` varchar(64) DEFAULT NULL,
  `software_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `FK7FBF1BCB41F2F66B` (`software_id`),
  CONSTRAINT `FK7FBF1BCB41F2F66B` FOREIGN KEY (`software_id`) REFERENCES `Softwares` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SoftwareOutputs`
--

LOCK TABLES `SoftwareOutputs` WRITE;
/*!40000 ALTER TABLE `SoftwareOutputs` DISABLE KEYS */;
INSERT INTO `SoftwareOutputs` (`id`, `created`, `defaultValue`, `description`, `fileTypes`, `outputKey`, `label`, `lastUpdated`, `maxCardinality`, `minCardinality`, `ontology`, `pattern`, `software_id`) VALUES (2,'2013-05-17 12:52:36','wc_out.txt','Results of WC',NULL,'outputWC','Text file','2013-05-17 12:52:36',1,1,'http://sswapmeet.sswap.info/util/TextDocument','',605);
/*!40000 ALTER TABLE `SoftwareOutputs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SoftwareParameters`
--

DROP TABLE IF EXISTS `SoftwareParameters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SoftwareParameters` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `defaultValue` varchar(255) NOT NULL,
  `description` longtext,
  `outputKey` varchar(32) NOT NULL,
  `label` varchar(64) DEFAULT NULL,
  `lastUpdated` datetime NOT NULL,
  `ontology` varchar(64) DEFAULT NULL,
  `required` bit(1) DEFAULT NULL,
  `valueType` varchar(16) NOT NULL,
  `validator` varchar(64) DEFAULT NULL,
  `visible` bit(1) DEFAULT NULL,
  `software_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `FK67BC171141F2F66B` (`software_id`),
  CONSTRAINT `FK67BC171141F2F66B` FOREIGN KEY (`software_id`) REFERENCES `Softwares` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SoftwareParameters`
--

LOCK TABLES `SoftwareParameters` WRITE;
/*!40000 ALTER TABLE `SoftwareParameters` DISABLE KEYS */;
/*!40000 ALTER TABLE `SoftwareParameters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SoftwarePermissions`
--

DROP TABLE IF EXISTS `SoftwarePermissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SoftwarePermissions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lastUpdated` datetime NOT NULL,
  `permission` varchar(16) NOT NULL,
  `softwareId` bigint(20) NOT NULL,
  `username` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SoftwarePermissions`
--

LOCK TABLES `SoftwarePermissions` WRITE;
/*!40000 ALTER TABLE `SoftwarePermissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `SoftwarePermissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Softwares`
--

DROP TABLE IF EXISTS `Softwares`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Softwares` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `available` bit(1) DEFAULT NULL,
  `checkpointable` bit(1) DEFAULT NULL,
  `checksum` varchar(64) DEFAULT NULL,
  `created` datetime NOT NULL,
  `deploymentPath` varchar(255) NOT NULL,
  `executablePath` varchar(255) NOT NULL,
  `executionType` varchar(8) NOT NULL,
  `helpURI` varchar(128) DEFAULT NULL,
  `label` varchar(64) DEFAULT NULL,
  `lastUpdated` datetime NOT NULL,
  `longDescription` longtext,
  `modules` varchar(255) DEFAULT NULL,
  `name` varchar(64) NOT NULL,
  `ontology` varchar(255) DEFAULT NULL,
  `owner` varchar(32) NOT NULL,
  `parallelism` varchar(8) NOT NULL,
  `publiclyAvailable` bit(1) DEFAULT NULL,
  `revisionCount` int(11) DEFAULT NULL,
  `shortDescription` varchar(255) DEFAULT NULL,
  `tags` varchar(255) DEFAULT NULL,
  `testPath` varchar(255) NOT NULL,
  `version` varchar(16) NOT NULL,
  `systemId` bigint(20) DEFAULT NULL,
  `storageSystemId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `name` (`name`,`version`,`publiclyAvailable`),
  KEY `FKFD3AF38C7E10F192` (`systemId`),
  KEY `FKFD3AF38C7BAF9E70` (`storageSystemId`),
  CONSTRAINT `FKFD3AF38C7BAF9E70` FOREIGN KEY (`storageSystemId`) REFERENCES `STORAGESYSTEMS` (`id`),
  CONSTRAINT `FKFD3AF38C7E10F192` FOREIGN KEY (`systemId`) REFERENCES `EXECUTIONSYSTEMS` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=606 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Softwares`
--

LOCK TABLES `Softwares` WRITE;
/*!40000 ALTER TABLE `Softwares` DISABLE KEYS */;
INSERT INTO `Softwares` (`id`, `available`, `checkpointable`, `checksum`, `created`, `deploymentPath`, `executablePath`, `executionType`, `helpURI`, `label`, `lastUpdated`, `longDescription`, `modules`, `name`, `ontology`, `owner`, `parallelism`, `publiclyAvailable`, `revisionCount`, `shortDescription`, `tags`, `testPath`, `version`, `systemId`, `storageSystemId`) VALUES (605,'','\0',NULL,'2013-05-17 12:52:36','/sterry1/applications/wca-1.00','wrapper.sh','CONDOR','http://www.gnu.org/s/coreutils/manual/html_node/wc-invocation.html','wc condor','2013-05-17 12:52:36','','purge,load TACC','wca','http://sswapmeet.sswap.info/algorithms/wc','sterry1','SERIAL','\0',1,'Count words in a file','textutils,gnu','test.sh','1.00',118,101);
/*!40000 ALTER TABLE `Softwares` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Softwares_SoftwareInputs`
--

DROP TABLE IF EXISTS `Softwares_SoftwareInputs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Softwares_SoftwareInputs` (
  `Softwares_id` bigint(20) NOT NULL,
  `inputs_id` bigint(20) NOT NULL,
  PRIMARY KEY (`Softwares_id`,`inputs_id`),
  UNIQUE KEY `inputs_id` (`inputs_id`),
  KEY `FK5F7513A382187A23` (`inputs_id`),
  KEY `FK5F7513A3B0828A46` (`Softwares_id`),
  CONSTRAINT `FK5F7513A382187A23` FOREIGN KEY (`inputs_id`) REFERENCES `SoftwareInputs` (`id`),
  CONSTRAINT `FK5F7513A3B0828A46` FOREIGN KEY (`Softwares_id`) REFERENCES `Softwares` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Softwares_SoftwareInputs`
--

LOCK TABLES `Softwares_SoftwareInputs` WRITE;
/*!40000 ALTER TABLE `Softwares_SoftwareInputs` DISABLE KEYS */;
INSERT INTO `Softwares_SoftwareInputs` (`Softwares_id`, `inputs_id`) VALUES (605,605);
/*!40000 ALTER TABLE `Softwares_SoftwareInputs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Softwares_SoftwareOutputs`
--

DROP TABLE IF EXISTS `Softwares_SoftwareOutputs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Softwares_SoftwareOutputs` (
  `Softwares_id` bigint(20) NOT NULL,
  `outputs_id` bigint(20) NOT NULL,
  PRIMARY KEY (`Softwares_id`,`outputs_id`),
  UNIQUE KEY `outputs_id` (`outputs_id`),
  KEY `FKD8BACD182C015FC1` (`outputs_id`),
  KEY `FKD8BACD18B0828A46` (`Softwares_id`),
  CONSTRAINT `FKD8BACD182C015FC1` FOREIGN KEY (`outputs_id`) REFERENCES `SoftwareOutputs` (`id`),
  CONSTRAINT `FKD8BACD18B0828A46` FOREIGN KEY (`Softwares_id`) REFERENCES `Softwares` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Softwares_SoftwareOutputs`
--

LOCK TABLES `Softwares_SoftwareOutputs` WRITE;
/*!40000 ALTER TABLE `Softwares_SoftwareOutputs` DISABLE KEYS */;
INSERT INTO `Softwares_SoftwareOutputs` (`Softwares_id`, `outputs_id`) VALUES (605,2);
/*!40000 ALTER TABLE `Softwares_SoftwareOutputs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Softwares_SoftwareParameters`
--

DROP TABLE IF EXISTS `Softwares_SoftwareParameters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Softwares_SoftwareParameters` (
  `Softwares_id` bigint(20) NOT NULL,
  `parameters_id` bigint(20) NOT NULL,
  PRIMARY KEY (`Softwares_id`,`parameters_id`),
  UNIQUE KEY `parameters_id` (`parameters_id`),
  KEY `FK7979C6A4B0828A46` (`Softwares_id`),
  KEY `FK7979C6A448985661` (`parameters_id`),
  CONSTRAINT `FK7979C6A448985661` FOREIGN KEY (`parameters_id`) REFERENCES `SoftwareParameters` (`id`),
  CONSTRAINT `FK7979C6A4B0828A46` FOREIGN KEY (`Softwares_id`) REFERENCES `Softwares` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Softwares_SoftwareParameters`
--

LOCK TABLES `Softwares_SoftwareParameters` WRITE;
/*!40000 ALTER TABLE `Softwares_SoftwareParameters` DISABLE KEYS */;
/*!40000 ALTER TABLE `Softwares_SoftwareParameters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Softwares_SoftwarePermissions`
--

DROP TABLE IF EXISTS `Softwares_SoftwarePermissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Softwares_SoftwarePermissions` (
  `Softwares_id` bigint(20) NOT NULL,
  `permissions_id` bigint(20) NOT NULL,
  PRIMARY KEY (`Softwares_id`,`permissions_id`),
  UNIQUE KEY `permissions_id` (`permissions_id`),
  KEY `FKA9B1870AF5F8F65D` (`permissions_id`),
  KEY `FKA9B1870AB0828A46` (`Softwares_id`),
  CONSTRAINT `FKA9B1870AB0828A46` FOREIGN KEY (`Softwares_id`) REFERENCES `Softwares` (`id`),
  CONSTRAINT `FKA9B1870AF5F8F65D` FOREIGN KEY (`permissions_id`) REFERENCES `SoftwarePermissions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Softwares_SoftwarePermissions`
--

LOCK TABLES `Softwares_SoftwarePermissions` WRITE;
/*!40000 ALTER TABLE `Softwares_SoftwarePermissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `Softwares_SoftwarePermissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `StagingTasks`
--

DROP TABLE IF EXISTS `StagingTasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `StagingTasks` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `bytesTransferred` bigint(20) DEFAULT NULL,
  `created` datetime NOT NULL,
  `eventId` varchar(64) DEFAULT NULL,
  `lastUpdated` datetime NOT NULL,
  `retryCount` int(11) NOT NULL,
  `status` varchar(32) NOT NULL,
  `totalBytes` bigint(20) DEFAULT NULL,
  `logicalFileId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `FK386FB433E4242808` (`logicalFileId`),
  CONSTRAINT `FK386FB433E4242808` FOREIGN KEY (`logicalFileId`) REFERENCES `LogicalFiles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `StagingTasks`
--

LOCK TABLES `StagingTasks` WRITE;
/*!40000 ALTER TABLE `StagingTasks` DISABLE KEYS */;
/*!40000 ALTER TABLE `StagingTasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TRANSFERTASKS`
--

DROP TABLE IF EXISTS `TRANSFERTASKS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TRANSFERTASKS` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `attempts` int(11) DEFAULT NULL,
  `bytesTransferred` double DEFAULT NULL,
  `created` datetime NOT NULL,
  `dest` varchar(255) NOT NULL,
  `endTime` datetime NOT NULL,
  `eventId` varchar(255) DEFAULT NULL,
  `lastUpdated` datetime NOT NULL,
  `owner` varchar(32) NOT NULL,
  `source` varchar(255) NOT NULL,
  `startTime` datetime NOT NULL,
  `status` varchar(16) NOT NULL,
  `totalSize` double DEFAULT NULL,
  `transferRate` double DEFAULT NULL,
  `parentTask_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `FK42D3166351D671B3` (`parentTask_id`),
  CONSTRAINT `FK42D3166351D671B3` FOREIGN KEY (`parentTask_id`) REFERENCES `TRANSFERTASKS` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TRANSFERTASKS`
--

LOCK TABLES `TRANSFERTASKS` WRITE;
/*!40000 ALTER TABLE `TRANSFERTASKS` DISABLE KEYS */;
/*!40000 ALTER TABLE `TRANSFERTASKS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `USERDEFAULTSYSTEMS`
--

DROP TABLE IF EXISTS `USERDEFAULTSYSTEMS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `USERDEFAULTSYSTEMS` (
  `systemId` bigint(20) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  KEY `FK2ACE6B4EEE3A9A9E` (`systemId`),
  CONSTRAINT `FK2ACE6B4EEE3A9A9E` FOREIGN KEY (`systemId`) REFERENCES `SYSTEMS` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `USERDEFAULTSYSTEMS`
--

LOCK TABLES `USERDEFAULTSYSTEMS` WRITE;
/*!40000 ALTER TABLE `USERDEFAULTSYSTEMS` DISABLE KEYS */;
/*!40000 ALTER TABLE `USERDEFAULTSYSTEMS` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-05-20 16:53:37
