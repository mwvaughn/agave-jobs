-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 11, 2013 at 02:52 PM
-- Server version: 5.5.29
-- PHP Version: 5.4.6-1ubuntu1.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `SYSTEMROLES`
--

CREATE TABLE IF NOT EXISTS `SYSTEMROLES` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `lastUpdated` datetime NOT NULL,
  `role` varchar(32) NOT NULL,
  `username` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=160 ;

--
-- Dumping data for table `SYSTEMROLES`
--

INSERT INTO `SYSTEMROLES` (`id`, `created`, `lastUpdated`, `role`, `username`) VALUES
(18, '2013-03-07 14:21:31', '2013-03-07 14:21:31', 'USER', 'bob'),
(23, '2013-03-07 14:21:35', '2013-03-07 14:21:35', 'USER', 'bob'),
(28, '2013-03-07 14:26:32', '2013-03-07 14:26:32', 'USER', 'bob'),
(33, '2013-03-07 14:26:37', '2013-03-07 14:26:37', 'USER', 'bob'),
(45, '2013-03-07 14:43:56', '2013-03-07 14:43:56', 'USER', 'bob'),
(50, '2013-03-07 14:44:01', '2013-03-07 14:44:01', 'USER', 'bob'),
(55, '2013-03-07 14:49:38', '2013-03-07 14:49:38', 'USER', 'bob'),
(60, '2013-03-07 14:49:44', '2013-03-07 14:49:44', 'USER', 'bob'),
(65, '2013-03-07 15:30:36', '2013-03-07 15:30:36', 'USER', 'bob'),
(70, '2013-03-07 15:30:39', '2013-03-07 15:30:39', 'USER', 'bob'),
(72, '2013-03-07 15:39:38', '2013-03-07 15:39:38', 'USER', 'bob'),
(77, '2013-03-07 15:39:41', '2013-03-07 15:39:41', 'USER', 'bob'),
(79, '2013-03-07 16:27:31', '2013-03-07 16:27:31', 'USER', 'bob'),
(84, '2013-03-07 16:27:39', '2013-03-07 16:27:39', 'USER', 'bob'),
(89, '2013-03-07 16:31:02', '2013-03-07 16:31:02', 'USER', 'bob'),
(94, '2013-03-07 16:31:06', '2013-03-07 16:31:06', 'USER', 'bob'),
(99, '2013-03-08 09:56:23', '2013-03-08 09:56:23', 'USER', 'bob'),
(104, '2013-03-08 09:56:28', '2013-03-08 09:56:28', 'USER', 'bob'),
(111, '2013-03-08 12:36:24', '2013-03-08 12:36:24', 'USER', 'bob'),
(116, '2013-03-08 12:36:28', '2013-03-08 12:36:28', 'USER', 'bob'),
(123, '2013-03-08 12:59:27', '2013-03-08 12:59:27', 'USER', 'bob'),
(128, '2013-03-08 12:59:30', '2013-03-08 12:59:30', 'USER', 'bob'),
(135, '2013-03-08 13:47:34', '2013-03-08 13:47:34', 'USER', 'bob'),
(140, '2013-03-08 13:47:38', '2013-03-08 13:47:38', 'USER', 'bob'),
(147, '2013-03-08 13:54:58', '2013-03-08 13:54:58', 'USER', 'bob'),
(152, '2013-03-08 13:55:02', '2013-03-08 13:55:02', 'USER', 'bob'),
(153, '2013-03-11 09:17:04', '2013-03-11 09:17:04', 'USER', 'bob'),
(158, '2013-03-11 09:17:08', '2013-03-11 09:17:08', 'USER', 'bob');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
