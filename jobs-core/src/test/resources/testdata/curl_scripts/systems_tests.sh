#################################################################
#						Environment Setup
#################################################################
# rsync -v --timeout=120 sterry1@iplant-dev.tacc.utexas.edu:/usr/local/www/apache-tomcat-6.0.36/logs/*.* /Users/steve/servers/iplant-dev/logs/

# export TEST_DATA_DIR=/Users/$API_USERNAME/workspace/apps-v1/src/test/resources
export PROJECT_DIR=/home/wcs/Projects/iplant/branches/dooley/iplant-apps/syststems-support
export TEST_DATA_DIR=$PROJECT_DIR/src/test/resources
export API_USERNAME=dooley
export API_TOKEN=#########
export API_SHARE_USERNAME=vaughn
export API_SHARE_TOKEN=#########
#Systems

#Register:
curl -sku "$API_USERNAME:$API_TOKEN" -X POST -F "fileToUpload=@systems/storage/data.iplantcollaborative.org.json" http://localhost:8080/apps-v1/systems/ | python -mjson.tool

curl -sku "$API_USERNAME:$API_TOKEN" -X POST -F "fileToUpload=@$TEST_DATA_DIR/systems/execution/execute.example.com.json" http://localhost:8080/apps-v1/systems/ | python -mjson.tool

#List:
curl -sku "$API_USERNAME:$API_TOKEN" http://localhost:8080/apps-v1/systems/list | python -mjson.tool


#################################################################
#						Profiles Services
#################################################################

#Current profile:
curl -sku "$API_USERNAME:$API_TOKEN" http://localhost:8080/profile-v1 | python -mjson.tool

curl -sku "$API_USERNAME:$API_TOKEN" http://localhost:8080/profile-v1/$API_USERNAME | python -mjson.tool

#List internal users:
curl -sku "$API_USERNAME:$API_TOKEN" http://localhost:8080/profile-v1/$API_USERNAME/users | python -mjson.tool

#Register internal user:
curl -sku "$API_USERNAME:$API_TOKEN" -X POST -F "fileToUpload=@$TEST_DATA_DIR/internal_users/sysowner.json" http://localhost:8080/profile-v1/$API_USERNAME/users | python -mjson.tool

curl -sku "$API_USERNAME:$API_TOKEN" -X POST -F "fileToUpload=@$TEST_DATA_DIR/internal_users/shareduser.json" http://localhost:8080/profile-v1/$API_USERNAME/users | python -mjson.tool

#List internal user:
curl -sku "$API_USERNAME:$API_TOKEN" http://localhost:8080/profile-v1/$API_USERNAME/users/sysowner | python -mjson.tool

#Delete internal user:
curl -X DELETE -sku "$API_USERNAME:$API_TOKEN" http://localhost:8080/profile-v1/$API_USERNAME/users/sysowner | python -mjson.tool

curl -sku "sterry1:232a28d8930d43fbc4c58069eaae8bba"  -X POST -F "fileToUpload=@.json"http://localhost:8080/apps-v1/apps | python -mjson.tool
# token just make good for 200 hrs 232a28d8930d43fbc4c58069eaae8bba on 05.15.2013.04:49 pm

#################################################################
#						Systems Services
#################################################################

# Add system
curl -sku "$API_USERNAME:$API_TOKEN" -X POST -F "fileToUpload=@$TEST_DATA_DIR/systems/storage/data.iplantcollaborative.org.json" http://localhost:8080/apps-v1/systems/ | python -mjson.tool

# List systems
curl -sku "$API_USERNAME:$API_TOKEN" http://localhost:8080/apps-v1/systems/list | python -mjson.tool

# List specific systems
curl -sku "$API_USERNAME:$API_TOKEN" http://localhost:8080/apps-v1/systems/data.iplantcollaborative.org | python -mjson.tool

# Update system
curl -sku "$API_USERNAME:$API_TOKEN" -X POST -F "fileToUpload=@$TEST_DATA_DIR/systems/storage/data.iplantcollaborative.org.json" http://localhost:8080/apps-v1/systems/ | python -mjson.tool

# Update system by name
curl -sku "$API_USERNAME:$API_TOKEN" -X POST -F "fileToUpload=@$TEST_DATA_DIR/systems/storage/data.iplantcollaborative.org.json" http://localhost:8080/apps-v1/systems/data.iplantcollaborative.org | python -mjson.tool

# List systems
curl -sku "$API_USERNAME:$API_TOKEN" -X POST -F "fileToUpload=@$TEST_DATA_DIR/systems/storage/data.iplantcollaborative.org.json" http://localhost:8080/apps-v1/systems/ | python -mjson.tool

# Make private default
curl -sku "$API_USERNAME:$API_TOKEN" -X PUT -d "action=default" http://localhost:8080/apps-v1/systems/data.iplantcollaborative.org | python -mjson.tool

# Clone system
curl -sku "$API_USERNAME:$API_TOKEN" -X PUT -d "action=clone&id=my.data.iplantcollaborative.org" http://localhost:8080/apps-v1/systems/data.iplantcollaborative.org | python -mjson.tool

# Publish system as public
curl -sku "$API_USERNAME:$API_TOKEN" -X PUT -d "action=publish" http://localhost:8080/apps-v1/systems/my.data.iplantcollaborative.org | python -mjson.tool

# List public systems
curl -sk http://localhost:8080/apps-v1/systems/list | python -mjson.tool

# List public systems by name
curl -sk http://localhost:8080/apps-v1/systems/my.data.iplantcollaborative.org | python -mjson.tool

# Make public default
curl -sku "$API_USERNAME:$API_TOKEN" -X PUT -d "action=default" http://localhost:8080/apps-v1/systems/my.data.iplantcollaborative.org | python -mjson.tool

# Delete system
curl -sku "$API_USERNAME:$API_TOKEN" -X DELETE http://localhost:8080/apps-v1/systems/my.data.iplantcollaborative.org | python -mjson.tool

#List credentials
curl -sku "$API_USERNAME:$API_TOKEN" http://localhost:8080/apps-v1/systems/data.iplantcollaborative.org/credentials | python -mjson.tool

#Add credential:
curl -sku "$API_USERNAME:$API_TOKEN" -X POST -F "fileToUpload=@$TEST_DATA_DIR/credentials/storage/sysowner.irods.example.com.json" http://localhost:8080/apps-v1/systems/data.iplantcollaborative.org/credentials/sysowner | python -mjson.tool

curl -sku "$API_USERNAME:$API_TOKEN" -X POST -F "fileToUpload=@$TEST_DATA_DIR/credentials/storage/shareduser.irods.example.com.json" http://localhost:8080/apps-v1/systems/data.iplantcollaborative.org/credentials/shareduser | python -mjson.tool

#Update credential
curl -sku "$API_USERNAME:$API_TOKEN" -X POST -F "fileToUpload=@$TEST_DATA_DIR/credentials/storage/sysowner.irods.example.com.json" http://localhost:8080/apps-v1/systems/data.iplantcollaborative.org/credentials/sysowner | python -mjson.tool

curl -sku "$API_USERNAME:$API_TOKEN" -X POST -F "fileToUpload=@$TEST_DATA_DIR/credentials/storage/sysowner.irods.example.com.json" http://localhost:8080/apps-v1/systems/data.iplantcollaborative.org/credentials/sysowner/storage | python -mjson.tool

#Delete the internal user's storage credentials
curl -sku "$API_USERNAME:$API_TOKEN" -X DELETE http://localhost:8080/apps-v1/systems/data.iplantcollaborative.org/credentials/shareduser/storage | python -mjson.tool

#Delete all of the internal user's credentials
curl -sku "$API_USERNAME:$API_TOKEN" -X POST -F "fileToUpload=@$TEST_DATA_DIR/credentials/storage/shareduser.irods.example.com.json" http://localhost:8080/apps-v1/systems/data.iplantcollaborative.org/credentials/shareduser | python -mjson.tool

curl -sku "$API_USERNAME:$API_TOKEN" -X DELETE http://localhost:8080/apps-v1/systems/data.iplantcollaborative.org/credentials/shareduser | python -mjson.tool

#Delete all internal user credentials
curl -sku "$API_USERNAME:$API_TOKEN" -X DELETE http://localhost:8080/apps-v1/systems/data.iplantcollaborative.org/credentials | python -mjson.tool

## System Roles

# List roles
curl -sku "$API_USERNAME:$API_TOKEN" http://localhost:8080/apps-v1/systems/data.iplantcollaborative.org/roles | python -mjson.tool

curl -sku "$API_SHARE_USERNAME:$API_SHARE_TOKEN" http://localhost:8080/apps-v1/systems/data.iplantcollaborative.org | python -mjson.tool

# Add user role
curl -sku "$API_USERNAME:$API_TOKEN" -X POST -d "username=$API_SHARE_USERNAME&role=USER" http://localhost:8080/apps-v1/systems/data.iplantcollaborative.org/roles | python -mjson.tool

curl -sku "$API_SHARE_USERNAME:$API_SHARE_TOKEN" http://localhost:8080/apps-v1/systems/data.iplantcollaborative.org | python -mjson.tool

# List roles
curl -sku "$API_USERNAME:$API_TOKEN" http://localhost:8080/apps-v1/systems/data.iplantcollaborative.org/roles | python -mjson.tool

# List user role
curl -sku "$API_USERNAME:$API_TOKEN" http://localhost:8080/apps-v1/systems/data.iplantcollaborative.org/roles/$API_SHARE_USERNAME | python -mjson.tool

# Update user role
curl -sku "$API_USERNAME:$API_TOKEN" -X POST -d "username=$API_SHARE_USERNAME&role=PUBLISHER" http://localhost:8080/apps-v1/systems/data.iplantcollaborative.org/roles | python -mjson.tool

curl -sku "$API_USERNAME:$API_TOKEN" http://localhost:8080/apps-v1/systems/data.iplantcollaborative.org/roles/$API_SHARE_USERNAME | python -mjson.tool

curl -sku "$API_USERNAME:$API_TOKEN" -X POST -d "role=ADMIN" http://localhost:8080/apps-v1/systems/data.iplantcollaborative.org/roles/$API_SHARE_USERNAME | python -mjson.tool

curl -sku "$API_USERNAME:$API_TOKEN" http://localhost:8080/apps-v1/systems/data.iplantcollaborative.org/roles/$API_SHARE_USERNAME | python -mjson.tool

curl -sku "$API_USERNAME:$API_TOKEN" -X POST -d "role=OWNER" http://localhost:8080/apps-v1/systems/data.iplantcollaborative.org/roles/$API_SHARE_USERNAME | python -mjson.tool

curl -sku "$API_USERNAME:$API_TOKEN" http://localhost:8080/apps-v1/systems/data.iplantcollaborative.org/roles/$API_SHARE_USERNAME | python -mjson.tool

# Delete user role by setting role to none
curl -sku "$API_USERNAME:$API_TOKEN" -X POST -d "role=NONE" http://localhost:8080/apps-v1/systems/data.iplantcollaborative.org/roles/$API_SHARE_USERNAME | python -mjson.tool

curl -sku "$API_USERNAME:$API_TOKEN" http://localhost:8080/apps-v1/systems/data.iplantcollaborative.org/roles/$API_SHARE_USERNAME | python -mjson.tool
 
# Delete user role by calling DELETE on user role
curl -sku "$API_USERNAME:$API_TOKEN" -X POST -d "role=USER" http://localhost:8080/apps-v1/systems/data.iplantcollaborative.org/roles/$API_SHARE_USERNAME | python -mjson.tool

curl -sku "$API_USERNAME:$API_TOKEN" -X DELETE http://localhost:8080/apps-v1/systems/data.iplantcollaborative.org/roles/$API_SHARE_USERNAME | python -mjson.tool

curl -sku "$API_USERNAME:$API_TOKEN" http://localhost:8080/apps-v1/systems/data.iplantcollaborative.org/roles/$API_SHARE_USERNAME | python -mjson.tool


# Delete all user roles by calling DELETE
curl -sku "$API_USERNAME:$API_TOKEN" -X POST -d "role=USER" http://localhost:8080/apps-v1/systems/data.iplantcollaborative.org/roles/$API_SHARE_USERNAME | python -mjson.tool

curl -sku "$API_USERNAME:$API_TOKEN" -X DELETE http://localhost:8080/apps-v1/systems/data.iplantcollaborative.org/roles | python -mjson.tool

curl -sku "$API_USERNAME:$API_TOKEN" http://localhost:8080/apps-v1/systems/data.iplantcollaborative.org/roles | python -mjson.tool


#################################################################
#						Apps Service
#################################################################

## Private system

# List all apps
curl -sku "$API_USERNAME:$API_TOKEN" http://localhost:8080/apps-v1/apps/list | python -mjson.tool

# Register private app
curl -sku "$API_USERNAME:$API_TOKEN" -X POST -F "fileToUpload=@$TEST_DATA_DIR/software/system-software.json" http://localhost:8080/apps-v1/apps/ | python -mjson.tool

# List private app
curl -sku "$API_USERNAME:$API_TOKEN" http://localhost:8080/apps-v1/apps/boilerplate-1.0 | python -mjson.tool

# Update private app
curl -sku "$API_USERNAME:$API_TOKEN" -X POST -F "fileToUpload=@$TEST_DATA_DIR/software/system-software.json" http://localhost:8080/apps-v1/apps/ | python -mjson.tool

curl -sku "$API_USERNAME:$API_TOKEN" -X POST -F "fileToUpload=@$TEST_DATA_DIR/software/system-software.json" http://localhost:8080/apps-v1/apps/boilerplate-1.0 | python -mjson.tool

# Publish private app
curl -sku "$API_USERNAME:$API_TOKEN" -X PUT -d "action=publish" http://localhost:8080/apps-v1/apps/boilerplate-1.0 | python -mjson.tool

# List public app
curl -sku "$API_USERNAME:$API_TOKEN" http://localhost:8080/apps-v1/apps/boilerplate-1.0u1 | python -mjson.tool

## App Permissions

# Add share app permissions
curl -sku "$API_USERNAME:$API_TOKEN" -X POST -d "username=$API_SHARE_USERNAME&permission=READ" http://localhost:8080/apps-v1/apps/boilerplate-1.0/share | python -mjson.tool

curl -sku "$API_SHARE_USERNAME:$API_SHARE_TOKEN" http://localhost:8080/apps-v1/apps/boilerplate-1.0 | python -mjson.tool

# List share app permissions
curl -sku "$API_USERNAME:$API_TOKEN" http://localhost:8080/apps-v1/apps/boilerplate-1.0/share | python -mjson.tool

curl -sku "$API_SHARE_USERNAME:$API_SHARE_TOKEN" http://localhost:8080/apps-v1/apps/boilerplate-1.0 | python -mjson.tool

# Remove share app user permission
curl -sku "$API_USERNAME:$API_TOKEN" -X DELETE http://localhost:8080/apps-v1/apps/boilerplate-1.0/share/$API_SHARE_USERNAME | python -mjson.tool

curl -sku "$API_SHARE_USERNAME:$API_SHARE_TOKEN" -X DELETE http://localhost:8080/apps-v1/apps/boilerplate-1.0 | python -mjson.tool

# Add share app user permission
curl -sku "$API_USERNAME:$API_TOKEN" -X POST -d "permission=READ" http://localhost:8080/apps-v1/apps/boilerplate-1.0/share/$API_SHARE_USERNAME | python -mjson.tool

curl -sku "$API_SHARE_USERNAME:$API_SHARE_TOKEN" -X DELETE http://localhost:8080/apps-v1/apps/boilerplate-1.0 | python -mjson.tool

# Update share app user permission
curl -sku "$API_USERNAME:$API_TOKEN" -X POST -d "permission=WRITE" http://localhost:8080/apps-v1/apps/boilerplate-1.0/share/$API_SHARE_USERNAME | python -mjson.tool

curl -sku "$API_SHARE_USERNAME:$API_SHARE_TOKEN" -X DELETE http://localhost:8080/apps-v1/apps/boilerplate-1.0 | python -mjson.tool

curl -sku "$API_USERNAME:$API_TOKEN" -X POST -d "permission=EXECUTE" http://localhost:8080/apps-v1/apps/boilerplate-1.0/share/$API_SHARE_USERNAME | python -mjson.tool

curl -sku "$API_SHARE_USERNAME:$API_SHARE_TOKEN" -X DELETE http://localhost:8080/apps-v1/apps/boilerplate-1.0 | python -mjson.tool

curl -sku "$API_USERNAME:$API_TOKEN" -X POST -d "permission=ALL" http://localhost:8080/apps-v1/apps/boilerplate-1.0/share/$API_SHARE_USERNAME | python -mjson.tool

curl -sku "$API_SHARE_USERNAME:$API_SHARE_TOKEN" -X DELETE http://localhost:8080/apps-v1/apps/boilerplate-1.0 | python -mjson.tool

# List share app user permission
curl -sku "$API_USERNAME:$API_TOKEN" http://localhost:8080/apps-v1/apps/boilerplate-1.0/share/$API_SHARE_USERNAME | python -mjson.tool

curl -sku "$API_SHARE_USERNAME:$API_SHARE_TOKEN" -X DELETE http://localhost:8080/apps-v1/apps/boilerplate-1.0 | python -mjson.tool

# Remove all share app user permissions
curl -sku "$API_USERNAME:$API_TOKEN" -X DELETE http://localhost:8080/apps-v1/apps/boilerplate-1.0/share | python -mjson.tool

curl -sku "$API_SHARE_USERNAME:$API_SHARE_TOKEN" -X DELETE http://localhost:8080/apps-v1/apps/boilerplate-1.0 | python -mjson.tool

# List share app pems
curl -sku "$API_USERNAME:$API_TOKEN" http://localhost:8080/apps-v1/apps/boilerplate-1.0/share | python -mjson.tool

## App search

# Apps search by name
curl -sku "$API_USERNAME:$API_TOKEN" http://localhost:8080/apps-v1/apps/name/boilerplate | python -mjson.tool

# Apps search by tag
curl -sku "$API_USERNAME:$API_TOKEN" http://localhost:8080/apps-v1/apps/tag/boilerplate | python -mjson.tool

# Apps search by ontology
curl -sku "$API_USERNAME:$API_TOKEN" http://localhost:8080/apps-v1/apps/term/boilerplate | python -mjson.tool

# Apps search by system
curl -sku "$API_USERNAME:$API_TOKEN" http://localhost:8080/apps-v1/apps/name/boilerplate | python -mjson.tool

# Shared apps search by name
curl -sku "$API_USERNAME:$API_TOKEN" http://localhost:8080/apps-v1/apps/shared/name/boilerplate | python -mjson.tool

# Shared apps search by tag
curl -sku "$API_USERNAME:$API_TOKEN" http://localhost:8080/apps-v1/apps/shared/tag/boilerplate | python -mjson.tool

# Shared apps search by ontology
curl -sku "$API_USERNAME:$API_TOKEN" http://localhost:8080/apps-v1/apps/shared/term/boilerplate | python -mjson.tool

# Shared apps search by system
curl -sku "$API_USERNAME:$API_TOKEN" http://localhost:8080/apps-v1/apps/shared/name/boilerplate | python -mjson.tool

# List share app


# Delete public app


# List public app


# Delete private app


# List private app



#################################################################
#						Jobs Service
#################################################################





#################################################################
#						IO Service
#################################################################



#################################################################
#						Data Service
#################################################################


